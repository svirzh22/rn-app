Diese App ermöglicht es Ihnen :

• Ihre Abschlagszeiten auf unseren Plätzen Seve Ballesteros und Jack Nicklaus direkt online zu buchen

• Immer alles über die neusten Aktivitäten und Neuigkeiten des Klubs zu erfahren

• Den Zugang zur allen Informationen über die Turniere (Anmeldungen, Startlisten, Ergebnisse, Scorekarten, …)

• Das Programm der Golf Academy zu verfolgen