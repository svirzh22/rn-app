# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Runs all the tests"
  lane :test do
    gradle(task: "test")
  end

  desc "Assemble release"
  lane :build do
    gradle(task: "clean assembleRelease", flags: " --no-daemon")
  end

  desc "Bundle release"
  lane :bundle do |options|
    sh("git", "branch", "--set-upstream-to=origin/#{options[:branch]}", options[:branch])
    git_pull
    gradle(task: "clean bundleRelease", flags: " --no-daemon")
    upload_to_play_store(track: "internal", aab: "app/build/outputs/bundle/release/app-release.aab")
  end

  desc "Assemble release"
  lane :deploy do |options|
    echo(message: "Branch: #{options[:branch]}")
    sh("git", "branch", "--set-upstream-to=origin/#{options[:branch]}", options[:branch])
    git_pull
    increment_version_code
    gradle(task: "clean assembleRelease", flags: " --no-daemon")
    git_commit(path: "./app/build.gradle", message: "Android Version Bump [ci skip]")
    push_to_git_remote
    # upload_to_play_store
  end
end
