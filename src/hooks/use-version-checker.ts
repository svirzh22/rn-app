import { Linking } from 'react-native';
import { useMutation } from 'react-query';

import { appVersionResource } from '@api/resources/app-version';
import { ANDROID_APP_LINK, IOS_APP_LINK, IS_IOS } from '@constants';
import { useAlert } from '@hooks';
import { compareLastVersions, translate } from '@helpers';

export const useVersionChecker = () => {
  const { showAlert } = useAlert();
  const { mutateAsync: getAppVersion } = useMutation(appVersionResource.fetchAppVersion);
  const compareVersions = async () => {
    const lastVersions = await getAppVersion();
    const compareResult = compareLastVersions(lastVersions);

    if (compareResult !== 1) {
      showAlert({
        text: translate('errors.updateRequired'),
        buttons: [
          {
            text: translate('common.update'),
            onPress: () => Linking.openURL(IS_IOS ? IOS_APP_LINK : ANDROID_APP_LINK),
          },
        ],
      });
    }
  };

  return {
    compareVersions,
  };
};
