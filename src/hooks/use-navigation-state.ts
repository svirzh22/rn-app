import { useState, useEffect, useCallback } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { NAVIGATION_KEY } from '@constants';

export const useNavigationStatePersist = ({
  onLoadFinish,
}: {
  onLoadFinish: () => void;
}) => {
  const [isReady, setIsReady] = useState(false);
  const [initialState, setInitialState] = useState();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const getActiveRouteName = (state: any): string => {
    const route = state.routes[state.index];

    if (route.state) {
      // Dive into nested navigators
      return getActiveRouteName(route.state);
    }

    return route.name;
  };

  useEffect(() => {
    const restoreState = async () => {
      try {
        const savedState = await AsyncStorage.getItem(NAVIGATION_KEY);
        const state = JSON.parse(savedState as any);

        setInitialState(state);
      } finally {
        setIsReady(true);
        onLoadFinish();
      }
    };

    if (!isReady) {
      restoreState();
    }
  }, [isReady, onLoadFinish]);

  const handleStateChange = useCallback((state: any) => {
    AsyncStorage.setItem(NAVIGATION_KEY, JSON.stringify(state));
  }, []);

  return {
    isReady,
    initialState,
    handleStateChange,
  };
};
