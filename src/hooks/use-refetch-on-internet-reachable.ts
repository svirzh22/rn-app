import { useNetInfo } from '@react-native-community/netinfo';
import { useEffect } from 'react';

export const useRefetchOnInternetReachable = ({
  extraCondition = true,
  refetch,
}: { extraCondition?: boolean; refetch: () => void }) => {
  const { isInternetReachable } = useNetInfo();
  useEffect(() => {
    if (extraCondition && isInternetReachable) {
      refetch();
    }
  }, [extraCondition, isInternetReachable, refetch]);
};
