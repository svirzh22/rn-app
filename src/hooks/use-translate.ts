import { useContext } from 'react';
import { LocaleContext } from '@services';

export const useTranslate = () => useContext(LocaleContext);
