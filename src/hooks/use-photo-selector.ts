import { useCallback, useState } from 'react';

import { openCamera, openGallery } from '@api';
import { noop } from '@helpers';

interface Photo {
  imageUri: string;
  name: string;
  uri: string;
  type: string;
  size: string;
}

export const usePhotoSelector = ({
  onPhotoUpdate = noop,
  onCancel = noop,
}: {
  onPhotoUpdate?: any;
  onCancel?: () => void;
}) => {
  const [photo, setPhoto] = useState<Partial<Photo> | undefined | null>({});

  const handleImageSelect = useCallback(
    (image) => {
      setPhoto(image);
      if (onPhotoUpdate) {
        onPhotoUpdate({
          photo: image,
        });
      }
    },
    [onPhotoUpdate],
  );

  const handleGalleryOpen = useCallback(() => {
    openGallery({
      onCancel,
      onSuccess: handleImageSelect,
    });
  }, [handleImageSelect, onCancel]);

  const handleCameraOpen = useCallback(() => {
    openCamera({
      onCancel,
      onSuccess: handleImageSelect,
    });
  }, [handleImageSelect, onCancel]);

  const handleDeletePhoto = useCallback(() => {
    setPhoto(null);
    if (onPhotoUpdate) {
      onPhotoUpdate({
        photo: null,
      });
    }
  }, [onPhotoUpdate]);

  return {
    photo,
    handleGalleryOpen,
    handleCameraOpen,
    handleDeletePhoto,
  };
};
