import moment from 'moment';
import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { timeSlotsResource } from '@api';
import { formatDate, REVERSED_DATE_FORMAT_DASH } from '@helpers';
import { TimeSlotsResult } from '@api/resources/time-slots/types';
import { memberSelector } from '@selectors';

export const useFetchTimeslots = ({
  selectedCourse,
  selectedDate,
  players,
}: {
  selectedCourse: number;
  selectedDate: number;
  players: number;
}) => {
  const member = useSelector(memberSelector);
  const {
    data: timeslotsData,
    isLoading: isLoadingTimeslots,
    isFetching: isFetchingTimeslots,
    refetch: refetchTimeslots,
  } = useQuery(
    [
      timeSlotsResource.cacheKey,
      {
        course: selectedCourse || 0,
        date: formatDate(selectedDate, REVERSED_DATE_FORMAT_DASH),
        memberType: member.member,
        numberOfPlayers: players,
      },
    ],
    timeSlotsResource.fetchTimeSlots,
    { retry: 0, cacheTime: 0 },
  );

  const filterTimeslots = (data?: TimeSlotsResult) => data?.timeSlots?.filter((item) => {
    const [hours, minutes] = item.start.split(':');
    const isPast = moment().date() === +moment(selectedDate).format('D') && +moment.duration({
      minutes: +minutes,
      hours: +hours,
    })
    < +moment.duration({
      minutes: moment().minutes(),
      hours: moment().hours(),
    });

    return item.event === null && !isPast;
  });

  return {
    timeslotsData,
    isLoadingTimeslots,
    isFetchingTimeslots,
    refetchTimeslots,
    filterTimeslots,
  };
};
