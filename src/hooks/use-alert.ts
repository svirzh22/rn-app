import { useContext } from 'react';

import { AlertContext } from '@components';

export const useAlert = () => useContext(AlertContext);

export const useApiErrorAlert = () => {
  const { showAlert } = useAlert();

  return { showApiErrorAlert: (e: Record<any, any>) => showAlert({ text: e?.data?.errorMessage }) };
};
