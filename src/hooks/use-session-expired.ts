import { useDispatch } from 'react-redux';

import { sessionExpired } from '@src/actions';
import { sessionExpiredAlert } from '@helpers';

export const useSessionExpired = () => {
  const dispatch = useDispatch();
  const onSessionExpired = (data: any) => {
    if (data?.data?.code?.includes('0.0.3') || data?.data?.errorType === 'NO_MATCH') {
      dispatch(sessionExpired());
      sessionExpiredAlert();
    }
  };
  return {
    onSessionExpired,
  };
};
