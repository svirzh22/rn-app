/* eslint-disable global-require */
// @ts-ignore
import logo from './logo.png';
// @ts-ignore
import userAvatar from './user-avatar.png';
// @ts-ignore
import creditCard from './credit-card.png';
// @ts-ignore
import frontDesk from './front-desk.png';

export const images = {
  logo,
  userAvatar,
  creditCard,
  frontDesk,
  severiano: require('./severino.jpg'),
  jack: require('./jack.jpg'),
};
