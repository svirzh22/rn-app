import RNSecureKeyStore, { ACCESSIBLE } from 'react-native-secure-key-store';

export enum AuthEntities {
  bearer = 'bearer',
  biometryEnabled = 'biometryEnabled',
}

class AuthService {
  async getFromStorage(entity: AuthEntities): Promise<string | null> {
    try {
      const res = await RNSecureKeyStore.get(entity);
      return res;
    } catch (_) {
      return null;
    }
  }

  async canAuthByBiometry() {
    const biometryEnabled = await this.getFromStorage(AuthEntities.biometryEnabled);
    const bearer = await this.getFromStorage(AuthEntities.bearer);

    return Boolean(biometryEnabled) && Boolean(bearer);
  }

  async saveToStorage(entity: AuthEntities, value: string) {
    await RNSecureKeyStore.set(entity, value, {
      accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY,
    });
  }

  async clearStorage() {
    try {
      await RNSecureKeyStore.remove(AuthEntities.bearer);
    } catch (e) {
      // in case of missing key
    }
  }
}

export const authService = new AuthService();
