import React, { FC, ReactNode, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import { setLocale } from '@src/actions/settings';
import { translate as t } from '@helpers';
import { isRehydratedSelector, localeSelector } from '@selectors';
import i18n from 'i18n-js';

export const LocaleContext = React.createContext(t);

interface LocaleProviderProps {
  children: ReactNode;
  locale?: string;
}

export const LocaleProvider: FC<LocaleProviderProps> = ({ children }) => {
  const dispatch = useDispatch();
  const locale = useSelector(localeSelector);
  const isRehydrated = useSelector(isRehydratedSelector);

  useEffect(() => {
    if (isRehydrated && (i18n.locale !== locale || moment.locale() !== locale)) {
      dispatch(setLocale(locale));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isRehydrated]);

  const translate = (key: string, options?: object) => t(key, { ...options, locale });

  return <LocaleContext.Provider value={translate}>{children}</LocaleContext.Provider>;
};
