import { ApiResponse } from 'apisauce';
import { FetchBlobResponse } from 'rn-fetch-blob';

interface FetchBlocResponseToApisauceAdapterParams {
  response: FetchBlobResponse;
  serializableData: Record<string, any>;
  method: 'POST' | 'PUT';
  url: string;
}

export const fetchBlocResponseToApisauceAdapter = <Response>({
  response,
  serializableData,
  method,
  url,
}: FetchBlocResponseToApisauceAdapterParams) => {
  const isOk = response.respInfo.status >= 200 && response.respInfo.status < 300;

  const getProblem = () => {
    if (response.respInfo.status >= 400 && response.respInfo.status < 500) {
      return 'CLIENT_ERROR';
    }

    if (response.respInfo.status >= 500) return 'SERVER_ERROR';

    return 'UNKNOWN_ERROR';
  };

  return {
    ok: isOk,
    status: response.respInfo.status,
    headers: response.respInfo.headers,
    problem: isOk ? null : getProblem(),
    originalError: null,
    config: { method, url, data: serializableData },
  } as ApiResponse<Response, ErrorResponse>;
};
