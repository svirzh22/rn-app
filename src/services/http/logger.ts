import { DEFAULT_LOGGER_LEVEL, LogLevel } from '@constants';

/* eslint-disable no-console */
class Logger {
  logLevel: LogLevel = DEFAULT_LOGGER_LEVEL;

  displayLoggerGroup = (title: string, fields: any[]) => {
    console.groupCollapsed(`%c ${title}`, 'font-weight: normal;');
    fields.forEach((param: any) => {
      console.log(param);
    });
    console.groupEnd();
  };

  logRequestResult = ({ config, data, status, originalError, ok }: any, { style }: { style: string }) => {
    const { method, url, headers, params } = config;

    console.groupCollapsed(`%c ${status} ${method} ${url}`, `${style};`);
    if (params && Object.keys(params).length) {
      this.displayLoggerGroup('Params', [params]);
    }
    if (headers && Object.keys(headers).length) {
      this.displayLoggerGroup('Headers', [headers]);
    }
    if (config.data) {
      this.displayLoggerGroup('Request body', [config?.data]);
    }
    if (data) {
      this.displayLoggerGroup('Response data', [data]);
    }
    if (!ok) {
      this.displayLoggerGroup('Error', [originalError]);
    }
    console.groupEnd();
  };

  logRequest = (response: any) => {
    if (response.config.method === 'get') {
      this.logRequestResult(response, { style: 'color: #757de8; font-weight: bold;' });
    } else {
      this.logRequestResult(response, { style: 'color: red' });
    }
  };
}

export const logger = new Logger();
