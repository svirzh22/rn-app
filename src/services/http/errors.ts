// eslint-disable-next-line max-classes-per-file
import {
  CLIENT_ERROR,
  SERVER_ERROR,
  CONNECTION_ERROR,
  CANCEL_ERROR,
  NETWORK_ERROR,
  TIMEOUT_ERROR,
  UNKNOWN_ERROR,
} from 'apisauce';

export class HttpError extends Error {
  name = 'HttpError';
  data = {};

  constructor(name?: string, data?: {}) {
    super(name);

    if (data) {
      this.data = data;
    }
  }
}

export class HttpClientError extends HttpError {
  name: string = CLIENT_ERROR;
}

export class HttpBusinessRuleError extends HttpError {
  name = 'BUSINESS_LIMITATION';
}

export class HttpAlreadyLockedError extends HttpError {
  name = 'ALREADY_LOCKED';
}

export class HttpAdvancedSessionRequiredError extends HttpClientError {
  name = '401 - Advanced session required';
}

export class HttpBadOtpCodeError extends HttpClientError {
  name = '401 - BAD_OTP_CODE';
}

export class HttpBadCredentialsError extends HttpClientError {
  name = '401 - BAD_CREDENTIALS';
}

export class HttpClientValidationError extends HttpClientError {
  name = '400 - client validation';
}

export class HttpForbiddenError extends HttpClientError {
  name = '403 - forbidden error';
}

export class HttpAdminActionError extends HttpClientError {
  name = '403 - admin action error';
}

export class HttpIdentityNokError extends HttpClientError {
  name: ErrorCode = 'IDENTITY_NOK';
}

export class HttpEsignNokError extends HttpClientError {
  name: ErrorCode = 'ESIGN_NOK';
}

export class HttpAdminWaitError extends HttpClientError {
  name: ErrorCode = 'ADMIN_WAIT';
}

export class HttpUploadsNokError extends HttpClientError {
  name: ErrorCode = 'UPLOADS_NOK';
}

export class HttpUnsafePasswordError extends HttpClientValidationError {
  name: ErrorCode = 'PASSWORD_NOT_SAFE';
}

export class HttpUserAlreadyExistsError extends HttpClientValidationError {
  name: ErrorCode = 'USER_ALREADY_EXISTS';
}

export class HttpBookingAlreadyConfirmedError extends HttpClientError {
  name: ErrorCode = 'BOOKING_ALREADY_CONFIRMED';
}

export class HttpSwiftNotCorrectError extends HttpClientValidationError {
  name: ErrorCode = 'SWIFT_NOT_CORRECT';
}

export class HttpServerError extends HttpError {
  name = SERVER_ERROR;
}

export class HttpConnectionError extends HttpError {
  name = CONNECTION_ERROR;
}

export class HttpNetworkError extends HttpError {
  name = NETWORK_ERROR;
}

export class HttpTimeoutError extends HttpError {
  name = TIMEOUT_ERROR;
}

export class HttpCancelError extends HttpError {
  name = CANCEL_ERROR;
}

export class HttpUnknownError extends HttpError {
  name = UNKNOWN_ERROR;
}
