interface HttpMethodBaseParams {
  url: string;
  params?: Record<string, any>;
  data?: Record<string, any>;
  headers?: Record<string, any>;
  baseURL?: string;
  responseType?: 'blob' | 'json';
}

interface ErrorMessagesOptions {
  errorSelector?: (response: any) => string;
  errorStatusMessages?: Record<number, string>;
}

interface SendFormDataParams extends ErrorMessagesOptions {
  method: 'POST' | 'PUT';
  url: string;
  fileApiKey?: string;
  file: IFile;
  serializableData?: Record<string, any>;
}

type HttpMethodParamsType = HttpMethodBaseParams & ErrorMessagesOptions;

interface ErrorResponse {
  'errorMessage': string;
  'errorType': string;
  'code': string;
}

type ErrorCode =
    // "Identity check is already OK",
    'IDENTITY_ALREADY_OK'
    |
    // "You need to upload at least 1 document",
    'UPLOADS_NOK'
    |
    // "User identity is not validated",
    'IDENTITY_NOK'
    |
    // "User terms of use signature is not done",
    'ESIGN_NOK'
    |
    // "An administrator will review data and enable your account",
    'ADMIN_WAIT'
    |
    // "You are not allowed to access this resource.",
    'NOT_PERMITED'
    |
    // "This account is disabled.",
    'DISABLED_ACCOUNT'
    |
    // "This resource is read only, contact an admin if you need to edit",
    'READONLY_RESOURCE'
    |
    // "Account is on error, please contact support",
    'ACCOUNT_ERROR'
    |
    // "You are logged in. Please Logout first.",
    'ALREADY_LOGGED_IN'
    |
    // "Email already exists in our base.",
    'USER_ALREADY_EXISTS'
    |
    // "You are not logged in",
    'NOT_AUTHENTICATED'
    |
    // "username not found or password didn't match.",
    'BAD_CREDENTIALS'
    |
    // "Please use PATCH instead.",
    'METHOD_NOT_ALLOWED'
    |
    // "OTP session code required",
    'OTP_SESSION_REQUIRED'
    |
    // "bad OTP code"
    'BAD_OTP_CODE'
    |
    // "Main server timeout after %s retries." % WILDHORN_RETRY,
    'MAIN_SERVER_TIMEOUT'
    |
    // "Bad or empty value for 'type' query parameter",
    'BAD_DOCUMENT_TYPE'
    |
    // "Max file number per request is %s" % MAX_FILE_NUMBER,
    'TOO_MUCH_FILES'
    |
    // "File extension not authorized, valid extensions are : %s"
    // % ";".join(ACCEPTED_FILE_EXTENSIONS|,
    'BAD_FILE_EXTENSION'
    |
    // "File have already been submitted",
    'FILE_ALREADY_SUBMITTED'
    |
    // "File is too large, max size is %s bytes" % FILE_MAX_SIZE,
    'FILE_TOO_LARGE'
    |
    // "Customer address have no country set (needed for onfido token creation|,
    // please set a country then retry.",
    'CUSTOMER_DATA_NO_COUNTRY'
    |
    // "Customer address no street set (needed for onfido token creation|, please set a street then retry.",
    'CUSTOMER_DATA_NO_STREET'
    |
    // "Customer address have no city set (needed for onfido token creation|, please set a city then retry.",
    'CUSTOMER_DATA_NO_CITY'
    |
    // "Customer address have no zipcode set (needed for onfido token creation|,
    // please set a zipcode then retry.",
    'CUSTOMER_DATA_NO_ZIPCODE'
    |
    // "IBAN is not valid.",
    'IBAN_NOT_CORRECT'
    |
    // "SWIFT code is not valid.",
    'SWIFT_NOT_CORRECT'
    |
    // "A Referer header must be present in the request",
    'NO_REFERER'
    |
    // "Contract not found for customer, did you retrieved it before ?",
    'CONTRACT_NOT_FOUND'
    |
    // "Passwords do not match",
    'PASSWORD_NOT_MATCH'
    |
    // "One or more field's value valididation failed",
    'FIELD_VALUE_ERROR'
    |
    // "Passwords is not safe enough",
    'PASSWORD_NOT_SAFE'
    | 'BOOKING_ALREADY_CONFIRMED';
