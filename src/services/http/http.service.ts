import { ApiErrorResponse, ApiResponse, ApisauceInstance, create } from 'apisauce';
import RNFetchBlob from 'rn-fetch-blob';

import { API_BASE_URL } from '@constants';
import { store } from '@src/App';
import { resetStore } from '@src/actions';
import { getAPIUrl, sessionExpiredAlert, translate } from '@helpers';
import { ServerException } from '@api/resources/exceptions';
import { fetchBlocResponseToApisauceAdapter } from './helpers';
import {
  HttpAdminActionError,
  HttpAdminWaitError,
  HttpAdvancedSessionRequiredError,
  HttpBadCredentialsError,
  HttpBadOtpCodeError,
  HttpBusinessRuleError,
  HttpCancelError,
  HttpClientError,
  HttpClientValidationError,
  HttpConnectionError,
  HttpEsignNokError,
  HttpForbiddenError,
  HttpIdentityNokError,
  HttpNetworkError,
  HttpServerError,
  HttpTimeoutError,
  HttpUnknownError,
  HttpUploadsNokError,
  HttpUserAlreadyExistsError,
  HttpBookingAlreadyConfirmedError,
} from './errors';
import { logger } from './logger';

class HTTPService {
  static AUTH_HEADER_KEY = 'Authorization';

  private instance: ApisauceInstance;

  constructor() {
    this.instance = create({
      baseURL: getAPIUrl(),
      timeout: 20000,
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
  }

  async get<Response>({
    url,
    params = {},
    headers = {},
    baseURL,
    responseType = 'json',
  }: HttpMethodParamsType) {
    const response = await this.instance.get<Response, ErrorResponse>(url, params, {
      headers,
      baseURL,
      responseType,
    });

    logger.logRequest(response);

    return this.handleResponse(response);
  }

  async post<Response>({
    url,
    data,
    headers = {},
    baseURL,
  }: HttpMethodParamsType) {
    const response = await this.instance.post<Response, ErrorResponse>(url, data, { headers, baseURL });

    logger.logRequest(response);

    return this.handleResponse<Response>(response);
  }

  async patch<Response>({
    url,
    data,
    headers = {},
    baseURL,
  }: HttpMethodParamsType) {
    const response = await this.instance.patch<Response, ErrorResponse>(url, JSON.stringify(data), {
      headers,
      baseURL,
    });

    logger.logRequest(response);

    return this.handleResponse<Response>(response);
  }

  async put<Response>({ url, data, headers = {} }: HttpMethodParamsType) {
    const response = await this.instance.put<Response, ErrorResponse>(url, JSON.stringify(data), {
      headers,
    });

    logger.logRequest(response);

    return this.handleResponse<Response>(response);
  }

  async delete<Response>({
    url,
    params = {},
    headers = {},
    data,
    baseURL,
  }: HttpMethodParamsType) {
    const response = await this.instance.delete<Response, ErrorResponse>(url, params, {
      headers,
      baseURL,
      data,
    });

    logger.logRequest(response);

    return this.handleResponse<Response>(response);
  }

  async sendFormData<Response>({
    method,
    url,
    fileApiKey,
    file,
    serializableData = {},
  }: SendFormDataParams) {
    const array = file.uri.split('/');
    const filename = file.name ?? array[array.length - 1];
    const data = [
      {
        filename,
        name: fileApiKey ?? filename,
        type: file.type,
        data: RNFetchBlob.wrap(file.uri.replace('file://', '')),
      },
      ...Object.entries(serializableData).map((entry) => ({
        name: entry[0],
        data: JSON.stringify(entry[1]),
      })),
    ];

    const response = await RNFetchBlob.fetch(
      method,
      `${API_BASE_URL}${url}`,
      {
        ...this.instance.headers,
        'Content-Type': 'multipart/form-data',
      },
      data,
    );

    const apisauceResponse = fetchBlocResponseToApisauceAdapter<Response>({
      url,
      method,
      response,
      serializableData,
    });

    logger.logRequest(apisauceResponse);

    return this.handleResponse<Response>(apisauceResponse);
  }

  handleSessionExpired = () => {
    if (this.instance.headers[HTTPService.AUTH_HEADER_KEY]) {
      sessionExpiredAlert();
    }

    this.removeAuthHeader();
    store.dispatch(resetStore());
  };

  handleResponse<Response>(response: ApiResponse<Response, ErrorResponse>) {
    if (response.ok) {
      // @ts-expect-error
      if (response.data && !response.data.code && !response.data.msg) {
        return response.data;
      }
      // BE always responds with status code 200, even with error.
      throw new ServerException(response.data as any);
    }

    switch (response.problem) {
      case 'CLIENT_ERROR': {
        // if invalid or expired token
        if (
          ['NO_MATCH', 'EXPIRED'].includes(response?.data?.errorType || '')
          || response?.data?.code?.includes('0.0.3')
          || response?.data?.code?.includes('0.0.2')
          || response?.data?.code?.includes('0.0.1') // Authorization missing from request
        ) {
          this.handleSessionExpired();
        }

        if (response.status === 400) {
          this.handle400Errors(response);
        } else if (response.status === 401) {
          this.handle401Errors(response);
        }

        if (response.status === 412) {
          throw new ServerException(response.data as any);
        }

        if (response.status === 403) {
          this.handle403Errors(response);
        }

        if (response?.data?.errorType === 'BUSINESS_LIMITATION') {
          throw new HttpBusinessRuleError(response?.data?.errorMessage, response.data);
        }

        throw new HttpClientError(translate('errors.unexpectedError'), response.data);
      }
      case 'SERVER_ERROR':
        if (response.data?.errorType === 'INTERNAL') {
          throw new HttpServerError(response.data.errorMessage, response.data);
        }
        throw new HttpServerError(
          response?.data?.errorMessage || translate('errors.http-server'),
          response.data,
        );
      case 'TIMEOUT_ERROR':
        throw new HttpTimeoutError(translate('errors.http-timeout'));
      case 'CONNECTION_ERROR':
        throw new HttpConnectionError(translate('errors.http-connection'));
      case 'NETWORK_ERROR':
        throw new HttpNetworkError(translate('errors.http-network'));
      case 'UNKNOWN_ERROR':
        throw new HttpUnknownError(translate('errors.http-unknown'));
      case 'CANCEL_ERROR':
        throw new HttpCancelError(translate('errors.http-cancel'));
      default:
        throw new HttpUnknownError(translate('errors.http-unknown'));
    }
  }

  setAPIUrl(url: string) {
    this.instance.setBaseURL(url);
  }

  setAuthHeader(token: string) {
    this.instance.setHeader(HTTPService.AUTH_HEADER_KEY, token);
  }

  removeAuthHeader() {
    this.instance.deleteHeader(HTTPService.AUTH_HEADER_KEY);
  }

  protected handle403Errors(response: ApiErrorResponse<ErrorResponse>) {
    if (!response.data?.errorType) {
      throw new HttpForbiddenError(translate('errors.http-forbidden'));
    }

    switch (response.data.errorType) {
      case 'NOT_AUTHENTICATED':
        throw new HttpForbiddenError(translate('error.http-forbidden'));
      case 'IDENTITY_NOK':
        throw new HttpIdentityNokError();
      case 'ESIGN_NOK':
        throw new HttpEsignNokError();
      case 'ADMIN_WAIT':
        throw new HttpAdminWaitError();
      case 'UPLOADS_NOK':
        throw new HttpUploadsNokError();
      case 'ADMIN_ACTION':
        throw new HttpAdminActionError();
      case 'USER_ALREADY_EXISTS':
        throw new HttpUserAlreadyExistsError();
      default:
        break;
    }
  }

  protected handle401Errors(response: ApiErrorResponse<ErrorResponse>) {
    if (response.data?.errorType === 'OTP_SESSION_REQUIRED') {
      throw new HttpAdvancedSessionRequiredError();
    } else if (response.data?.errorType === 'BAD_OTP_CODE') {
      throw new HttpBadOtpCodeError();
    } else if (response.data?.errorType === 'BAD_CREDENTIALS') {
      throw new HttpBadCredentialsError();
    }
  }

  protected handle400Errors(response: ApiErrorResponse<ErrorResponse>) {
    if (response.data?.errorType === 'BAD_OTP_CODE') {
      throw new HttpBadOtpCodeError();
    }

    if (response.data?.errorType === 'FIELD_VALUE_ERROR') {
      const errors: Array<[string, Array<string>]> = Object.entries(response?.data ?? {}).filter(
        ([key]) => key !== 'code' && key !== 'detail',
      );

      const errorMessage = errors.length && errors[0].length === 2 && errors[0][1].length
        ? `${errors[0][0].toUpperCase()} - ${errors[0][1][0].toLowerCase()}`
        : null;
      if (errorMessage) {
        throw new HttpClientValidationError(errorMessage);
      }
    }

    if (response.data?.errorType === 'BOOKING_ALREADY_CONFIRMED') {
      throw new HttpBookingAlreadyConfirmedError();
    }

    return response;
  }
}

export const httpService = new HTTPService();
