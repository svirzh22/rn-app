import { TextStyle } from 'react-native';
import { DefaultTheme } from 'react-native-paper';

export const COLORS = {
  PRIMARY: {
    500: '#F5C70A',
  },
  SECONDARY: {
    200: '#75C653',
    500: '#287D3C',
  },
  GREY: {
    200: '#F7F7FA',
    400: '#F2F2F7',
    500: '#E5E5EA',
    600: '#C7C7CC',
    650: '#AEAEB2',
    700: '#8E8E93',
  },
  TRANSPARENT: 'transparent',
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  STATUS_BAR: '#d7b108',
  RED: '#FF3E33',
  BLUE: '#001CF5',
  ERROR: '#B00020',
};

export const FONT_SIZES = {
  XXS: 12,
  XS: 14,
  S: 16,
  M: 18,
  L: 20,
  XL: 24,
  XXL: 32,
};

export type WeightType = 'light' | 'regular' | 'medium' | 'semiBold' | 'bold';

interface PrimaryFont {
  [field: string]: TextStyle;
  light: TextStyle;
  regular: TextStyle;
  medium: TextStyle;
  semiBold: TextStyle;
  bold: TextStyle;
}

export const PRIMARY_FONT: PrimaryFont = {
  light: {
    fontFamily: 'FuturaPT-Light',
    fontWeight: '300',
  },
  regular: {
    fontFamily: 'FuturaPT-Book',
    fontWeight: '400',
  },
  medium: {
    fontFamily: 'FuturaPT-Medium',
    fontWeight: '500',
  },
  semiBold: {
    fontFamily: 'FuturaPT-Demi',
    fontWeight: '600',
  },
  bold: {
    fontFamily: 'FuturaPT-Bold',
    fontWeight: '700',
  },
};

export const SPACING = {
  XXS: 4,
  XS: 8,
  S: 16,
  M: 24,
  L: 32,
  XL: 40,
  XXL: 48,
};

export const RNPaperTheme = {
  ...DefaultTheme,
  roundness: 8,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.BLACK,
    accent: '#f1c40f',
  },
};
