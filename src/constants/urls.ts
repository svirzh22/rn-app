export const TEST_API_URL = 'https://booking-crm.as-105507.allsquaregolf.com/en/';
export const PROD_API_URL = 'https://teetime.golfcrans.ch/en/';
export const STAGING_API_URL = 'https://booking-crm-staging.as-105507.allsquaregolf.com/en/';

export const API_BASE_URL = TEST_API_URL;

// prettier-ignore
// eslint-disable-next-line max-len
export const ACADEMY_LINK = 'https://www.golfcrans.ch/fr/page/crans-golf-school/presentation/presentation-crans-montana-golf-academy-90';
export const TOURNAMENTS_LINK = 'https://www.golfcrans.ch/fr/page/golf-club/competitions/competitions-600';
export const CONDITIONS_URL = 'https://teetime.golfcrans.ch/{{language}}/conditions';
export const IOS_APP_LINK = 'https://apps.apple.com/app/golf-club-crans-sur-sierre/id1382210998';
export const ANDROID_APP_LINK = 'https://play.google.com/store/apps/details?id=ch.iomedia.golfcrans';
