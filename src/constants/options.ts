export enum LogLevel {
  disabled,
  enabled,
}

export const DEFAULT_LOGGER_LEVEL = LogLevel.enabled;
