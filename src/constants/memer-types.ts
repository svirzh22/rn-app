export const memberTypes = [
  {
    idMember: 2,
    label: 'Crans Abo Général (SB-JN)',
    short_label: 'Crans',
  },
  {
    idMember: 4,
    label: 'Visiteurs',
    short_label: 'Visiteurs',
  },
  {
    idMember: 7,
    label: 'Montreux',
    short_label: 'Montreux',
  },
  {
    idMember: 8,
    label: 'ASGI',
    short_label: 'ASGI',
  },
  {
    idMember: 9,
    label: 'Crans sans abo',
    short_label: 'Crans sans abo',
  },
  {
    idMember: 11,
    label: 'AVG (sion, sierre, loeche, verbier, zermatt, source du rhône, riederalp)',
    short_label: 'AVG',
  },
  {
    idMember: 15,
    label: 'Abo 3 clubs',
    short_label: 'Abo 3 clubs',
  },
  {
    idMember: 16,
    label: 'Abo Jack Nicklaus',
    short_label: 'Abo Jack Nicklaus',
  },
  {
    idMember: 17,
    label: 'Juniors (visiteurs -21)',
    short_label: 'Juniors (visiteurs -21)',
  },
  {
    idMember: 18,
    label: 'Crans juniors sans abo ( -21 )',
    short_label: 'Crans juniors sans abo ( -21 )',
  },
  {
    idMember: 19,
    label: 'Pro',
    short_label: 'Pro',
  },
  {
    idMember: 20,
    label: 'Hôtels',
    short_label: 'Hôtels',
  },
  {
    idMember: 21,
    label: 'Tour Operator',
    short_label: 'TO',
  },
  {
    idMember: 22,
    label: 'Pro Crans',
    short_label: '',
  },
  {
    idMember: 23,
    label: 'TO CMT',
    short_label: 'CMT',
  },
  {
    idMember: 24,
    label: 'Golf Hotel Guests',
    short_label: 'Golf Hotel Guests',
  },
];
