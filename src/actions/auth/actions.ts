import { createAction } from 'redux-actions';

import { AuthResourceVerifyOTPResult } from '@api/resources/auth-resource/types';
import * as types from './action-types';

export const signIn = createAction<AuthResourceVerifyOTPResult>(types.SIGN_IN);
export const sessionExpired = createAction(types.SESSION_EXPIRED);
export const resetStore = createAction(types.RESET_STORE);
export const logOut = createAction<boolean | undefined>(types.LOG_OUT);

export const setMember = createAction<UserEntity>(types.SET_MEMBER);
export const updateProfile = createAction<Partial<UserEntity>>(types.UPDATE_PROFILE);
