export const SIGN_IN = 'SIGN_IN';
export const SESSION_EXPIRED = 'SESSION_EXPIRED';
export const SET_MEMBER = 'SET_MEMBER';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const LOG_OUT = 'LOG_OUT';

export const RESET_STORE = 'RESET_STORE';
