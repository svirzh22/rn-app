import { createAction } from 'redux-actions';

import * as types from './action-types';

export const setLockInfo = createAction<{ checkId: number; timeSlot: string }>(types.SET_LOCK_INFO);
export const freeBooking = createAction(types.FREE_BOOKING);
export const resetLockInfo = createAction(types.RESET_LOCK_INFO);
