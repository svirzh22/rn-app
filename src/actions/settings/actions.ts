import { createAction } from 'redux-actions';

import * as types from './action-types';

export const setLocale = createAction<string>(types.SET_LOCALE);
