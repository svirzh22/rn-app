import { createAction } from 'redux-actions';

import { SET_APP_LOADING, SET_SENT_OTP_TIME } from './action-types';

export const setAppLoading = createAction(SET_APP_LOADING);
export const setSentOtpTime = createAction(SET_SENT_OTP_TIME);
