import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider as PaperProvider } from 'react-native-paper';
import React from 'react';

import { RootNavigator, DevRootNavigator } from '@navigation';
import { AlertContextProvider } from '@components';
import { configureStore } from '@store/configure-store';
import { LocaleProvider } from '@services';
import './helpers/yup';
import { RNPaperTheme } from '@constants';

const queryClient = new QueryClient();
export const { store, persistor } = configureStore();

export const App = () => (
  <QueryClientProvider client={queryClient}>
    <Provider store={store}>
      <PaperProvider theme={RNPaperTheme}>
        <LocaleProvider>
          <PersistGate loading={null} persistor={persistor}>
            <AlertContextProvider>{__DEV__ ? <DevRootNavigator /> : <RootNavigator />}</AlertContextProvider>
          </PersistGate>
        </LocaleProvider>
      </PaperProvider>
    </Provider>
  </QueryClientProvider>
);
