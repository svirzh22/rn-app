import { Action } from 'redux';

class ActionsHistory {
  private actionsHistory: Action[] = [];

  private size = 1;

  constructor(size: number) {
    this.size = size;
  }

  add = (action: Action) => {
    if (this.actionsHistory.length < this.size) {
      this.actionsHistory.push(action);
    } else {
      this.actionsHistory.splice(0, 1);
      this.actionsHistory.push(action);
    }
  };

  get actions() {
    return this.actionsHistory;
  }
}

export const actionsHistory = new ActionsHistory(10);

export const historyMiddleware = () => (next: any) => (action: Action) => {
  if (['persist/REHYDRATE', 'persist/PERSIST'].includes(action.type)) {
    next(action);
  } else {
    actionsHistory.add(action);
    next(action);
  }
};
