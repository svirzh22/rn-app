import { applyMiddleware, Middleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { configureLogger } from '@store/logger';
import { historyMiddleware } from './history';

export const sagaMiddleware = createSagaMiddleware();

const middlewareList: Middleware[] = [sagaMiddleware, historyMiddleware];

if (__DEV__) {
  // GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
  const loggerMiddleware = configureLogger();
  middlewareList.push(loggerMiddleware);
}

export const middleware = applyMiddleware(...middlewareList);
