export const createHitSlop = (...args: number[]) => {
  const defaultValue = args.filter(Boolean).length === 1 ? args[0] : 0;
  const [top = defaultValue, right = defaultValue, bottom = defaultValue, left = defaultValue] = args;

  return { top, right, bottom, left };
};
