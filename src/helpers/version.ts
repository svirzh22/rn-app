import { Platform } from 'react-native';
import semver from 'semver';

import { AppVersionResult } from '@api/resources/app-version/types';
import packageJSON from '../../package.json';

export const getVersion = () => packageJSON.version;

const isVersionHigher = (activeVersion: string, lastVersion: string) =>
  semver.gte(activeVersion, lastVersion);

export const compareLastVersions = (lastVersions: AppVersionResult) => {
  const actualVersion = getVersion();
  let versionHigher;
  if (Platform.OS === 'ios') {
    versionHigher = isVersionHigher(actualVersion, lastVersions.iosVersion);
  } else {
    versionHigher = isVersionHigher(actualVersion, lastVersions.androidVersion);
  }
  return versionHigher ? 1 : -1;
};
