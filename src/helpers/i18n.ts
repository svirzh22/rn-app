import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import en from '@assets/translations/en.json';
import fr from '@assets/translations/fr.json';
import de from '@assets/translations/de.json';

i18n.fallbacks = true;
i18n.translations = { en, fr, de };

const fallback = { languageTag: 'en', isRTL: false };
const { languageTag } = RNLocalize.findBestAvailableLanguage(Object.keys(i18n.translations)) || fallback;
i18n.locale = languageTag;

export const initialLocale = languageTag;
export const translate = (key: string, options?: object) => (key ? i18n.t(key, options) : '');
