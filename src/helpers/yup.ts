import * as Yup from 'yup';
import { isValidNumber } from 'libphonenumber-js';

// eslint-disable-next-line func-names
Yup.addMethod<Yup.StringSchema>(Yup.string, 'isValidNumber', function (message = 'is invalid') {
  return this.test('isValidNumber', message, (number = '') => isValidNumber(number as string));
});

export const handicapValidation = Yup.string().test('isValidHandicap', (handicap) => {
  const value = parseFloat(handicap || '');

  if (value > 8 || value < -54) {
    return false;
  }

  return true;
});
