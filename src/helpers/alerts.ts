import { translate } from 'i18n-js';
import { Alert } from 'react-native';

let alertPresent = false;

export const sessionExpiredAlert = () => {
  if (!alertPresent) {
    alertPresent = true;

    Alert.alert(translate('common.sessionExpired'), undefined, [
      {
        text: 'OK',
        onPress: () => {
          alertPresent = false;
        },
      },
    ]);
  }
};
