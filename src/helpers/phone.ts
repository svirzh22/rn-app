import { parsePhoneNumberFromString as parsePhoneNumberLib } from 'libphonenumber-js';

export const parsePhoneNumber = (value: string) => {
  const { countryCallingCode, nationalNumber } = parsePhoneNumberLib(value) as any;

  return {
    countryCallingCode,
    nationalNumber,
  };
};
