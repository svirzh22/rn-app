export const getRoleLabel = (role: UserRole) => {
  switch (role) {
    case 'ROLE_ADMIN':
    case 'ROLE_DEV':
      return 'Member';
    default:
      return 'Visitor';
  }
};
