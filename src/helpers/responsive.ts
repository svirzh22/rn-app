import { SCREEN_HEIGHT, SCREEN_WIDTH } from '@constants/device';

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = (size: number) => (SCREEN_WIDTH / guidelineBaseWidth) * size;
export const responsiveFontSize = (size: number, factor = 0.5) => size + (scale(size) - size) * factor;
export const responsiveHeight = (size: number) => (SCREEN_HEIGHT / guidelineBaseHeight) * size;
export const responsiveWidth = (size: number, factor = 0.5) => size + (scale(size) - size) * factor;
