import Color from 'color';

export const opacity = (color: string, amount = 0.5) => Color(color).alpha(amount).hsl().string();

export const darken = (color: string, amount = 0.5) => Color(color).darken(amount).hsl().string();

export const lighten = (color: string, amount = 0.5) => Color(color).lighten(amount).hsl().string();
