import { TimeSlotEntity } from '@api/resources/time-slots/types';

export const isTimeslotFilled = (item: TimeSlotEntity) =>
  Object.values(item.players).reduce((acc, player) => {
    if (player.id || player.addedId || player.memberType) return acc + 1;

    return acc;
  }, 0) === 4;
