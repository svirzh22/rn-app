import replace from 'lodash/replace';

export const callSafely = (method: any, args?: any) => {
  if (typeof method === 'function') {
    return method(args);
  }

  return null;
};

export const removeSpaces = (text: string) => replace(text, /\s/g, '');

export const noop = () => {};

export const formatAPIErrorMessage = (errorMessage = '') => errorMessage?.replace(/[0-9.]/g, '').trim();
