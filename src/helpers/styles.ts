import merge from 'lodash/merge';
import get from 'lodash/get';

type ShadowDepth = 0 | 1 | 2 | 3 | 4;

const mapShadowDepth = {
  0: {
    elevation: 0,
    shadowOpacity: 0,
  },
  1: {
    elevation: 1,
    shadowOpacity: 0.1,
    shadowRadius: 6,
  },
  3: {
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 18,
    shadowOffset: { height: 4 },
  },
  4: {
    elevation: 4,
    shadowOpacity: 0.4,
    shadowRadius: 24,
    shadowOffset: { height: 6 },
  },
};

export const shadowDepth = (depth: ShadowDepth = 2) => {
  const shadow = {
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 12,
  };

  return merge(shadow, get(mapShadowDepth, depth, {}));
};
