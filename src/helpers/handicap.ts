// For sending to the API
export const handicapValue = (handicap?: string) => {
  if (handicap) {
    // prettier-ignore
    const nextHandicap = parseFloat(handicap) % 1
      ? parseFloat(handicap).toFixed(1)
      : parseFloat(handicap).toFixed(0);

    return handicap[0] !== '-' ? `+${nextHandicap}` : nextHandicap;
  }

  return '';
};

// For displaying in the app
export const displayHandicap = (handicap?: string | null) => {
  if (handicap) {
    if (parseFloat(handicap) === 0) {
      return '0';
    }

    return handicap[0] === '-' ? handicap.replace('-', '') : `+${handicap}`;
  }

  return '';
};
