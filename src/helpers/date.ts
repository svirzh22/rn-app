import moment from 'moment';

export const DATE_FORMAT = 'DD.MM.YYYY';
export const DATE_TIME_BE_FORMAT = 'YYYY-MM-DD HH:mm';
export const DATE_FORMAT_DASH = 'DD-MM-YYYY';
export const REVERSED_DATE_FORMAT_DASH = 'YYYY-MM-DD';
export const DATE_FORMAT_SINCE = 'D MMMM YYYY';
export const TIME_FORMAT = 'HH:mm';
export const MONTH_YEAR_FORMAT = 'MMMM YYYY';
export const DATE_TIME_FORMAT = `${DATE_FORMAT} – ${TIME_FORMAT}`;

export const getTodayDate = (format?: string) => moment().format(format ?? DATE_FORMAT);
export const getMonthName = (date?: moment.MomentInput) => moment(date).format('MMM');
export const getDayName = (date?: moment.MomentInput) =>
  moment(date).format('ddd');
export const formatTime = (date?: moment.MomentInput) =>
  moment(date).format(TIME_FORMAT);
export const formatDate = (date?: moment.MomentInput, format?: string | null) =>
  moment(date)
    .format(format ?? DATE_FORMAT);
export const getTimeslotDate = (date: moment.MomentInput, time: string) => {
  const timeParts = time.split(':');
  return moment(date).hours(+timeParts[0]).minutes(+timeParts[1]);
};
export const formatDateTimeBE = (date?: moment.MomentInput, format?: string) =>
  moment(date).format(format ?? DATE_TIME_BE_FORMAT);
export const formatMonthYear = (date: moment.MomentInput) =>
  moment(date).format(MONTH_YEAR_FORMAT);
export const getSeasonStartDate = () => {
  const currentDate = moment();
  const seasonStartDate = moment(1620680400000); // May 11 2021

  return currentDate < seasonStartDate ? seasonStartDate : currentDate;
};

export const getMaxBirthdayDate = () => moment().year(2002);
export const getInitialBirthDate = () => moment().year(1990);

export const getMonthYearDate = (date: moment.MomentInput, month: number, year: number) =>
  moment(date).year(year).month(month);

export const getDate = (date: moment.MomentInput, month: number, year: number, day: number) =>
  moment(date).set('years', year).set('months', month).set('date', day);

export const getMonthDays = (date: moment.MomentInput) => moment(date).daysInMonth();
export const getWeekDay = (date: moment.MomentInput, day: number) =>
  moment(date).set('date', day).isoWeekday();

export const MAX_DATE = moment()
  .endOf('year')
  .toDate();
