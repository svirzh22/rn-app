import { API_BASE_URL, CONDITIONS_URL } from '@constants';
import i18n from 'i18n-js';

const supportedLanguages = ['fr', 'en', 'de'];
const getLanguage = (locale = i18n.locale) => (supportedLanguages.includes(locale) ? locale : 'fr');

export const getConditionsUrl = () => CONDITIONS_URL.replace('{{language}}', getLanguage());

export const getAPIUrl = (locale?: string) => API_BASE_URL.replace('en', getLanguage(locale));
