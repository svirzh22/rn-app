import ImagePicker, { Image, Options } from 'react-native-image-crop-picker';

import { noop } from '@helpers';
import { cameraPermission, checkPermission, galleryPermission } from './permissions';

type SingleImagePicker = (opt?: Options) => Promise<Image>;
type CancelCallback = () => void;
type ErrorCallback = (exc?: any) => void;
type SuccessCallback = (image?: any) => void;

const imageProperties = { height: 500, width: 787 };

const options: Options = {
  // width: AVATAR_SIZE * 3,
  // height: AVATAR_SIZE * 3,
  // includeBase64: true,
  // useFrontCamera: true,
};

interface ImageSelectorParams {
  cropper?: boolean;
  onSuccess: SuccessCallback;
  onCancel?: CancelCallback;
}

const handleException = ({
  exc,
  onCancel,
  onError = noop,
}: {
  exc: any;
  onCancel: CancelCallback;
  onError?: ErrorCallback;
}) => {
  if (exc.code === 'E_PICKER_CANCELLED') {
    onCancel();
  }
  onError();
};

const openPicker: SingleImagePicker = () =>
  ImagePicker.openPicker({
    multiple: false,
    includeBase64: true,
    height: imageProperties.height,
    width: imageProperties.width,
    cropping: true,
    cropperCircleOverlay: true,
  }) as Promise<Image>;

const openCropper = async ({
  image,
  onSuccess,
}: {
  image: Image;
  onSuccess: SuccessCallback;
  onCancel: CancelCallback;
}) => {
  if (image) {
    const croppedImage = await ImagePicker.openCropper({
      // ...options,
      mediaType: 'photo',
      path: image.path,
      includeBase64: true,
    });

    onSuccess({
      imageUri: image.data,
      name: croppedImage.filename,
      uri: croppedImage.path,
      type: croppedImage.mime,
      size: croppedImage.size,
    });
  }
};

export const openGallery = async ({
  cropper = false,
  onSuccess,
  onCancel = noop,
}: // eslint-disable-next-line consistent-return
ImageSelectorParams) => {
  const status = await checkPermission(galleryPermission);

  if (!status) return onCancel();

  try {
    const image = await openPicker();

    if (cropper) {
      await openCropper({
        image,
        onSuccess,
        onCancel,
      });
    } else {
      onSuccess({
        imageUri: image.data,
        name: image.filename,
        uri: image.path,
        type: image.mime,
        size: image.size,
      });
    }
  } catch (exc) {
    handleException({
      exc,
      onCancel,
    });
  }
};

export const openCamera = async ({
  onSuccess,
  onCancel = noop,
  cropper = false,
}: // eslint-disable-next-line consistent-return
ImageSelectorParams) => {
  const status = await checkPermission(cameraPermission);

  if (!status) return onCancel();

  try {
    const image = (await ImagePicker.openCamera({
      ...options,
      multiple: false,
      includeBase64: true,
      height: imageProperties.height,
      width: imageProperties.width,
      cropping: true,
      cropperCircleOverlay: true,
    })) as Image;

    if (cropper) {
      await openCropper({
        image,
        onSuccess,
        onCancel,
      });
    } else {
      onSuccess({
        imageUri: image.data,
        name: image.filename,
        uri: image.path,
        type: image.mime,
        size: image.size,
      });
    }
  } catch (exc) {
    handleException({
      exc,
      onCancel,
    });
  }
};
