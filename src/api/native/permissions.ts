import { Platform } from 'react-native';
import Permissions, { Permission, PERMISSIONS, RESULTS } from 'react-native-permissions';

export const cameraPermission = Platform.OS === 'ios'
  ? PERMISSIONS.IOS.CAMERA
  : PERMISSIONS.ANDROID.CAMERA;
export const galleryPermission = Platform.OS === 'ios'
  ? PERMISSIONS.IOS.PHOTO_LIBRARY
  : PERMISSIONS.ANDROID.CAMERA;

export const checkPermission = async (permissionName: Permission) => {
  const permissionStatus = await Permissions.check(permissionName);

  if (permissionStatus === RESULTS.GRANTED) {
    return true;
  }

  const requestStatus = await Permissions.request(permissionName);

  if (requestStatus === RESULTS.GRANTED) {
    return true;
  }

  return false;
};
