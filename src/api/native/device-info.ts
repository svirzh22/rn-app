import { getUniqueId } from 'react-native-device-info';

export const getDeviceId = () => getUniqueId();
