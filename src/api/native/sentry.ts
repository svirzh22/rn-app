/* eslint-disable arrow-body-style */

import { API_BASE_URL } from '@constants';
import * as Sentry from '@sentry/react-native';
import { actionsHistory } from '@store/middleware/history';

export const setSentryUser = (user: any | null) => {
  if (__DEV__) return null;

  return Sentry.setUser(user);
};

export const sendExceptionToSentry = (exc: any) => {
  if (__DEV__) return null;

  Sentry.setExtra('Exception', exc);
  Sentry.setExtra('Actions', actionsHistory.actions);

  return Sentry.captureException(exc);
};

export const initializeSentry = () => {
  if (__DEV__) return null;

  Sentry.init({
    dsn: 'https://9b982f8349544a429f960c791cc7ada1@o78595.ingest.sentry.io/5745522',
    environment: API_BASE_URL,
    ignoreErrors: ['Network error'],
  });

  Sentry.configureScope((scope) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    scope.addEventProcessor((event, _hint) => {
      return {
        ...event,
        extra: {
          ...event.extra,
          actions: actionsHistory.actions,
        },
      };
    });
  });

  return undefined;
};

initializeSentry();
