interface UserCancelPlayerResourceParams {
  userToken: string;
  reservationId: string;
  player: string;
}

interface UserCancelPlayerResourceResult {
  error: boolean;
  datas: UserEntity;
}
