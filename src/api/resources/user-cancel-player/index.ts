import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class UserCancelPlayerResource extends Resource {
  cacheKey = 'user-cancel-player';
  static getUrl = ({ userToken, reservationId, player }: UserCancelPlayerResourceParams) =>
    `/api-apps/user-cancel-player/${userToken}/${reservationId}/${player}`;

  cancelPlayer = async ({ queryKey }: QueryFunctionContext) => {
    const [, { userToken, reservationId, player }] = queryKey as [string, UserCancelPlayerResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<UserCancelPlayerResourceResult>({
      url: UserCancelPlayerResource.getUrl({ userToken, reservationId, player }),
      headers: {
        ...headers,
      },
    });
  };
}

export const userCancelPlayerResource = new UserCancelPlayerResource();
