export interface BookingMemberEntity {
  firstname: string;
  lastname: string;
  handicap: string;
  asg: string;
  country: string;
  memberType: number;
  cost: string;
  paymentState: number;
  comment: string;
  started: null;
  id: number;
  addedId: number;
  admin: number;
  billingId: number;
  picture: string;
}

export interface BookingMemberEntityInternal extends BookingMemberEntity {
  editable?: boolean;
  own?: boolean;
}

export interface BookingOptionEntity {
  quantity: null;
  totalCost?: null;
  cost?: null;
  paymentState: null;
  billingId: null;
  admin: null;
  label?: string;
}

export type BookingOptionType = 'smallCar' | 'electricCar' | 'caddy';
export interface BookingOptions {
  [key: BookingOptionType]: BookingOptionEntity;
  smallCar: BookingOptionEntity;
  electricCar: BookingOptionEntity;
  caddy: BookingOptionEntity;
}

export interface BookingEntity {
  id: number;
  sessionId: string;
  online: null;
  app: null;
  sourceApp: string;
  member: number;
  timeSlot: string;
  golfCourse: number;
  nbPlayers: number;
  players: {
    [key: '1' | '2' | '3' | '4']: BookingMemberEntity;
    1: BookingMemberEntity;
    2: BookingMemberEntity;
    3: BookingMemberEntity;
    4: BookingMemberEntity;
  };
  paymentState: number;
  totalCost: string;
  reduceCost: null;
  options: BookingOptions;
  insurance: BookingOptionEntity;
  byPhone: number;
  createdAt: string;
  updatedAt: string;
  isPast?: boolean;
  canCancel?: boolean;
  canUpdate?: boolean;
  canView?: boolean;
}
