type UserRole = 'ROLE_ADMIN' | 'ROLE_DEV' | 'ROLE_MEMBER' | 'ROLE_VISITOR';

interface EntityDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

interface UserEntity {
  firstname: string;
  lastname: string;
  username: string;
  id: number;
  member: number;
  role: UserRole;
  hcp: number;
  handicap: string;
  photo?: string;
  favourites?: any[];
  private?: boolean;
  shortLastname?: string;
  mobilePhone?: string;
  email?: string;
  language?: string;
  asg?: string;
  picture?: string;
  createdAt?: EntityDate;
  updatedAt?: EntityDate;
}

interface OwnUserEntity extends UserEntity {
  token: string;
}
