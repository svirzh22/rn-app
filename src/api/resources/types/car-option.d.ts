interface CarDate {
  date: string;
  timezone_type: number;
  timezone: string;
}
type CarType = 'caddy' | 'electric_car' | 'small_car';

export interface CarOption {
  createdat: CarDate;
  updatedat: CarDate;
  label: string;
  baseStock: number;
  onRepair: number;
  type: CarType;
  maxByOrder: number;
  orderable: boolean;
  unityPrice: number;
  unityPrice2: number;
  id: number;
  availability: number;
  used: number;
}
