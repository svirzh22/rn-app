export interface PlayerEntity {
  id: number | string | null;
  addedId: number | string | null;
  firstname: string | null;
  shortLastname: string | null;
  lastname: string | null;
  handicap: string | null;
  country: string | null;
  asg: string | null;
  type: string | null;
  picture: string | null;
  member: number;
  memberType: number;
  private: boolean;
}

interface PlayerEntityInternal extends PlayerEntity {
  editable?: boolean;
  own?: boolean;
}
