interface CourseEntity {
  id: number;
  title: string;
  nbHoles: number;
  requiredHandicap: number;
  status: boolean;
  par: number;
  distance: number;
  backgroundPicture: string;
  holesMap: string;
  storyline: string;
  description: string;
  rules: string;
}
