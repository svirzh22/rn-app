export interface HoleEntity {
  number: number;
  distance: number;
  par: number;
}

export interface HoleDetailsEntity extends HoleEntity {
  holeMapPicture: string;
  holeMapFootage: string | null;
  holeMapDescription: string;
}

export interface GetHolesParams {
  golfCourse: number;
}

export interface GetHolesResult {
  error: boolean;
  holes: HoleEntity[];
}

export interface GetHoleParams {
  golfCourse: number;
  holeNumber: number;
}

export interface GetHoleResult {
  error: boolean;
  hole: HoleDetailsEntity;
}
