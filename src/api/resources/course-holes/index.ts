import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { GetHolesParams, GetHolesResult, GetHoleParams, GetHoleResult } from './types';

class CourseHolesResource extends Resource {
  cacheKey = 'course-holes';
  static getCourseDetailsUrl = (golfCourse: number) => `/api-apps/course/${golfCourse}`;

  getHoles = async ({ queryKey }: QueryFunctionContext) => {
    const [, params] = queryKey as [string, GetHolesParams];
    const headers = await getAuthHeader();

    return httpService.get<GetHolesResult>({
      url: `${CourseHolesResource.getCourseDetailsUrl(params.golfCourse)}/holes`,
      headers: {
        ...headers,
      },
    });
  };

  getHole = async ({ queryKey }: QueryFunctionContext) => {
    const [, params] = queryKey as [string, GetHoleParams];
    const headers = await getAuthHeader();

    return httpService.get<GetHoleResult>({
      url: `${CourseHolesResource.getCourseDetailsUrl(params.golfCourse)}/hole/${params.holeNumber}`,
      headers: {
        ...headers,
      },
    });
  };
}

export const courseHolesResource = new CourseHolesResource();
