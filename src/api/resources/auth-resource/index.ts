import { httpService } from '@services';
import {
  AuthResourceAskOTPParams,
  AuthResourceAskOTPResult,
  AuthResourceVerifyOTPParams,
  AuthResourceVerifyOTPResult,
} from '@api/resources/auth-resource/types';
import { Resource } from '../common/resource';

export interface AuthResourceRequestCodeResponse {
  code: '200';
}

export class AuthResource extends Resource {
  cacheKey = 'auth';
  static getUrl = () => '/api-apps/otp';

  askOTP = async (params: AuthResourceAskOTPParams) =>
    httpService.get<AuthResourceAskOTPResult>({
      url: AuthResource.getUrl(),
      params,
    });

  verifyOTP = async (params: AuthResourceVerifyOTPParams) =>
    httpService.post<AuthResourceVerifyOTPResult>({
      url: AuthResource.getUrl(),
      data: params,
    });
}

export const authResource = new AuthResource();
