export enum AskOtpErrorTypes {
  MULTIPLE_MATCH = 'MULTIPLE_MATCH',
  NO_MATCH = 'NO_MATCH',
}

export interface AuthResourceAskOTPParams {
  countryCode: string;
  mobilePhone: string;
  // (yyyy-mm-dd)
  birthDate?: string;
}

export interface AuthResourceAskOTPResult {
  error: boolean;
  devOtp: string;
  errorType?: AskOtpErrorTypes;
  errorMessage?: string;
}

export interface AuthResourceVerifyOTPParams {
  countryCode: string;
  mobilePhone: string;
  otpCode: string;
  // (yyyy-mm-dd)
  birthDate?: string;
}

export interface AuthResourceVerifyOTPResult {
  datas: UserEntity & { token: string };
  error: boolean;
  authorization: string;
}
