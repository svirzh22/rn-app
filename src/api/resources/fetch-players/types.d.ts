import { PlayerEntityInternal } from '../types/player';

interface PlayersResourceParams {
  userToken: string;
  query?: string;
}

interface PlayersResourceResult {
  error: boolean;
  suggestions: {
    addedPlayers: PlayerEntityInternal[];
    users: PlayerEntityInternal[];
  };
}
