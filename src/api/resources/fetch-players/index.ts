import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { PlayersResourceParams, PlayersResourceResult } from './types';

class PlayersResource extends Resource {
  cacheKey = 'searchUser';
  static getUrl = ({ userToken, query }: PlayersResourceParams) =>
    `/api-apps/searchUser/${userToken}/${query}`;

  fetchPlayers = async ({ queryKey }: QueryFunctionContext) => {
    const [, { userToken, query }] = queryKey as [string, PlayersResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<PlayersResourceResult>({
      url: PlayersResource.getUrl({ userToken, query }),
      headers: {
        ...headers,
      },
    });
  };
}

export const playersResource = new PlayersResource();
