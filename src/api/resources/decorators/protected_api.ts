import { AuthEntities, authService, httpService } from '@services';

export function protectedApi(): any {
  // eslint-disable-next-line func-names
  return function (_: any, __: any, descriptor: PropertyDescriptor) {
    const original = descriptor.value;

    // eslint-disable-next-line no-param-reassign,func-names
    descriptor.value = async function (...args: any[]) {
      const token = await authService.getFromStorage(AuthEntities.bearer);

      if (token) {
        httpService.setAuthHeader(token);
      }

      return original.apply(this, args);
    };

    return descriptor;
  };
}
