import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { CourseDetailsResourceParams, CourseDetailsResourceResult } from './types';

class CourseDetailsResource extends Resource {
  cacheKey = 'course-details';
  static getCourseDetailsUrl = (golfCourse: number) => `/api-apps/course/${golfCourse}`;

  getCourseDetails = async ({ queryKey }: QueryFunctionContext) => {
    const [, params] = queryKey as [string, CourseDetailsResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<CourseDetailsResourceResult>({
      url: CourseDetailsResource.getCourseDetailsUrl(params.golfCourse),
      headers: {
        ...headers,
      },
    });
  };
}

export const courseDetailsResource = new CourseDetailsResource();
