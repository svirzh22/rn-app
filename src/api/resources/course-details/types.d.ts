export interface CourseDetailsResourceParams {
  golfCourse: number;
}

export interface CourseDetailsResourceResult {
  error: boolean;
  course: CourseEntity;
}
