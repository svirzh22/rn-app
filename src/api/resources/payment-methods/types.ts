export interface PaymentMethod {
  label: string;
  short_label: string;
}

export interface PaymentMethodsResult {
  error: boolean;
  paymentMethods: {
    ONLINE?: PaymentMethod;
    CASH?: PaymentMethod;
    CREDITCARD?: PaymentMethod;
  };
}

export interface PaymentMethodsParams {
  userToken: string;
}
