import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { PaymentMethodsParams, PaymentMethodsResult } from './types';

class PaymentMethodsResource extends Resource {
  cacheKey = 'paymentMethods';
  static getUrl = ({ userToken }: PaymentMethodsParams) => `/api-apps/paymentMethods/${userToken}`;

  fetchPaymentMethods = async ({ queryKey }: QueryFunctionContext) => {
    const headers = await getAuthHeader();
    const params = queryKey[1] as PaymentMethodsParams;

    return httpService.get<PaymentMethodsResult>({
      url: PaymentMethodsResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const paymentMethodsResource = new PaymentMethodsResource();
