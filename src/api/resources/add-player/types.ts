import { PlayerEntity } from '../types/player';

export interface AddPlayerResourceParams {
  firstName: string;
  lastName: string;
  handicap: string;
  birthDate?: string;
  mobilePhone?: string;
  userToken?: string;
}

export interface AddPlayerResourceResult {
  error: boolean;
  checkId: number;
  addedPlayer: PlayerEntity;
}
