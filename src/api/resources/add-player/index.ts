import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { AddPlayerResourceParams, AddPlayerResourceResult } from './types';

class AddPlayerResource extends Resource {
  static getUrl = () => '/api-apps/addPlayer';

  addPlayer = async (params: AddPlayerResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.post<AddPlayerResourceResult>({
      url: AddPlayerResource.getUrl(),
      headers: {
        ...headers,
      },
      data: params,
    });
  };
}

export const addPlayerResource = new AddPlayerResource();
