import { BookingEntity } from '../types/booking';

interface Extra {
  id: string | number;
  quantity: string | number;
}

interface Player {
  type: 'REGISTERED' | 'ADDED';
  id: number;
}

export interface BookingDetailsResourceParams {
  bookingId: string;
  userToken: string;
}

export interface BookingDetailsResourceResult {
  error: boolean;
  booking: BookingEntity;
}

export interface UpdateBookingDetailsParams {
  userToken: string;
  bookingId: string;

  timeSlot?: string; // yyyy-mm-dd hh:mm
  players?: Player[];
  extras?: Extra[];
}

export interface UpdateBookingDetailsResourceResult {
  error: boolean;
  booking: BookingEntity;
  reason?: string;
}
