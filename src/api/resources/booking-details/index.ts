import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import {
  BookingDetailsResourceParams,
  BookingDetailsResourceResult,
  UpdateBookingDetailsParams,
  UpdateBookingDetailsResourceResult,
} from './types';

class BookingDetailsResource extends Resource {
  cacheKey = 'booking-details';
  static getUrl = ({ userToken, bookingId }: BookingDetailsResourceParams) =>
    `/api-apps/user/${userToken}/booking/${bookingId}`;

  fetchBookingDetails = async ({ queryKey }: QueryFunctionContext) => {
    const [, params] = queryKey as [string, BookingDetailsResourceParams];
    const headers = await getAuthHeader();
    return httpService.get<BookingDetailsResourceResult>({
      url: BookingDetailsResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };

  updateBookingDetails = async (params: UpdateBookingDetailsParams) => {
    const headers = await getAuthHeader();
    return httpService.patch<UpdateBookingDetailsResourceResult>({
      url: BookingDetailsResource.getUrl({
        userToken: params.userToken,
        bookingId: params.bookingId,
      }),
      headers: {
        ...headers,
      },
      data: {
        timeSlot: params.timeSlot,
        players: params.players,
        extras: params.extras,
      },
    });
  };
}

export const bookingDetailsResource = new BookingDetailsResource();
