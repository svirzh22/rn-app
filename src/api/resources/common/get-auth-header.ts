import { AuthEntities, authService } from '@services';

export const getAuthHeader = async () => {
  const token = await authService.getFromStorage(AuthEntities.bearer);

  if (token) {
    return {
      Authorization: token,
    };
  }

  return undefined;
};
