import { ServerException } from '../exceptions';

export class Resource {
  handleError(res: any) {
    if (res && res.error === false) return res;
    // BE always responds with status code 200, even with error.
    throw new ServerException(res as any);
  }
}
