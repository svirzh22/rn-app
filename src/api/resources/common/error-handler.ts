import { Alert } from 'react-native';

export const errorHandler = (exc: ServerExceptionResponse) => {
  Alert.alert(exc.toString());
};
