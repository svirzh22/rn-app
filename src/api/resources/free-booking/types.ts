export interface FreeBookingResourceParams {
  checkId: number;
}

export interface FreeBookingResourceResult {
  error: boolean;
}
