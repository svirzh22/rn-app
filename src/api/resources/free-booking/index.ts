import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { FreeBookingResourceParams, FreeBookingResourceResult } from './types';

class FreeBookingResource extends Resource {
  static getUrl = () => '/api-apps/freeBooking';

  freeBooking = async (params: FreeBookingResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.post<FreeBookingResourceResult>({
      url: FreeBookingResource.getUrl(),
      headers: {
        ...headers,
      },
      data: params,
    });
  };
}

export const freeBookingResource = new FreeBookingResource();
