import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { BookingCalculatePriceResourceParams, BookingCalculatePriceResourceResult } from './types';

class BookingCalculatePriceResource extends Resource {
  cacheKey = 'booking-calculate-price';
  static getUrl = ({ date, memberType, parcours }: BookingCalculatePriceResourceParams) =>
    `/api-apps/getRealCost/${date}/${memberType}/${parcours}/`;

  calculatePrice = async ({ queryKey }: QueryFunctionContext) => {
    const params = queryKey[1] as BookingCalculatePriceResourceParams;
    const headers = await getAuthHeader();

    return httpService.get<BookingCalculatePriceResourceResult>({
      url: BookingCalculatePriceResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };

  calculatePriceMultiple = async (params: BookingCalculatePriceResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.get<BookingCalculatePriceResourceResult>({
      url: BookingCalculatePriceResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const bookingCalculatePriceResource = new BookingCalculatePriceResource();
