export interface BookingCalculatePriceResourceParams {
  date: string;
  memberType: number;
  parcours: number;
}

export interface BookingCalculatePriceResourceResult {
  tarif: number;
  sign: number | null;
  percent: number;
  normal_price: number;
}
