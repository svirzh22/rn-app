export enum TeesTypeEnum {
  champions = 1,
  back = 2,
  forward = 3,
}
export enum GenderEnum {
  woman,
  man,
}

export interface CourseHandicapResourceParams {
  handicap: number;
  golfCourse: number;
  teesType: TeesTypeEnum;
  gender: GenderEnum;
}

export interface CourseHandicapResourceResult {
  error: boolean;
  courseHandicap: number;
}
