import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { CourseHandicapResourceParams, CourseHandicapResourceResult } from './types';

export * from './types';

class CourseHandicapResource extends Resource {
  cacheKey = 'course-handicap';
  static getUrl = ({
    handicap,
    golfCourse,
    teesType,
    gender,
  }: CourseHandicapResourceParams) =>
    `/api-apps/courseHandicap/${handicap}/${golfCourse}/${teesType}/${gender}`;

  getCourseHandicap = async ({ queryKey }: QueryFunctionContext) => {
    const [, params] = queryKey as [string, CourseHandicapResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<CourseHandicapResourceResult>({
      url: CourseHandicapResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const courseHandicapResource = new CourseHandicapResource();
