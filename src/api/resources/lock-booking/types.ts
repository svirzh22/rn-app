export interface LockBookingResourceParams {
  golfCourse: number;
  timeSlot: string;
  nbPlayers: number;
}

export interface LockBookingResourceResult {
  error: boolean;
  checkId: number;
  bookable?: boolean;
  reason?: string;
  validityEnd?: any;
}
