import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { LockBookingResourceParams, LockBookingResourceResult } from './types';

class LockBookingResource extends Resource {
  static getUrl = () => '/api-apps/lockBooking';

  lockBooking = async (params: LockBookingResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.post<LockBookingResourceResult>({
      url: LockBookingResource.getUrl(),
      headers: {
        ...headers,
      },
      data: params,
    });
  };
}

export const lockBookingResource = new LockBookingResource();
