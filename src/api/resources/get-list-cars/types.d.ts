interface GetListCarsResourceParams {
  date: string;
}

type GetListCarsResourceResult = CarOption[];
