import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class GetListCarsResource extends Resource {
  cacheKey = 'get-car-list';
  static getUrl = (date: string) => `/api-apps/get-list-cars/?date=${date}`;

  getCars = async ({ queryKey }: QueryFunctionContext) => {
    const [, { date }] = queryKey as [string, GetListCarsResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<GetListCarsResourceResult>({
      url: GetListCarsResource.getUrl(date),
      headers: {
        ...headers,
      },
    });
  };
}

export const getListCarsResource = new GetListCarsResource();
