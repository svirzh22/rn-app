export class ServerException {
  value: ServerExceptionResponse | object;
  message: string;

  constructor(value?: ServerExceptionResponse) {
    if (value) {
      this.value = value;
      this.message = value.msg;
    } else {
      this.value = {};
      this.message = 'Unexpected error';
    }
    this.toString = () => this.message;
  }
}
