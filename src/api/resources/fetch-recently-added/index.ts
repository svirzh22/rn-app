import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { RecentlyAddedResourceParams, RecentlyAddedResourceResult } from './types';

class RecentlyAddedResource extends Resource {
  cacheKey = 'recentPlayers';
  static getUrl = ({ userToken }: RecentlyAddedResourceParams) =>
    `/api-apps/user/${userToken}/recentPlayers`;

  fetchRecentlyAdded = async ({ queryKey }: QueryFunctionContext) => {
    const [, { userToken }] = queryKey as [string, RecentlyAddedResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<RecentlyAddedResourceResult>({
      url: RecentlyAddedResource.getUrl({ userToken }),
      headers: {
        ...headers,
      },
    });
  };
}

export const recentlyAddedResource = new RecentlyAddedResource();
