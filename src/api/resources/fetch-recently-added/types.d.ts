import { PlayerEntityInternal } from '../types/player';

interface RecentlyAddedResourceParams {
  userToken: string;
}

interface RecentlyAddedResourceResult {
  error: boolean;
  recentlyAdded: PlayerEntityInternal[];
}
