import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { BookingConfirmResourceParams, BookingConfirmResourceResult } from './types';

class BookingConfirmResource extends Resource {
  static getUrl = () => '/api-apps/booking/confirm';

  confirmBooking = async (params: BookingConfirmResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.post<BookingConfirmResourceResult>({
      url: BookingConfirmResource.getUrl(),
      headers: {
        ...headers,
      },
      data: {
        ...params,
      },
    });
  };
}

export const bookingConfirmResource = new BookingConfirmResource();
