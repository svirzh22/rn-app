export interface SaferPayDetails {
  frameUrl: string;
  requestId: string;
  transactionToken: string;
  successUrl: string;
  failUrl: string;
  abortUrl: string;
}

export interface BookingConfirmResourceParams {
  userToken: string;
  booking: number;
  paymentMethod?: string;
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
}

export interface BookingConfirmResourceResult {
  error: boolean;
  booking: number;
  totalAmount: number;
  confirmationState: string;
  saferPay: SaferPayDetails;
}
