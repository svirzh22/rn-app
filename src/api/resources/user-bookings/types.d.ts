import { BookingEntity } from '../types/booking';

export interface UserBookingsResult {
  error: boolean;
  bookings: BookingEntity[];
}

export interface UserBookingsParams {
  userToken: string;
}
