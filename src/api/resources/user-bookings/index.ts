import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { UserBookingsParams, UserBookingsResult } from './types';

class UserBookingsResource extends Resource {
  cacheKey = 'user-bookings';
  static getUrl = ({ userToken }: UserBookingsParams) => `/api-apps/user/${userToken}/bookings`;

  fetchUserBookings = async ({ queryKey }: QueryFunctionContext) => {
    const params = queryKey[1] as UserBookingsParams;
    const headers = await getAuthHeader();

    return httpService.get<UserBookingsResult>({
      url: UserBookingsResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const userBookingsResource = new UserBookingsResource();
