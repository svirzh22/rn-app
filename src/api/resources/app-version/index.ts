import { httpService } from '@services';
// import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { AppVersionResult } from './types';

class AppVersionResource extends Resource {
  cacheKey = 'appVersion';
  static getUrl = () => '/api-apps/appVersion';

  fetchAppVersion = async () => httpService.get<AppVersionResult>({
    url: AppVersionResource.getUrl(),
  });
}

export const appVersionResource = new AppVersionResource();
