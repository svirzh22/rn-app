export interface AppVersionResult {
  iosVersion: string;
  androidVersion: string;
}
