export interface PaymentStatusResult {
  error: boolean;
  transactionStatus: {
    Transaction: {
      Status: string;
    };
  };
}

export interface PaymentStatusParams {
  transactionToken: string;
}
