import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { PaymentStatusParams, PaymentStatusResult } from './types';

class PaymentStatusResource extends Resource {
  cacheKey = 'paymentStatus';
  static getUrl = ({ transactionToken }: PaymentStatusParams) =>
    `/api-apps/transaction/${transactionToken}`;

  fetchPaymentStatus = async ({ queryKey }: QueryFunctionContext) => {
    const headers = await getAuthHeader();
    const params = queryKey[1] as PaymentStatusParams;

    return httpService.get<PaymentStatusResult>({
      url: PaymentStatusResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const paymentStatusResource = new PaymentStatusResource();
