interface UserCancelBookingResourceParams {
  userToken: string;
  bookingId: number;
}

interface UserCancelBookingResourceResult {
  error: boolean;
}
