import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class UserCancelBookingResource extends Resource {
  static getUrl = () => '/api-apps/booking';

  cancelBooking = async (params: UserCancelBookingResourceParams) => {
    // const [, { userToken, reservationId }] = queryKey as [string, UserCancelBookingResourceParams];
    const headers = await getAuthHeader();

    return httpService.delete<UserCancelBookingResourceResult>({
      url: UserCancelBookingResource.getUrl(),
      headers: {
        ...headers,
      },
      data: {
        ...params,
      },
    });
  };
}

export const userCancelBookingResource = new UserCancelBookingResource();
