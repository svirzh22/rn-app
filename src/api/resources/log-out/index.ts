import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class LogOutResource extends Resource {
  static getUrl = (userToken: string) => `/api-apps/logout/${userToken}`;

  logOut = async ({ queryKey }: QueryFunctionContext) => {
    const [, { userToken }] = queryKey as [string, LogOutResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<LogOutResourceResult>({
      url: LogOutResource.getUrl(userToken),
      headers: {
        ...headers,
      },
    });
  };
}

export const logOutResource = new LogOutResource();
