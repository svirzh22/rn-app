interface LogOutResourceParams {
  userToken: string;
}

interface LogOutResourceResult {
  error: boolean;
}
