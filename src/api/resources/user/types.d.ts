interface UserInfosResourceParams {
  userToken: string;
}

interface UserInfosResourceResult {
  error: boolean;
  user: UserEntity;
}

interface CreateUserParams {
  firstname: string;
  lastname: string;
  handicap: string;
  // (yyyy-mm-dd)
  birthDate: string;
  email: string;
  mobilePhone: string;
}

interface CreateUserResult {
  error: boolean;
  user: UserEntity;
}

interface UpdateUserParams {
  userToken: string;
  picture?: string;
  handicap?: string;
  language?: string;
  private?: boolean;
  firstname?: string;
  lastname?: string;
  email?: string;
  asg?: string;
}

interface UpdateUserResult {
  error: boolean;
  user: UserEntity;
}
