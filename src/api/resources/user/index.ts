import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class UserResource extends Resource {
  cacheKey = 'user';
  static getUrl = (userToken?: string) => (userToken ? `/api-apps/user/${userToken}` : '/api-apps/user');

  // create
  createUser = async (params: CreateUserParams) => {
    const headers = await getAuthHeader();

    return httpService.post<CreateUserResult>({
      url: UserResource.getUrl(),
      headers: {
        ...headers,
      },
      data: params,
    });
  };

  // read
  getUserInfo = async ({ queryKey }: QueryFunctionContext) => {
    const [, { userToken }] = queryKey as [string, UserInfosResourceParams];
    const headers = await getAuthHeader();

    return httpService.get<UserInfosResourceResult>({
      url: UserResource.getUrl(userToken),
      headers: {
        ...headers,
      },
    });
  };

  // update
  updateUser = async (params: UpdateUserParams) => {
    const headers = await getAuthHeader();

    return httpService.patch<UpdateUserResult>({
      url: UserResource.getUrl(),
      headers: {
        ...headers,
      },
      data: params,
    });
  };
}

export const userResource = new UserResource();
