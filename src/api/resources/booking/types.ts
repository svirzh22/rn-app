export interface BookingResourceParams {
  checkId: number;
  golfCourse: number;
  timeSlot: string;
  players: { id: number | null | string }[];
  byPhone?: boolean;
  version: string;
  insurance: boolean;
  userToken: string;
}

export interface BookingResourceResult {
  error: boolean;
  checkId: number;
  bookable?: boolean;
  reason?: string;
  booking?: {
    id: number;
    sessionId: string;
    online: null;
    app: null;
    sourceApp: string;
    member: number;
    timeSlot: string;
    golfCourse: number;
    nbPlayers: number;
    players: {1: any; 2: any; 3: any; 4: any};
    paymentState: number;
    totalCost: string;
    reduceCost: null;
    smallCar: {quantity: number; totalCost: number; paymentState: number; billingId: null; admin: null};
    electricCar: {quantity: number; totalCost: number; paymentState: number; billingId: null; admin: null};
    caddy: {quantity: number; totalCost: number; paymentState: number; billingId: null; admin: null};
    insurance: {cost: number; paymentState: number; billingId: null; admin: null};
    byPhone: number;
    createdAt: string;
    updatedAt: null;
  };
}
