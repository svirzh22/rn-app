import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { BookingResourceParams, BookingResourceResult } from './types';

class BookingResource extends Resource {
  static getUrl = () => '/api-apps/booking';

  createBooking = async (params: BookingResourceParams) => {
    const headers = await getAuthHeader();

    return httpService.post<BookingResourceResult>({
      url: BookingResource.getUrl(),
      headers: {
        ...headers,
      },
      data: {
        byPhone: false,
        ...params,
      },
    });
  };
}

export const bookingResource = new BookingResource();
