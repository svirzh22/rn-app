import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { TimeSlotsParams, TimeSlotsResult } from './types';

class TimeSlotsResource extends Resource {
  cacheKey = 'timeslots';
  static getUrl = ({ course, date, memberType, numberOfPlayers }: TimeSlotsParams) =>
    `/api-apps/timeslots/${course}/${date}/${memberType}/${numberOfPlayers || 0}`;

  fetchTimeSlots = async ({ queryKey }: QueryFunctionContext) => {
    const params = queryKey[1] as TimeSlotsParams;
    const headers = await getAuthHeader();

    return httpService.get<TimeSlotsResult>({
      url: TimeSlotsResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const timeSlotsResource = new TimeSlotsResource();
