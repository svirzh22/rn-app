import { PlayerEntity } from '../types/player';

interface Event {
  event: string;
  display: boolean;
}

export interface TimeSlotEntity {
  start: string;
  availability: number;
  price: string | number;
  players: {
    [key: number]: PlayerEntity;
  };
  event: Event | null;
}

export interface TimeSlotsResult {
  error: boolean;
  bookable: boolean;
  timeSlots: TimeSlotEntity[];
}

export interface TimeSlotsParams {
  course: string;
  date: string;
  memberType: number;
  numberOfPlayers: number;
}
