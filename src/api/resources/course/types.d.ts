interface CoursesResourceParams {}

type CoursesResourceResult = {
  error: boolean;
  courses: CourseEntity[];
};
