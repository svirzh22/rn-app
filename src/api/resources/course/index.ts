import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';

class CourseResource extends Resource {
  cacheKey = 'course';
  static getCoursesUrl = () => '/api-apps/courses';

  getCourses = async () => {
    const headers = await getAuthHeader();

    return httpService.get<CoursesResourceResult>({
      url: CourseResource.getCoursesUrl(),
      headers: {
        ...headers,
      },
    });
  };
}

export const courseResource = new CourseResource();
