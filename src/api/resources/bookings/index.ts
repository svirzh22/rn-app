import { QueryFunctionContext } from 'react-query';

import { httpService } from '@services';
import { getAuthHeader } from '../common';
import { Resource } from '../common/resource';
import { BookingsParams, BookingsResult } from './types';

class BookingsResource extends Resource {
  cacheKey = 'bookings';
  static getUrl = ({ date }: BookingsParams) => `/api-apps/getRealCost/${date}`;

  fetchBookings = async ({ queryKey }: QueryFunctionContext) => {
    const params = queryKey[1] as BookingsParams;
    const headers = await getAuthHeader();

    return httpService.get<BookingsResult>({
      url: BookingsResource.getUrl(params),
      headers: {
        ...headers,
      },
    });
  };
}

export const bookingsResource = new BookingsResource();
