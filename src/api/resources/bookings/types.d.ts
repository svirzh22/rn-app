import { BookingEntity } from '../types/booking';

export interface BookingsResult {
  data: BookingEntity[];
}

export interface BookingsParams {
  date: string;
}
