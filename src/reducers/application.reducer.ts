import { handleActions } from 'redux-actions';

import { RESET_STORE, SET_APP_LOADING, SET_SENT_OTP_TIME } from '../actions';

export interface InitialState {
  appLoading: boolean;
  sentOtpTime: null | number;
}

export const initialState: InitialState = {
  appLoading: false,
  sentOtpTime: null,
};

export const reducer = handleActions<InitialState, any>(
  {
    [SET_APP_LOADING]: (state, { payload }) => ({
      ...state,
      appLoading: payload,
    }),
    [SET_SENT_OTP_TIME]: (state, { payload }) => ({
      ...state,
      sentOtpTime: payload,
    }),
    [RESET_STORE]: () => initialState,
  },
  initialState,
);
