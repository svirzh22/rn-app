import { handleActions } from 'redux-actions';

import { RESET_LOCK_INFO, RESET_STORE, SET_LOCK_INFO } from '../actions';

export interface InitialState {
  checkId?: number;
  timeSlot?: string;
}

export const initialState: InitialState = {
};

export const reducer = handleActions<InitialState, any>(
  {
    [SET_LOCK_INFO]: (state, { payload }) => ({
      ...state,
      checkId: payload.checkId,
      timeSlot: payload.timeSlot,
    }),
    [RESET_LOCK_INFO]: () => initialState,
    [RESET_STORE]: () => initialState,
  },
  initialState,
);
