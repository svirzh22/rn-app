import { Action, handleActions } from 'redux-actions';

import * as types from '../actions/settings/action-types';

export interface InitialState {
  locale: string;
}

export const initialState: InitialState = {
  locale: 'fr',
};

export const reducer = handleActions<InitialState, any>(
  {
    [types.SET_LOCALE]: (state, { payload }: Action<string>) => ({
      ...state,
      locale: payload,
    }),
  },
  initialState,
);
