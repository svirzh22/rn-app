import { combineReducers, Reducer } from 'redux';
import * as authReducer from './auth.reducer';
import * as settingsReducer from './settings.reducer';
import * as applicationReducer from './application.reducer';
import * as reservationReducer from './reservation.reducer';

export const initialState = {
  authReducer: authReducer.initialState,
  settingsReducer: settingsReducer.initialState,
  applicationReducer: applicationReducer.initialState,
  reservationReducer: reservationReducer.initialState,
};

export interface InitialState {
  authReducer: authReducer.InitialState;
  settingsReducer: settingsReducer.InitialState;
  applicationReducer: applicationReducer.InitialState;
  reservationReducer: reservationReducer.InitialState;
  _persist?: {
    version: number;
    rehydrated: boolean;
  };
}

export const rootReducer: Reducer<InitialState, any> = combineReducers({
  authReducer: authReducer.reducer,
  settingsReducer: settingsReducer.reducer,
  applicationReducer: applicationReducer.reducer,
  reservationReducer: reservationReducer.reducer,
});
