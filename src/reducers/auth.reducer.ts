import { Action, handleActions } from 'redux-actions';

import * as types from '../actions/auth/action-types';

export interface InitialState {
  member: OwnUserEntity;
}

export const initialState: InitialState = {
  // @TODO Fix any
  member: {} as any,
};

export const reducer = handleActions<InitialState, any>(
  {
    [types.SIGN_IN]: (state) => ({
      ...state,
    }),
    [types.SET_MEMBER]: (state, { payload }: Action<OwnUserEntity>) => ({
      ...state,
      member: payload,
    }),
    [types.UPDATE_PROFILE]: (state, { payload }: Action<OwnUserEntity>) => ({
      ...state,
      member: {
        ...state.member,
        ...payload,
      },
    }),
    [types.RESET_STORE]: () => initialState,
  },
  initialState,
);
