import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  avatarLoading: {
    position: 'absolute',
    opacity: 0,
    zIndex: -1,
  },
});
