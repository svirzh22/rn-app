import React, { FC, useState, useCallback } from 'react';

import { FlexView, Image } from '@components';
import { images } from '@assets/images';
import { styles } from './styles';

interface AvatarProps {
  image?: string | null;
  size?: number;
}

export const Avatar: FC<AvatarProps> = ({ image, size = 48 }) => {
  const [isLoading, setLoading] = useState<boolean>(false);
  const style = {
    height: size,
    width: size,
    borderRadius: size / 2,
  };

  const onProfileImageLoadStart = useCallback(() => setLoading(true), []);
  const onProfileImageLoad = useCallback(() => setLoading(false), []);
  const showDefaultImage = isLoading || !image;

  return (
    <FlexView>
      {showDefaultImage && <Image source={images.userAvatar} style={style} />}
      <Image
        source={{ uri: image || '' }}
        onLoadStart={onProfileImageLoadStart}
        onLoad={onProfileImageLoad}
        style={[showDefaultImage ? styles.avatarLoading : style]}
      />
    </FlexView>
  );
};
