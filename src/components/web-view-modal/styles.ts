import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { COLORS, IS_IOS, SCREEN_HEIGHT, SCREEN_WIDTH } from '@constants';

export const styles = StyleSheet.create({
  content: {
    backgroundColor: COLORS.WHITE,
    height: SCREEN_HEIGHT,
    width: SCREEN_WIDTH,
  },
  header: {
    zIndex: 10,
    backgroundColor: COLORS.WHITE,
    justifyContent: 'center',
    height: 44,
    paddingHorizontal: 8,
    alignItems: 'flex-end',
    marginTop: IS_IOS ? getStatusBarHeight() : 0,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: COLORS.GREY[700],
    elevation: 3,
  },
  closeButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: SCREEN_HEIGHT,
    width: SCREEN_WIDTH,
  },
  loaderContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
});
