import React, { FC, useRef } from 'react';
import { View, TouchableOpacity, Modal } from 'react-native';

import { COLORS, SPACING } from '@constants';
import { styles } from './styles';
import { Gutter } from '../gutter';
import { Icon } from '../icon';
import { Text } from '../text';
import { FlexView } from '../flex-view';
import { ActivityIndicator } from '../activity-indicator';
import { WebViewInternal } from '../web-view/component';

interface WebViewModalProps {
  isVisible: boolean;
  onClose: () => void;
  uri: string;
}

export const WebViewModal: FC<WebViewModalProps> = ({ isVisible, onClose, uri }) => {
  const ref = useRef();
  return (
    <Modal
      visible={isVisible}
      onRequestClose={onClose}
    // @ts-ignore
      onBackButtonPress={onClose}
      animationType="slide"
      transparent
    >
      <View style={styles.content}>
        <View style={styles.header}>
          <TouchableOpacity style={styles.closeButton} onPress={onClose}>
            <Text t="common.close" />
            <Gutter amount={SPACING.XS} />
            <Icon color={COLORS.GREY[700]} name="close" size={14} />
          </TouchableOpacity>
        </View>

        {uri ? (
          <WebViewInternal
            ref={ref}
            // @ts-ignore
            useWebKit
            source={{ uri }}
            renderLoading={() => (
              <FlexView style={styles.loaderContainer}>
                <ActivityIndicator size="large" />
              </FlexView>
            )}
            startInLoadingState
          />
        ) : null}
      </View>
    </Modal>
  );
};
