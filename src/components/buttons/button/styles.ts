import { StyleSheet, ViewStyle } from 'react-native';
import { COLORS } from '@constants';
import { opacity } from '@helpers';

export const BUTTON_BORDER_RADIUS = 8;

const button: ViewStyle = {
  height: 50,
  borderRadius: BUTTON_BORDER_RADIUS,
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
};

export const styles = StyleSheet.create({
  buttonPrimary: button,
  buttonOutline: {
    ...button,
    borderWidth: 1,
    borderColor: opacity(COLORS.GREY[600], 0.6),
  },
  buttonFlat: {
    ...button,
    borderRadius: 0,
  },
  disabledButtonText: {
    opacity: 0.2,
  },
});
