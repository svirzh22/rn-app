import React, { FC } from 'react';
import { ViewStyle } from 'react-native';

import { COLORS } from '@constants';
import { Text, Touchable, ActivityIndicator } from '@components';
import { BUTTON_BORDER_RADIUS, styles } from './styles';
import { ButtonProps } from './types';

export const Button: FC<ButtonProps> = ({
  onPress,
  loading,
  disabled,
  t,
  label,
  color = COLORS.BLACK,
  bgColor = COLORS.PRIMARY[500],
  style,
  textStyle,
  weight,
  loaderColor = color,
  preset = 'primary',
  size = 'L',
  ...rest
}) => {
  const buttonPresets = {
    primary: styles.buttonPrimary,
    outline: styles.buttonOutline,
    flat: styles.buttonFlat,
  };
  const buttonStyle = [buttonPresets[preset], preset !== 'outline' && { backgroundColor: bgColor }, style];

  return (
    <Touchable
      borderRadius={preset === 'flat' ? 0 : BUTTON_BORDER_RADIUS}
      onPress={onPress}
      disabled={!!loading || !!disabled}
      style={buttonStyle as ViewStyle}
      {...rest}
    >
      {loading ? (
        <ActivityIndicator color={loaderColor} size="small" />
      ) : (
        <Text
          color={color}
          align="center"
          t={t}
          label={label}
          weight={weight}
          size={size}
          style={[textStyle, disabled && styles.disabledButtonText]}
        />
      )}
    </Touchable>
  );
};
