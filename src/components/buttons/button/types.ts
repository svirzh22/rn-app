import { ViewStyle } from 'react-native';
import { FONT_SIZES, WeightType } from '@constants';

type ButtonPresets = 'primary' | 'outline' | 'flat';

export interface ButtonProps {
  onPress: () => void;
  loading?: boolean;
  disabled?: boolean;
  t?: string;
  label?: string;
  color?: string;
  bgColor?: string;
  style?: ViewStyle;
  textStyle?: ViewStyle;
  weight?: WeightType;
  loaderColor?: string;
  preset?: ButtonPresets;
  size?: number | keyof typeof FONT_SIZES;
}
