interface AccessibilityState {
  selected: boolean;
}

interface TabBarButtonProps {
  onPress?: () => void;
  to?: string;
  accessibilityState?: AccessibilityState;
}
