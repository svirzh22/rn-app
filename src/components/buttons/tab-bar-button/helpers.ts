import replace from 'lodash/replace';
import { IconNameType } from '@components';
import lowerFirst from 'lodash/lowerFirst';

export const getScreenName = (to: string) => `Tabs/${replace(to, '/', '').split('?')[0].split('/')[1]}`;

export const getLabelByScreenName = (to: string) => `${lowerFirst(getScreenName(to).split('/')[1])}.tabLabel`;

interface ScreenToIconConfig {
  [field: string]: IconNameType;
}

export const getIconByScreenName = (to: string): IconNameType => {
  const screenName = getScreenName(to);
  const config: ScreenToIconConfig = {
    'Tabs/Home': 'home',
    'Tabs/TeeTime': 'teeetime',
    'Tabs/Tournaments': 'tournaments',
    'Tabs/Academy': 'academy',
    'Tabs/Profile': 'profile',
  };

  return config[screenName];
};
