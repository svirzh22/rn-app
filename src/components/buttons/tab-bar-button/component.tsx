import React, { FC } from 'react';
import { IconButton, Gutter, Text } from '@components';
import noop from 'lodash/noop';

import { COLORS, SPACING } from '@constants';
import { getIconByScreenName, getLabelByScreenName } from './helpers';
import { styles } from './styles';

export const TabBarButton: FC<TabBarButtonProps> = ({ onPress = noop, to = '', accessibilityState = {} }) => {
  const label = getLabelByScreenName(to);
  const iconName = getIconByScreenName(to);
  const { selected } = accessibilityState;

  return (
    <IconButton
      onPress={onPress}
      name={iconName}
      containerStyle={styles.container}
      size={17}
      color={selected ? COLORS.SECONDARY[500] : COLORS.BLACK}
      activeOpacity={1}
      rippleRadius={42}
    >
      <Gutter amount={SPACING.XXS} isVertical />
      <Text t={label} size={12} numberOfLines={1} color={selected ? COLORS.SECONDARY[500] : COLORS.BLACK} />
    </IconButton>
  );
};
