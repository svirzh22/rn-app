import { StyleSheet } from 'react-native';
import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  linkButton: {
    color: COLORS.SECONDARY[500],
  },
  disabled: {
    opacity: 0.5,
  },
});
