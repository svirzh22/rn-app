import React, { FC, useState } from 'react';
import { LayoutChangeEvent, StyleProp, TextStyle, TouchableOpacity, ViewStyle } from 'react-native';

import { createHitSlop } from '@helpers';
import { COLORS } from '@constants';
import { Text, TextProps } from '../../text';
import { styles } from './styles';
import { FlexView } from '../../flex-view';
import { ActivityIndicator } from '../../activity-indicator';

interface LinkButtonProps extends TextProps {
  onPress: () => void;
  containerStyle?: StyleProp<ViewStyle>;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  isLoading?: boolean;
}

export const LinkButton: FC<LinkButtonProps> = ({
  style,
  textStyle,
  disabled,
  containerStyle,
  onPress,
  isLoading = false,
  ...props
}) => {
  const [textWidth, setTextWidth] = useState(0);
  const handleLayout = (event: LayoutChangeEvent) => {
    setTextWidth(event.nativeEvent?.layout?.width);
  };

  return (
    <FlexView alignItems="baseline" style={containerStyle}>
      <TouchableOpacity
        onPress={onPress}
        style={[style, disabled && styles.disabled]}
        disabled={disabled || isLoading}
        hitSlop={createHitSlop(10)}
      >
        {isLoading ? (
          <ActivityIndicator
            color={COLORS.SECONDARY[500]}
            style={{
              width: textWidth,
            }}
          />
        ) : (
          <Text onLayout={handleLayout} style={[styles.linkButton, textStyle]} {...props} />
        )}
      </TouchableOpacity>
    </FlexView>
  );
};
