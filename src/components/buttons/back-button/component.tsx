import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';

import { IconButton, IconButtonProps, IconNameType } from '@components';
import { createHitSlop } from '@helpers';
import { styles } from './styles';

// @ts-ignore
export interface BackButtonProps extends IconButtonProps {
  onPress?: () => void;
  name?: IconNameType;
}

export const BackButton: FC<BackButtonProps> = ({
  onPress,
  name = 'back',
  size = 16,
  rippleRadius = 18,
  ...props
}) => {
  const navigation = useNavigation();
  const onButtonPress = () => {
    if (onPress) {
      return onPress();
    }
    return navigation.goBack();
  };

  return (
    <IconButton
      size={size}
      onPress={onButtonPress}
      name={name}
      hitSlop={createHitSlop(15, 50, 15, 15)}
      rippleRadius={rippleRadius}
      containerStyle={styles.container}
      {...props}
    />
  );
};
