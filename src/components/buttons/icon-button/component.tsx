import React, { FC } from 'react';
import { BorderlessButton } from 'react-native-gesture-handler';
import { TouchableNativeFeedback, ViewStyle } from 'react-native';

import { createHitSlop, opacity } from '@helpers';
import { COLORS, IS_IOS } from '@constants';
import { FlexView, Icon, IconProps, ActivityIndicator } from '@components';

interface HitSlop {
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

export interface IconButtonProps extends IconProps {
  onPress: () => void;
  containerStyle?: ViewStyle;
  isLoading?: boolean;
  isDisabled?: boolean;
  hitSlop?: number | HitSlop;
  activeOpacity?: number;
  rippleRadius?: number;
  rippleColor?: string;
}

export const IconButton: FC<IconButtonProps> = ({
  name,
  size = 12,
  color,
  onPress,
  hitSlop = 10,
  containerStyle = {},
  isLoading,
  isDisabled = false,
  children,
  activeOpacity = 0.5,
  rippleRadius = size * 0.9,
  rippleColor = opacity(COLORS.BLACK[900], 0.25),
  ...props
}) => {
  if (isLoading) {
    return <ActivityIndicator style={[{ width: size, height: size }, containerStyle]} size="small" />;
  }

  if (IS_IOS) {
    return (
      <BorderlessButton
        hitSlop={typeof hitSlop === 'number' ? createHitSlop(hitSlop) : hitSlop}
        style={[{ width: size, height: size }, containerStyle]}
        onPress={onPress}
        enabled={!isDisabled}
        activeOpacity={activeOpacity}
      >
        <Icon name={name} size={size} color={color} {...props} />
        {children}
      </BorderlessButton>
    );
  }

  return (
    <TouchableNativeFeedback
      background={TouchableNativeFeedback.Ripple(rippleColor, true, rippleRadius)}
      onPress={onPress}
      useForeground={false}
      disabled={isDisabled}
      hitSlop={typeof hitSlop === 'number' ? createHitSlop(hitSlop) : hitSlop}
    >
      <FlexView style={[{ width: size, height: size }, containerStyle]}>
        <Icon name={name} size={size} color={color} {...props} />
        {children}
      </FlexView>
    </TouchableNativeFeedback>
  );
};
