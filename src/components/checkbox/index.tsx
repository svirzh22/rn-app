import React from 'react';
import Tooltip from 'rn-tooltip';
import { View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { Icon, Text, FlexView, Touchable, IconButton } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';
import { CheckboxProps } from './types';

export const Checkbox: React.FC<CheckboxProps> = ({
  label,
  t,
  forwardRef,
  value,
  onPress,
  info,
  onPressInfo,
  isDisabled,
}) => (
  <FlexView flexDirection="row" alignItems="center">
    <Touchable onPress={onPress} style={styles.box} disabled={isDisabled}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#ffffff', '#F9FAFB']}
        style={{ flex: 1 }}
      />
      {Boolean(value) && <Icon name="tick" color="#F5C70A" style={styles.tick} />}
    </Touchable>
    <Text t={t} label={label} style={styles.label} />

    {onPressInfo && (
      <IconButton
        name="info-bullet"
        size={16}
        color={COLORS.GREY[600]}
        containerStyle={styles.infoButton}
        onPress={onPressInfo}
      />
    )}

    {info && (
      <View ref={forwardRef}>
        <Tooltip
          popover={<Text label={info} size="S" lineHeight={18} />}
          withOverlay={false}
          containerStyle={styles.tooltip}
          withPointer
          pointerColor={COLORS.PRIMARY[500]}
          actionType="press"
        >
          <Icon name="info-bullet" size={16} color={COLORS.GREY[600]} style={styles.infoButton} />
        </Tooltip>
      </View>
    )}
  </FlexView>
);
