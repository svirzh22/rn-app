import { StyleProp, ViewStyle } from 'react-native';

interface CheckboxProps {
  label?: string;
  t?: string;
  value: boolean;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  onPressInfo?: () => void;
  forwardRef?: RefObject<View>;
  info?: string;
  onPressInfo?: () => void;
  isDisabled?: boolean;
}
