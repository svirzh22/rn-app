import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'row',
  },
  box: {
    width: 24,
    height: 24,
    borderRadius: 4,
    borderColor: '#E5E5EA',
    borderWidth: 1,
  },
  tick: {
    position: 'absolute',
    top: 5,
    left: 5,
  },
  label: {
    marginLeft: 16,
  },
  infoButton: {
    marginLeft: SPACING.XS,
  },
  tooltip: {
    backgroundColor: COLORS.PRIMARY[500],
    height: undefined,
    width: undefined,
    padding: 6,
    marginRight: 120,
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.3,
    shadowRadius: 8,
  },
});
