import { TranslateOptions } from 'i18n-js';
import React from 'react';
import { Switch as SwitchNative } from 'react-native';

import { translate } from '@helpers';
import { COLORS } from '@constants';
import { FlexView } from '../flex-view';
import { Text } from '../text';
import { styles } from './styles';

interface SwitchProps {
  t?: string;
  label?: string;
  value: boolean;
  disabled?: boolean;
  translateOptions?: TranslateOptions;
  onChange: (value: boolean) => void;
}

export const Switch: React.FC<SwitchProps> = ({
  label,
  t,
  translateOptions,
  disabled,
  value,
  onChange,
}) => {
  const i18nText = t ? translate(t, translateOptions) : label;
  return (
    <FlexView
      flexDirection="row"
    >
      <FlexView flex={2} justifyContent="center">
        <Text label={i18nText} style={[styles.text]} />
      </FlexView>
      <FlexView
        flex={1}
        justifyContent="center"
        alignItems="flex-end"
      >
        <SwitchNative
          disabled={disabled}
          value={value}
          thumbColor={COLORS.WHITE}
          trackColor={{
            true: COLORS.PRIMARY[500],
            false: COLORS.GREY[500],
          }}
          onValueChange={onChange}
        />
      </FlexView>
    </FlexView>
  );
};
