import React, { FC } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '@assets/icomoon/selection.json';
import { TextProps } from 'react-native';
import icons from '@assets/icomoon/icons.json';

const IcoMoonIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export type IconNameType = keyof typeof icons;

export interface IconProps extends TextProps {
  size?: number;
  name: IconNameType;
  color?: string;
}
// @ts-ignore
export const Icon: FC<IconProps> = ({ ...props }) => <IcoMoonIcon {...props} />;
