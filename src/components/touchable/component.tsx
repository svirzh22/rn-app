import React, { FC, ReactNode } from 'react';
import { RectButton } from 'react-native-gesture-handler';
import { StyleProp, TouchableNativeFeedback, TouchableWithoutFeedbackProps, ViewStyle } from 'react-native';

import { COLORS, IS_IOS } from '@constants';
import { opacity } from '@helpers';
import { FlexView } from '../flex-view';
import { styles } from './styles';

interface TouchableProps extends TouchableWithoutFeedbackProps {
  borderRadius?: number;
  style?: StyleProp<ViewStyle>;
  backgroundColor?: string;
  disabled?: boolean;
  onPress: () => void;
  children?: ReactNode;
}

export const Touchable: FC<TouchableProps> = ({
  children,
  style = {},
  borderRadius,
  backgroundColor,
  disabled = false,
  onPress,
  ...props
}) => {
  if (IS_IOS) {
    return (
      <RectButton {...props} style={[style]} enabled={!disabled} onPress={onPress}>
        {children}
      </RectButton>
    );
  }

  if (borderRadius) {
    return (
      <FlexView style={[{ overflow: 'hidden' }, style, styles.noPadding]}>
        <TouchableNativeFeedback
          {...props}
          background={TouchableNativeFeedback.Ripple(opacity(COLORS.BLACK[900], 0.25), false)}
          useForeground={false}
          disabled={disabled}
          onPress={onPress}
        >
          <FlexView style={[style, styles.noMargin]}>{children}</FlexView>
        </TouchableNativeFeedback>
      </FlexView>
    );
  }

  return (
    <TouchableNativeFeedback
      {...props}
      background={TouchableNativeFeedback.Ripple(opacity(COLORS.BLACK[900], 0.25), false)}
      useForeground={false}
      disabled={disabled}
      onPress={onPress}
    >
      <FlexView style={style}>{children}</FlexView>
    </TouchableNativeFeedback>
  );
};
