import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  noMargin: {
    marginTop: 0,
    marginBottom: 0,
    margin: 0,
    marginVertical: 0,
    marginHorizontal: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  noPadding: {
    paddingTop: 0,
    paddingBottom: 0,
    padding: 0,
    paddingVertical: 0,
    paddingHorizontal: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
});
