import React, { RefObject } from 'react';
import { StyleProp, ViewStyle, View } from 'react-native';

import { APP_CURRENCY, COLORS, SPACING } from '@constants';
import { FlexView, FlexViewProps } from '@components/flex-view';
import { SpinnerInput } from '@components/inputs';
import { Gutter } from '@components/gutter';
import { Text } from '@components/text';
import { IconButton } from '@components/buttons';
import { styles } from './styles';

export interface OptionItemProps extends FlexViewProps {
  label?: string;
  t?: string;
  cost: number;
  value: number;
  maxValue: number;
  style?: StyleProp<ViewStyle>;
  onPressInfo?: () => void;
  forwardRef?: RefObject<View>;
  name: string;
  onChange: (value: number | string) => void;
}

export const OptionItem: React.FC<OptionItemProps> = ({
  cost,
  value,
  label,
  t,
  forwardRef,
  name,
  maxValue,
  onPressInfo,
  onChange,
  ...props
}) => (
  <FlexView flexDirection="row" alignItems="center" justifyContent="space-between" {...props}>
    <FlexView flexDirection="row" alignItems="center" flex={0.8}>
      <SpinnerInput value={value} maxValue={maxValue} onChange={onChange} />
      <Gutter amount={SPACING.S} />
      <FlexView flex={1} flexDirection="row" alignItems="center">
        <Text t={t} label={label} />
        {onPressInfo && (
          <View ref={forwardRef}>
            <IconButton
              name="info-bullet"
              size={16}
              color={COLORS.GREY[600]}
              onPress={onPressInfo}
              containerStyle={styles.infoButton}
            />
          </View>
        )}
      </FlexView>
    </FlexView>

    <FlexView flex={0.2} alignItems="flex-end">
      <Text label={`${cost * value || 0} ${APP_CURRENCY}`} color={COLORS.SECONDARY[500]} align="right" />
    </FlexView>
  </FlexView>
);
