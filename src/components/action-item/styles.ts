import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 64,
    alignItems: 'center',
    paddingHorizontal: 24,
  },
  text: {
    flex: 1,
    paddingLeft: 21,
  },
});
