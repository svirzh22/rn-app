import React from 'react';

import { Divider, Icon, Text, Touchable } from '@components';
import { COLORS } from '@constants';
import { FlexView } from '@components/flex-view';
import { ActionItemProps } from './types';
import { styles } from './styles';

export const ActionItem: React.FC<ActionItemProps> = ({
  name,
  label,
  color,
  withArrow,
  iconStyle,
  onPress,
}) => (
  <>
    <Touchable style={styles.container} onPress={onPress}>
      <FlexView style={iconStyle}>
        <Icon color={color} size={15} name={name} />
      </FlexView>
      <Text style={[styles.text, Boolean(color) && { color }]} label={label} />
      {withArrow && (<Icon color={COLORS.GREY[600]} name="arrow-right" />)}
    </Touchable>
    <Divider />
  </>
);
