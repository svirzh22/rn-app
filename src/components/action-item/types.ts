import { IconNameType } from '@components';
import { StyleProp, ViewStyle } from 'react-native';

export interface ActionItemProps {
  name: IconNameType;
  label: string;
  color?: string;
  withArrow?: boolean;
  iconStyle?: StyleProp<ViewStyle>;
  onPress: () => void;
}
