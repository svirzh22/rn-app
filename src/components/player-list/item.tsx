import React from 'react';
import { useSelector } from 'react-redux';

import { memberSelector } from '@selectors';
import { useTranslate } from '@hooks';
import { FlexView, Text, Avatar, IconButton, Divider } from '@components';
import { displayHandicap } from '@helpers';
import { COLORS } from '@constants';
import { itemStyles } from './styles';
import { PlayerItemProps } from './types';

export const PlayerItem: React.FC<PlayerItemProps> = ({ item, editable, onDelete }) => {
  const member = useSelector(memberSelector);

  const isMe = item.id === member.id;
  const isVisitor = (item.addedId && !item.id) || item.memberType === 4;
  const translate = useTranslate();
  const renderActions = () => {
    if (isVisitor) {
      return <IconButton name="trash" size={16} color={COLORS.GREY[600]} onPress={onDelete} />;

      // return (
      // <>
      //   <IconButton name="pencil" size={16} color={COLORS.GREY[600]} onPress={onEdit} />
      //    <Gutter amount={24} />
      //   <IconButton name="trash" size={16} color={COLORS.GREY[600]} onPress={onDelete} />
      // </>
      // );
    }

    return isMe ? null : <IconButton name="trash" size={16} color={COLORS.GREY[600]} onPress={onDelete} />;
  };

  const memberRole = translate(isVisitor ? 'common.visitor' : 'common.member');

  return (
    <>
      <FlexView flexDirection="row" style={itemStyles.item}>
        <Avatar image={item.picture} />
        <FlexView style={itemStyles.labels}>
          <Text label={`${item.firstname} ${item.lastname}`} />
          <Text label={`${memberRole} • ${displayHandicap(item.handicap)}`} style={itemStyles.description} />
        </FlexView>
        {editable && <FlexView flexDirection="row">{renderActions()}</FlexView>}
      </FlexView>
      <Divider />
    </>
  );
};
