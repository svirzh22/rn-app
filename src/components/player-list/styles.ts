import { StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const listStyles = StyleSheet.create({
});
export const itemStyles = StyleSheet.create({
  item: {
    paddingHorizontal: 24,
    alignItems: 'center',
    paddingVertical: 16,
  },
  labels: {
    flex: 1,
    paddingLeft: 16,
  },
  actions: {
    justifyContent: 'flex-end',
  },
  description: {
    fontSize: 14,
    color: COLORS.GREY[700],
  },
  addButton: {
    height: 48,
    width: 48,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.GREY[600],
    borderStyle: 'dashed',
    borderRadius: 24,
  },
});
