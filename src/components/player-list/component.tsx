import React, { useCallback } from 'react';

import { BookingMemberEntity } from '@api/resources/types/booking';
import { FlexView, ActionItem } from '@components';
import { noop, translate } from '@helpers';
import { PlayerItem } from './item';
import { itemStyles } from './styles';
import { PlayerListProps } from './types';

export const PlayerList: React.FC<PlayerListProps> = ({
  items,
  editable = false,
  onDelete = noop,
  onEdit = noop,
  onAddItem = noop,
}) => {
  const handleEdit = useCallback(
    (item: BookingMemberEntity) => () => {
      onEdit(item);
    },
    [onEdit],
  );
  const handleDelete = useCallback(
    (item: BookingMemberEntity) => () => {
      onDelete(item);
    },
    [onDelete],
  );

  if (items) {
    return (
      <FlexView>
        {items.map((item) => (
          <PlayerItem
            key={item.id || item.addedId}
            item={item}
            editable={editable}
            onDelete={handleDelete(item)}
            onEdit={handleEdit(item)}
          />
        ))}
        {editable && items.length < 4 && (
          <ActionItem
            withArrow
            name="plus-medium"
            iconStyle={itemStyles.addButton}
            label={translate('common.addPlayer')}
            onPress={onAddItem}
          />
        )}
      </FlexView>
    );
  }

  return null;
};
