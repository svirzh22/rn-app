import { BookingMemberEntity } from '@api/resources/types/booking';

export interface PlayerItemProps {
  item: BookingMemberEntity;
  editable: boolean;
  onDelete: () => void;
  onEdit: () => void;
}

export interface PlayerListProps {
  items?: BookingMemberEntity[];
  editable: boolean;
  onDelete?: (item: BookingMemberEntity) => void;
  onEdit?: (item: BookingMemberEntity) => void;
  onAddItem?: () => void;
}
