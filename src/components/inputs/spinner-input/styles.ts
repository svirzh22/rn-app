import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    width: 120,
    height: 48,
    padding: SPACING.S - 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    borderRadius: 8,
  },
  button: {
    height: 26,
    width: 26,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    justifyContent: 'center',
    alignItems: 'center',
  },
});
