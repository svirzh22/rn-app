import React, { FC, useCallback } from 'react';

import { FlexView, Touchable, Icon, Text } from '@components';
import { createHitSlop } from '@helpers';
import { styles } from './styles';

export interface SpinnerInputProps {
  onChange?: (value: number | string) => void;
  value?: number;
  minValue?: number;
  maxValue?: number;
}

export const SpinnerInput: FC<SpinnerInputProps> = ({
  onChange,
  value = 0,
  minValue = 0,
  maxValue = 100,
}) => {
  const handlePressMinus = useCallback(() => {
    const nextValue = Number(value) - 1;
    if (nextValue >= minValue) {
      onChange?.(String(nextValue));
    }
  }, [minValue, onChange, value]);

  const handlePressPlus = useCallback(() => {
    const nextValue = Number(value) + 1;
    if (nextValue <= maxValue) {
      onChange?.(String(nextValue));
    }
  }, [maxValue, onChange, value]);

  return (
    <FlexView style={styles.container}>
      <Touchable onPress={handlePressMinus} style={styles.button} borderRadius={4} hitSlop={createHitSlop(5)}>
        <Icon name="minus" size={8} />
      </Touchable>
      <Text label={`${value}`} />
      <Touchable onPress={handlePressPlus} style={styles.button} borderRadius={4} hitSlop={createHitSlop(5)}>
        <Icon name="plus-medium" size={8} />
      </Touchable>
    </FlexView>
  );
};
