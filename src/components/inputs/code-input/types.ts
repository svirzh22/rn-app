import { KeyboardType, StyleProp, ViewStyle } from 'react-native';

export interface CodeInputProps {
  code: string;
  onChangeCode: (value: string) => void;
  cellCount: number;
  autoFocus?: boolean;
  keyboardType?: KeyboardType;
  containerStyle?: StyleProp<ViewStyle>;
  onFocus?: () => void;
  onFulfill?: () => void;
}

export interface RenderCellParams {
  symbol: string;
  index: number;
  isFocused: boolean;
}
