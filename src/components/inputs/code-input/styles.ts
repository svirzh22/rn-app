import { StyleSheet } from 'react-native';

import { COLORS, PRIMARY_FONT } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  cellRoot: {
    width: 30,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: COLORS.GREY[600],
    borderBottomWidth: 1,
  },
  text: {
    ...PRIMARY_FONT.regular,
    fontSize: responsiveFontSize(30),
    textAlign: 'center',
  },
  cellFocused: {
    borderBottomColor: COLORS.PRIMARY[500],
    borderBottomWidth: 2,
  },
  container: {
    marginTop: 20,
    width: 240,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
