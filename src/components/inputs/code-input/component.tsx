import React from 'react';
import { CodeField, Cursor, useClearByFocusCell } from 'react-native-confirmation-code-field';
import { TextInput, Text, View } from 'react-native';

import { styles } from './styles';
import { RenderCellParams, CodeInputProps } from './types';

export const CodeInput = React.forwardRef<TextInput, CodeInputProps>(
  (
    {
      code,
      onChangeCode,
      autoFocus = true,
      cellCount,
      onFocus,
      onFulfill,
      containerStyle,
    },
    ref,
  ) => {
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
      value: code,
      setValue: onChangeCode,
    });

    const renderCell = ({ index, symbol, isFocused }: RenderCellParams) => (
      <View
        key={index}
        style={[styles.cellRoot, isFocused && styles.cellFocused]}
        onLayout={getCellOnLayoutHandler(index)}
      >
        <Text style={styles.text}>{symbol || (isFocused ? <Cursor /> : null)}</Text>
      </View>
    );

    return (
      <CodeField
        ref={ref}
        {...props}
        value={code}
        onChangeText={onChangeCode}
        cellCount={cellCount}
        rootStyle={[styles.container, containerStyle]}
        keyboardType="number-pad"
        renderCell={renderCell}
        textContentType="oneTimeCode"
        autoCapitalize="characters"
        autoFocus={autoFocus}
        onFocus={onFocus}
        onBlur={onFulfill}
      />
    );
  },
);
