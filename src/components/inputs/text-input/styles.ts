import { COLORS, PRIMARY_FONT } from '@constants';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
    // borderRadius: 8,
  },
  input: {
    flex: 1,
    height: 56,
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    backgroundColor: COLORS.WHITE,
    ...PRIMARY_FONT.regular,
    fontSize: 16,
    color: COLORS.BLACK,
    borderRadius: 8,
  },
  regular: {
    paddingHorizontal: 15,
  },
  error: {
    borderColor: 'red',
  },
  focused: {
    borderColor: COLORS.BLACK,
  },
  disabled: {
    backgroundColor: COLORS.GREY[200],
  },
  errorInput: {
    marginBottom: 40,
  },
  multiline: {
    height: 160,
    paddingVertical: 12,
    borderColor: '#D8D8D8',
    borderWidth: 1,
  },
  errorStyle: {
    borderWidth: 1,
    borderColor: COLORS.ERROR,
    borderRadius: 8,
  },
});
