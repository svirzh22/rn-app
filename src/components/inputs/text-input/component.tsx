import React, { useEffect, useRef } from 'react';
import { View } from 'react-native';
import { TextInput } from 'react-native-paper';

import { COLORS } from '@constants';
import { translate } from '@helpers';
import { styles } from './styles';
import { InputProps } from './types';
import { InputError } from '../error-input';
import { useFocusState } from '../hooks';

export * from './types';

export const Input: React.FC<InputProps> = ({
  value,
  placeholder,
  keyboardType = 'default',
  secureTextEntry = false,
  label,
  style,
  inputWrapperStyle,
  onChangeText,
  error,
  multiline,
  infoPopupText,
  LeadingComponent,
  editable = true,
  assignRef,
  clearRef,
  name,
  returnKeyType,
  t,
  translateOptions,
  ...rest
}) => {
  // @ts-ignore
  const ref = useRef<TextInput>(null);
  const i18nText = t ? translate(t, translateOptions) : label;
  const { focused, handleFocus, handleBlur } = useFocusState({
    onBlur: rest.onBlur,
  });

  useEffect(() => {
    if (assignRef && name) assignRef(name, ref);

    return () => {
      if (name && clearRef) clearRef(name);
    };
  }, [assignRef, clearRef, name]);

  return (
    <>
      <View style={[styles.wrapper, inputWrapperStyle, typeof error === 'string' && styles.errorInput]}>
        {Boolean(LeadingComponent) && LeadingComponent}
        <TextInput
          ref={ref}
          label={i18nText}
          placeholder={placeholder}
          // @ts-expect-error
          selectionColor={COLORS.BLACK}
          value={value}
          editable={editable}
          error={error}
          textAlignVertical={multiline ? 'top' : 'center'}
          style={[
            styles.input,
            multiline && styles.multiline,
            !editable && styles.disabled,
            focused && styles.focused,
            error && styles.errorStyle,
            style,
          ]}
          // mode='outlined'
          underlineColor="transparent"
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType}
          multiline={multiline}
          onChangeText={onChangeText}
          returnKeyType={returnKeyType}
          blurOnSubmit={!(returnKeyType === 'next')}
          {...rest}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
        <InputError error={error} />
      </View>
    </>
  );
};
