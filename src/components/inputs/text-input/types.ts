import {
  StyleProp,
  TextInput,
  TextInputProps,
  ViewStyle,
} from 'react-native';
import { ComponentType, MutableRefObject } from 'react';
import { TranslateOptions } from 'i18n-js';

export interface InputProps extends TextInputProps {
  label?: string;
  withShadow?: boolean;
  wrapperStyle?: StyleProp<ViewStyle>;
  inputWrapperStyle?: StyleProp<ViewStyle>;
  error?: any;
  infoPopupText?: string;
  LeadingComponent?: ComponentType | JSX.Element;
  name?: string;
  t?: string;
  translateOptions?: TranslateOptions;
  assignRef?: (fieldName: string, ref: MutableRefObject<TextInput | null>) => void;
  clearRef?: (fieldName: string) => void;
}
