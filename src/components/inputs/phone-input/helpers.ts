import toUpper from 'lodash/toUpper';

import { LocaleToCountryIsoCode } from './types';

const LOCALE_TO_COUNTRY_ISO_CODE: LocaleToCountryIsoCode = {
  en: 'US',
  de: 'DE',
  fr: 'FR',
};

export const localeToIsoCode = (locale: string) =>
  LOCALE_TO_COUNTRY_ISO_CODE[locale] || toUpper(locale) || 'US';

export const theme = {
  flagSizeButton: 24,
};
