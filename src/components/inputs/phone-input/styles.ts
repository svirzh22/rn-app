import { StyleSheet } from 'react-native';

import { COLORS, IS_IOS, PRIMARY_FONT, SPACING } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  inputContainer: {
    height: 48,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    width: '100%',
    paddingHorizontal: SPACING.S - 4,
    alignItems: 'center',
  },
  inputContainerError: {
    borderColor: COLORS.ERROR,
  },
  focused: {
    borderColor: COLORS.BLACK,
  },
  disabled: {
    backgroundColor: COLORS.GREY[200],
  },
  input: {
    fontSize: responsiveFontSize(18),
    paddingBottom: IS_IOS ? 2 : -8,
    paddingTop: 0,
    ...PRIMARY_FONT.regular,
  },
  flagIconStyle: {
    height: 26,
    width: 34,
    marginTop: -3,
    marginLeft: -3,
  },
  label: {
    top: 2,
  },
});
