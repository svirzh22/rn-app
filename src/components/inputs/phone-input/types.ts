import { CountryCode } from 'react-native-country-picker-modal';
import { TextInputProps, ViewStyle } from 'react-native';

export interface PhoneInputProps extends TextInputProps {
  containerStyle?: ViewStyle;
  error?: string;
  value?: string;
}

export interface LocaleToCountryIsoCode {
  [field: string]: CountryCode;
}
