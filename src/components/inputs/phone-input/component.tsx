import React, { FC, useCallback, useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import * as RNLocalize from 'react-native-localize';
import replace from 'lodash/replace';
import includes from 'lodash/includes';
import size from 'lodash/size';
import CountryPicker, { CountryCode } from 'react-native-country-picker-modal';
import {
  AsYouType,
  getCountryCallingCode,
  isValidNumber,
  CountryCode as PhoneCountryCode,
  parsePhoneNumber,
} from 'libphonenumber-js';

import { FlexView, Text } from '@components';
import { COLORS } from '@constants';
import { callSafely, removeSpaces } from '@helpers';
import { PhoneInputProps } from './types';
import { styles } from './styles';
import { theme } from './helpers';

let asYouType: AsYouType;

export const PhoneInput: FC<PhoneInputProps> = ({
  containerStyle = {},
  style = {},
  onChangeText,
  error,
  value = '',
  editable = true,
  onFocus,
  onBlur,
  ...props
}) => {
  const initialCountry = RNLocalize.getCountry();
  const [countryCode, setCountryCode] = useState<CountryCode>(initialCountry as CountryCode);
  const [focused, setFocused] = useState(false);
  const [phone, setPhone] = useState('');
  const [initialized, setInitialized] = useState(false);
  const countryCallingCode = `+${getCountryCallingCode(countryCode as PhoneCountryCode)}`;

  useEffect(() => {
    try {
      asYouType = new AsYouType(countryCode as PhoneCountryCode);
      const phoneNumber = parsePhoneNumber(value);
      setCountryCode(phoneNumber.country as CountryCode);
      setPhone(asYouType.input(value));
    // eslint-disable-next-line no-empty
    } catch (_exc) {} finally {
      setInitialized(true);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const code = `+${getCountryCallingCode(countryCode as PhoneCountryCode)}`;
    if (initialized && !phone.includes(code)) {
      asYouType = new AsYouType(countryCode as PhoneCountryCode);
      setPhone(code);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countryCode]);

  const handleFocus = useCallback(
    (evt) => {
      setFocused(true);
      if (onFocus) onFocus(evt);
    },
    [onFocus],
  );
  const handleBlur = useCallback(
    (evt) => {
      setFocused(false);
      if (onFocus) onFocus(evt);
    },
    [onFocus],
  );

  const handleChangePhoneNumber = (text: string) => {
    if (includes(text, '+')) {
      asYouType.reset();
      const number = removeSpaces(replace(text, countryCallingCode, ''));
      const formattedPhoneNumber = asYouType.input(`${countryCallingCode}${number}`);
      const isPrevValid = isValidNumber(removeSpaces(phone));
      const isCurrentValid = isValidNumber(removeSpaces(text));

      if (isPrevValid && !isCurrentValid && size(removeSpaces(phone)) < size(removeSpaces(text))) {
        return false;
      }

      setPhone(formattedPhoneNumber);

      return callSafely(onChangeText, formattedPhoneNumber);
    }

    return true;
  };

  return (
    <FlexView
      flexDirection="row"
      style={[
        styles.inputContainer,
        error ? styles.inputContainerError : {},
        focused && styles.focused,
        !editable && styles.disabled,
        containerStyle,
      ]}
    >
      <FlexView flex={1}>
        <Text t="login.phoneNumber" size={12} color={COLORS.GREY[700]} style={styles.label} />
        <FlexView flexDirection="row" flex={1} alignItems="center" pointerEvents={editable ? 'auto' : 'none'}>
          <CountryPicker
            countryCode={countryCode}
            withFilter
            onSelect={(country) => setCountryCode(country.cca2)}
            containerButtonStyle={styles.flagIconStyle}
            theme={theme}
            preferredCountries={['CH', 'FR', 'DE', 'IT', 'LU']}
            excludeCountries={['AQ']}
          />

          <FlexView flex={1}>
            <TextInput
              editable={editable}
              style={[styles.input, style]}
              selectionColor={COLORS.BLACK}
              onChangeText={handleChangePhoneNumber}
              keyboardType="numeric"
              maxLength={20}
              value={phone || countryCallingCode}
              onFocus={handleFocus}
              onBlur={handleBlur}
              {...props}
            />
          </FlexView>
        </FlexView>
      </FlexView>
    </FlexView>
  );
};
