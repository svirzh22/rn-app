import React from 'react';
import { TextInput } from 'react-native';

import { COLORS } from '@constants';
import { IconButton } from '@components/buttons';
import { FlexView } from '@components/flex-view';
import { InputProps } from '../text-input/types';
import { styles } from './styles';

export const SearchInput: React.FC<InputProps> = ({ value, ...props }) => (
  <FlexView style={styles.wrapper}>
    <TextInput
      {...props}
      value={value}
      placeholderTextColor={COLORS.GREY[700]}
      style={styles.searchInput}
    />
    {Boolean(value) && (
      <IconButton
        name="close"
        containerStyle={{ position: 'absolute', right: 26 }}
        color={COLORS.GREY[700]}
        hitSlop={20}
        onPress={() => props.onChangeText && props.onChangeText('')}
      />
    )}
  </FlexView>
);
