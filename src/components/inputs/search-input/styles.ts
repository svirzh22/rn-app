import { StyleSheet } from 'react-native';

import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  searchInput: {
    fontSize: responsiveFontSize(16),
    width: '100%',
  },
  wrapper: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
