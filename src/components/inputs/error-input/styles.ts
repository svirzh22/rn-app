import { Platform, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  helper: {
    position: 'absolute',
    left: 0,
    ...Platform.select({
      android: { bottom: -31 },
      default: { bottom: -28 },
    }),
    letterSpacing: 1.18,
  },
});
