import { StyleProp, TextStyle } from 'react-native';

export interface InputErrorProps {
  error?: any;
  errorStyle?: StyleProp<TextStyle>;
}
