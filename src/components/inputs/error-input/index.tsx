import React from 'react';
import { HelperText } from 'react-native-paper';

import { styles } from './styles';
import { InputErrorProps } from './types';

export const InputError: React.FC<InputErrorProps> = ({
  error,
  errorStyle,
}) => (
  <>
    {Boolean(error) && (
      <HelperText
        visible
        padding="none"
        type="error"
        style={[styles.helper, errorStyle]}
      >
        {error}
      </HelperText>
    )}
  </>
);
