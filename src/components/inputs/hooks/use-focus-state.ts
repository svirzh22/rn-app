import { useCallback, useState } from 'react';

export const useFocusState = ({
  onFocus,
  onBlur,
}: {
  onFocus?: (evt: any) => void;
  onBlur?: (evt: any) => void;
} = {}) => {
  const [focused, setFocused] = useState(false);

  const handleFocus = useCallback((evt) => {
    setFocused(true);
    if (onFocus) onFocus(evt);
  }, [onFocus]);
  const handleBlur = useCallback((evt) => {
    setFocused(false);
    if (onBlur) onBlur(evt);
  }, [onBlur]);

  return {
    focused,
    handleBlur,
    handleFocus,
  };
};
