import React, { FC } from 'react';

import { FlexView, Input, Text, Touchable, Gutter } from '@components';
import { COLORS, SPACING } from '@constants';
import { handicapValue } from '@helpers';
import { styles } from './styles';

export interface HandicapInputProps {
  value: string;
  withoutLabel?: boolean;
  onChange: (value: string) => void;
  error?: string;
}

interface ChangeHandicapParams {
  nextState?: string;
  nextHandicap?: string;
  nextHandicapDecimal?: string;
}

const formatHandicap = (text = '') => text.replace(/[^0-9]/g, '');

export const HandicapInput: FC<HandicapInputProps> = ({
  value = '.',
  onChange,
  error,
  withoutLabel,
}) => {
  const isPlusActive = handicapValue(value).includes('+');
  const [handicap = '0', handicapDecimal = '0'] = value.replace('+', '').replace('-', '').split('.');

  const changeHandicap = ({ nextState, nextHandicap, nextHandicapDecimal }: ChangeHandicapParams) => {
    const state = isPlusActive ? '+' : '-';

    const stateValue = typeof nextState === 'string' ? nextState : state;
    const hcp = typeof nextHandicap === 'string' ? formatHandicap(nextHandicap) : handicap;
    // prettier-ignore
    const handicapDecimalValue = typeof nextHandicapDecimal === 'string'
      ? formatHandicap(nextHandicapDecimal)
      : handicapDecimal;

    onChange(`${stateValue}${hcp}.${handicapDecimalValue}`);
  };

  const handleChangeHandicap = (nextHandicap: string) => changeHandicap({ nextHandicap });
  const handleChangeHandicapDecimal = (nextHandicapDecimal: string) =>
    changeHandicap({ nextHandicapDecimal });
  const handleChangePlus = () => changeHandicap({ nextState: isPlusActive ? '-' : '+' });
  const handleBlurHandicap = () => {
    if (!handicap) {
      changeHandicap({ nextHandicap: '0' });
    }
  };
  const handleBlurHandicapDecimal = () => {
    if (!handicapDecimal) {
      changeHandicap({ nextHandicapDecimal: '0' });
    }
  };

  return (
    <FlexView style={styles.container}>
      {!withoutLabel && <Text style={styles.label} t="common.handicap" size="XS" color="grey" />}
      <Touchable style={[styles.plus, isPlusActive && styles.plusActive]} onPress={handleChangePlus}>
        <Text label="+" size="L" color={isPlusActive ? COLORS.WHITE : COLORS.GREY[700]} />
      </Touchable>
      <Gutter amount={SPACING.S} />
      <Input
        value={handicap}
        onChangeText={handleChangeHandicap}
        keyboardType="number-pad"
        inputWrapperStyle={styles.inputWrapperStyle}
        maxLength={2}
        style={styles.input}
        error={!!error}
        onBlur={handleBlurHandicap}
      />
      <FlexView justifyContent="flex-end" style={{ height: '100%' }}>
        <Text label=" , " size="L" />
      </FlexView>
      <Input
        value={handicapDecimal}
        onChangeText={handleChangeHandicapDecimal}
        keyboardType="number-pad"
        inputWrapperStyle={styles.inputWrapperStyle}
        style={styles.input}
        maxLength={1}
        error={!!error}
        onBlur={handleBlurHandicapDecimal}
      />
    </FlexView>
  );
};
