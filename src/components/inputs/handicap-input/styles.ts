import { StyleSheet } from 'react-native';

import { COLORS, FONT_SIZES, SPACING } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: SPACING.M,
    alignItems: 'center',
    width: '100%',
  },
  inputWrapperStyle: {
    width: 62,
    height: 48,
  },
  input: {
    fontSize: responsiveFontSize(FONT_SIZES.S),
    paddingHorizontal: 0,
    width: '100%',
    height: 48,
    textAlign: 'center',
  },
  plus: {
    height: 28,
    width: 28,
    borderRadius: 28 / 2,
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    justifyContent: 'center',
    alignItems: 'center',
  },
  plusActive: {
    backgroundColor: COLORS.PRIMARY[500],
    borderWidth: 0,
  },
  label: {
    position: 'absolute',
    top: -28,
  },
});
