import React, { useContext } from 'react';
import { HeaderHeightContext } from '@react-navigation/stack';
import {
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {
  getStatusBarHeight,
} from 'react-native-iphone-x-helper';

import { styles } from './styles';

interface KeyboardAvoidingViewInternalProps {
  children: React.ReactNode;
  style?: any;
  iosOffset?: number;
  androidOffset?: number;
  additionalVerticalOffset?: number;
  withoutHeaderOffset?: boolean;
}

export const KeyboardAvoidingViewInternal: React.FC<KeyboardAvoidingViewInternalProps> = ({
  children,
  style,
  additionalVerticalOffset = 0,
  withoutHeaderOffset = false,
}) => {
  const headerHeight = useContext(HeaderHeightContext) || 0;
  const iosOffset = withoutHeaderOffset ? 0 : headerHeight;

  const androidOffset = withoutHeaderOffset ? 0 : headerHeight + getStatusBarHeight();

  return (
    <KeyboardAvoidingView
      enabled
      style={[styles.container, style]}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      keyboardVerticalOffset={Platform.OS === 'ios'
        ? (iosOffset + additionalVerticalOffset)
        : (androidOffset + additionalVerticalOffset)}
    >
      {children}
    </KeyboardAvoidingView>
  );
};
