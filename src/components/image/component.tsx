import React, { FC } from 'react';
import FastImage, { FastImageProps } from 'react-native-fast-image';

interface ImageProps extends FastImageProps {}

// Wrap of RN image for simple possibility to add custom logic in future
export const Image: FC<ImageProps> = ({ source, style, ...props }: ImageProps) => (
  <FastImage {...props} style={style} source={source} />
);
