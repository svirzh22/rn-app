import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    paddingBottom: 0,
    justifyContent: 'flex-end',
    borderBottomColor: '#ffffff',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  center: {
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  wrapper: {
    backgroundColor: '#ffffff',
  },
  overlay: {
    height,
    width,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
  },
});
