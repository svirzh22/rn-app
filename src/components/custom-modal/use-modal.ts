import { useCallback, useState } from 'react';

export const useModal = () => {
  const [visible, setVisible] = useState(false);
  const toggleModal = useCallback(() => { setVisible(!visible); }, [visible]);

  return {
    visible,
    toggleModal,
  };
};
