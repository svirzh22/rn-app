import { Backdrop } from '@components/backdrop';
import React from 'react';
import {
  Modal,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import { Portal } from 'react-native-paper';

import { styles } from './styles';

interface CustomModalProps {
  style?: any;
  position?: 'bottom' | 'center';
  visible: boolean;
  onCancel: () => void;
  children: any;
}

export const CustomModal: React.FC<CustomModalProps> = ({
  style = {},
  visible,
  position = 'bottom',
  onCancel,
  children,
}) => (
  <>
    <Portal>
      <Backdrop visible={visible} />
    </Portal>
    <Modal
      animationType="slide"
      transparent
      visible={visible}
      onRequestClose={onCancel}
    >
      <TouchableWithoutFeedback onPress={onCancel}>
        <View style={[styles.modal, position === 'center' && styles.center]}>
          <TouchableWithoutFeedback>
            <View style={[styles.wrapper, style]}>
              {children}
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  </>
);
