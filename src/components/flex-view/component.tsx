import React, { FC } from 'react';
import { View, SafeAreaView } from 'react-native';

import { FlexViewProps } from './types';

export const FlexView: FC<FlexViewProps> = ({
  flex,
  alignItems,
  justifyContent,
  flexDirection,
  children,
  style,
  flexWrap,
  alignSelf,
  useSafeArea,
  ...props
}) => {
  const Component = useSafeArea ? SafeAreaView : View;

  return (
    <Component
      {...props}
      style={[
        flex ? { flex } : {},
        alignItems && { alignItems },
        justifyContent && { justifyContent },
        flexDirection && { flexDirection },
        flexWrap && { flexWrap },
        style,
      ]}
    >
      {children}
    </Component>
  );
};
