import { FlexAlignType, StyleProp, ViewProps, ViewStyle } from 'react-native';

export interface FlexViewProps extends ViewProps {
  flex?: number;
  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline';
  justifyContent?: 'space-between' | 'space-around' | 'center' | 'flex-start' | 'flex-end';
  flexDirection?: 'row' | 'column' | 'row-reverse' | 'column-reverse';
  style?: StyleProp<ViewStyle>;
  flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse';
  alignSelf?: 'auto' | FlexAlignType;
  useSafeArea?: boolean;
}
