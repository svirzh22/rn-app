import React from 'react';
import HTML from 'react-native-render-html';

import { PRIMARY_FONT } from '@constants';

interface HTMLContentProps {
  content?: string;
  customTagStyle?: { ul?: any };
}

export const HTMLContent: React.FC<HTMLContentProps> = ({
  content = '',
  customTagStyle = {},
}) => (
  <HTML
    source={{ html: content }}
    containerStyle={{ paddingHorizontal: 24 }}
    baseFontStyle={{
      ...PRIMARY_FONT.regular,
      fontSize: 16,
    }}
    tagsStyles={{
      ul: {
        paddingTop: 20,
        paddingLeft: 0,
        paddingBottom: 0,
        marginBottom: 0,
        fontSize: 16,
        ...customTagStyle.ul,
      },
      h3: {
        paddingTop: 10,
        paddingBottom: 20,
        fontSize: 18,
      },
      p: {
        paddingBottom: 20,
      },
    }}
  />
);
