import React, { FC } from 'react';
import { WebViewErrorStub } from '@components/web-view/error-stub';
import { LinkButton, Text } from '@src/components';

interface NoInternetConnectionProps {
  onPressRetry: () => void;
}

export const NoInternetConnection: FC<NoInternetConnectionProps> = ({ onPressRetry }) => (
  <WebViewErrorStub>
    <Text t="errors.noConnection" />
    <LinkButton t="errors.noConnectionRetry" onPress={onPressRetry} />
  </WebViewErrorStub>
);
