import React, { FC, useRef, useState } from 'react';
import {
  Directions,
  FlingGestureHandler,
  FlingGestureHandlerStateChangeEvent,
  State,
} from 'react-native-gesture-handler';
import {
  Animated,
  NativeScrollEvent,
  NativeSyntheticEvent,
  ScrollView,
  ScrollViewProps,
  StyleProp,
  ViewStyle,
} from 'react-native';

import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { SCREEN_HEIGHT, SPACING } from '@constants';
import { IMAGE_HEIGHT } from '@screens/course/styles';
import { styles } from './styles';

interface SwipeableScrollViewProps extends ScrollViewProps {
  children: React.ReactNode;
  style?: StyleProp<ViewStyle>;
}

export const SwipeableScrollView: FC<SwipeableScrollViewProps> = ({ children, style, ...props }) => {
  const insets = useSafeAreaInsets();
  const HEADER_HEIGHT = 40;
  const windowHeight = SCREEN_HEIGHT - (insets.top + HEADER_HEIGHT + SPACING.XS);
  const DOWN_TRANSLATE = IMAGE_HEIGHT - (insets.top + HEADER_HEIGHT + SPACING.M);
  let beginDragOffset = 0;

  const [position, setPosition] = useState<Directions>(Directions.DOWN);
  const [scrollEnabled, setScrollEnabled] = useState<boolean>(false);
  const translateY = useRef(new Animated.Value(DOWN_TRANSLATE)).current;

  const animateListTranslate = (value: number) =>
    Animated.spring(translateY, {
      toValue: value,
      useNativeDriver: true,
      tension: 40,
      friction: 10,
    }).start();

  const handleScrollBeginDrag = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    beginDragOffset = event.nativeEvent.contentOffset.y;
  };
  const handleScrollEndDrag = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { contentOffset } = event.nativeEvent;

    if (beginDragOffset >= contentOffset.y) {
      if (!contentOffset.y) {
        setScrollEnabled(false);
        setPosition(Directions.DOWN);
        animateListTranslate(DOWN_TRANSLATE);
      }
    }
  };

  const onSwipe = (direction: Directions) => (event: FlingGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.END) {
      if (direction === Directions.UP && direction === position) {
        return setScrollEnabled(true);
      }

      if (direction === Directions.UP) {
        setScrollEnabled(true);
      }

      setPosition(direction);
      animateListTranslate(direction === Directions.UP ? 0 : DOWN_TRANSLATE);
    }

    return false;
  };

  return (
    <FlingGestureHandler direction={Directions.DOWN} onHandlerStateChange={onSwipe(Directions.DOWN)}>
      <FlingGestureHandler direction={Directions.UP} onHandlerStateChange={onSwipe(Directions.UP)}>
        <Animated.View
          style={[
            styles.listWrapper,
            {
              height: windowHeight,
              transform: [{ translateY }],
            },
            style,
          ]}
        >
          <ScrollView
            onScrollBeginDrag={handleScrollBeginDrag}
            onScrollEndDrag={handleScrollEndDrag}
            scrollEnabled={scrollEnabled}
            bounces={false}
            {...props}
          >
            {children}
          </ScrollView>
        </Animated.View>
      </FlingGestureHandler>
    </FlingGestureHandler>
  );
};
