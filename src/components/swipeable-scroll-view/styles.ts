import { StyleSheet } from 'react-native';
import { COLORS, SCREEN_WIDTH } from '@constants';

export const styles = StyleSheet.create({
  listWrapper: {
    width: SCREEN_WIDTH,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: COLORS.WHITE,
    position: 'absolute',
    bottom: 0,
  },
});
