import React, { FC } from 'react';
import { StatusBarProps as RNStatusBarProps, StatusBar as RNStatusBar } from 'react-native';
import { COLORS } from '@constants';

export interface StatusBarProps extends RNStatusBarProps {
  networkActivity?: boolean;
}

export const StatusBar: FC<StatusBarProps> = ({
  networkActivity = false,
  translucent = false,
  hidden = false,
  barStyle,
  ...props
}: StatusBarProps) => (
  <RNStatusBar
    hidden={hidden}
    backgroundColor={COLORS.STATUS_BAR}
    translucent={translucent}
    barStyle={barStyle}
    // ios
    networkActivityIndicatorVisible={networkActivity}
    {...props}
  />
);
