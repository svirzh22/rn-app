import { IconButtonProps, IconNameType, TextProps } from '@components';
import React from 'react';
import { ViewStyle } from 'react-native';

type LeftButtonProps = Partial<IconButtonProps>;
type TitleProps = Partial<TextProps>;

export interface HeaderProps {
  showBackButton?: boolean;
  title?: string;
  label?: string;
  backIconName?: IconNameType;
  renderHeaderRight?: () => React.ReactNode;
  renderHeaderLeft?: () => React.ReactNode;
  leftButtonProps?: LeftButtonProps;
  titleProps?: TitleProps;
  style?: ViewStyle;
  titleColor?: string;
  leftButtonColor?: string;
  preset?: HeaderPresets;
  onPressBack?: () => void;
  isSafe?: boolean;
}

export type HeaderPresets = 'withBorder';
