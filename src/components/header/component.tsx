import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { callSafely, createHitSlop } from '@helpers';
import { FlexView, IconButton, Text } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';
import { HeaderProps } from './types';

export const Header: FC<HeaderProps> = ({
  showBackButton = true,
  title,
  label,
  backIconName = 'back',
  renderHeaderRight,
  renderHeaderLeft,
  leftButtonProps = {},
  style = {},
  titleColor = COLORS.BLACK,
  leftButtonColor = COLORS.BLACK,
  titleProps = {},
  preset,
  onPressBack,
  isSafe = false,
  ...props
}) => {
  const navigation = useNavigation();
  const headerPresets = {
    withBorder: styles.withBorder,
  };
  const headerStyle = [styles.header, preset && headerPresets[preset], style];
  const insets = useSafeAreaInsets();

  return (
    <FlexView style={[headerStyle, isSafe && { marginTop: insets.top }]} {...props}>
      <FlexView>
        {!renderHeaderLeft && showBackButton && (
          <IconButton
            size={16}
            onPress={() => (onPressBack ? onPressBack() : navigation.goBack())}
            name={backIconName}
            hitSlop={createHitSlop(15, 50, 15, 15)}
            rippleRadius={18}
            color={leftButtonColor}
            {...leftButtonProps}
          />
        )}
        <FlexView>{callSafely(renderHeaderLeft)}</FlexView>
      </FlexView>
      <FlexView>
        <Text t={title} label={label} weight="medium" size="L" color={titleColor} {...titleProps} />
      </FlexView>
      <FlexView>{callSafely(renderHeaderRight)}</FlexView>
    </FlexView>
  );
};
