import { Platform, StyleSheet } from 'react-native';

import { COLORS, SCREEN_WIDTH, SPACING } from '@constants';

export const styles = StyleSheet.create({
  header: {
    height: 40,
    width: SCREEN_WIDTH,
    flexDirection: 'row',
    paddingHorizontal: SPACING.M,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: COLORS.WHITE,
    ...Platform.select({
      android: {
        height: 52,
      },
    }),
  },
  withBorder: {
    height: 44,
    borderBottomColor: COLORS.GREY[600],
    borderBottomWidth: StyleSheet.hairlineWidth,
    ...Platform.select({
      android: {
        height: 56,
      },
    }),
  },
});
