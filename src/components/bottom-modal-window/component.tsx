import React, { FC, ReactNode } from 'react';
import Modal from 'react-native-modal';
import { StyleProp, ViewStyle } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { FlexView } from '@components';
import { styles } from './styles';

interface BottomModalWindowProps {
  isVisible: boolean;
  hide: () => void;
  windowStyle?: StyleProp<ViewStyle>;
  style?: StyleProp<ViewStyle>;
  children: ReactNode;
}

export const BottomModalWindow: FC<BottomModalWindowProps> = ({
  isVisible,
  hide,
  windowStyle,
  style,
  children,
}) => {
  const insets = useSafeAreaInsets();
  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={hide}
      // @ts-expect-error
      onRequestClose={hide}
      style={[styles.modal, style]}
    >
      <FlexView style={[styles.window, { paddingBottom: insets.bottom }, windowStyle]}>{children}</FlexView>
    </Modal>
  );
};
