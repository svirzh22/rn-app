import { StyleSheet } from 'react-native';
import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  window: {
    height: 220,
    backgroundColor: COLORS.WHITE,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  modal: {
    flex: 1,
    marginLeft: 0,
    marginBottom: 0,
    width: '100%',
    justifyContent: 'flex-end',
  },
});
