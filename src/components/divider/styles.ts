import { StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  divider: {
    backgroundColor: COLORS.GREY[500],
    height: StyleSheet.hairlineWidth,
  },
});
