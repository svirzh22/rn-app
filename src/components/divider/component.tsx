import React, { FC } from 'react';
import { View, ViewProps } from 'react-native';

import { styles } from './styles';

interface DividerProps extends ViewProps {}

export const Divider: FC<DividerProps> = ({ style, ...rest }: DividerProps) => (
  <View {...rest} style={[styles.divider, style]} />
);
