import React, { FC, ReactNode } from 'react';
import { StatusBarStyle, ViewStyle } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { FlexView, FlexViewProps, NoInternetConnection, StatusBar } from '@components';
import { COLORS } from '@constants';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import NetInfo from '@react-native-community/netinfo';

interface ScreenProps extends FlexViewProps {
  unsafe?: boolean;
  children: ReactNode;
  style?: ViewStyle;
  withScroll?: boolean;
  extraHeight?: number;
  containerStyle?: ViewStyle;
  statusBarStyle?: StatusBarStyle;
  statusBarBackgroundColor?: string;
  networkActivity?: boolean;
  onRetry?: () => void;
}

export const Screen: FC<ScreenProps> = ({
  children,
  unsafe = false,
  withScroll,
  style = {},
  extraHeight,
  containerStyle = {},
  statusBarStyle,
  statusBarBackgroundColor,
  networkActivity = false,
  onRetry,
  ...props
}: ScreenProps) => {
  const insets = useSafeAreaInsets();
  const insetStyle = {
    paddingTop: unsafe ? 0 : insets.top,
    backgroundColor: COLORS.WHITE,
  };

  const { isInternetReachable } = NetInfo.useNetInfo();

  if (isInternetReachable === false && onRetry) {
    return <NoInternetConnection onPressRetry={onRetry} />;
  }

  if (withScroll) {
    return (
      <KeyboardAwareScrollView
        extraHeight={extraHeight}
        bounces={false}
        keyboardShouldPersistTaps="handled"
        style={containerStyle}
      >
        <>
          <StatusBar
            barStyle={statusBarStyle}
            networkActivity={networkActivity}
            backgroundColor={statusBarBackgroundColor}
          />
          <FlexView style={[insetStyle, style]} flex={1} {...props}>
            {children}
          </FlexView>
        </>
      </KeyboardAwareScrollView>
    );
  }

  return (
    <FlexView flex={1} style={containerStyle}>
      <StatusBar barStyle={statusBarStyle} backgroundColor={statusBarBackgroundColor} />
      <FlexView style={[insetStyle, style]} flex={1} {...props}>
        {children}
      </FlexView>
    </FlexView>
  );
};
