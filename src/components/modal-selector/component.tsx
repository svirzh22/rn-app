import React, { useMemo } from 'react';
import { View } from 'react-native';
import ModalSelectorLib from 'react-native-modal-selector';
import { Modal, Portal } from 'react-native-paper';

import { translate } from '@helpers';
import { styles } from './styles';

export const ModalSelector: React.FC<ModalSelectorProps> = ({
  data,
  children,
  visible,
  onChange,
  onDismiss,
}) => {
  // lib has broken typings
  const selectorOptions = useMemo<any[]>(() =>
    data.map((item, index) => ({ ...item, key: index })), [data]);

  return (
    <Portal>
      <Modal visible={visible} onDismiss={onDismiss}>
        <ModalSelectorLib
          backdropPressToClose
          visible={visible}
          data={selectorOptions}
          cancelText={translate('common.cancel')}
          customSelector={<View />}
          sectionStyle={styles.sectionStyle}
          sectionTextStyle={styles.title}
          overlayStyle={styles.overlayStyle}
          optionContainerStyle={styles.optionContainerStyle}
          optionStyle={styles.optionStyle}
          optionTextStyle={styles.optionTextStyle}
          cancelStyle={styles.cancelStyle}
          cancelTextStyle={{ ...styles.optionTextStyle, ...styles.cancelTextStyle }}
          cancelContainerStyle={styles.cancelContainerStyle}
          onModalClose={onDismiss}
          onChange={onChange}
        >
          {children}
        </ModalSelectorLib>
      </Modal>
    </Portal>
  );
};
