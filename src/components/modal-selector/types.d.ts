interface ModalSelectorItem {
  section?: boolean;
  label: string;
  customKey?: string;
  withValue?: boolean;
}

interface ModalSelectorProps {
  data: ModalSelectorItem[];
  visible: boolean;
  onChange: (action: string) => void;
  onDismiss: () => void;
}
