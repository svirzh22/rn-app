import {
  StyleSheet,
} from 'react-native';

import { PRIMARY_FONT } from '@constants';

export const styles = StyleSheet.create({
  title: {
    ...PRIMARY_FONT.semiBold,
    color: '#8F8E94',
    letterSpacing: -0.08,
    margin: 0,
  },
  overlayStyle: {
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
  },
  sectionStyle: {
    paddingTop: 18,
    paddingBottom: 10,
  },
  optionContainerStyle: {
    backgroundColor: 'rgba(242,242,242,1)',
    borderRadius: 13,
    padding: 0,
  },
  optionStyle: {
    paddingVertical: 20,
  },
  optionTextStyle: {
    ...PRIMARY_FONT.semiBold,
    fontSize: 20,
    color: '#007AFF',
    letterSpacing: 0.38,
  },
  cancelContainerStyle: {
    backgroundColor: '#ffffff',
    height: 55,
    borderRadius: 13,
  },
  cancelStyle: {
    height: 55,
    borderRadius: 13,
  },
  cancelTextStyle: {
    paddingTop: 7,
  },
});
