import React, { FC } from 'react';
import { View } from 'react-native';
import { SPACING } from '@constants';

interface GutterProps {
  amount?: number;
  isVertical?: boolean;
}

export const Gutter: FC<GutterProps> = ({ amount = SPACING.M, isVertical = false }) => {
  const style = isVertical ? { height: amount } : { width: amount };
  return <View style={style} />;
};
