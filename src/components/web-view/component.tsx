import React, { forwardRef, useCallback, useState } from 'react';
import WebView, { WebViewProps } from 'react-native-webview';
import NetInfo from '@react-native-community/netinfo';

import { Text } from '@components/text';
import { NoInternetConnection } from '@components/no-internet-connection';
import { IS_IOS } from '@constants';
import { WebViewErrorStub } from './error-stub';
import { webViewErrorStubStyles } from './styles';

export const WebViewInternal = forwardRef<any, WebViewProps>((props, ref) => {
  const { isInternetReachable } = NetInfo.useNetInfo();
  const [isLoaded, setIsLoaded] = useState(false);
  const handleRetryPress = useCallback(() => {
    // @ts-ignore
    if (isInternetReachable) ref?.current?.reload();
  }, [isInternetReachable, ref]);

  if (isInternetReachable === false) {
    return <NoInternetConnection onPressRetry={handleRetryPress} />;
  }

  return (
    <>
      <WebView
        ref={ref}
        containerStyle={!isInternetReachable && !isLoaded && { position: 'absolute' }}
        renderError={(_error, errorCode) => {
          if ((IS_IOS && errorCode === -1009) || (!IS_IOS && errorCode === -2)) {
            return <NoInternetConnection onPressRetry={handleRetryPress} />;
          }

          return (
            <WebViewErrorStub style={webViewErrorStubStyles.overlay}>
              <Text t="errors.somethingWrong" />
            </WebViewErrorStub>
          );
        }}
        onLoadStart={() => isLoaded && setIsLoaded(false)}
        onLoadEnd={() => setIsLoaded(true)}
        {...props}
      />
    </>
  );
});
