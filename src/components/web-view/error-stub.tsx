import React from 'react';
import { View } from 'react-native';

import { webViewErrorStubStyles } from './styles';

interface WebViewErrorStubProps {
  style?: any;
}

export const WebViewErrorStub: React.FC<WebViewErrorStubProps> = ({ children, style }) => (
  <View style={[webViewErrorStubStyles.container, style]}>{children}</View>
);
