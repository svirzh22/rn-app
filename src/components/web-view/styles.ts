import { StyleSheet } from 'react-native';

export const webViewErrorStubStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  overlay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
});
