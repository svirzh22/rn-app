import React from 'react';
import { ScrollView, TouchableWithoutFeedback, View } from 'react-native';

import { KeyboardAvoidingViewInternal } from '../keyboard-avoiding-view';
import { styles } from './styles';

interface KeyboardAvoidingScrollViewProps {
  children: React.ReactNode;
  style?: any;
  buttonStyle?: any;
  iosOffset?: number;
  androidOffset?: number;
  ButtonComponent?: React.ReactNode;
  additionalVerticalOffset?: number;
  withoutHeaderOffset?: boolean;
}

export const KeyboardAvoidingScrollView: React.FC<KeyboardAvoidingScrollViewProps> = ({
  children,
  style,
  buttonStyle,
  iosOffset,
  androidOffset,
  ButtonComponent,
  additionalVerticalOffset,
  withoutHeaderOffset = false,
}) => (
  <KeyboardAvoidingViewInternal
    iosOffset={iosOffset}
    androidOffset={androidOffset}
    additionalVerticalOffset={additionalVerticalOffset}
    withoutHeaderOffset={withoutHeaderOffset}
  >
    <ScrollView
      bounces={false}
      style={{ flex: 1 }}
      contentContainerStyle={[styles.wrapper, style]}
      keyboardShouldPersistTaps="handled"
    >
      <TouchableWithoutFeedback>{children}</TouchableWithoutFeedback>
    </ScrollView>
    {Boolean(ButtonComponent) && (
      <View style={[styles.button, buttonStyle]}>{ButtonComponent}</View>
    )}
  </KeyboardAvoidingViewInternal>
);
