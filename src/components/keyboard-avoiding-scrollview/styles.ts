import { StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  wrapper: {
    // width: SCREEN_WIDTH,
    flexGrow: 1,
    backgroundColor: COLORS.WHITE,
    paddingTop: 20,
    paddingBottom: 20,
    paddingHorizontal: 24,
  },
  button: {
    backgroundColor: COLORS.PRIMARY[500],
  },
});
