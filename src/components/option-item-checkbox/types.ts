import { CheckboxProps } from '@components/checkbox/types';

export interface OptionItemCheckboxProps extends CheckboxProps {
  cost: number;
}
