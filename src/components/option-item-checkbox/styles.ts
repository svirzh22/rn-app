import { StyleSheet } from 'react-native';

import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'row',
  },
  infoButton: {
    marginLeft: SPACING.XS,
  },
  disabled: {
    opacity: 0.4,
  },
});
