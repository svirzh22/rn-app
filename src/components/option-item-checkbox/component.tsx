import React from 'react';

import { Checkbox, FlexView, Text } from '@components';
import { APP_CURRENCY, COLORS } from '@constants';
import { styles } from './styles';
import { OptionItemCheckboxProps } from './types';

export const OptionItemCheckbox: React.FC<OptionItemCheckboxProps> = ({
  label,
  t,
  forwardRef,
  style,
  value,
  cost,
  onPress,
  info,
  onPressInfo,
  isDisabled,
}) => (
  <FlexView style={[styles.root, style, isDisabled && styles.disabled]}>
    <FlexView flex={0.8}>
      <Checkbox
        label={label}
        t={t}
        value={value}
        forwardRef={forwardRef}
        onPress={onPress}
        info={info}
        onPressInfo={onPressInfo}
        isDisabled={isDisabled}
      />
    </FlexView>
    <FlexView flex={0.2} alignItems="flex-end">
      <Text label={`${value ? cost : 0} ${APP_CURRENCY}`} color={COLORS.SECONDARY[500]} />
    </FlexView>
  </FlexView>
);
