import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  modal: {
    padding: 24,
    height: 150,
    backgroundColor: 'white',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
});
