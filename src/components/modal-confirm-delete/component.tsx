import React from 'react';

import { FlexView, Button } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface ModalConfirmDeleteProps {
  onConfirm: () => void;
  onCancel: () => void;
}

export const ModalConfirmDelete: React.FC<ModalConfirmDeleteProps> = ({
  onConfirm,
  onCancel,
}) => (
  <FlexView style={styles.modal}>
    <Button
      bgColor={COLORS.RED}
      color={COLORS.WHITE}
      t="common.confirm"
      onPress={onConfirm}
    />
    <Button
      bgColor="transparent"
      preset="flat"
      t="common.cancel"
      onPress={onCancel}
    />
  </FlexView>
);
