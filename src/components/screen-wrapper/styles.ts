import { StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
  },
  keyboardWrapper: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});
