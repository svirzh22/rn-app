import React from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import { isIphoneX } from 'react-native-iphone-x-helper';

import NetInfo from '@react-native-community/netinfo';
import { NoInternetConnection } from '@src/components';
import { KeyboardAvoidingViewInternal } from '@components/keyboard-avoiding-view';
import { KeyboardAvoidingScrollView } from '../keyboard-avoiding-scrollview';
import { styles } from './styles';
import { ScreenWrapperProps } from './types';

export const ScreenWrapper: React.FC<ScreenWrapperProps> = ({
  style,
  buttonStyle,
  children,
  footer,
  additionalVerticalOffset,
  withoutHeaderOffset = false,
  withoutSafeArea,
  withoutScrollView,
  disableForceBottomInset = false,
  onRetry,
}) => {
  const { isInternetReachable } = NetInfo.useNetInfo();
  const renderContent = () => (withoutScrollView ? (
    <KeyboardAvoidingViewInternal
      style={[styles.keyboardWrapper, style]}
    >
      {children}

    </KeyboardAvoidingViewInternal>
  ) : (
    <KeyboardAvoidingScrollView
      style={[styles.keyboardWrapper, style]}
      buttonStyle={buttonStyle}
      ButtonComponent={footer}
      additionalVerticalOffset={additionalVerticalOffset}
      withoutHeaderOffset={withoutHeaderOffset}
    >
      <>{children}</>
    </KeyboardAvoidingScrollView>
  ));

  if (isInternetReachable === false && onRetry) {
    return <NoInternetConnection onPressRetry={onRetry} />;
  }

  if (withoutSafeArea && isIphoneX()) return renderContent();

  return (
    <SafeAreaView
      forceInset={disableForceBottomInset ? { bottom: 'never' } : undefined}
      style={styles.container}
    >
      {renderContent()}
    </SafeAreaView>
  );
};
