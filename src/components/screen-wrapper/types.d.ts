import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';

interface ScreenWrapperProps {
  style?: StyleProp<ViewStyle>;
  buttonStyle?: StyleProp<ViewStyle>;
  footer?: React.ReactNode;
  additionalVerticalOffset?: number;
  withoutHeaderOffset?: boolean;
  withoutSafeArea?: boolean;
  withoutScrollView?: boolean;
  disableForceBottomInset?: boolean;
  onRetry?: () => void ;
}
