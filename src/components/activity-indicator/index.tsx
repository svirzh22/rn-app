import React from 'react';
import { ActivityIndicator as RNActivityIndicator, View } from 'react-native';
import { Portal } from 'react-native-paper';

import { COLORS } from '@constants';
import { Text } from '@components/text';
import { Gutter } from '@components/gutter';
import { styles } from './styles';
import { KeyboardAvoidingViewInternal } from '../keyboard-avoiding-view';
import { ActivityIndicatorProps } from './types';

export const ActivityIndicator: React.FC<ActivityIndicatorProps> = ({
  mode = 'regular',
  color = COLORS.PRIMARY[500],
  t,
  withTopOffset = false,
  size,
  style,
}) => {
  if (mode === 'portal') {
    return (
      <Portal>
        <KeyboardAvoidingViewInternal style={[styles.container, styles.portal]}>
          <RNActivityIndicator
            animating
            color={color}
            size={size === undefined ? 'large' : size}
            style={[styles.spinner, withTopOffset && styles.topOffset, style]}
          />
        </KeyboardAvoidingViewInternal>
      </Portal>
    );
  }

  if (mode === 'standalone') {
    return (
      <View style={styles.container}>
        <RNActivityIndicator
          animating
          color={color}
          size={size === undefined ? 'large' : size}
          style={[styles.spinner, style]}
        />
        <Gutter isVertical />
        <Text t={t} style={{ color: '#000000' }} />
      </View>
    );
  }

  return (
    <RNActivityIndicator
      animating
      color={color}
      size={size === undefined ? 'small' : size}
      style={[styles.spinner, withTopOffset && styles.topOffset, style]}
    />
  );
};
