import { StyleSheet } from 'react-native';
import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.WHITE,
  },
  portal: {
    backgroundColor: '#0F1324',
    opacity: 0.5,
  },
  spinner: {
    alignSelf: 'center',
  },
  topOffset: {
    marginTop: 30,
  },
});
