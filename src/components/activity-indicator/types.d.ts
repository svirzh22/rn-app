import { StyleProp, ViewStyle } from 'react-native';

export interface ActivityIndicatorProps {
  mode?: 'portal' | 'standalone' | 'regular';
  size?: 'small' | 'large';
  color?: string;
  t?: string;
  withTopOffset?: boolean;
  style?: StyleProp<ViewStyle>;
}
