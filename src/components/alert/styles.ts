import { StyleSheet, PlatformColor } from 'react-native';

import { IS_IOS, PRIMARY_FONT } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  title: {
    ...PRIMARY_FONT.medium,
    fontSize: responsiveFontSize(21),
    textAlign: IS_IOS ? 'center' : 'left',
    color: IS_IOS ? PlatformColor('label') : PlatformColor('@android:color/primary_text_light'),
  },
  button: {
    ...PRIMARY_FONT.regular,
    fontSize: responsiveFontSize(18),
  },
});
