import React, { FC } from 'react';
import Dialog from 'react-native-dialog';

import { useTranslate } from '@hooks';
import { styles } from './styles';

export interface AlertButton {
  text: string;
  onPress: () => void;
}

export interface AlertProps {
  text?: string;
  t?: string;
  options?: any;
  isVisible: boolean;
  setVisible: (value: boolean) => void;
  buttons?: AlertButton[];
}

export const Alert: FC<AlertProps> = ({ text, t, options, isVisible, setVisible, buttons }) => {
  const translate = useTranslate();
  const renderButtons = () =>
    buttons?.map((button) => (
      <Dialog.Button
        label={button.text}
        onPress={() => {
          button.onPress();
          setVisible(false);
        }}
        style={styles.button}
      />
    ));

  return (
    <Dialog.Container visible={isVisible}>
      <Dialog.Title style={styles.title}>{text || translate(t || '', options)}</Dialog.Title>
      {/* @ts-ignore */}
      {buttons ? (
        renderButtons()
      ) : (
        <Dialog.Button label="Ok" onPress={() => setVisible(false)} style={styles.button} />
      )}
    </Dialog.Container>
  );
};
