import React, { FC, useState } from 'react';

import { Alert, AlertButton } from './component';

interface ShowAlertParams {
  text?: string;
  t?: string;
  options?: any;
  buttons?: AlertButton[];
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const AlertContext = React.createContext({ showAlert: (_params: ShowAlertParams) => {} });

interface AlertContextProviderProps {
  children: React.ReactNode;
}

const initialState = {
  isVisible: false,
};

export const AlertContextProvider: FC<AlertContextProviderProps> = ({ children }) => {
  const [alertProps, setAlertProps] = useState(initialState);
  const showAlert = (params: ShowAlertParams) =>
    setAlertProps({
      ...params,
      isVisible: true,
    });
  const setVisible = (isVisible: boolean) =>
    setAlertProps({
      ...alertProps,
      isVisible,
    });

  return (
    <AlertContext.Provider
      value={{
        showAlert,
      }}
    >
      <Alert {...alertProps} setVisible={setVisible} />
      {children}
    </AlertContext.Provider>
  );
};
