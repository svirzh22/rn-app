import React, { FC, useMemo } from 'react';
import times from 'lodash/times';

import { COLORS, SPACING } from '@constants';
import { PlayerEntity } from '@api/resources/types/player';
import { Avatar } from '../../avatar';
import { FlexView } from '../../flex-view';
import { Gutter } from '../../gutter';
import { Icon } from '../../icon';
import { Text } from '../../text';

interface MembersProps {
  isCompleted: boolean;
  bookable?: boolean;
  members: PlayerEntity[];
}

export const Members: FC<MembersProps> = ({ isCompleted, bookable, members }) => {
  const data: PlayerEntity[] = useMemo(
    () => Object.values(members).filter((item) => item.id || item.addedId),
    [members],
  );

  if (isCompleted) {
    return <Text t="teeTime.complete" size={16} color={COLORS.GREY[700]} />;
  }

  if (bookable === false) {
    return <Text t="teeTime.impossible" size={16} color={COLORS.GREY[700]} />;
  }

  return (
    <>
      {times(4, (index) => (
        <FlexView key={`${index}`} flexDirection="row">
          {!!index && <Gutter amount={SPACING.XXS} />}
          {data[index] ? (
            <Avatar size={24} image={data[index]?.picture} />
          ) : (
            <Icon name="add-circle-dashed" color={COLORS.GREY[700]} size={24} />
          )}
        </FlexView>
      ))}
    </>
  );
};
