import React, { FC } from 'react';
import values from 'lodash/values';

import { TimeSlotEntity } from '@api/resources/time-slots/types';
import { APP_CURRENCY, COLORS, SPACING } from '@constants';
import { isTimeslotFilled } from '@helpers';
import { FlexView } from '../../flex-view';
import { Text } from '../../text';
import { Gutter } from '../../gutter';
import { Icon } from '../../icon';

import { styles } from './styles';
import { Members } from './members';
import { Offer } from './offer';

interface BookingSlotItemProps {
  item: TimeSlotEntity;
  bookable?: boolean;
  showBook?: boolean;
}

export const BookingSlotItem: FC<BookingSlotItemProps> = ({ item, bookable, showBook = true }) => {
  const isCompleted = isTimeslotFilled(item);
  const hasOffer = false;

  return (
    <FlexView style={styles.item}>
      <FlexView flexDirection="row">
        <Text label={item.start} size={16} />
        <Gutter />
        <Members bookable={bookable} isCompleted={isCompleted} members={values(item.players)} />
      </FlexView>

      {!isCompleted && bookable !== false && (
        <FlexView flexDirection="row" alignItems="center">
          {hasOffer && <Offer />}
          {!item.price && <Text label={`${item.price} ${APP_CURRENCY}`} size={16} />}
          <Gutter amount={SPACING.M - 2} />
          {showBook && <Text t="teeTime.book" size={16} color={COLORS.GREY[700]} />}
          <Gutter amount={SPACING.XS} />
          <Icon name="arrow-right" color={COLORS.GREY[700]} />
        </FlexView>
      )}
    </FlexView>
  );
};
