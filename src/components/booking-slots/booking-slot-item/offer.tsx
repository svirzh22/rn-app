import React, { FC } from 'react';

import { APP_CURRENCY, COLORS } from '@constants';
import { Text } from '../../text';
import { FlexView } from '../../flex-view';
import { styles } from './styles';

interface OfferProps {}

export const Offer: FC<OfferProps> = () => (
  <FlexView style={styles.offerContainer}>
    <FlexView style={styles.offer}>
      <Text t="teeTime.offer" size={8} color={COLORS.WHITE} weight="bold" textFormat="upper" />
    </FlexView>
    <Text label={`50 ${APP_CURRENCY}`} size={16} color={COLORS.RED} weight="medium" />
  </FlexView>
);
