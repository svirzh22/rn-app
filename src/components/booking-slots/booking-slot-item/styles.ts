import { StyleSheet } from 'react-native';

import { COLORS, SCREEN_WIDTH, SPACING } from '@constants';

export const BOOKING_ITEM_HEIGHT = 63;
export const styles = StyleSheet.create({
  item: {
    height: BOOKING_ITEM_HEIGHT,
    width: SCREEN_WIDTH,
    paddingHorizontal: SPACING.M,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  offer: {
    position: 'absolute',
    width: 30,
    height: 14,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor: COLORS.RED,
    bottom: 20,
    left: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  offerContainer: {
    marginRight: SPACING.XS,
  },
});
