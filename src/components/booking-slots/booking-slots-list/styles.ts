import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  contentContainerStyle: {
    flexGrow: 1,
    alignItems: 'center',
  },
});
