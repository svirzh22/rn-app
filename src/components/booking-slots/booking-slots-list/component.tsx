import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { FlatList, ListRenderItem } from 'react-native';
import { useSelector } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

import { isTimeslotFilled, opacity } from '@helpers';
import { TimeSlotEntity, TimeSlotsResult } from '@api/resources/time-slots/types';
import { COLORS } from '@constants';
import { memberSelector } from '@selectors';
import { useAlert } from '@hooks';
import { Divider } from '../../divider';
import { Touchable } from '../../touchable';
import { FlexView } from '../../flex-view';
import { Text } from '../../text';
import { ActivityIndicator } from '../../activity-indicator';
import { NoInternetConnection } from '../../no-internet-connection';
import { BookingSlotItem } from '../booking-slot-item';
import { styles } from './styles';
import { BOOKING_ITEM_HEIGHT } from '../booking-slot-item/styles';

interface BookingSlotsListProps {
  showBook?: boolean;
  data?: TimeSlotsResult;
  isLoading: boolean;
  isFetching: boolean;
  selectedTimeslot?: string;
  filterTimeslots: (data?: TimeSlotsResult) => TimeSlotEntity[] | undefined;
  refetch: () => void;
  onPressItem: (item: TimeSlotEntity) => void;
}

export const BookingSlotsList: FC<BookingSlotsListProps> = ({
  showBook,
  data,
  isLoading,
  isFetching,
  selectedTimeslot,
  filterTimeslots,
  refetch,
  onPressItem,
}) => {
  const flatListRef = useRef<FlatList<any>>();
  const { showAlert } = useAlert();
  const { isInternetReachable } = NetInfo.useNetInfo();
  const member = useSelector(memberSelector);
  const [canScroll, setCanScroll] = useState(false);
  const isFocused = useIsFocused();

  useFocusEffect(useCallback(() => {
    refetch();
    setCanScroll(true);
  }, [refetch]));

  useEffect(() => {
    const items = filterTimeslots(data);

    if (selectedTimeslot && isFocused && canScroll) {
      const itemIndex = items?.findIndex((item, index) => {
        if (item.start === selectedTimeslot) return index;

        return undefined;
      });

      // flatlist re-render
      if (itemIndex !== -1 && flatListRef) {
        setTimeout(() => {
          flatListRef.current?.scrollToIndex({ index: itemIndex || 0 });
          if (flatListRef.current) {
            setCanScroll(false);
          }
        }, 300);
      }
    }
  }, [canScroll, data, filterTimeslots, flatListRef, isFocused, selectedTimeslot]);

  const filteredTimeslots = filterTimeslots(data);

  const handlePress = useCallback(
    (item: TimeSlotEntity) => () => {
      const timeSlotPlayers = Object.values(item.players);
      if (!timeSlotPlayers.find((player) => player.id === member.id)) {
        onPressItem(item);
      } else if (isTimeslotFilled(item)) {
        showAlert({ t: 'errors.alreadyFilled' });
      } else {
        showAlert({ t: 'errors.alreadyBooked' });
      }
    },
    [member.id, onPressItem, showAlert],
  );

  const renderItem: ListRenderItem<TimeSlotEntity> = ({ item }) => (
    <Touchable onPress={handlePress(item)} disabled={data?.bookable === false}>
      <BookingSlotItem bookable={data?.bookable} item={item} showBook={showBook} />
    </Touchable>
  );
  const keyExtractor = (item: TimeSlotEntity) => item.start;
  const renderSeparator = () => <Divider />;

  if (isInternetReachable === false) {
    return <NoInternetConnection onPressRetry={refetch} />;
  }

  if (isLoading || isFetching) {
    return (
      <FlexView flex={1} justifyContent="center" alignItems="center">
        <ActivityIndicator size="large" />
      </FlexView>
    );
  }

  return (
    <FlatList
      data={filteredTimeslots}
      contentContainerStyle={styles.contentContainerStyle}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      ItemSeparatorComponent={renderSeparator}
      ref={flatListRef as any}
      getItemLayout={(_data, index) => ({
        length: BOOKING_ITEM_HEIGHT,
        offset: BOOKING_ITEM_HEIGHT * index,
        index,
      })}
      ListEmptyComponent={() => (
        <FlexView flex={1} justifyContent="center" alignItems="center">
          <Text t="teeTime.noTimeSlots" color={opacity(COLORS.BLACK)} size="L" />
        </FlexView>
      )}
    />
  );
};
