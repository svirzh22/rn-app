import { ReactNativeFile } from 'apollo-upload-client';

interface AvatarProps {
  image: ReactNativeFile;
  photoUrl?: string;
  bottomLabel?: string;
  avatarSize?: number;
  size: number;
  onGalleryOpen?: () => void;
  onCameraOpen?: () => void;
  onDelete?: () => void;
}
