import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  image: {
    // width: AVATAR_SIZE,
    // height: AVATAR_SIZE,
  },
  editIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 32,
    height: 32,
    borderRadius: 32,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
