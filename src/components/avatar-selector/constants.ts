import { translate } from '@helpers';

export const avatarModalItemList = [{
  customKey: 'gallery',
  label: translate('common.gallery'),
}, {
  customKey: 'camera',
  label: translate('common.camera'),
}, {
  withValue: true,
  customKey: 'delete',
  label: translate('common.delete'),
}];
