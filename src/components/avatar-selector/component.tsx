import React, { useCallback, useState } from 'react';
import { TouchableOpacity } from 'react-native';

import { Icon, FlexView } from '@components';
import { noop, shadowDepth } from '@helpers';
import { ModalSelector } from '../modal-selector';
import { avatarModalItemList } from './constants';
import { styles } from './styles';
import { AvatarProps } from './types';
import { Avatar } from '../avatar';

export const AvatarSelector: React.FC<AvatarProps> = ({
  image,
  photoUrl,
  size,
  onGalleryOpen = noop,
  onCameraOpen = noop,
  onDelete = noop,
}) => {
  const [modalVisible, setModalVisibility] = useState(false);
  const toggleModal = useCallback(() => setModalVisibility(!modalVisible), [modalVisible]);
  const modalItemList = avatarModalItemList.filter(
    ({ withValue }) => !withValue || (withValue && image && image.imageUri),
  );

  const handleSelectItem = useCallback((item) => {
    switch (item.customKey) {
      case 'gallery':
        // setTimeout required to fix modals conflict, when the previuos one is not finished
        setTimeout(onGalleryOpen, 700);
        break;
      case 'camera':
        setTimeout(onCameraOpen, 700);
        break;
      case 'delete':
        // wait till modal animation ends
        setTimeout(onDelete, 300);
        break;
      default:
        break;
    }
  }, [onCameraOpen, onDelete, onGalleryOpen]);

  const imageSource = image && image.imageUri ? image.imageUri : null;
  const photoUrlSource = photoUrl || null;

  return (
    <>
      <TouchableOpacity onPress={toggleModal}>
        <Avatar size={size} image={imageSource || photoUrlSource} />
        <FlexView style={[styles.editIcon, shadowDepth(1)]}>
          <Icon name="pencil" />
        </FlexView>
      </TouchableOpacity>
      <ModalSelector
        data={modalItemList}
        visible={modalVisible}
        onChange={handleSelectItem}
        onDismiss={toggleModal}
      />
    </>
  );
};
