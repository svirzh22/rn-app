import lowerFirst from 'lodash/lowerFirst';
import upperFirst from 'lodash/upperFirst';
import toUpper from 'lodash/toUpper';
import toLower from 'lodash/toLower';
import startCase from 'lodash/startCase';

export const TEXT_FORMATS = {
  lowerFirst,
  upperFirst,
  upper: toUpper,
  lower: toLower,
  upperEach: (text: string) => startCase(upperFirst(text)),
  none: String,
};

export const formatText = (format: keyof typeof TEXT_FORMATS, text: string) => TEXT_FORMATS[format](text);
