import React from 'react';
import { Text as RNText } from 'react-native';

import { COLORS, FONT_SIZES, PRIMARY_FONT } from '@constants';
import { responsiveFontSize } from '@helpers';
import { useTranslate } from '@hooks';
import { TextProps } from './types';
import { formatText } from './helpers';

export const Text = ({
  style,
  weight = 'regular',
  size = 'M',
  color = COLORS.BLACK,
  align = 'left',
  label = '',
  t = '',
  textFormat = 'none',
  lineHeight,
  translateOptions,
  ...rest
}: TextProps) => {
  const translate = useTranslate();

  const i18nText = t ? translate(t, translateOptions) : String(label);
  const text = textFormat !== 'none' ? formatText(textFormat, i18nText) : i18nText;
  const fontSize = typeof size === 'string' ? responsiveFontSize(FONT_SIZES[size]) : responsiveFontSize(size);
  const fontStyle = PRIMARY_FONT[weight];

  return (
    <RNText
      {...rest}
      style={[
        {
          fontSize,
          color,
          textAlign: align,
        },
        lineHeight ? { lineHeight: responsiveFontSize(lineHeight) } : null,
        fontStyle,
        style,
      ]}
    >
      {text}
    </RNText>
  );
};
