import { TextProps as TextProperties } from 'react-native';
import { TranslateOptions } from 'i18n-js';

import { FONT_SIZES, WeightType } from '@constants';
import { TEXT_FORMATS } from './helpers';

export interface TextProps extends TextProperties {
  t?: string; // Translate key
  label?: string | number | null;
  weight?: WeightType;
  size?: number | keyof typeof FONT_SIZES;
  color?: string;
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  lineHeight?: number;
  letterSpacing?: number;
  textFormat?: keyof typeof TEXT_FORMATS;
  translateOptions?: TranslateOptions;
}
