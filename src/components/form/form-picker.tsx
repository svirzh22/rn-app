import { useField } from 'formik';
import * as React from 'react';

import { Picker, PickerProps } from '../pickers/picker';

interface FormPickerProps extends Partial<PickerProps> {
  options?: any;
  name: string;
}

export const FormPicker: React.FC<FormPickerProps> = ({
  options = {},
  name,
  // tslint:disable-next-line
  ...props
}) => {
  // eslint-disable-next-line no-empty-pattern
  const [field, {}, { setValue }] = useField({ name, type: 'number' });
  const handleChange = (value: any) => {
    setValue(value);
  };

  return (
    <Picker
      {...props}
      {...options}
      value={field.value}
      onValueChange={handleChange}
    />
  );
};
