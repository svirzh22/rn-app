import { useField } from 'formik';
import React, { useEffect } from 'react';

import { PhoneInput, PhoneInputProps } from '../inputs';
import { styles } from './styles';

interface FormPhoneInputProps extends PhoneInputProps {
  options: any;
  name: string;
  initialValue?: string;
}

export const FormPhoneInput: React.FC<FormPhoneInputProps> = ({
  options = {},
  name,
  initialValue,
  // tslint:disable-next-line
  ...props
}) => {
  const [field, { touched, error }, { setValue }] = useField(name);
  const { onChange, onBlur } = field;

  useEffect(() => {
    initialValue && setValue(initialValue);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <PhoneInput
      containerStyle={[styles.input, options.containerStyle]}
      style={options.inputStyle}
      onChangeText={onChange(name)}
      onBlur={onBlur(name)}
      error={touched && error}
      {...props}
      {...options}
      value={field.value}
    />
  );
};
