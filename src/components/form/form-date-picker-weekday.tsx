import { DatePickerWeekDay } from '@components/pickers';
import { useField } from 'formik';
import * as React from 'react';

import { DatePickerProps } from '../pickers/date-picker';

interface FormDatePickerWeekdayProps extends Partial<DatePickerProps> {
  options?: any;
  name: string;
}

export const FormDatePickerWeekday: React.FC<FormDatePickerWeekdayProps> = ({
  options = {},
  name,
  // tslint:disable-next-line
  ...props
}) => {
  const [field, { error }, { setValue }] = useField(name);
  const handleChange = (value: any) => {
    setValue(value);
  };

  return (
    <DatePickerWeekDay
      {...props}
      {...options}
      onChangeDate={handleChange}
      value={field.value}
      error={error}
    />
  );
};
