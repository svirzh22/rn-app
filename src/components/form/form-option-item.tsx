import { useField } from 'formik';
import * as React from 'react';

import { OptionItem } from '@components/option-item';
import { StyleProp, View, ViewStyle } from 'react-native';

interface FormOptionItemProps {
  label?: string;
  t?: string;
  cost: number;
  maxValue: number;
  style?: StyleProp<ViewStyle>;
  onPressInfo?: () => void;
  forwardRef?: React.RefObject<View>;
  name: string;
}

export const FormOptionItem: React.FC<FormOptionItemProps> = ({
  name,
  // tslint:disable-next-line
  ...props
}) => {
  const [field] = useField(name);
  const { onChange } = field;

  // @ts-ignore
  return <OptionItem {...props} onChange={onChange(name)} value={+field.value || 0} />;
};
