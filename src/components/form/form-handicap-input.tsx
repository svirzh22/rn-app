import { useField } from 'formik';
import * as React from 'react';
import { View } from 'react-native';

import { HandicapInput } from '../inputs';

interface FormHandicapInputProps {
  forwardRef?: React.RefObject<View>;
  name: string;
}

export const FormHandicapInput: React.FC<FormHandicapInputProps> = ({
  name,
  // tslint:disable-next-line
  ...props
}) => {
  const [field, { error }] = useField(name);
  const { onChange } = field;

  // @ts-ignore
  return <HandicapInput {...props} onChange={onChange(name)} value={field.value} error={error} />;
};
