import { useField } from 'formik';
import * as React from 'react';
import { NativeSyntheticEvent } from 'react-native';

import { Input, InputProps } from '../inputs/text-input';
import { styles } from './styles';

interface FormTextInputProps extends InputProps {
  options: any;
  name: string;
  initialValue?: string;
}

export const FormTextInput: React.FC<FormTextInputProps> = ({
  options = {},
  name,
  // tslint:disable-next-line
  ...props
}) => {
  const [field, { touched, error }] = useField(name);
  const { onChange, onBlur } = field;
  const handleFieldChange = onChange(name);
  const handleFieldBlur = onBlur(name);
  const handleBlur = (evt: NativeSyntheticEvent<any>) => {
    handleFieldChange(field.value.trim());
    handleFieldBlur(evt);
  };

  return (
    <Input
      inputWrapperStyle={[styles.input, options.containerStyle]}
      style={options.inputStyle}
      onChangeText={handleFieldChange}
      onBlur={handleBlur}
      error={touched && error}
      {...props}
      {...options}
      value={field.value}
    />
  );
};
