import { useField } from 'formik';
import * as React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';

import { OptionItemCheckbox, OptionItemCheckboxProps } from '@components/option-item-checkbox';

interface FormOptionItemProps extends Partial<OptionItemCheckboxProps> {
  label?: string;
  t?: string;
  cost: number;
  style?: StyleProp<ViewStyle>;
  info?: string;
  forwardRef?: React.RefObject<View>;
  name: string;
  onPressInfo?: () => void;
}

export const FormOptionItemCheckbox: React.FC<FormOptionItemProps> = ({
  name,
  // tslint:disable-next-line
  ...props
}) => {
  // eslint-disable-next-line no-empty-pattern
  const [field, {}, { setValue }] = useField({ name, type: 'boolean' });

  const handlePress = () => {
    setValue(!field.value);
  };

  // @ts-ignore
  return <OptionItemCheckbox {...props} onPress={handlePress} value={field.value} />;
};
