import { useField } from 'formik';
import * as React from 'react';

import { DatePicker, DatePickerProps } from '../pickers/date-picker';

interface FormDatePickerProps extends Partial<DatePickerProps> {
  options?: any;
  name: string;
}

export const FormDatePicker: React.FC<FormDatePickerProps> = ({
  options = {},
  name,
  // tslint:disable-next-line
  ...props
}) => {
  const [field, { error }, { setValue }] = useField(name);
  const handleChange = (value: any) => {
    setValue(value);
  };

  return (
    <DatePicker
      {...props}
      {...options}
      onChangeDate={handleChange}
      value={field.value}
      error={error}
    />
  );
};
