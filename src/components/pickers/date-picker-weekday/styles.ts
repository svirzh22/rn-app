import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    marginLeft: 0,
    marginBottom: 0,
    width: '100%',
    justifyContent: 'flex-end',
  },

  item: {
    color: COLORS.BLACK,
    fontSize: 19,
  },
  itemDay: {
    color: COLORS.BLACK,
    fontSize: 19,
  },
  itemMonth: {
    color: COLORS.BLACK,
    fontSize: 19,
  },
  picker: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    borderColor: COLORS.GREY[500],
    borderWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 8,
    flexDirection: 'row',
    paddingHorizontal: SPACING.S - 4,
  },
  pickerContainer: {
    height: 44,
    marginBottom: 20,
  },
});
