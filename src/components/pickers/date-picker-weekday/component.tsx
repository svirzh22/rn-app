import React, { FC, useCallback, useState, useEffect } from 'react';
import WheelPicker from '@gregfrench/react-native-wheel-picker';
import moment from 'moment';
import { useSelector } from 'react-redux';

import { localeSelector } from '@selectors';
import { CustomModal } from '@components/custom-modal';
import { DATE_FORMAT, getDate, getMonthDays, getWeekDay } from '@helpers';
import { Button } from '@components/buttons/button';
import { SCREEN_WIDTH, SPACING } from '@constants';
import { FlexView } from '@components/flex-view';
import { Touchable } from '@components/touchable';
import { Icon } from '@components/icon';
import { Gutter } from '@components/gutter';
import { Text } from '@components/text';
import { noop } from 'lodash';
import { styles } from './styles';
import { DatePickerWeekdayProps } from './types';

const MIN_DATE = new Date();
const MAX_DATE = new Date(2025, 5);
const CURRENT_MONTH = MIN_DATE.getMonth();
const CURRENT_DAY = MIN_DATE.getDate();
const CURRENT_YEAR = MIN_DATE.getFullYear();
const MIN_YEAR = MIN_DATE.getFullYear();
const MAX_YEAR = MAX_DATE.getFullYear();

const getItemSpace = (locale: string) => {
  switch (locale) {
    case 'en':
      return [36, 40, 40];
    case 'fr':
      return [40, 40, 44];
    case 'de':
      return [37, 40, 40];
    default: return [40, 40, 40];
  }
};

export const DatePickerWeekDay: FC<DatePickerWeekdayProps> = ({
  value,
  label,
  style,
  containerStyle,
  maximumDate,
  minimumDate,
  onHide = noop,
  onChangeDate,
}) => {
  const years: number[] = [];
  const maxYear = maximumDate ? maximumDate.getFullYear() : MAX_YEAR;
  const minYear = minimumDate ? minimumDate.getFullYear() : MIN_YEAR;

  for (let index = minYear; index <= maxYear; index += 1) {
    years.push(index);
  }
  const locale = useSelector(localeSelector);
  const [months, setMonths] = useState(moment.months());
  const [weekdays, setWeekdays] = useState(moment.weekdaysShort());
  const [year, setYear] = useState(years[0]);
  const [month, setMonth] = useState(CURRENT_MONTH);
  const [day, setDay] = useState(CURRENT_DAY);
  const [currentValue, setCurrentValue] = useState(value.valueOf());
  const [isVisible, setIsVisible] = useState(false);

  const handleValueChange = useCallback((type) => (pickerValue: any) => {
    const selectedMonth = type === 'M' ? pickerValue : month;
    const selectedYear = type === 'Y' ? pickerValue : year;
    let selectedDay = type === 'D' ? pickerValue : day;
    const possibleDate = getDate(currentValue, selectedMonth, selectedYear, selectedDay);

    if (type === 'Y') {
      setYear(pickerValue);
    } else if (type === 'D') {
      setDay(pickerValue);
    } else {
      setMonth(pickerValue);
      if (possibleDate.month() !== selectedMonth) {
        setDay(1);
        selectedDay = 1;
      }
    }

    const nextDate = getDate(currentValue, selectedMonth, selectedYear, selectedDay);
    const nextDateUnix = nextDate.valueOf();

    setCurrentValue(nextDateUnix < Date.now() ? Date.now() : nextDateUnix);
  }, [currentValue, day, month, year]);

  useEffect(() => {
    const weekDaysISO = moment.localeData(locale).weekdaysShort();
    weekDaysISO.push(weekDaysISO[0]);

    setMonths(moment.localeData(locale).months());
    setWeekdays(weekDaysISO);
  }, [locale]);

  const handleConfirm = useCallback(() => {
    onChangeDate(currentValue);
    setIsVisible(false);
    onHide();
  }, [currentValue, onChangeDate, onHide]);
  const monthItems = year === years[0]
    ? months.map((item, index) => ({
      label: item,
      value: index,
    })).filter((_item, index) => index > CURRENT_MONTH - 1)
    : months.map((item, index) => ({
      label: item,
      value: index,
    }));
  const daysAmount = getMonthDays(currentValue);

  const dayItems = [];
  const currentDate = moment(currentValue);

  for (
    let index = currentDate.month() === CURRENT_MONTH && currentDate.year() === CURRENT_YEAR
      ? moment().date()
      : 1;
    index <= daysAmount;
    index += 1
  ) {
    dayItems.push({
      label: `${weekdays[getWeekDay(currentValue, index)]} ${index}`,
      value: index,
    });
  }
  const itemSpaces = getItemSpace(locale);

  const openModal = useCallback(() => setIsVisible(true), []);
  const closeModal = useCallback(() => setIsVisible(false), []);

  return (
    <FlexView style={[styles.pickerContainer, containerStyle]}>
      <Touchable onPress={openModal} style={[styles.picker, style]}>
        <FlexView flexDirection="row" justifyContent="center" alignItems="center">
          <Icon name="calendar" size={16} />
          <Gutter amount={SPACING.XS + 2} />
          <Text t={label || 'common.date'} size={16} />
        </FlexView>
        <Text label={moment(value).format(DATE_FORMAT)} size={16} weight="medium" />
      </Touchable>
      <CustomModal
        visible={isVisible}
        onCancel={closeModal}
      >
        <FlexView useSafeArea>
          <FlexView flexDirection="row" style={{ backgroundColor: '#ffffff' }}>
            <WheelPicker
              selectedValue={moment(currentValue).month()}
              style={{ width: SCREEN_WIDTH / 2, height: 200 }}
              itemStyle={styles.itemMonth}
              itemSpace={itemSpaces[0]}
              onValueChange={handleValueChange('M')}
            >
              {monthItems.map((item) => (
                <WheelPicker.Item
                  label={item.label}
                  value={item.value}
                  key={`picker_item_day_${item}`}
                />
              ))}
            </WheelPicker>
            <WheelPicker
              selectedValue={moment(currentValue).date()}
              style={{ width: SCREEN_WIDTH / 4, height: 200 }}
              itemStyle={styles.itemDay}
              itemSpace={itemSpaces[1]}
              onValueChange={handleValueChange('D')}
            >
              {dayItems.map((item) => (
                <WheelPicker.Item
                  label={item.label}
                  value={item.value}
                  key={`picker_item_month_${item}`}
                />
              ))}
            </WheelPicker>
            <WheelPicker
              selectedValue={moment(currentValue).year()}
              style={{ width: SCREEN_WIDTH / 4, height: 200 }}
              itemStyle={styles.item}
              itemSpace={itemSpaces[2]}
              onValueChange={handleValueChange('Y')}
            >
              {years.map((item) => (
                <WheelPicker.Item
                  label={item.toString()}
                  value={item}
                  key={`picker_item_year_${item}`}
                />
              ))}
            </WheelPicker>
          </FlexView>
          <Button onPress={handleConfirm} t="common.ok" preset="flat" />
        </FlexView>
      </CustomModal>
    </FlexView>
  );
};
