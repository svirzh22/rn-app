export interface DatePickerWeekdayProps {
  value: number;
  maximumDate?: Date;
  minimumDate?: Date;
  label?: string;
  style?: any;
  containerStyle?: any;
  onHide?: () => void;
  onChangeDate: (date: number) => void;
}
