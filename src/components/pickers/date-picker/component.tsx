import React, { FC, useState } from 'react';
import moment from 'moment';
import DateTimePicker from 'react-native-date-picker';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import Modal from 'react-native-modal';
import { useSelector } from 'react-redux';

import { localeSelector } from '@selectors';
import { SCREEN_WIDTH, SPACING } from '@constants';
import { IconNameType, FlexView, Button, Text, Touchable, Icon, Gutter } from '@components';
import { callSafely, DATE_FORMAT, DATE_TIME_FORMAT, TIME_FORMAT } from '@helpers';
import { styles } from './styles';

type Mode = 'time' | 'date' | 'datetime';

export interface DatePickerProps {
  containerStyle?: StyleProp<ViewStyle>;
  textInputStyle?: StyleProp<TextStyle>;
  labelStyle?: StyleProp<TextStyle>;
  style?: StyleProp<ViewStyle>;
  value: string;
  rightIcon?: IconNameType;
  type?: Mode;
  error?: string;
  maximumDate?: Date;
  minimumDate?: Date;
  onChangeDate: (text?: any) => void;
  label?: string;
  children?: React.ReactNode;
}

export const DatePicker: FC<DatePickerProps> = ({
  value,
  error,
  style,
  type = 'date',
  maximumDate,
  onChangeDate,
  containerStyle,
  label,
  children,
  ...props
}) => {
  const initialMode = type === 'time' ? 'time' : 'date';
  const locale = useSelector(localeSelector);

  const formatType = (): string => {
    if (type === 'time') {
      return TIME_FORMAT;
    }

    if (type === 'date') {
      return DATE_FORMAT;
    }

    return DATE_TIME_FORMAT;
  };

  const getInitialDate = () => {
    if (value) {
      return moment(value, formatType()).toDate();
    }
    if (maximumDate && Date.now() > +maximumDate) {
      return maximumDate;
    }

    return new Date();
  };

  const [isVisible, setVisible] = useState<boolean>(false);
  const [mode, setMode] = useState<Mode>(initialMode);
  const [date, setDate] = useState<Date>(getInitialDate());
  const hideDatePicker = () => setVisible(false);

  const submit = () => {
    const shouldShowTimePicker = type === 'datetime' && mode === 'date';
    if (shouldShowTimePicker) {
      return setMode('time');
    }

    const timestamp = +date;
    callSafely(onChangeDate, timestamp);

    return hideDatePicker();
  };

  const onChange = (newDate: Date) => {
    const timestamp = +newDate;

    if (timestamp) {
      setDate(new Date(timestamp));
      if (!isVisible) {
        setVisible(true);
      }
    }
  };

  const showDatePicker = () => {
    setMode(initialMode);
    setVisible(true);
    onChange(date);
  };

  return (
    <FlexView style={[styles.pickerContainer, containerStyle]}>
      <Touchable onPress={showDatePicker} style={[styles.picker, style]}>
        {children || (
          <>
            <FlexView flexDirection="row" justifyContent="center" alignItems="center">
              <Icon name="calendar" size={16} />
              <Gutter amount={SPACING.XS + 2} />
              <Text t={label || 'common.date'} size={16} />
            </FlexView>
            <Text label={moment(value || date).format(formatType())} size={16} weight="medium" />
          </>
        )}
      </Touchable>
      <Modal
        isVisible={isVisible}
        onBackdropPress={hideDatePicker}
        style={styles.modal}
        // @ts-expect-error
        onRequestClose={hideDatePicker}
      >
        {isVisible && (
          <FlexView style={styles.datePickerContainer} useSafeArea>
            <FlexView style={styles.datePickerWrapper}>
              <DateTimePicker
                date={date}
                mode={mode}
                onDateChange={onChange}
                androidVariant="iosClone"
                style={{ width: SCREEN_WIDTH }}
                locale={locale}
                maximumDate={maximumDate}
                {...props}
              />
            </FlexView>
            <Button onPress={submit} t="common.ok" preset="flat" />
          </FlexView>
        )}
      </Modal>
    </FlexView>
  );
};
