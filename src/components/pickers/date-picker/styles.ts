import { Platform, StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    marginLeft: 0,
    marginBottom: 0,
    width: '100%',
    justifyContent: 'flex-end',
  },
  datePickerContainer: {
    backgroundColor: COLORS.WHITE,
    width: '100%',
  },
  datePickerWrapper: {
    ...Platform.select({
      android: {
        paddingVertical: SPACING.S,
      },
    }),
  },
  picker: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    borderColor: COLORS.GREY[500],
    borderWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 8,
    paddingHorizontal: SPACING.S - 4,
  },
  pickerContainer: {
    height: 44,
    marginBottom: 20,
  },
});
