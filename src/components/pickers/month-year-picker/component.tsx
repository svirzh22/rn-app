import React, { FC, useCallback, useState, useEffect } from 'react';
import WheelPicker from '@gregfrench/react-native-wheel-picker';
import moment from 'moment';
import { useSelector } from 'react-redux';

import { localeSelector } from '@selectors';
import { CustomModal } from '@components/custom-modal';
import { FlexView } from '@components/flex-view';
import { getMonthYearDate } from '@helpers';
import { Button } from '@components/buttons/button';
import { SCREEN_WIDTH } from '@constants';
import { styles } from './styles';

interface MonthYearPickerProps {
  isVisible: boolean;
  selectedDate: Date;
  onHide: () => void;
  onDateChange: (date: number) => void;
}

const MIN_DATE = new Date();
const MAX_DATE = new Date(2025, 5);
const CURRENT_MONTH = MIN_DATE.getMonth();
const MIN_YEAR = MIN_DATE.getFullYear();
const MAX_YEAR = MAX_DATE.getFullYear();
const years: number[] = [];
for (let index = MIN_YEAR; index <= MAX_YEAR; index += 1) {
  years.push(index);
}

export const MonthYearPicker: FC<MonthYearPickerProps> = ({
  isVisible,
  onHide,
  selectedDate,
  onDateChange,
}) => {
  const locale = useSelector(localeSelector);
  const [months, setMonths] = useState(moment.months());
  const [year, setYear] = useState(years[0]);
  const [month, setMonth] = useState(CURRENT_MONTH);
  const [value, setValue] = useState(selectedDate.valueOf());
  const handleValueChange = useCallback((type) => (pickerValue: any) => {
    const selectedMonth = type === 'Y' ? month : pickerValue;
    const selectedYear = type === 'M' ? year : pickerValue;
    if (type === 'Y') {
      setYear(pickerValue);
    } else {
      setMonth(selectedMonth);
    }

    const nextDate = getMonthYearDate(selectedDate, selectedMonth, selectedYear).valueOf();
    setValue(nextDate < Date.now() ? Date.now() : nextDate);
  }, [month, selectedDate, year]);

  useEffect(() => {
    setMonths(moment.localeData(locale).months());
  }, [locale]);

  const handleConfirm = useCallback(() => {
    onDateChange(value);
    onHide();
  }, [onDateChange, onHide, value]);
  const monthItems = year === years[0]
    ? months.map((item, index) => ({
      label: item,
      value: index,
    })).filter((_item, index) => index > CURRENT_MONTH - 1)
    : months.map((item, index) => ({
      label: item,
      value: index,
    }));

  return (
    <CustomModal
      visible={isVisible}
      onCancel={onHide}
    >
      <FlexView useSafeArea>
        <FlexView flexDirection="row" style={{ backgroundColor: '#ffffff' }}>
          <WheelPicker
            selectedValue={moment(value).month()}
            style={{ width: SCREEN_WIDTH / 2, height: 200 }}
            itemStyle={styles.itemMonth}
            onValueChange={handleValueChange('M')}
          >
            {monthItems.map((item) => (
              <WheelPicker.Item
                label={item.label}
                value={item.value}
                key={`picker_item_month_${item}`}
              />
            ))}
          </WheelPicker>
          <WheelPicker
            selectedValue={moment(value).year()}
            style={{ width: SCREEN_WIDTH / 2, height: 200 }}
            itemStyle={styles.item}
            onValueChange={handleValueChange('Y')}
          >
            {years.map((item) => (
              <WheelPicker.Item
                label={item.toString()}
                value={item}
                key={`picker_item_year_${item}`}
              />
            ))}
          </WheelPicker>
        </FlexView>
        <Button onPress={handleConfirm} t="common.ok" preset="flat" />
      </FlexView>
    </CustomModal>
  );
};
