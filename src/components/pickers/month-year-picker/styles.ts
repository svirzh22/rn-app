import { Platform, StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    marginLeft: 0,
    marginBottom: 0,
    width: '100%',
    justifyContent: 'flex-end',
  },

  item: {
    color: COLORS.BLACK,
    fontSize: 26,
    padding: 10,
  },
  itemMonth: {
    color: COLORS.BLACK,
    fontSize: Platform.OS === 'ios' ? 26 : 21,
  },
});
