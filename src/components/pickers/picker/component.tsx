import React, { FC, useCallback, useState } from 'react';
import { ViewStyle } from 'react-native';
import Modal from 'react-native-modal';
import WheelPicker from '@gregfrench/react-native-wheel-picker';

import { Button, FlexView, Gutter, Icon, Text, Touchable } from '@src/components';
import { SCREEN_WIDTH, SPACING } from '@constants';
import { styles } from './styles';

interface Item {
  label: string;
  value: any;
}

export interface PickerProps {
  items: Item[];
  onValueChange: (value: any) => void;
  value: string;
  style?: ViewStyle;
  label: string;
}

export const Picker: FC<PickerProps> = ({ items, onValueChange, value, style, label, ...props }) => {
  const [selected, setSelected] = useState(value);
  const [isVisible, setVisible] = useState<boolean>(false);

  const showPicker = useCallback(() => setVisible(true), []);
  const hidePicker = useCallback(() => setVisible(false), []);
  const handleValueChange = useCallback((newValue: any) => setSelected(newValue), []);
  const handlePress = useCallback(() => {
    onValueChange(selected);
    hidePicker();
  }, [hidePicker, onValueChange, selected]);

  return (
    <>
      <Touchable onPress={showPicker} style={[styles.pickerContainer, style]}>
        <FlexView flexDirection="row" justifyContent="center" alignItems="center">
          <Icon name="golf-player" size={16} />
          <Gutter amount={SPACING.XS + 2} />
          <Text t={label} size={16} />
        </FlexView>
        <Text label={value} size={16} weight="medium" />
      </Touchable>
      <Modal
        isVisible={isVisible}
        onBackdropPress={hidePicker}
        style={styles.modal}
        // @ts-expect-error
        onRequestClose={hidePicker}
      >
        {isVisible && (
          <FlexView style={styles.container} useSafeArea>
            <WheelPicker
              selectedValue={selected}
              style={{ width: SCREEN_WIDTH, height: 200 }}
              itemStyle={styles.item}
              onValueChange={handleValueChange}
              {...props}
            >
              {items.map((item) => (
                <WheelPicker.Item
                  label={item.label}
                  value={item.value}
                  key={`picker_item_${item.label}`}
                />
              ))}
            </WheelPicker>
            <Button onPress={handlePress} t="common.ok" preset="flat" />
          </FlexView>
        )}
      </Modal>
    </>
  );
};
