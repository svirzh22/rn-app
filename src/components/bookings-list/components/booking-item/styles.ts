import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    paddingVertical: SPACING.S - 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    height: 72,
  },
  disabled: {
    opacity: 0.5,
  },
});
