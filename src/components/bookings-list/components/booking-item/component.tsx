import React, { FC, useMemo } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import moment from 'moment';
import noop from 'lodash/noop';
import find from 'lodash/find';

import { BookingEntity } from '@api/resources/types/booking';
import { FlexView, Text, Icon, Avatar, Gutter, Touchable } from '@components';
import { COLORS, SPACING } from '@constants';
import { DATE_TIME_FORMAT } from '@helpers';
import { styles } from './styles';

interface BookingItemProps {
  item: BookingEntity;
  showArrow?: boolean;
  readonly?: boolean;
  style?: StyleProp<ViewStyle>;
  onPress?: () => void;
  golfCourses: CourseEntity[];
}

export const BookingItem: FC<BookingItemProps> = ({
  item,
  showArrow = true,
  style = {},
  onPress = noop,
  golfCourses,
  readonly,
}) => {
  const disabled = readonly || +moment(item.timeSlot) < Date.now();
  const title = useMemo(() => find(golfCourses, ['id', item.golfCourse])?.title, [
    golfCourses,
    item.golfCourse,
  ]);
  const date = moment(item.timeSlot).format(
    `${Date.now() < +moment(item.timeSlot) ? 'dddd, ' : ''}${DATE_TIME_FORMAT}`,
  );

  const players = Object.keys(item.players);

  return (
    <Touchable
      disabled={false}
      style={[styles.container, disabled && !readonly && styles.disabled, style]}
      onPress={onPress}
    >
      <FlexView>
        <Text label={title} size={18} />
        <Text label={date} size={14} color={COLORS.GREY[700]} />
      </FlexView>
      <FlexView flexDirection="row" justifyContent="center" alignItems="center">
        {players.map((key, index) => {
          // @ts-ignore
          const user = item.players[key];

          if (user.id || user.addedId) {
            return (
              <FlexView flexDirection="row" key={user.id || user.addedId}>
                {!!index && <Gutter amount={4} />}
                <Avatar image={user.picture} size={24} />
              </FlexView>
            );
          }

          return null;
        })}
        {showArrow && (
          <>
            <Gutter amount={SPACING.XS + 4} />
            <Icon name="arrow-right" color={COLORS.GREY[700]} />
          </>
        )}
      </FlexView>
    </Touchable>
  );
};
