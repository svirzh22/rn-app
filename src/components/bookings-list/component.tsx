import React, { FC, useCallback } from 'react';
import { FlatList, ListRenderItem } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import { useQuery } from 'react-query';

import { courseResource } from '@api';
import { BookingEntity } from '@api/resources/types/booking';
import { FlexView, Text } from '@components';
import { COLORS } from '@constants';
import { opacity } from '@helpers';
import { BookingItem } from './components';
import { styles } from './styles';

interface BookingsListProps {
  bookings: BookingEntity[];
  onPress: (item: BookingEntity) => void;
}

export const BookingsList: FC<BookingsListProps> = ({ bookings, onPress }) => {
  const { data } = useQuery(
    [courseResource.cacheKey],
    courseResource.getCourses,
    { retry: 0 },
  );
  const handlePress = useCallback(
    (item: BookingEntity) => () => {
      onPress(item);
    },
    [onPress],
  );
  const renderItem: ListRenderItem<BookingEntity> = ({ item }) => (
    <BookingItem item={item} onPress={handlePress(item)} golfCourses={data?.courses || []} />
  );
  const keyExtractor = (item: BookingEntity) => `${item.id}`;
  const renderSeparator = () => <FlexView style={styles.separator} />;

  return (
    <FlatList
      style={{ width: '100%' }}
      data={bookings}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      ItemSeparatorComponent={renderSeparator}
      ListEmptyComponent={() => (
        <FlexView style={styles.emptyMessageContainer}>
          <Text t="home.noBookings" color={opacity(COLORS.BLACK)} />
        </FlexView>
      )}
      contentContainerStyle={isEmpty(bookings) && styles.bookingsListContainer}
    />
  );
};
