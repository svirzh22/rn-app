import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from '@constants';

const { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
  backdrop: {
    height,
    width,
    overflow: 'visible',
    top: 0,
    left: 0,
    position: 'absolute',
    backgroundColor: COLORS.BLACK,
    elevation: 2,
    opacity: 0.6,
    zIndex: 999,
  },
});
