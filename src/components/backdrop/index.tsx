import { StatusBar } from '@components/status-bar';
import React from 'react';
import {
  View,
} from 'react-native';

import { styles } from './style';

interface BackdropProps {
  visible: boolean;
}

export const Backdrop: React.FC<BackdropProps> = ({
  visible,
}) => (visible ? (
  <>
    <View style={styles.backdrop} />
    <StatusBar backgroundColor="transparent" barStyle="light-content" />
  </>
) : null);
