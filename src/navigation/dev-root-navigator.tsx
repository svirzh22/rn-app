import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import RNBootSplash from 'react-native-bootsplash';

import { isRehydratedSelector, memberSelector } from '@selectors';
import { LoginNavigator, AppNavigator } from '@navigation';
import { useNavigationStatePersist, useVersionChecker } from '@hooks';

export const DevRootNavigator = () => {
  const member = useSelector(memberSelector);
  const isRehydrated = useSelector(isRehydratedSelector);
  const [isFinished, setIsFinished] = useState(false);

  const { compareVersions } = useVersionChecker();

  useEffect(() => {
    if (isFinished) compareVersions();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFinished]);

  const { isReady, initialState, handleStateChange } = useNavigationStatePersist({
    onLoadFinish: () => {
      RNBootSplash.hide({ fade: true });
      // cb called 2 times for some reason
      setIsFinished(true);
    },
  });

  const handleNavigationStateChange = (state: any) => {
    handleStateChange(state);
  };

  if (isReady && isRehydrated) {
    return (
      <NavigationContainer
        initialState={initialState}
        onStateChange={handleNavigationStateChange}
      >
        {member?.id ? <AppNavigator /> : <LoginNavigator />}
      </NavigationContainer>
    );
  }

  return null;
};
