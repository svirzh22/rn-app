import * as React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { TournamentsScreen } from '@screens';
import { defaultHeaderOptions } from './default-options';

type ParamList = {
  Tournaments: undefined;
};

const Stack = createStackNavigator<ParamList>();

export const TournamentsNavigator = () => (
  <Stack.Navigator screenOptions={defaultHeaderOptions}>
    <Stack.Screen name="Tournaments" component={TournamentsScreen} />
  </Stack.Navigator>
);
