import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RNBootSplash from 'react-native-bootsplash';
import { useSelector } from 'react-redux';

import { LoginNavigator, AppNavigator } from '@navigation';
import { memberSelector, isRehydratedSelector } from '@selectors';
import { useVersionChecker } from '@hooks';

export const RootNavigator = () => {
  const member = useSelector(memberSelector);
  const isRehydrated = useSelector(isRehydratedSelector);
  const { compareVersions } = useVersionChecker();

  useEffect(() => {
    if (isRehydrated) {
      RNBootSplash.hide({ fade: true });
      compareVersions();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isRehydrated]);

  if (isRehydrated) {
    return <NavigationContainer>{member?.id ? <AppNavigator /> : <LoginNavigator />}</NavigationContainer>;
  }

  return null;
};
