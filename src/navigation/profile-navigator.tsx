import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProfileScreen } from '@screens';
import { defaultHeaderOptions, transparentScreenOptions } from './default-options';

type ParamList = {
  Profile: undefined;
  MyBookings: undefined;
};

const Stack = createStackNavigator<ParamList>();

export const ProfileNavigator = () => (
  <Stack.Navigator screenOptions={defaultHeaderOptions}>
    <Stack.Screen name="Profile" component={ProfileScreen} options={{ ...transparentScreenOptions }} />
  </Stack.Navigator>
);
