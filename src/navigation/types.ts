import { SaferPayDetails } from '@api/resources/booking-confirm/types';
import { BookingMemberEntity } from '@api/resources/types/booking';
import { PlayerEntity, PlayerEntityInternal } from '@api/resources/types/player';

export type LoginNavigatorParams = {
  Welcome?: {};
  Login?: {};
  VerificationCode: {
    mobilePhone: string;
    countryCode: string;
    devOtp: string;
    birthDate?: string;
    errorMessage?: string;
  };
  ChooseLanguage?: {};
  ChooseBirthday: {
    mobilePhone: string;
    countryCode: string;
  };
  SignUp: {
    phoneNumber: string;
  };
};

export type AppNavigatorParams = {
  Home: {};
  MyBookings?: {};
  Profile?: {};
  ChooseLanguage?: {};
  ProfileEdit?: {};
  Tabs?: {};
  TeeTime: {
    bookingDate: number;
    golfCourse: number;
    players: number;
    onChangeDate: (date: number) => void;
  };
  Reservation: {
    bookingDate: number;
    golfCourse: number;
    golfCourseTitle?: string;
    time: string;
    timeslotPlayers: PlayerEntity[];
    availability: number;
    newPlayers?: PlayerEntityInternal[];
  };
  ReservationDetails: {
    reservationId: number;
  };
  ReservationEdit: {
    reservationId: number;
    visitor?: BookingMemberEntity;
    newPlayers?: BookingMemberEntity[];
    removedPlayers?: number[];
  };
  AddVisitor: {
    visitor?: BookingMemberEntity;
  };
  AddPlayer: {
    available: number;
    routeName: string;
    currentPlayers?: PlayerEntityInternal[];
    visitor?: PlayerEntity;
  };
  ChoosePaymentMethod: {
    bookingId: number;
  };
  OnlinePayment: {
    paymentMethod: string;
    booking: number;
  };
  ProcessPayment: {
    saferPay: SaferPayDetails;
    booking: number;
  };
  BookingCompleted: {
    booking: number;
  };
  Course: {
    golfCourse: {
      id: number;
      title: string;
      url: string;
    };
  };
  CourseHole: {
    golfCourse: {
      id: number;
      title: string;
      url: string;
    };
    holeNumber: number;
  };
  News?: {};
};
