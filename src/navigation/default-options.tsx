import React from 'react';
import { StyleSheet } from 'react-native';

import { BackButton, BackButtonProps } from '@components';
import { COLORS, PRIMARY_FONT, SPACING } from '@constants';
import { StackNavigationOptions } from '@react-navigation/stack';

export const headerWithBackButtonOptions = (props?: BackButtonProps): Partial<StackNavigationOptions> => ({
  headerBackImage: () => (
    <BackButton
      {...props}
      containerStyle={{
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: 66,
      }}
    />
  ),
  headerBackTitleVisible: false,
  headerLeftContainerStyle: { alignItems: 'center' },
  headerTitleAlign: 'center',
  headerRightContainerStyle: { marginRight: SPACING.M, alignItems: 'center' },
});

export const transparentScreenOptions = {
  headerTitle: '',
  headerTransparent: true,
  borderBottomWidth: 0,
};

export const defaultHeaderOptions: Partial<StackNavigationOptions> = {
  headerTitleStyle: {
    ...PRIMARY_FONT.semiBold,
    fontSize: 22,
    alignSelf: 'center',
  },
  headerStyle: {
    elevation: 0,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: COLORS.GREY[500],
  },
};
