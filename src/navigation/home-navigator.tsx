import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen } from '@screens';

type ParamList = {
  Home: undefined;
  Course: {
    title: string;
  };
  News: undefined;
};

const Stack = createStackNavigator<ParamList>();

export const HomeNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
  </Stack.Navigator>
);
