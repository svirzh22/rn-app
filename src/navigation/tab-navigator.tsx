import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useNavigation } from '@react-navigation/native';
import {
  HomeNavigator,
  ProfileNavigator,
  TeeTimeNavigator,
  TournamentsNavigator,
  AcademyNavigator,
} from '@navigation';
import { TabBarButton } from '@components';

const Tab = createBottomTabNavigator();

export const TabNavigator = () => {
  const navigation = useNavigation();
  const options = {
    tabBarButton: (props: object) => <TabBarButton {...props} />,
    headerShown: false,
  };

  const handlePressTeeTime = () => {
    navigation.navigate('Tabs', {
      screen: 'TeeTime',
      params: {
        screen: 'TeeTime',
        params: {
          golfCourse: null,
          bookingDate: null,
          players: null,
        },
      },
    });
  };

  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeNavigator} options={options} />
      <Tab.Screen
        name="TeeTime"
        component={TeeTimeNavigator}
        options={{
          tabBarButton: (props: object) => <TabBarButton {...props} onPress={handlePressTeeTime} />,
          // @ts-ignore
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Tournaments"
        component={TournamentsNavigator}
        options={{ ...options, unmountOnBlur: true }}
      />
      <Tab.Screen name="Academy" component={AcademyNavigator} options={{ ...options, unmountOnBlur: true }} />
      <Tab.Screen name="Profile" component={ProfileNavigator} options={options} />
    </Tab.Navigator>
  );
};
