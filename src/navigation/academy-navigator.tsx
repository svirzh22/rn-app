import * as React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { AcademyScreen } from '@screens';
import { defaultHeaderOptions } from './default-options';

type ParamList = {
  Academy: undefined;
};

const Stack = createStackNavigator<ParamList>();

export const AcademyNavigator = () => (
  <Stack.Navigator screenOptions={defaultHeaderOptions}>
    <Stack.Screen name="Academy" component={AcademyScreen} />
  </Stack.Navigator>
);
