import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Login, ChooseLanguage, VerificationCode, ChooseBirthdayScreen, SignUpScreen } from '@screens';
import { defaultHeaderOptions } from './default-options';
import { LoginNavigatorParams } from './types';

const Stack = createStackNavigator<LoginNavigatorParams>();

export const LoginNavigator = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
      ...defaultHeaderOptions,
    }}
    initialRouteName="Login"
  >
    {/* <Stack.Screen name="Welcome" component={Welcome} /> */}
    <Stack.Screen name="Login" component={Login} />
    <Stack.Screen name="ChooseLanguage" component={ChooseLanguage} />
    <Stack.Screen name="VerificationCode" component={VerificationCode} />
    <Stack.Screen name="ChooseBirthday" component={ChooseBirthdayScreen} />
    <Stack.Screen name="SignUp" component={SignUpScreen} />
  </Stack.Navigator>
);
