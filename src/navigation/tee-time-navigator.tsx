import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { useTranslate } from '@hooks';
import { TeeTimeScreen } from '@screens';
import { defaultHeaderOptions } from './default-options';

type ParamList = {
  TeeTime: undefined;
};

const Stack = createStackNavigator<ParamList>();

export const TeeTimeNavigator = () => {
  const translate = useTranslate();

  return (
    <Stack.Navigator screenOptions={defaultHeaderOptions}>
      <Stack.Screen
        name="TeeTime"
        component={TeeTimeScreen}
        options={{ headerTitle: translate('teeTime.headerTitle') }}
      />
    </Stack.Navigator>
  );
};
