import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import * as Screens from '@screens';
import { useTranslate } from '@hooks';
import { useDispatch } from 'react-redux';
import { freeBooking } from '@src/actions';
import { CourseScreen, ConditionsScreen } from '@screens';
import { TabNavigator } from './tab-navigator';
import { defaultHeaderOptions, headerWithBackButtonOptions } from './default-options';

export type AppParamList = {
  Tabs: undefined;
  MyBookings: undefined;
  News: undefined;
  NewsArticle: undefined;
  CourseHole: undefined;
  ChooseLanguage: undefined;
  Reservation: {
    course: number;
    date: number;
    time: number;
  };
  AddVisitor: undefined;
  ReservationEdit: undefined;
  ReservationDetails: undefined;
  BookingCompleted: undefined;
  OnlinePayment: undefined;
  ProcessPayment: undefined;
  ChoosePaymentMethod: undefined;
  AddPlayer: undefined;
  ProfileEdit: undefined;
  Course: {
    title: string;
  };
  Conditions: undefined;
};

const Stack = createStackNavigator<AppParamList>();

export const AppNavigator = () => {
  const translate = useTranslate();
  const dispatch = useDispatch();
  const options = {
    headerShown: false,
  };

  return (
    <Stack.Navigator screenOptions={defaultHeaderOptions}>
      <Stack.Screen name="Tabs" component={TabNavigator} options={options} />
      <Stack.Screen
        name="MyBookings"
        component={Screens.MyBookingsScreen}
        options={() => ({
          headerTitle: translate('myBookings.headerTitle'),
          ...headerWithBackButtonOptions(),
        })}
      />
      <Stack.Screen
        name="News"
        component={Screens.NewsScreen}
        options={{
          // headerTitle: translate('news.headerTitle'),
          // ...headerWithBackButtonOptions(),
          headerShown: false,
        }}
      />
      <Stack.Screen name="NewsArticle" component={Screens.NewsArticleScreen} options={options} />
      <Stack.Screen
        name="CourseHole"
        component={Screens.CourseHoleScreen}
        options={({ route }: { route: any }) => ({
          headerTitle: translate('courseHole.headerTitle', { holeNumber: route?.params?.holeNumber }),
          ...headerWithBackButtonOptions(),
        })}
      />
      <Stack.Screen
        name="ChooseLanguage"
        component={Screens.ChooseLanguage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Reservation"
        component={Screens.ReservationScreen}
        options={({ navigation }) => ({
          headerTitle: translate('reservation.headerTitle'),
          ...headerWithBackButtonOptions({
            onPress: () => {
              dispatch(freeBooking());
              navigation.goBack();
            },
          }),
        })}
      />
      <Stack.Screen
        name="ProfileEdit"
        component={Screens.ProfileEditScreen}
        options={() => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="AddVisitor"
        component={Screens.AddVisitorScreen}
        options={() => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="AddPlayer"
        component={Screens.AddPlayerScreen}
        options={() => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="ChoosePaymentMethod"
        component={Screens.ChoosePaymentMethodScreen}
        options={() => ({
          headerTitle: translate('choosePaymentMethod.headerTitle'),
          ...headerWithBackButtonOptions(),
        })}
      />
      <Stack.Screen
        name="OnlinePayment"
        component={Screens.OnlinePaymentScreen}
        options={() => ({
          headerTitle: translate('creditCardPayment.headerTitle'),
          ...headerWithBackButtonOptions(),
        })}
      />
      <Stack.Screen
        name="ProcessPayment"
        component={Screens.ProcessPaymentScreen}
        options={() => ({
          headerTitle: translate('creditCardPayment.headerTitle'),
          ...headerWithBackButtonOptions(),
        })}
      />
      <Stack.Screen
        name="BookingCompleted"
        component={Screens.BookingCompletedScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ReservationDetails"
        component={Screens.ReservationDetailsScreen}
        options={{
          headerTitle: translate('reservationDetails.headerTitle'),
          ...headerWithBackButtonOptions(),
        }}
      />
      <Stack.Screen
        name="ReservationEdit"
        component={Screens.ReservationEditScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Course"
        component={CourseScreen}
        options={() => ({
          // title: route.params.title,
          // ...headerWithBackButtonOptions(),
          unmountOnBlur: true,
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="Conditions"
        component={ConditionsScreen}
        options={() => ({
          title: translate('conditions.headerTitle'),
          ...headerWithBackButtonOptions(),
        })}
      />
    </Stack.Navigator>
  );
};
