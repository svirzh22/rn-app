import { InitialState } from '@src/reducers';

export const memberSelector = (state: InitialState) => state.authReducer.member;
