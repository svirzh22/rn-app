import { InitialState } from '@src/reducers';

export const appLoadingSelector = (state: InitialState) => state.applicationReducer.appLoading;
export const sentOtpTimeSelector = (state: InitialState) => state.applicationReducer.sentOtpTime;
