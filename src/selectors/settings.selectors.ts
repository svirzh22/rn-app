import { InitialState } from '@src/reducers';

export const localeSelector = (state: InitialState) => state.settingsReducer.locale;

// eslint-disable-next-line no-underscore-dangle
export const isRehydratedSelector = (state: InitialState) => state._persist?.rehydrated;
