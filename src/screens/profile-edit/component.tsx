import React, { useCallback, useRef, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import { useMutation, useQuery } from 'react-query';
import isEmpty from 'lodash/isEmpty';
import { useDispatch, useSelector } from 'react-redux';

import {
  Gutter,
  FormTextInput,
  FormPhoneInput,
  AvatarSelector,
  Header,
  LinkButton,
  Screen,
  FormHandicapInput,
} from '@components';
import { usePhotoSelector, useSessionExpired, useApiErrorAlert } from '@hooks';
import { userResource } from '@api';
import { handicapValue } from '@helpers';
import { memberSelector } from '@selectors';
import { updateProfile } from '@src/actions';
import { ActivityIndicator } from '@components/activity-indicator';
import { styles } from './styles';
import { profileValidationSchema } from './schema';

export const ProfileEditScreen: React.FC<ProfileEditScreenProps> = () => {
  const { showApiErrorAlert } = useApiErrorAlert();
  const dispatch = useDispatch();
  const member = useSelector(memberSelector);
  const [isLoadingUpdateUser, setLoadingUpdateUser] = useState<boolean>(false);
  const { mutateAsync: updateUser } = useMutation(userResource.updateUser);
  const { onSessionExpired } = useSessionExpired();
  const { data, isLoading, refetch: refetchUserInfo } = useQuery(
    [userResource.cacheKey, { userToken: member.token }],
    userResource.getUserInfo,
    { retry: 0, onError: onSessionExpired, onSuccess: (result) => dispatch(updateProfile(result.user)) },
  );
  const { photo, handleGalleryOpen, handleCameraOpen, handleDeletePhoto } = usePhotoSelector({});
  const navigation = useNavigation();
  const formRef = useRef<any>();
  const handleCancelPress = useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);
  const handleSubmit = useCallback(
    async (values) => {
      try {
        setLoadingUpdateUser(true);
        await updateUser({
          userToken: member.token,
          picture: photo === null ? '' : photo?.imageUri,
          handicap: handicapValue(values.handicap),
          firstname: values.firstname,
          lastname: values.lastname,
        });
        await refetchUserInfo();
        setLoadingUpdateUser(false);
        navigation.goBack();
      } catch (e) {
        setLoadingUpdateUser(false);
        return showApiErrorAlert(e);
      }
      return null;
    },
    [refetchUserInfo, navigation, updateUser, member.token, photo, showApiErrorAlert],
  );

  if (isLoading) {
    return <ActivityIndicator mode="standalone" />;
  }
  const image = !isEmpty(photo) || photo === null
    ? { imageUri: `data:image/jpeg;base64,${photo?.imageUri}` }
    : { imageUri: data?.user?.picture };

  return (
    <Screen withScroll containerStyle={styles.container} style={styles.content} onRetry={refetchUserInfo}>
      <Formik
        innerRef={formRef}
        validationSchema={profileValidationSchema()}
        initialValues={{
          ...data?.user,
          phonenumber: data?.user.mobilePhone,
          handicap: `${data?.user.handicap || '-0.0'}`,
        }}
        onSubmit={handleSubmit}
      >
        {() => (
          <>
            <Header
              title="profileEdit.headerTitle"
              preset="withBorder"
              renderHeaderLeft={() => (
                <LinkButton t="common.cancel" onPress={handleCancelPress} textStyle={styles.cancelButton} />
              )}
              renderHeaderRight={() => (
                <LinkButton t="common.done" onPress={handleSubmitPress} isLoading={isLoadingUpdateUser} />
              )}
            />
            <Gutter amount={32} isVertical />
            <AvatarSelector
              image={image}
              size={112}
              onGalleryOpen={handleGalleryOpen}
              onCameraOpen={handleCameraOpen}
              onDelete={handleDeletePhoto}
            />
            <Gutter amount={16} isVertical />
            <FormTextInput
              name="firstname"
              options={{
                t: 'common.firstName',
              }}
            />
            <FormTextInput
              name="lastname"
              options={{
                t: 'common.lastName',
              }}
            />
            <FormHandicapInput name="handicap" />
            <FormPhoneInput
              name="phonenumber"
              options={{
                t: 'common.phone',
                editable: false,
              }}
              containerStyle={styles.inputContainerStyle}
            />
          </>
        )}
      </Formik>
    </Screen>
  );
};
