import { StyleSheet } from 'react-native';
import { COLORS, SCREEN_WIDTH, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.WHITE,
    paddingTop: 0,
  },
  content: {
    alignItems: 'center',
    paddingHorizontal: SPACING.M,
  },
  delimiter: {
    width: SCREEN_WIDTH,
    height: 8,
    backgroundColor: COLORS.GREY[200],
  },
  name: {
    fontSize: 24,
  },
  buttonText: {},
  cancelButton: {
    color: COLORS.BLACK,
  },
  inputContainerStyle: {
    height: 56,
    marginBottom: SPACING.M,
  },
});
