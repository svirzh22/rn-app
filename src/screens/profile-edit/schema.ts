import * as Yup from 'yup';
import { handicapValidation } from '@helpers/yup';
import { translate } from '@helpers';

export const profileValidationSchema = () => Yup.object().shape({
  firstname: Yup
    .string()
    .trim()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  lastname: Yup
    .string()
    .trim()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  handicap: handicapValidation,
});
