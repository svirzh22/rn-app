import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    paddingTop: SPACING.L,
  },
  wrapper: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
  },
  inputContainerStyle: {
    height: 56,
  },
});
