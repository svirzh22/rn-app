import React, { FC, useCallback, useRef } from 'react';
import { Formik } from 'formik';
import { useNavigation } from '@react-navigation/native';
import { useMutation } from 'react-query';
import { useSelector } from 'react-redux';

import {
  Screen,
  FormTextInput,
  Header,
  FlexView,
  LinkButton,
  FormPhoneInput,
  FormHandicapInput,
} from '@components';
import { addPlayerResource } from '@api';
import { memberSelector } from '@selectors';
import { useApiErrorAlert } from '@hooks';
import { formatDate, REVERSED_DATE_FORMAT_DASH } from '@helpers';
import { visitorValidationSchema } from './schema';
import { styles } from './styles';
import { AddVisitorScreenProps } from './types';

export const AddVisitorScreen: FC<AddVisitorScreenProps> = ({ route }) => {
  const { showApiErrorAlert } = useApiErrorAlert();
  const navigation = useNavigation();
  const formRef = useRef<any>();
  const { params } = route;
  const member = useSelector(memberSelector);
  const { mutateAsync: addPlayer, isLoading } = useMutation(addPlayerResource.addPlayer);

  const handleSubmit = useCallback(
    async (values) => {
      // Edit visitor
      if (params?.visitor) {
        navigation.navigate('ReservationEdit', {
          visitor: {
            ...params?.visitor,
            ...values,
          },
        });
      } else {
        try {
          const player = await addPlayer({
            ...values,
            birthDate: formatDate(values.birthDate, REVERSED_DATE_FORMAT_DASH),
            userToken: member.token,
          });

          navigation.navigate('AddPlayer', {
            visitor: {
              ...player.addedPlayer,
            },
          });
        } catch (e) {
          showApiErrorAlert(e);
        }
      }
    },
    [addPlayer, member.token, navigation, params, showApiErrorAlert],
  );

  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);

  return (
    <Screen withScroll containerStyle={styles.wrapper}>
      <Formik
        innerRef={formRef}
        initialValues={params?.visitor || { firstname: '', lastname: '', handicap: '-0.0' }}
        onSubmit={handleSubmit}
        validationSchema={visitorValidationSchema()}
      >
        {() => (
          <>
            <Header
              title="addVisitor.headerTitle"
              preset="withBorder"
              renderHeaderRight={() => (
                <LinkButton
                  t="common.done"
                  onPress={handleSubmitPress}
                  weight="medium"
                  isLoading={isLoading}
                />
              )}
            />
            <FlexView style={styles.container}>
              <FormTextInput name="firstname" options={{ t: 'addVisitor.firstName' }} />
              <FormTextInput name="lastname" options={{ t: 'addVisitor.lastName' }} />
              <FormHandicapInput name="handicap" />
              <FormPhoneInput name="mobilePhone" options={{}} containerStyle={styles.inputContainerStyle} />
            </FlexView>
          </>
        )}
      </Formik>
    </Screen>
  );
};
