import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type AddVisitorScreenRouteProp = RouteProp<AppNavigatorParams, 'AddVisitor'>;
type AddVisitorNavigationProp = StackNavigationProp<
AppNavigatorParams,
'AddVisitor'
>;

export interface AddVisitorScreenProps {
  navigation: AddVisitorNavigationProp;
  route: AddVisitorScreenRouteProp;
}
