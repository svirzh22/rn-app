import * as Yup from 'yup';
import { handicapValidation } from '@helpers/yup';
import { translate } from '@helpers';

export const visitorValidationSchema = () => Yup.object().shape({
  firstname: Yup
    .string()
    .transform((value) => value.trim())
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  lastname: Yup.string()
    .transform((value) => value.trim())
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  handicap: handicapValidation,
  mobilePhone: Yup.string(),
});
