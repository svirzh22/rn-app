import React, { FC } from 'react';
import { Placeholder as PH, PlaceholderLine, Fade } from 'rn-placeholder';

import { FlexView, Gutter } from '@components';
import { SPACING } from '@constants';
import { StyleProp, ViewStyle } from 'react-native';

interface PlaceholderProps {
  style: StyleProp<ViewStyle>;
}

export const Placeholder: FC<PlaceholderProps> = ({ style }) => (
  <PH Animation={Fade}>
    <FlexView style={[style, { padding: 8 }]} justifyContent="center" alignItems="center">
      <PlaceholderLine noMargin width={50} height={28} />
      <Gutter amount={SPACING.XXS} />
      <PlaceholderLine noMargin width={50} height={28} />
    </FlexView>
  </PH>
);
