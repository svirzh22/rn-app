import { StyleSheet } from 'react-native';

import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.GREY[200],
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 4,
    borderRadius: 10,
  },
  tabItem: {
    height: 36,
    borderRadius: 9,
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedTab: {
    height: 36,
    borderRadius: 9,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,

    elevation: 3,
    shadowOpacity: 0.1,
    shadowRadius: 3,
    shadowOffset: { height: 3, width: 0 },
  },
});
