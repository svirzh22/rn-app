import { ViewStyle } from 'react-native';

export interface TabItem {
  id: number;
  title: string;
}

export interface TabsProps {
  tabs: TabItem[];
  style?: ViewStyle;
  isLoading?: boolean;
  onChangeTab?: (id: number) => void;
}
