import { useMemo } from 'react';
import { map, pick } from 'ramda';

export const useTabs = (data: any) => useMemo(() => (data ? map(pick(['id', 'title']))(data) : []), [data]);
