import React, { FC, useEffect, useState, Fragment, useCallback } from 'react';
import map from 'lodash/map';
import size from 'lodash/size';
import isEmpty from 'lodash/isEmpty';

import { FlexView, Gutter, Text, Touchable } from '@components';
import { SPACING } from '@constants';
import { TabItem, TabsProps } from './types';
import { styles } from './styles';
import { Placeholder } from './placeholder';

export const Tabs: FC<TabsProps> = ({
  tabs,
  style = {},
  isLoading,
  onChangeTab,
}) => {
  const [selectedTab, setTab] = useState<number>();

  useEffect(() => {
    if (!selectedTab && !isEmpty(tabs)) {
      setTab(tabs[0].id);
    }
  }, [selectedTab, tabs]);

  const handleTabPress = useCallback((id: number) => () => {
    if (onChangeTab) {
      onChangeTab(id);
    }
    setTab(id);
  }, [onChangeTab]);

  const renderTab = (item: TabItem, index: number) => {
    const { id } = item;
    const isSelected = id === selectedTab;
    const showSeparator = index + 1 < size(tabs);

    return (
      <Fragment key={item.title}>
        <Touchable
          onPress={handleTabPress(id)}
          disabled={isSelected}
          style={isSelected ? styles.selectedTab : styles.tabItem}
        >
          <Text label={item.title} weight={isSelected ? 'medium' : 'regular'} size={16} />
        </Touchable>
        {showSeparator && <Gutter amount={SPACING.XXS} />}
      </Fragment>
    );
  };

  if (isLoading || isEmpty(tabs)) {
    return <Placeholder style={[styles.container, style]} />;
  }

  return <FlexView style={[styles.container, style]}>{map<TabItem>(tabs, renderTab)}</FlexView>;
};
