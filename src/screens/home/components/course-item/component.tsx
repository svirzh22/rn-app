import React, { FC } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { COLORS } from '@constants';
import { Image, Text } from '@components';
import { styles } from './styles';

export interface Course {
  id: number;
  image: number;
  title: string;
  url: string;
}

interface CourseItemProps {
  item: Course;
}

export const CourseItem: FC<CourseItemProps> = ({ item }) => (
  <Image style={styles.container} source={item.image}>
    <Text label={item.title} color={COLORS.WHITE} style={styles.title} size={16} />
    <LinearGradient
      colors={[COLORS.BLACK, COLORS.TRANSPARENT]}
      style={styles.gradient}
      start={{ x: 0, y: 0.9 }}
      end={{ x: 0, y: 0.09 }}
    />
  </Image>
);
