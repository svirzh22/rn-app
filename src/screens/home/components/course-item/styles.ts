import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    width: 160,
    height: 120,
    borderRadius: 12,
    justifyContent: 'flex-end',
    zIndex: 1,
    paddingHorizontal: SPACING.S,
    paddingVertical: SPACING.XS,
  },
  gradient: {
    width: 160,
    height: 120,
    borderRadius: 12,
    opacity: 0.6,
    position: 'absolute',
    zIndex: 2,
  },
  title: {
    zIndex: 3,
  },
});
