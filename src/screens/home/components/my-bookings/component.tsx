import React, { FC, useCallback } from 'react';
import { ViewStyle } from 'react-native';
import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { BookingEntity } from '@api/resources/types/booking';
import { useNavigation } from '@react-navigation/native';
import { BookingsList, FlexView, Text, LinkButton, ActivityIndicator } from '@components';
import { memberSelector } from '@selectors';
import { userBookingsResource } from '@api';
import { styles } from './styles';

interface MyBookingsProps {
  headerStyle?: ViewStyle;
  onSeeAllPress: () => void;
}

export const MyBookings: FC<MyBookingsProps> = ({ headerStyle = {}, onSeeAllPress }) => {
  const navigation = useNavigation();
  const member = useSelector(memberSelector);
  const { data, isLoading } = useQuery(
    [userBookingsResource.cacheKey, { userToken: member.token }],
    userBookingsResource.fetchUserBookings,
    { retry: 0 },
  );

  const handlePress = useCallback(
    (item: BookingEntity) => {
      navigation.navigate('ReservationDetails', { reservationId: item.id });
    },
    [navigation],
  );

  return (
    <FlexView>
      <FlexView
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        style={[styles.header, headerStyle]}
      >
        <Text t="home.myBookings" size={24} />
        <LinkButton t="common.seeAll" onPress={onSeeAllPress} />
      </FlexView>
      {isLoading ? (
        <ActivityIndicator style={styles.listLoader} />
      ) : (
        <BookingsList bookings={data ? data?.bookings?.slice(0, 2) : []} onPress={handlePress} />
      )}
    </FlexView>
  );
};
