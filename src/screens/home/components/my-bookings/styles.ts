import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  header: {
    marginTop: SPACING.S,
    marginBottom: SPACING.XS,
    paddingHorizontal: SPACING.M,
  },
  listLoader: {
    paddingBottom: 22,
    paddingTop: 8,
  },
});
