import React, { FC, useCallback } from 'react';
import { ViewStyle } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { FlexView, LinkButton, Text } from '@components';
import { styles } from './styles';
// import { NewsItem } from '../news-item';

interface NewsProps {
  contentStyle?: ViewStyle;
  containerStyle?: ViewStyle;
  headerStyle?: ViewStyle;
}

export const News: FC<NewsProps> = ({
  // contentStyle,
  containerStyle,
  headerStyle = {},
}) => {
  // @TODO MOCK
  // const data: NewsItem[] = [
  //   {
  //     id: '1',
  //     image: 'https://i.ytimg.com/vi/Zj2dBWmzX9E/maxresdefault.jpg',
  //     timestamp: Date.now(),
  //     title: 'Infos sur la suite de la saison !',
  //   },
  //   {
  //     id: '2',
  //     image: 'https://i.ytimg.com/vi/Zj2dBWmzX9E/maxresdefault.jpg',
  //     timestamp: Date.now(),
  //     title: 'string',
  //   },
  // ];

  const navigation = useNavigation();

  // const keyExtractor = (item: NewsItem) => item.id;
  // const handleItemPress = useCallback(() => navigation.navigate('NewsArticle'), [navigation]);
  const handleSeeAll = useCallback(() => navigation.navigate('News'), [navigation]);
  // const renderItem: ListRenderItem<NewsItem> = ({ item }) => (
  //   <TouchableOpacity onPress={handleItemPress}>
  //     <NewsItem item={item} />
  //   </TouchableOpacity>
  // );
  return (
    <FlexView style={containerStyle}>
      <FlexView
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        style={[styles.header, headerStyle]}
      >
        <Text t="home.news" size={24} />
        <LinkButton t="common.seeAll" onPress={handleSeeAll} />
      </FlexView>
      {/* <FlatList
        horizontal
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ItemSeparatorComponent={() => <Gutter amount={SPACING.S} />}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={contentStyle}
      /> */}
    </FlexView>
  );
};
