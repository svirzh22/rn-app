import React, { FC } from 'react';
import { ViewStyle, FlatList, ListRenderItem, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { SPACING } from '@constants';
import { FlexView, Gutter, Text } from '@components';
import { images } from '@assets/images';
import { styles } from './styles';
import { CourseItem, Course } from '../course-item';

interface CoursesProps {
  contentStyle?: ViewStyle;
  containerStyle?: ViewStyle;
  headerStyle?: ViewStyle;
}

export const Courses: FC<CoursesProps> = ({ containerStyle, headerStyle = {}, contentStyle }) => {
  const navigation = useNavigation();
  const handleCoursePress = (golfCourse: Course) => () =>
    navigation.navigate('Course', { golfCourse });
  const data: Course[] = [
    {
      id: 0,
      url: 'https://www.golfcrans.ch/fr/page/golf-club/parcours/parcours-severiano-ballesteros-68',
      image: images.severiano,
      title: 'Severiano Ballesteros',
    },
    {
      id: 1,
      url: 'https://www.golfcrans.ch/fr/page/golf-club/parcours/parcours-jack-nicklaus-69',
      image: images.jack,
      title: 'Jack Nicklaus',
    },
  ];

  const keyExtractor = (item: Course) => `course_${item.id}`;
  const renderItem: ListRenderItem<Course> = ({ item }) => (
    <TouchableOpacity onPress={handleCoursePress(item)}>
      <CourseItem item={item} />
    </TouchableOpacity>
  );
  return (
    <FlexView style={containerStyle}>
      <FlexView
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        style={[styles.header, headerStyle]}
      >
        <Text t="home.courses" size={24} />
      </FlexView>
      <FlatList
        horizontal
        data={data}
        renderItem={renderItem}
        ItemSeparatorComponent={() => <Gutter amount={SPACING.S} />}
        showsHorizontalScrollIndicator={false}
        keyExtractor={keyExtractor}
        contentContainerStyle={contentStyle}
      />
    </FlexView>
  );
};
