import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  item: {},
  image: {
    width: 216,
    height: 144,
    borderRadius: 20,
  },
});
