import React, { FC } from 'react';
import moment from 'moment';
import upperFirst from 'lodash/upperFirst';

import { FlexView, Image, Text, Gutter } from '@components';
import { COLORS, SPACING } from '@constants';
import { styles } from './styles';

export interface NewsItem {
  id: string;
  image: string;
  timestamp: number;
  title: string;
}

interface NewsItemProps {
  item: NewsItem;
}

export const NewsItem: FC<NewsItemProps> = ({ item }) => {
  const timeFromNow = upperFirst(moment(item.timestamp).fromNow());

  return (
    <FlexView style={styles.item}>
      <Image style={styles.image} source={{ uri: item.image }} />
      <Gutter isVertical amount={SPACING.XXS} />
      <Text color={COLORS.GREY[700]} weight="medium" size={14} label={timeFromNow} />
      <Text size={16} label={item.title} />
    </FlexView>
  );
};
