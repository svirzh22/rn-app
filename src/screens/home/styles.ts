import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.WHITE,
  },
  content: {
    paddingHorizontal: SPACING.M,
    marginTop: SPACING.S,
  },
  logo: {
    height: 65,
    width: 52,
  },
  tabs: {
    marginTop: SPACING.S + 4,
  },
  separator: {
    height: 8,
    backgroundColor: COLORS.GREY[200],
  },
  teeTimeButton: {
    marginTop: SPACING.XXS,
    marginBottom: SPACING.L,
  },
  pickersContainer: {
    marginTop: SPACING.S,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  newsContent: {
    paddingHorizontal: SPACING.M,
    marginBottom: SPACING.M,
  },
  newsHeader: {
    marginVertical: SPACING.M,
    paddingHorizontal: SPACING.M,
  },
  coursesContent: {
    paddingHorizontal: SPACING.M,
    paddingBottom: SPACING.L,
  },
  coursesHeader: {
    marginVertical: SPACING.M,
    paddingHorizontal: SPACING.M,
  },
  datepickerContainerStyle: {
    flex: 1,
  },
});
