import React, { FC, useCallback, useState, useRef } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useQuery } from 'react-query';
import { FlatList, ListRenderItem } from 'react-native';
import { identity } from 'ramda';
import { useFocusEffect } from '@react-navigation/native';

import { Screen, Text, FlexView, Gutter, Image, Button, FormPicker, DatePickerWeekDay } from '@components';
import { images } from '@assets/images';
import { courseResource } from '@api';
import { COLORS, SPACING } from '@constants';
import { getSeasonStartDate, MAX_DATE } from '@helpers';
import { HomeScreenProps } from './types';
import { styles } from './styles';
import { Tabs, News, Courses, MyBookings, useTabs } from './components';
import { players } from './constants';

type HomeScreenItem = 'header' | 'bookings' | 'news' | 'courses';

export const HomeScreen: FC<HomeScreenProps> = ({ navigation }) => {
  const flatListRef = useRef<FlatList>();
  const [golfCourse, setGolfCourse] = useState(0);
  const [date, setDate] = useState<number>(+getSeasonStartDate());
  useFocusEffect(() => {
    if (flatListRef.current) {
      flatListRef.current?.scrollToOffset({ offset: 0, animated: false });
    }
  });

  const bookTeeTime = (values: any) =>
    navigation.navigate('Tabs', {
      screen: 'TeeTime',
      params: {
        screen: 'TeeTime',
        params: {
          golfCourse,
          bookingDate: date,
          players: values.players,
          onChangeDate: setDate,
        },
      },
    });

  const handleSeeAllBookings = useCallback(() => navigation.navigate('MyBookings'), [navigation]);
  const { data } = useQuery([courseResource.cacheKey], courseResource.getCourses, { retry: 0 });

  const tabs = useTabs(data?.courses);

  const homeScreenContent: HomeScreenItem[] = ['header', 'bookings', 'news', 'courses'];
  const renderHomeScreenItem: ListRenderItem<HomeScreenItem> = ({ item }) => {
    if (item === 'header') {
      return (
        <FlexView style={styles.content}>
          <FlexView alignItems="center">
            <Image source={images.logo} style={styles.logo} />
          </FlexView>
          <Gutter amount={SPACING.L} isVertical />
          <FlexView flexDirection="row">
            <Text t="home.bookYour" size={24} />
            <Gutter amount={SPACING.XXS} />
            <Text t="home.teeTime" size={24} weight="bold" color={COLORS.SECONDARY[500]} />
          </FlexView>
          <Tabs tabs={tabs} style={styles.tabs} onChangeTab={setGolfCourse} />

          <Formik
            initialValues={{
              players: 1,
            }}
            onSubmit={bookTeeTime}
            validationSchema={Yup.object().shape({})}
          >
            {({ handleSubmit }) => (
              <>
                <FlexView style={styles.pickersContainer}>
                  <DatePickerWeekDay
                    onChangeDate={setDate}
                    containerStyle={styles.datepickerContainerStyle}
                    maximumDate={MAX_DATE}
                    minimumDate={new Date()}
                    value={+date}
                  />
                  <Gutter amount={SPACING.S} />
                  <FormPicker items={players} name="players" label="home.players" />
                </FlexView>

                <Button onPress={handleSubmit} t="home.bookTeeTime" style={styles.teeTimeButton} />
              </>
            )}
          </Formik>
        </FlexView>
      );
    }

    if (item === 'bookings') {
      return <MyBookings onSeeAllPress={handleSeeAllBookings} />;
    }

    if (item === 'news') {
      return <News contentStyle={styles.newsContent} headerStyle={styles.newsHeader} />;
    }

    if (item === 'courses') {
      return <Courses contentStyle={styles.coursesContent} headerStyle={styles.coursesHeader} />;
    }

    return null;
  };

  const renderSeparator = () => <FlexView style={styles.separator} />;

  return (
    <Screen containerStyle={styles.container}>
      <FlatList
        data={homeScreenContent}
        renderItem={renderHomeScreenItem}
        ItemSeparatorComponent={renderSeparator}
        keyExtractor={identity}
        ref={flatListRef as any}
      />
    </Screen>
  );
};
