import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type HomeScreenRouteProp = RouteProp<AppNavigatorParams, 'Tabs'>;
type HomeNavigationProp = StackNavigationProp<
AppNavigatorParams,
'Tabs'
>;

export interface HomeScreenProps {
  navigation: HomeNavigationProp;
  route: HomeScreenRouteProp;
}
