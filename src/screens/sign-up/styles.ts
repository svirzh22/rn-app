import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    backgroundColor: COLORS.WHITE,
  },
  header: {
    marginTop: SPACING.S,
    height: 36,
  },
  inputContainerStyle: {
    height: 56,
  },
});
