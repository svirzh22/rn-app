import React, { FC, useCallback, useRef } from 'react';
import { Formik } from 'formik';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';

import {
  BackButton,
  Button,
  FlexView,
  FormDatePicker,
  FormHandicapInput,
  FormPhoneInput,
  FormTextInput,
  Gutter,
  Screen,
  Text,
} from '@components';
import { authResource, userResource } from '@api';
import { validationSchema } from '@screens/sign-up/schema';
import { RouteProp, useRoute } from '@react-navigation/native';
import { LoginNavigatorParams } from '@navigation';
import {
  formatDate,
  getMaxBirthdayDate,
  parsePhoneNumber,
  REVERSED_DATE_FORMAT_DASH,
} from '@helpers';
import { setSentOtpTime } from '@src/actions';
import { useAlert } from '@hooks';
import { styles } from './styles';
import { FormValues, SignUpScreenProps } from './types';

export const SignUpScreen: FC<SignUpScreenProps> = ({
  navigation,
}) => {
  const { showAlert } = useAlert();
  const dispatch = useDispatch();
  const route = useRoute<RouteProp<LoginNavigatorParams, 'SignUp'>>();
  const { phoneNumber } = route?.params || {};
  const { mutateAsync: createUser, isLoading: isLoadingCreateUser } = useMutation(userResource.createUser);
  const { mutateAsync: askOtp, isLoading: isLoadingAskTop } = useMutation(authResource.askOTP);

  const formRef = useRef<any>();
  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);

  const handleSubmit = useCallback(
    async (values: FormValues) => {
      const { nationalNumber, countryCallingCode } = parsePhoneNumber(phoneNumber);
      const params = {
        firstname: values.firstName,
        lastname: values.lastName,
        handicap: values.handicap,
        birthDate: formatDate(values.birthDate, REVERSED_DATE_FORMAT_DASH),
        email: values.email,
        mobilePhone: `+${countryCallingCode}${nationalNumber}`,
      };

      try {
        await createUser(params);

        const res = await askOtp({
          mobilePhone: nationalNumber,
          countryCode: countryCallingCode,
        });

        if (res) {
          const { error, devOtp } = res;
          if (!error) {
            dispatch(setSentOtpTime(Date.now()));
            return navigation.replace('VerificationCode', {
              ...params,
              mobilePhone: nationalNumber,
              countryCode: countryCallingCode,
              devOtp,
            });
          }

          return showAlert({ t: 'errors.somethingWrong' });
        }
      } catch (e) {
        const { errorMessage } = e?.data || {};

        return showAlert(errorMessage ? { text: errorMessage } : { t: 'errors.somethingWrong' });
      }
      return null;
    },
    [createUser, phoneNumber, askOtp, navigation, dispatch, showAlert],
  );

  return (
    <Screen containerStyle={styles.container} withScroll>
      <FlexView style={styles.header}>
        <BackButton />
      </FlexView>

      <FlexView>
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            birthDate: new Date(+getMaxBirthdayDate()),
            handicap: '-0.0',
            email: '',
            mobilePhone: phoneNumber,
          }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema()}
          innerRef={formRef}
        >
          {() => (
            <>
              <Text t="signUp.title" size={24} align="center" />
              <Gutter isVertical />
              <FormTextInput name="firstName" options={{ t: 'signUp.firstName' }} />
              <FormTextInput name="lastName" options={{ t: 'signUp.lastName' }} />
              <FormDatePicker
                name="birthDate"
                maximumDate={new Date(+getMaxBirthdayDate())}
                containerStyle={styles.inputContainerStyle}
                label="signUp.birthDate"
              />
              <FormHandicapInput name="handicap" />
              <FormTextInput
                name="email"
                options={{
                  t: 'signUp.email',
                  keyboardType: 'email-address',
                  autoCapitalize: 'none',
                }}
              />
              <FormPhoneInput
                name="mobilePhone"
                options={{}}
                editable={false}
                initialValue={phoneNumber}
                containerStyle={styles.inputContainerStyle}
              />
              <Gutter isVertical />
              <Button
                onPress={handleSubmitPress}
                t="common.save"
                loading={isLoadingCreateUser || isLoadingAskTop}
              />
              <Gutter isVertical />
            </>
          )}
        </Formik>
      </FlexView>
    </Screen>
  );
};
