import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { LoginNavigatorParams } from '@navigation';

type SignUpScreenRouteProp = RouteProp<LoginNavigatorParams, 'SignUp'>;
type SignUpNavigationProp = StackNavigationProp<
LoginNavigatorParams,
'SignUp'
>;

export interface SignUpScreenProps {
  navigation: SignUpNavigationProp;
  route: SignUpScreenRouteProp;
}

export interface FormValues {
  firstName: string;
  lastName: string;
  birthDate: Date;
  handicap: string;
  email: string;
  mobilePhone: string;
}
