import * as Yup from 'yup';

import { EMAIL_REGEX, translate } from '@helpers';
import { handicapValidation } from '@helpers/yup';

export const validationSchema = () => Yup.object().shape({
  firstName: Yup
    .string()
    .trim()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  lastName: Yup
    .string()
    .trim()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  // birthDate: Yup.date().required(),
  handicap: handicapValidation,
  email: Yup
    .string()
    .matches(EMAIL_REGEX, translate('signUp.invalidEmail'))
    .required(translate('validations.required')),
  mobilePhone: Yup.string().required(translate('validations.required')),
});
