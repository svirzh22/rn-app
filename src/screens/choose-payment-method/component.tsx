import React, { FC, useCallback } from 'react';
import { useMutation, useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { Screen, Gutter, Text, FlexView, ActivityIndicator } from '@components';
import { images } from '@assets/images';
import { COLORS } from '@constants';
import { bookingConfirmResource, paymentMethodsResource, userResource } from '@api';
import { useAlert } from '@hooks';
import { memberSelector } from '@selectors';
import { PaymentOption } from './components';
import { styles } from './styles';
import { ChoosePaymentMethodScreenProps } from './types';

export const ChoosePaymentMethodScreen: FC<ChoosePaymentMethodScreenProps> = ({
  navigation,
  route,
}) => {
  const { showAlert } = useAlert();
  const member = useSelector(memberSelector);
  const { data, isLoading } = useQuery(
    [paymentMethodsResource.cacheKey, { userToken: member.token }],
    paymentMethodsResource.fetchPaymentMethods,
  );
  const { data: userData, isLoading: isLoadingUserData } = useQuery(
    [userResource.cacheKey, { userToken: member.token }],
    userResource.getUserInfo,
  );
  const {
    mutateAsync: confirmBooking,
    isLoading: confirmBookingLoading,
  } = useMutation(bookingConfirmResource.confirmBooking);

  const handlePressOnline = useCallback(async () => {
    navigation.navigate('OnlinePayment', {
      booking: route.params.bookingId,
      paymentMethod: 'ONLINE',
    });
  }, [navigation, route.params.bookingId]);
  const handlePressFrontDesk = useCallback((paymentMethod: string) => async () => {
    try {
      if (userData) {
        await confirmBooking({
          userToken: member.token,
          firstname: userData?.user.firstname,
          lastname: userData?.user.lastname,
          paymentMethod,
          booking: route.params.bookingId,
          email: userData.user.email || '',
          phone: userData.user.mobilePhone || '',
        });

        navigation.navigate('BookingCompleted', { booking: route.params.bookingId });
      }
    } catch (exc) {
      showAlert({ text: exc.message });
    }
  }, [confirmBooking, member.token, navigation, route.params.bookingId, userData, showAlert]);

  if (isLoading || isLoadingUserData) return (<ActivityIndicator mode="standalone" />);

  const paymentMethods = data?.paymentMethods;

  return (
    <Screen style={styles.container} unsafe>
      <Gutter isVertical />

      <Text t="choosePaymentMethod.selectPaymentMethod" size={22} weight="medium" />
      <FlexView flexDirection="row">
        <Text t="choosePaymentMethod.descriptionPart1" size={16} />
        <Text label=" 5 CHF " size={16} color={COLORS.SECONDARY[500]} weight="medium" />
        <Text t="choosePaymentMethod.descriptionPart2" size={16} />
      </FlexView>
      <Gutter isVertical />

      {paymentMethods?.ONLINE && (
        <PaymentOption
          image={images.creditCard}
          title={paymentMethods?.ONLINE.label}
          onPress={handlePressOnline}
        />
      )}
      <Gutter isVertical />
      {paymentMethods?.CASH && (
        <PaymentOption
          image={images.frontDesk}
          title={paymentMethods?.CASH.label}
          onPress={handlePressFrontDesk('CASH')}
        />
      )}
      <Gutter isVertical />
      {paymentMethods?.CREDITCARD && (
        <PaymentOption
          image={images.frontDesk}
          title={paymentMethods?.CREDITCARD.label}
          onPress={handlePressFrontDesk('CREDITCARD')}
        />
      )}
      {confirmBookingLoading && <ActivityIndicator mode="portal" />}
    </Screen>
  );
};
