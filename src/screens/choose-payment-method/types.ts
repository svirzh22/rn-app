import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ChoosePaymentMethodScreenRouteProp = RouteProp<AppNavigatorParams, 'ChoosePaymentMethod'>;
type ChoosePaymentMethodNavigationProp = StackNavigationProp<
AppNavigatorParams,
'ChoosePaymentMethod'
>;

export interface ChoosePaymentMethodScreenProps {
  navigation: ChoosePaymentMethodNavigationProp;
  route: ChoosePaymentMethodScreenRouteProp;
}
