import React, { FC } from 'react';
import { Source } from 'react-native-fast-image';

import { Image, Text, Gutter, Touchable } from '@components';
import { SPACING } from '@constants';
import { styles } from './styles';

interface PaymentOptionProps {
  image: Source;
  title?: string;
  t?: string;
  onPress: () => void;
}

export const PaymentOption: FC<PaymentOptionProps> = ({ image, title, onPress, t }) => (
  <Touchable style={styles.container} onPress={onPress}>
    <Image source={image} style={styles.image} />
    <Gutter amount={SPACING.S} />
    <Text label={title} t={t} />
  </Touchable>
);
