import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 100,
    borderRadius: 16,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: SPACING.L,
    flexDirection: 'row',
    alignItems: 'center',

    elevation: 3,
    shadowOpacity: 0.15,
    shadowRadius: 5,
    shadowOffset: { height: 3, width: 0 },
  },
  image: {
    height: 32,
    width: 32,
  },
});
