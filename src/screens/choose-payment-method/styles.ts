import { StyleSheet } from 'react-native';

import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
  },
});
