import React, { FC, useCallback } from 'react';
import { StackActions } from '@react-navigation/native';
import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { bookingDetailsResource, courseResource } from '@api';
import {
  FlexView,
  Text,
  Icon,
  Gutter,
  BookingItem,
  Button,
  ScreenWrapper,
  ActivityIndicator,
} from '@components';
import { COLORS, SPACING } from '@constants';
import { memberSelector } from '@selectors';
import { styles } from './styles';
import { BookingCompletedScreenProps } from './types';

export const BookingCompletedScreen: FC<BookingCompletedScreenProps> = ({
  route,
  navigation,
}) => {
  const member = useSelector(memberSelector);
  const { data: courseData, isLoading: courseDataLoading } = useQuery(
    [courseResource.cacheKey],
    courseResource.getCourses,
    { retry: 0 },
  );
  const { data: bookingDetails, isLoading: bookingDetailsLoading } = useQuery(
    [bookingDetailsResource.cacheKey, { bookingId: route.params.booking, userToken: member.token }],
    bookingDetailsResource.fetchBookingDetails,
    { retry: 0 },
  );
  const handlePressViewBooking = useCallback(() => {
    navigation.dispatch(StackActions.popToTop());
    navigation.navigate('ReservationDetails', { reservationId: route.params.booking });
  }, [navigation, route.params.booking]);
  const handlePressDone = useCallback(() => {
    navigation.dispatch(StackActions.popToTop());
    navigation.navigate('Home', {});
  }, [navigation]);

  if (courseDataLoading || bookingDetailsLoading) {
    return (<ActivityIndicator mode="standalone" />);
  }
  return (
    <ScreenWrapper style={styles.container}>
      <FlexView flex={0.20} />
      <FlexView flex={0.80} alignItems="center">
        <FlexView style={styles.checkMarkContainer}>
          <Icon name="check" size={36} color={COLORS.WHITE} style={styles.checkMark} />
        </FlexView>
        <Gutter isVertical amount={SPACING.XXL} />

        <Text t="bookingCompleted.title" size={32} />
        <Text t="bookingCompleted.subHeader" color={COLORS.GREY[700]} size={16} />
        <Gutter isVertical amount={SPACING.L} />

        <BookingItem
          item={bookingDetails?.booking as any}
          showArrow={false}
          style={styles.bookingItem}
          golfCourses={courseData?.courses || []}
        />
      </FlexView>
      <FlexView>
        <Button t="bookingCompleted.viewBooking" preset="outline" onPress={handlePressViewBooking} />
        <Gutter isVertical />
        <Button t="common.done" onPress={handlePressDone} />
      </FlexView>
    </ScreenWrapper>
  );
};
