import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    flex: 1,
  },
  checkMark: {
    marginTop: 2,
    marginLeft: 2,
  },
  checkMarkContainer: {
    height: 80,
    width: 80,
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.SECONDARY[200],
  },
  bookingItem: {
    minHeight: 70,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: COLORS.GREY[500],
    alignItems: 'center',
  },
});
