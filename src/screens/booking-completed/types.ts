import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type BookingCompletedScreenRouteProp = RouteProp<AppNavigatorParams, 'BookingCompleted'>;
type BookingCompletedNavigationProp = StackNavigationProp<
AppNavigatorParams,
'BookingCompleted'
>;

export interface BookingCompletedScreenProps {
  navigation: BookingCompletedNavigationProp;
  route: BookingCompletedScreenRouteProp;
}
