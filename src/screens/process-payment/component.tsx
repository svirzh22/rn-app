import React, { useState } from 'react';
import { WebView } from 'react-native-webview';
import { useQuery } from 'react-query';

import { ActivityIndicator } from '@components';
import { useAlert } from '@hooks';
import { paymentStatusResource } from '@api';
import { ProcessPaymentScreenProps } from './types';

export const ProcessPaymentScreen: React.FC<ProcessPaymentScreenProps> = ({
  navigation,
  route,
}) => {
  const { showAlert } = useAlert();
  const [waitingStatus, setWaitingStatus] = useState(false);
  const [paymentFinalized, setPaymentFinalized] = useState(false);
  const {
    refetch,
  } = useQuery(
    [paymentStatusResource.cacheKey, { transactionToken: route.params?.saferPay.transactionToken }],
    paymentStatusResource.fetchPaymentStatus,
    {
      enabled: false,
      onSuccess: (result) => {
        if (result.transactionStatus.Transaction.Status === 'AUTHORIZED') {
          navigation.navigate('BookingCompleted', { booking: route.params.booking });
        } else {
          setTimeout(() => refetch(), 15000);
        }
      },
    },
  );

  if (waitingStatus) {
    return (
      <ActivityIndicator mode="standalone" t="paymentProcess.processing" />
    );
  }

  return (
    <WebView
      source={{ uri: route.params.saferPay.frameUrl }}
      renderLoading={() => (
        <ActivityIndicator mode="standalone" />
      )}
      onNavigationStateChange={(navState) => {
        if (navState.url === route.params.saferPay.successUrl) {
          setPaymentFinalized(true);
          if (paymentFinalized) {
            setWaitingStatus(true);
            refetch();
          }
        } else if (navState.url === route.params.saferPay.failUrl) {
          setPaymentFinalized(true);
          if (paymentFinalized) {
            showAlert({ t: 'errors.paymentFail' });
            navigation.goBack();
          }
        } else if (navState.url === route.params.saferPay.abortUrl) {
          setPaymentFinalized(true);
          if (paymentFinalized) {
            navigation.goBack();
          }
        }
      }}
      startInLoadingState
    />
  );
};
