import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ProcessPaymentScreenRouteProp = RouteProp<AppNavigatorParams, 'ProcessPayment'>;
type ProcessPaymentNavigationProp = StackNavigationProp<
AppNavigatorParams,
'ProcessPayment'
>;

export interface ProcessPaymentScreenProps {
  navigation: ProcessPaymentNavigationProp;
  route: ProcessPaymentScreenRouteProp;
}
