import { IconNameType } from '@components';

export interface ActionItemProps {
  name: IconNameType;
  label: string;
  color?: string;
  withArrow?: boolean;
  onPress: () => void;
}
