import React from 'react';

import { Divider, Icon, Text, Touchable } from '@components';
import { COLORS } from '@constants';
import { ActionItemProps } from './types';
import { styles } from './styles';

export const ActionItem: React.FC<ActionItemProps> = ({
  name,
  label,
  color,
  withArrow,
  onPress,
}) => (
  <>
    <Touchable style={styles.container} onPress={onPress}>
      <Icon color={color} size={15} name={name} />
      <Text style={[styles.text, { color }]} label={label} />
      {withArrow && (<Icon color={COLORS.GREY[600]} name="arrow-right" />)}
    </Touchable>
    <Divider />
  </>
);
