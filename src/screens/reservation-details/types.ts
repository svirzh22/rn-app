import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ReservationDetailsRouteProp = RouteProp<AppNavigatorParams, 'ReservationDetails'>;
type ReservationDetailsNavigationProp = StackNavigationProp<
AppNavigatorParams,
'ReservationDetails'
>;

export interface ReservationDetailsScreenProps {
  navigation: ReservationDetailsNavigationProp;
  route: ReservationDetailsRouteProp;
}
