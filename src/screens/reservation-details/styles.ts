import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  header: {
    height: 100,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
    paddingHorizontal: SPACING.M,
    justifyContent: 'center',
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
  },
  title: {
    paddingLeft: SPACING.M,
    paddingTop: 14,
    color: COLORS.GREY[700],
  },
  extras: {
    paddingLeft: SPACING.M,
    paddingTop: 10,
    paddingBottom: 22,
  },
  container: {
    backgroundColor: COLORS.WHITE,
  },
});
