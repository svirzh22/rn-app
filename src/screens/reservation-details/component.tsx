import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import find from 'lodash/find';

import {
  CustomModal,
  FlexView,
  Gutter,
  PlayerList,
  Screen,
  Text,
  useModal,
  ModalConfirmDelete,
  ActivityIndicator,
  ActionItem,
} from '@components';
import { COLORS, SPACING } from '@constants';
import { formatDate, formatTime, translate } from '@helpers';
import { bookingDetailsResource, courseResource, userCancelBookingResource } from '@api';
import { useAlert, useRefetchOnInternetReachable, useSessionExpired } from '@hooks';
import { memberSelector } from '@selectors';
import { BookingOptions, BookingOptionType } from '@api/resources/types/booking';
import { styles } from './styles';
import { ReservationDetailsScreenProps } from './types';

export const getExtrasLabel = (extras?: BookingOptions) => (extras ? Object
  .keys(extras)
  .reduce((acc, key: string) => {
    const { quantity, label } = extras[key as BookingOptionType];
    if (extras[key as BookingOptionType].quantity) {
      const text = label || translate(`reservationDetails.${key}`, { count: quantity });
      return `${acc ? `${acc},` : acc} ${text}`;
    }

    return acc;
  }, '').trim() : '');

export const ReservationDetailsScreen: React.FC<ReservationDetailsScreenProps> = ({
  navigation,
  route,
}) => {
  const { showAlert } = useAlert();
  const { reservationId } = route.params;

  const queryClient = useQueryClient();
  const { onSessionExpired } = useSessionExpired();
  const member = useSelector(memberSelector);

  const { data: coursesResponse, isLoading: isLoadingCourses, refetch: refetchCourses } = useQuery(
    [courseResource.cacheKey],
    courseResource.getCourses,
    { retry: 0, cacheTime: 0 },
  );
  const {
    data: detailsResponse,
    isLoading: isLoadingDetails,
    refetch: refetchDetails,
  } = useQuery(
    [bookingDetailsResource.cacheKey, { bookingId: reservationId, userToken: member.token }],
    bookingDetailsResource.fetchBookingDetails,
    { onError: onSessionExpired },
  );

  const { mutateAsync, isLoading: isLoadingCancel } = useMutation(userCancelBookingResource.cancelBooking, {
    onSuccess: () => {
      queryClient.invalidateQueries('user-bookings');
    },
  });

  const { visible, toggleModal } = useModal();
  const handleEditPress = useCallback(() => {
    navigation.navigate('ReservationEdit', { reservationId });
  }, [navigation, reservationId]);

  const refetchRequests = useCallback(() => {
    refetchCourses();
    refetchDetails();
  }, [refetchCourses, refetchDetails]);

  useRefetchOnInternetReachable({
    refetch: refetchRequests,
  });

  const handlePaymentPress = useCallback(() => {
    navigation.navigate('ChoosePaymentMethod', { bookingId: reservationId });
  }, [navigation, reservationId]);
  const booking = detailsResponse?.booking;

  const handleDeletePress = useCallback(async () => {
    try {
      toggleModal();
      await mutateAsync({ bookingId: reservationId, userToken: member.token });
      navigation.goBack();
    } catch (exc) {
      showAlert({ text: exc.message });
    }
  }, [toggleModal, mutateAsync, reservationId, member.token, navigation, showAlert]);

  if (isLoadingCourses || isLoadingDetails) {
    return <ActivityIndicator size="large" mode="standalone" />;
  }

  const course = find(coursesResponse?.courses, ['id', booking?.golfCourse]);
  const players = Object
    .values(booking?.players || [])
    .filter(({ id, addedId }) => id || addedId);
  const hasOptions = Object
    .values(booking?.options || [])
    .reduce((acc, item) => acc || Boolean(item.quantity), false);

  const isOwner = booking?.member === member.id;

  return (
    <Screen
      style={{ paddingHorizontal: 0 }}
      onRetry={refetchRequests}
      unsafe
      withScroll
      containerStyle={styles.container}
    >
      <FlexView style={styles.header}>
        <FlexView>
          <Text label={course?.title} size={24} />
        </FlexView>
        <Gutter isVertical amount={SPACING.XXS} />
        <FlexView flexDirection="row" justifyContent="space-between">
          <Text label={formatDate(detailsResponse?.booking?.timeSlot)} size={20} />
          <Text label={formatTime(detailsResponse?.booking?.timeSlot)} size={20} />
        </FlexView>
      </FlexView>
      <Text t="reservation.players" size={18} style={styles.title} />
      <PlayerList editable={false} items={players} />
      {hasOptions && (
        <FlexView style={styles.separator}>
          <Text t="reservation.extras" size={18} style={styles.title} />
          <Text style={styles.extras} label={getExtrasLabel(detailsResponse?.booking.options)} />
        </FlexView>
      )}
      {detailsResponse?.booking?.canUpdate && (
        <ActionItem
          color={COLORS.BLACK}
          withArrow
          name="edit"
          label={translate('reservationDetails.editItem')}
          onPress={handleEditPress}
        />
      )}
      {[0, 1].includes(Number(detailsResponse?.booking?.paymentState)) && isOwner && (
        <ActionItem
          color={COLORS.BLACK}
          withArrow
          name="cart"
          label={translate('reservationDetails.paymentItem')}
          onPress={handlePaymentPress}
        />
      )}
      {detailsResponse?.booking?.canCancel && isOwner && (
        <ActionItem
          color={COLORS.RED}
          name="close"
          label={translate('reservationDetails.deleteItem')}
          onPress={toggleModal}
        />
      )}
      <CustomModal visible={visible} onCancel={toggleModal}>
        <ModalConfirmDelete onConfirm={handleDeletePress} onCancel={toggleModal} />
      </CustomModal>
      {isLoadingCancel && <ActivityIndicator mode="portal" />}
    </Screen>
  );
};
