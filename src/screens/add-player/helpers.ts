export const diffPlayers = ({
  selectedPlayers,
  previousSelectedPlayers,
}: {
  selectedPlayers: number[];
  previousSelectedPlayers: number[];
}) => {
  const newPlayers = selectedPlayers.filter((selectedPlayer) =>
    !previousSelectedPlayers.includes(selectedPlayer),
  );
  const removedPlayers = previousSelectedPlayers.filter((memberId) =>
    !selectedPlayers.find((member) => member === memberId),
  );

  return {
    newPlayers,
    removedPlayers,
  };
};
