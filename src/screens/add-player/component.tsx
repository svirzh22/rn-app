import React, { FC, useState, useEffect } from 'react';
import { SectionList, SectionListRenderItem, SectionListData } from 'react-native';
import { useDebounce } from 'use-debounce';

import {
  FlexView,
  Gutter,
  Icon,
  Screen,
  Touchable,
  Divider,
  Text,
  Header,
  LinkButton,
  SearchInput,
} from '@components';
import { COLORS, SPACING } from '@constants';
import { useTranslate } from '@hooks';
import { ActivityIndicator } from '@components/activity-indicator';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { EmptyPlayers, UserItem } from './components';
import { styles } from './styles';
import { AddPlayerScreenProps } from './types';
import { useAddPlayerHandlers } from './hooks/use-handlers';
import { useQueries } from './hooks/use-queries';

export const AddPlayerScreen: FC<AddPlayerScreenProps> = ({ route, navigation }) => {
  const { currentPlayers } = route.params;
  const translate = useTranslate();
  const [searchText, setSearchText] = useState<string>('');
  const [selectedPlayers, setSelectedPlayers] = useState<PlayerEntityInternal[]>(currentPlayers || []);
  const [selectedVisitors, setSelectedVisitors] = useState<PlayerEntityInternal[]>([]);
  const [debouncedSearchText] = useDebounce(searchText, 300);

  const { isLoading, players, recentPlayers, refetch } = useQueries({
    debouncedSearchText,
  });
  const { handleDonePress, handlePressAddVisitor, handleUserItemPress } = useAddPlayerHandlers({
    navigation,
    route,
    selectedPlayers,
    setSelectedPlayers,
  });

  useEffect(() => {
    if (debouncedSearchText.length >= 3) refetch();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchText]);

  useEffect(() => {
    if (route.params.visitor) {
      handleUserItemPress(route.params.visitor)();
      setSelectedVisitors([...selectedVisitors, route.params.visitor]);
      navigation.setParams({ visitor: undefined });
    }
  }, [handleUserItemPress, navigation, route.params.visitor, setSelectedVisitors, selectedVisitors]);

  const renderItem: SectionListRenderItem<PlayerEntityInternal> = ({ item }) => (
    <Touchable onPress={handleUserItemPress(item)}>
      <UserItem
        item={item}
        selected={Boolean(selectedPlayers.find((player) =>
          (item.id && item.id === player.id)
          || (item.addedId && item.addedId === player.addedId)),
        )}
      />
    </Touchable>
  );
  const renderSeparator = () => <Divider />;
  const keyExtractor = (item: PlayerEntityInternal) => String(item.id || item.addedId);

  const renderSectionHeader = ({ section }: { section: SectionListData<PlayerEntityInternal> }) => (
    <FlexView style={styles.listHeader}>
      <Text label={section.title} color={COLORS.GREY[700]} />
    </FlexView>
  );
  const renderContent = () => {
    if (isLoading) {
      return <ActivityIndicator mode="standalone" />;
    }

    if (!debouncedSearchText && recentPlayers?.recentlyAdded.length) {
      return (
        <SectionList
          renderSectionHeader={renderSectionHeader}
          sections={[
            {
              title: translate('addPlayer.recentlyAdded'),
              data: selectedVisitors
                ? [...selectedVisitors, ...recentPlayers?.recentlyAdded]
                : recentPlayers?.recentlyAdded,
            },
          ]}
          renderItem={renderItem}
          ItemSeparatorComponent={renderSeparator}
          keyExtractor={keyExtractor}
          stickySectionHeadersEnabled={false}
        />
      );
    }

    if (
      (debouncedSearchText.length < 3 && !recentPlayers?.recentlyAdded.length)
      || (!players?.suggestions.addedPlayers.length && !players?.suggestions.users.length)
    ) {
      return (
        <EmptyPlayers />
      );
    }

    return (
      <SectionList
        renderSectionHeader={renderSectionHeader}
        sections={[{
          title: translate('addPlayer.allMembers'),
          data: players ? [...players?.suggestions.addedPlayers, ...players?.suggestions.users] : [],
        }]}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
        keyExtractor={keyExtractor}
        stickySectionHeadersEnabled={false}
      />
    );
  };

  return (
    <Screen>
      <Header
        title="addPlayer.headerTitle"
        backIconName="close"
        preset="withBorder"
        renderHeaderRight={() => <LinkButton t="common.done" onPress={handleDonePress} weight="medium" />}
      />
      <FlexView style={styles.searchInputContainer}>
        <Icon name="search" size={16} color={COLORS.GREY[700]} />
        <Gutter amount={SPACING.XS} />
        <SearchInput
          value={searchText}
          onChangeText={setSearchText}
          placeholder={translate('addPlayer.searchMember')}
        />
      </FlexView>

      <Touchable style={styles.addVisitor} onPress={handlePressAddVisitor}>
        <FlexView flexDirection="row" alignItems="center">
          <Icon name="add-member" style={styles.memberIcon} size={17} />
          <Gutter amount={SPACING.L} />
          <Text t="addPlayer.addVisitor" />
        </FlexView>
        <Icon name="arrow-right" style={styles.memberIcon} size={11} />
      </Touchable>
      <Divider />
      {renderContent()}
    </Screen>
  );
};
