import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  searchInputContainer: {
    height: 42,
    backgroundColor: COLORS.GREY[200],
    marginHorizontal: SPACING.M,
    borderRadius: 24,
    paddingHorizontal: SPACING.S,
    marginTop: SPACING.M,
    marginBottom: SPACING.XS,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVisitor: {
    paddingHorizontal: SPACING.M,
    height: 56,
    marginVertical: SPACING.XXS,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  memberIcon: {
    marginLeft: SPACING.S,
  },
  listHeader: {
    paddingLeft: SPACING.M,
    paddingTop: SPACING.M,
    paddingBottom: SPACING.XXS,
  },
});
