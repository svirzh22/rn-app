import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { playersResource, recentlyAddedResource } from '@api';
import { useSessionExpired } from '@hooks';
import { memberSelector } from '@selectors';

export const useQueries = ({
  debouncedSearchText,
}: {
  debouncedSearchText: string;
}) => {
  const member = useSelector(memberSelector);
  const { onSessionExpired } = useSessionExpired();
  const { data: players, isLoading: isLoadingPlayers, refetch } = useQuery(
    [playersResource.cacheKey, { query: debouncedSearchText, userToken: member.token }],
    playersResource.fetchPlayers,
    { onError: onSessionExpired, enabled: false },
  );
  const { data: recentPlayers, isLoading: isLoadingRecent } = useQuery(
    [recentlyAddedResource.cacheKey, { userToken: member.token }],
    recentlyAddedResource.fetchRecentlyAdded,
  );

  return {
    players,
    recentPlayers,
    refetch,
    isLoading: isLoadingPlayers || isLoadingRecent,
  };
};
