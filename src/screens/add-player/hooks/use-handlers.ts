import { PlayerEntityInternal } from '@api/resources/types/player';
import { useCallback } from 'react';

import { useAlert } from '@hooks';
import { AddPlayerNavigationProp, AddPlayerScreenRouteProp } from '../types';

export const useAddPlayerHandlers = ({
  navigation,
  route,
  selectedPlayers,
  setSelectedPlayers,
}: {
  selectedPlayers: PlayerEntityInternal[];
  navigation: AddPlayerNavigationProp;
  route: AddPlayerScreenRouteProp;
  setSelectedPlayers: (players: PlayerEntityInternal[]) => void;
}) => {
  const { showAlert } = useAlert();
  const { currentPlayers = [], routeName, available } = route.params;

  const handlePressAddVisitor = useCallback(() => navigation.navigate('AddVisitor', {}), [navigation]);

  const handleUserItemPress = useCallback((player: PlayerEntityInternal) => () => {
    const isReservationEdit = routeName === 'ReservationEdit';

    const nonSelectablePlayer = selectedPlayers.find((item) => (
      (item.addedId && item.addedId === player.addedId)
      || (item.id && item.id === player.id)
    ) && !player.editable);

    const isMemberInList = selectedPlayers.find(
      (item) => (item.id && item.id === player.id) || (item.addedId && item.addedId === player.addedId),
    );

    if (nonSelectablePlayer && !nonSelectablePlayer?.editable && !isReservationEdit) {
      showAlert({ t: 'addPlayer.notEditablePlayer' });
    } else if (isMemberInList) {
      setSelectedPlayers(selectedPlayers.filter((item) =>
        !(item.id && item.id === player.id)
          && !(item.addedId && item.addedId === player.addedId)),
      );
    } else if (selectedPlayers.length - currentPlayers?.length <= available - 1) {
      setSelectedPlayers([...selectedPlayers, { ...player, editable: true }]);
    } else {
      showAlert({ t: 'errors.maxNumberOfMembers' });
    }
    return null;
  },
  [available, currentPlayers, selectedPlayers, setSelectedPlayers, routeName, showAlert]);

  const handleDonePress = useCallback(() => {
    navigation.navigate(routeName as any, {
      newPlayers: selectedPlayers,
    });
  }, [navigation, routeName, selectedPlayers]);

  return {
    handleDonePress,
    handlePressAddVisitor,
    handleUserItemPress,
  };
};
