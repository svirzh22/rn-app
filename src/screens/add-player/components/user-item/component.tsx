import React, { FC } from 'react';

import { displayHandicap } from '@helpers';
import { COLORS, SPACING } from '@constants';
import { FlexView, Avatar, Text, Gutter, Icon } from '@components';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { styles } from './styles';

interface UserItemProps {
  item: PlayerEntityInternal;
  selected: boolean;
}

export const UserItem: FC<UserItemProps> = ({ item, selected }) => (
  <FlexView style={styles.item}>
    <Avatar image={item.picture} />
    <Gutter amount={SPACING.S} />
    <FlexView flex={1}>
      <Text label={`${item.firstname} ${item.lastname}`} />
      <Text label={`HCP ${displayHandicap(item.handicap)}`} size={14} color={COLORS.GREY[700]} />
    </FlexView>
    {selected && (
      <FlexView justifyContent="center">
        <Icon name="check" size={16} color={COLORS.SECONDARY[500]} />
      </FlexView>
    )}
  </FlexView>
);
