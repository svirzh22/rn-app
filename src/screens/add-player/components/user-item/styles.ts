import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  item: {
    paddingHorizontal: SPACING.M,
    paddingVertical: SPACING.S - 4,
    flexDirection: 'row',
  },
});
