import { FlexView, Gutter, Icon, Text } from '@components';
import { COLORS } from '@constants';
import React from 'react';

export const EmptyPlayers = () => (
  <FlexView flex={1} justifyContent="center" alignItems="center">
    <Icon name="empty-players" size={46} color={COLORS.GREY[600]} />
    <Gutter isVertical amount={12} />
    <Text t="addPlayer.emptyPlayers" />
  </FlexView>
);
