import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

export type AddPlayerScreenRouteProp = RouteProp<AppNavigatorParams, 'AddPlayer'>;
export type AddPlayerNavigationProp = StackNavigationProp<
AppNavigatorParams,
'AddPlayer'
>;

export interface AddPlayerScreenProps {
  navigation: AddPlayerNavigationProp;
  route: AddPlayerScreenRouteProp;
}
