import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  searchInputContainer: {
    height: 42,
    backgroundColor: COLORS.GREY[200],
    marginHorizontal: SPACING.M,
    borderRadius: 24,
    paddingHorizontal: SPACING.S,
    marginTop: SPACING.M,
    marginBottom: SPACING.XS,
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchInput: {
    fontSize: responsiveFontSize(16),
    width: '100%',
  },
  languageItem: {
    flexDirection: 'row',
    paddingHorizontal: SPACING.M,
    height: 60,
    borderBottomColor: COLORS.GREY[400],
    borderBottomWidth: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
