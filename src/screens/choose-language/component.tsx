import React, { FC, useState } from 'react';
import { FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import i18n from 'i18n-js';
import keys from 'lodash/keys';
import isEmpty from 'lodash/isEmpty';
import identity from 'lodash/identity';
import { useNavigation, CommonActions } from '@react-navigation/native';

import { setLocale } from '@src/actions/settings';
import { localeSelector, memberSelector } from '@selectors';
import { Screen, Header, FlexView, Icon, Text, Touchable, LinkButton } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface ChooseLanguageProps {
  locale: string;
}

export const ChooseLanguage: FC<ChooseLanguageProps> = () => {
  const member = useSelector(memberSelector);
  const locale = useSelector(localeSelector);
  const [selectedLanguage, setLanguage] = useState<string>(locale);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const data = keys(i18n.translations);
  const renderItem = ({ item }: { item: string }) => (
    <Touchable onPress={() => setLanguage(item)}>
      <FlexView style={styles.languageItem}>
        <Text t={`common.${item}`} size={18} />
        {selectedLanguage === item && <Icon name="check" size={16} color={COLORS.SECONDARY[500]} />}
      </FlexView>
    </Touchable>
  );
  const reloadApp = () => {
    isEmpty(member)
      ? navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: 'Login',
            },
          ],
        }),
      )
      : navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: 'Tabs',
              state: {
                routes: [
                  {
                    name: 'Profile',
                  },
                ],
              },
            },
          ],
        }),
      );
  };
  const onPressDone = () => {
    if (locale !== selectedLanguage) {
      dispatch(setLocale(selectedLanguage));
      return reloadApp();
    }
    return navigation.goBack();
  };
  const renderHeaderRight = () => <LinkButton onPress={onPressDone} t="common.done" weight="medium" />;

  return (
    <Screen>
      <Header
        preset="withBorder"
        backIconName="close"
        title="chooseLanguage.title"
        renderHeaderRight={renderHeaderRight}
      />
      <FlatList data={data} keyExtractor={identity} renderItem={renderItem} />
    </Screen>
  );
};
