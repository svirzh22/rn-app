import * as Yup from 'yup';
import '@helpers/yup';
import { translate } from '@helpers';

export const billingAddressValidationSchema = () => Yup.object().shape({
  firstname: Yup
    .string()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  lastname: Yup.string()
    .required(translate('validations.required'))
    .min(3, translate('validations.minChars', { count: 3 }))
    .max(16, translate('validations.maxChars', { count: 16 })),
  // @ts-expect-error
  phone: Yup.string().isValidNumber(),
  email: Yup
    .string()
    .email(translate('validations.email'))
    .required(translate('validations.required')),
});
