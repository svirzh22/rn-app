import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  stepIcon: {
    height: 36,
    width: 36,
    borderRadius: 18,
    borderWidth: 1,
    borderColor: COLORS.PRIMARY[500],
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    marginBottom: SPACING.M - 4,
    flexDirection: 'row',
  },
});
