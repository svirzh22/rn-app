import React, { FC } from 'react';

import { FlexView, Text, Gutter } from '@components';
import { COLORS, SPACING } from '@constants';
import { styles } from './styles';

interface StepHeaderProps {
  number: number | string;
  t: string;
}

export const StepHeader: FC<StepHeaderProps> = ({ number, t }) => (
  <FlexView style={styles.container}>
    <FlexView style={styles.stepIcon}>
      <Text label={number} color={COLORS.PRIMARY[500]} />
    </FlexView>
    <Gutter amount={SPACING.M - 4} />
    <Text t={t} size={24} />
  </FlexView>
);
