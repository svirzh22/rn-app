import React, { FC } from 'react';

import { FlexView } from '@components';
import { StepHeader } from '../step-header';

interface PaymentCardProps {}

export const PaymentCard: FC<PaymentCardProps> = () => (
  <FlexView>
    <StepHeader number={2} t="creditCardPayment.preferredPaymentCard" />
  </FlexView>
);
