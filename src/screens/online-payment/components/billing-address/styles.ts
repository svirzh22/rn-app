import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    marginBottom: SPACING.M,
  },
  checkbox: {
    height: 24,
    width: 24,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.GREY[600],
    justifyContent: 'center',
    alignItems: 'center',
  },
});
