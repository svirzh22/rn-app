import React, { FC, useCallback } from 'react';
import { TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { FlexView, FormPhoneInput, FormTextInput, Text, Gutter, Icon, LinkButton } from '@components';
import { COLORS, SPACING } from '@constants';
import { StepHeader } from '../step-header';
import { styles } from './styles';

interface BillingAddressProps {
  onPressCondition: (value: boolean) => void;
  isConditionAccepted: boolean;
}

export const BillingAddress: FC<BillingAddressProps> = ({ isConditionAccepted, onPressCondition }) => {
  const navigation = useNavigation();
  const handleConditionsPress = useCallback(() => {
    navigation.navigate('Conditions');
  }, [navigation]);

  return (
    <FlexView style={styles.container}>
      <StepHeader number={1} t="creditCardPayment.billingAddress" />
      <FormTextInput
        name="firstname"
        options={{
          t: 'common.firstName',
        }}
      />
      <FormTextInput
        name="lastname"
        options={{
          t: 'common.lastName',
        }}
      />
      <FormPhoneInput
        name="phone"
        options={{
          t: 'common.phone',
        }}
      />
      <FormTextInput
        name="email"
        options={{
          t: 'common.email',
          keyboardType: 'email-address',
          autoCapitalize: 'none',
        }}
      />
      <TouchableOpacity onPress={() => onPressCondition(!isConditionAccepted)}>
        <FlexView flexDirection="row" alignItems="center">
          <FlexView style={styles.checkbox}>
            {isConditionAccepted && <Icon name="check" color={COLORS.PRIMARY[500]} />}
          </FlexView>
          <Gutter amount={SPACING.S} />
          <Text t="creditCardPayment.acceptCondition" />
          <Gutter isVertical amount={10} />
        </FlexView>
      </TouchableOpacity>
      <Gutter isVertical />
      <FlexView alignItems="center">
        <LinkButton t="creditCardPayment.conditions" onPress={handleConditionsPress} />
      </FlexView>
    </FlexView>
  );
};
