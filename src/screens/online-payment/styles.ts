import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    backgroundColor: COLORS.WHITE,
    paddingVertical: SPACING.M + 2,
  },
  buttonContainer: {
    paddingHorizontal: SPACING.M,
    marginBottom: SPACING.XXS,
  },
});
