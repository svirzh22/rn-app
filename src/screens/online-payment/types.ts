import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type OnlinePaymentScreenRouteProp = RouteProp<AppNavigatorParams, 'OnlinePayment'>;
type OnlinePaymentNavigationProp = StackNavigationProp<
AppNavigatorParams,
'OnlinePayment'
>;

export interface OnlinePaymentScreenProps {
  navigation: OnlinePaymentNavigationProp;
  route: OnlinePaymentScreenRouteProp;
}
