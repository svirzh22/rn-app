import React, { FC, useCallback, useRef, useState } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Formik } from 'formik';
import { useMutation, useQuery } from 'react-query';
import { useSelector } from 'react-redux';

import { Screen, Button, FlexView, ActivityIndicator } from '@components';
import { bookingConfirmResource, userResource } from '@api';
import { memberSelector } from '@selectors';
import { useAlert } from '@hooks';
import { HttpBookingAlreadyConfirmedError } from '@services';
import { BillingAddress } from './components';
import { styles } from './styles';
import { billingAddressValidationSchema } from './schema';
import { OnlinePaymentScreenProps } from './types';

export const OnlinePaymentScreen: FC<OnlinePaymentScreenProps> = ({
  navigation,
  route,
}) => {
  const member = useSelector(memberSelector);
  const { showAlert } = useAlert();
  const [isConditionAccepted, setConditionAccepted] = useState<boolean>(false);
  const formRef = useRef<any>();
  const insets = useSafeAreaInsets();
  const { data: userData, isLoading: isLoadingUserData } = useQuery(
    [userResource.cacheKey, { userToken: member.token }],
    userResource.getUserInfo,
  );
  const {
    mutateAsync: confirmBooking,
    isLoading: confirmBookingLoading,
  } = useMutation(bookingConfirmResource.confirmBooking);
  const handlePressConfirm = useCallback(() => formRef.current?.submitForm(), []);
  const handleConfirm = useCallback(async (values) => {
    try {
      const res = await confirmBooking({
        userToken: member.token,
        ...values,
        ...route.params,
      });
      if (res.confirmationState === 'DONE') {
        navigation.navigate('BookingCompleted', { booking: route.params.booking });
      } else {
        navigation.navigate('ProcessPayment', { saferPay: res.saferPay, booking: route.params.booking });
      }
    } catch (exc) {
      if (exc instanceof HttpBookingAlreadyConfirmedError) {
        navigation.navigate('BookingCompleted', { booking: route.params.booking });
      } else {
        showAlert({ text: exc.message });
      }
    }
  }, [confirmBooking, member.token, navigation, route.params, showAlert]);
  const isConfirmDisabled = !isConditionAccepted;

  if (isLoadingUserData) return (<ActivityIndicator mode="standalone" />);

  return (
    <Screen unsafe>
      <Formik
        initialValues={{
          firstname: userData?.user.firstname,
          lastname: userData?.user.lastname,
          phone: userData?.user.mobilePhone, // userData?.user.mobilePhone,
          email: userData?.user.email,
        }}
        onSubmit={handleConfirm}
        validationSchema={billingAddressValidationSchema()}
        innerRef={formRef}
      >
        {() => (
          <>
            <KeyboardAwareScrollView contentContainerStyle={styles.container}>
              <BillingAddress
                onPressCondition={setConditionAccepted}
                isConditionAccepted={isConditionAccepted}
              />
            </KeyboardAwareScrollView>
            <FlexView style={[styles.buttonContainer, { paddingBottom: insets.bottom }]}>
              <Button
                t="creditCardPayment.confirmBooking"
                onPress={handlePressConfirm}
                disabled={isConfirmDisabled}
                loading={confirmBookingLoading}
              />
            </FlexView>
          </>
        )}
      </Formik>
    </Screen>
  );
};
