import React, { FC, useState } from 'react';
import { useQuery } from 'react-query';
import Video from 'react-native-video';
import { RouteProp, useRoute } from '@react-navigation/native';

import { Screen, Image, FlexView, ActivityIndicator, Gutter } from '@components';
import { AppNavigatorParams } from '@navigation';
import { courseHolesResource } from '@api';
import { styles } from './styles';

interface CourseHoleProps {}

export const CourseHoleScreen: FC<CourseHoleProps> = () => {
  const route = useRoute<RouteProp<AppNavigatorParams, 'CourseHole'>>();
  const { golfCourse, holeNumber } = route?.params || {};
  const [videoLoading, setVideoLoading] = useState(true);

  const { data, isLoading } = useQuery(
    [courseHolesResource.cacheKey, { golfCourse: golfCourse.id, holeNumber }],
    courseHolesResource.getHole,
    { retry: 0 },
  );

  const renderContent = () => (
    <FlexView>
      {data?.hole.holeMapFootage && (
        <>
          {videoLoading && <ActivityIndicator style={[styles.video, { position: 'absolute' }]} />}
          <Video
            source={{ uri: data?.hole.holeMapFootage }}
            style={styles.video}
            resizeMode="contain"
            repeat
            onReadyForDisplay={() => setVideoLoading(false)}
            // eslint-disable-next-line no-console
            onError={(e) => console.log(e, 'e')}
            controls
          />
        </>
      )}
      <Gutter isVertical />
      {data?.hole.holeMapPicture && (
        <Image source={{ uri: data?.hole.holeMapPicture }} style={styles.holeMapPicture} />
      )}
    </FlexView>
  );
  return (
    <Screen
      style={!isLoading ? styles.screen : {}}
      containerStyle={styles.containerStyle}
      unsafe
      withScroll
    >
      {isLoading ? (
        <FlexView flex={1} justifyContent="center" alignItems="center" style={styles.loaderContainer}>
          <ActivityIndicator size="large" />
        </FlexView>
      ) : (
        renderContent()
      )}
    </Screen>
  );
};
