import { StyleSheet } from 'react-native';
import { COLORS, SCREEN_HEIGHT, SPACING } from '@constants';

export const styles = StyleSheet.create({
  screen: {
    paddingTop: SPACING.S,
    paddingHorizontal: SPACING.M,
    paddingBottom: SPACING.M,
  },
  containerStyle: {
    backgroundColor: COLORS.WHITE,
  },
  loaderContainer: {
    height: SCREEN_HEIGHT,
    paddingBottom: SPACING.XXL,
  },
  video: {
    height: 200,
    borderRadius: 30,
  },
  holeMapPicture: {
    height: 500,
    borderRadius: 30,
  },
});
