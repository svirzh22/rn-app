import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  screen: {
    backgroundColor: COLORS.WHITE,
  },
  header: {
    height: 100,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
    paddingHorizontal: SPACING.M,
    justifyContent: 'center',
  },
  container: {
    paddingHorizontal: 0,
    paddingTop: 0,
    marginBottom: SPACING.XXL,
    backgroundColor: 'white',
    flex: 1,
  },
  datePicker: {
    borderWidth: 0,
    paddingHorizontal: 0,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  datePickerContainer: {
    height: 100,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
    width: '100%',
    marginBottom: 0,
  },
  editTimeSlotIcon: {
    position: 'absolute',
    right: -34,
    top: 4,
  },
  timeSlotContainer: {
    marginRight: SPACING.L + 4,
  },
});
