import { StyleSheet } from 'react-native';
import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    marginLeft: 0,
    marginBottom: 0,
    marginTop: 0,
    width: '100%',
    justifyContent: 'flex-end',
    backgroundColor: COLORS.WHITE,
  },
});
