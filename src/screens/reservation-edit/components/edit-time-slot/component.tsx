import React, { FC } from 'react';
import moment from 'moment';
import Modal from 'react-native-modal';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { TimeSlotEntity } from '@api/resources/time-slots/types';
import { BookingEntity } from '@api/resources/types/booking';
import { Header, BookingSlotsList } from '@components';
import { useFetchTimeslots } from '@hooks';
import { styles } from './styles';

interface EditTimeSlotProps {
  isVisible: boolean;
  hide: () => void;
  booking: BookingEntity | undefined;
  players: number | undefined;
  setTimeSlot: (date: Date) => void;
}

export const EditTimeSlot: FC<EditTimeSlotProps> = ({
  isVisible,
  hide,
  booking,
  players,
  setTimeSlot,
}) => {
  const [date] = booking?.timeSlot?.split(' ') || [];
  const insets = useSafeAreaInsets();
  const handlePressItem = (timeSlot: TimeSlotEntity) => {
    const nextTimeSlotTime = `${date} ${timeSlot.start}`;
    setTimeSlot(moment(nextTimeSlotTime).toDate());
    hide();
  };
  const {
    timeslotsData,
    isFetchingTimeslots,
    isLoadingTimeslots,
    refetchTimeslots,
    filterTimeslots,
  } = useFetchTimeslots({
    selectedDate: +moment(date),
    selectedCourse: booking?.golfCourse || 0,
    players: players || 0,
  });

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={hide}
      // @ts-expect-error
      onRequestClose={hide}
      style={[styles.modal, { paddingTop: insets.top }]}
    >
      <Header preset="withBorder" title="reservationEditTimeSlot.headerTitle" onPressBack={hide} />
      <BookingSlotsList
        data={timeslotsData}
        isLoading={isLoadingTimeslots}
        isFetching={isFetchingTimeslots}
        refetch={refetchTimeslots}
        filterTimeslots={filterTimeslots}
        showBook={false}
        onPressItem={handlePressItem}
      />
    </Modal>
  );
};
