import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ReservationEditScreenRouteProp = RouteProp<AppNavigatorParams, 'ReservationEdit'>;
type ReservationEditNavigationProp = StackNavigationProp<
AppNavigatorParams,
'ReservationEdit'
>;

export interface ReservationEditScreenProps {
  navigation: ReservationEditNavigationProp;
  route: ReservationEditScreenRouteProp;
}
