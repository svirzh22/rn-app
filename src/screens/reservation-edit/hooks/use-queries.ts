import { useQuery } from 'react-query';

import { getListCarsResource } from '@api';
import { useSessionExpired } from '@hooks';

export const useQueries = ({ bookingDate }: { bookingDate: string }) => {
  const { onSessionExpired } = useSessionExpired();
  const { isLoading: isLoadingCarsList, data } = useQuery(
    [getListCarsResource.cacheKey, { date: bookingDate }],
    getListCarsResource.getCars,
    { onError: onSessionExpired },
  );

  return {
    data,
    isLoadingCarsList,
  };
};
