import { useCallback, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useMutation, useQuery } from 'react-query';
import { useSelector } from 'react-redux';
import map from 'lodash/map';
import values from 'lodash/values';
import omit from 'lodash/omit';

import { memberSelector } from '@selectors';
import { BookingEntity, BookingMemberEntity } from '@api/resources/types/booking';
import { bookingDetailsResource, userBookingsResource } from '@api';
import { useAlert, useApiErrorAlert } from '@hooks';
import { formatDateTimeBE } from '@helpers';

interface Params {
  members?: BookingMemberEntity[];
  initialMembers?: number[];
  toggleModal: () => void;
  setMembers: (members?: BookingMemberEntity[]) => void;
  refetch: () => void;
  booking: BookingEntity | undefined;
  timeSlot: Date;
}

export const useHandlers = ({ members, toggleModal, setMembers, refetch, booking, timeSlot }: Params) => {
  const { showApiErrorAlert } = useApiErrorAlert();
  const { showAlert } = useAlert();
  const member = useSelector(memberSelector);
  const [isLoading, setLoading] = useState(false);
  const { mutateAsync: updateBooking } = useMutation(bookingDetailsResource.updateBookingDetails);
  const { refetch: refetchBookings } = useQuery(
    [userBookingsResource.cacheKey, { userToken: member.token }],
    userBookingsResource.fetchUserBookings,
    { retry: 0 },
  );

  const [memberToDelete, setMemberToDelete] = useState<BookingMemberEntity>();
  const navigation = useNavigation();

  const handleDeletePlayer = useCallback(
    (bookingMember: BookingMemberEntity) => {
      setMemberToDelete(bookingMember);
      toggleModal();
    },
    [setMemberToDelete, toggleModal],
  );

  const handleConfirmDeletePlayer = useCallback(() => {
    setMembers(
      members?.filter((item) => {
        if (memberToDelete?.id) {
          return item.id !== memberToDelete?.id;
        }

        return item.addedId !== memberToDelete?.addedId;
      }),
    );
    toggleModal();
  }, [memberToDelete, members, setMembers, toggleModal]);

  const handleCancelModal = useCallback(() => {
    toggleModal();
    setMemberToDelete(undefined);
  }, [toggleModal]);

  const handleAddItem = useCallback(() => {
    navigation.navigate('AddPlayer', {
      currentPlayers: members,
      routeName: 'ReservationEdit',
      available: 4 - Number(members?.length),
    });
  }, [navigation, members]);

  const handleEditVisitor = useCallback(
    (visitor) => {
      navigation.navigate('AddVisitor', { visitor });
    },
    [navigation],
  );

  const handleSubmit = useCallback(
    async ({ ...extras }) => {
      try {
        setLoading(true);
        const response = await updateBooking({
          bookingId: String(booking?.id),
          userToken: member.token,
          players: members?.map((item) => ({
            id: item.id || item.addedId,
            type: item.id ? 'REGISTERED' : 'ADDED',
          })),
          extras: map(values(omit(extras, 'cancellationInsurance')), (quantity, index) => ({
            id: index + 1,
            quantity,
          })),
          timeSlot: formatDateTimeBE(timeSlot),
        });

        if (response.reason) {
          showAlert({ text: response.reason });
        }
        await refetch();
        refetchBookings();
        setLoading(false);
        navigation.goBack();
      } catch (e) {
        setLoading(false);
        showApiErrorAlert(e);
      }
    },
    [
      members,
      navigation,
      refetch,
      updateBooking,
      member,
      booking,
      timeSlot,
      refetchBookings,
      showAlert,
      showApiErrorAlert,
    ],
  );

  return {
    handleAddItem,
    handleCancelModal,
    handleConfirmDeletePlayer,
    handleDeletePlayer,
    handleEditVisitor,
    handleSubmit,
    isLoadingUpdateBooking: isLoading,
  };
};
