import React, { useCallback, useEffect, useRef, useState } from 'react';
import moment from 'moment';
import find from 'lodash/find';
import objectValues from 'lodash/values';
import { Formik } from 'formik';
import { useMutation, useQuery } from 'react-query';
import { useSelector } from 'react-redux';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { memberSelector } from '@selectors';
import {
  CustomModal,
  FlexView,
  Gutter,
  Header,
  LinkButton,
  ModalConfirmDelete,
  PlayerList,
  Screen,
  Text,
  useModal,
  Icon,
  Touchable,
} from '@components';
import { COLORS, SPACING } from '@constants';
import { usePlayersCost } from '@screens/reservation/hooks/use-players-cost';
import { Options } from '@screens/reservation/components';
import { BookingMemberEntityInternal } from '@api/resources/types/booking';
import { formatDate, formatTime } from '@helpers';
import { useSessionExpired } from '@hooks';
import { bookingCalculatePriceResource, bookingDetailsResource, courseResource } from '@api';
import { ActivityIndicator } from '@components/activity-indicator';
import { styles } from './styles';
import { useHandlers, useQueries } from './hooks';
import { ReservationEditScreenProps } from './types';
import { EditTimeSlot } from './components';

// bookingDetails -> listCars
const OPTION_TYPE = {
  small_car: 'smallCar',
  electric_car: 'electricCar',
  caddy: 'caddy',
};

export const ReservationEditScreen: React.FC<ReservationEditScreenProps> = ({ route }) => {
  const formRef = useRef<any>();
  const navigation = useNavigation();
  const { reservationId, visitor, newPlayers, removedPlayers } = route.params;
  const { onSessionExpired } = useSessionExpired();
  const member = useSelector(memberSelector);
  const { data: coursesResponse } = useQuery([courseResource.cacheKey], courseResource.getCourses, {
    retry: 0,
  });
  const { data: detailsResponse, isFetching, refetch } = useQuery(
    [bookingDetailsResource.cacheKey, { bookingId: reservationId, userToken: member.token }],
    bookingDetailsResource.fetchBookingDetails,
    { onError: onSessionExpired },
  );
  const { visible, toggleModal } = useModal();
  const [isVisibleEditTimeSlot, setVisibleEditTimeSlot] = useState(false);
  const [timeSlot, setTimeSlot] = useState<Date>(moment(detailsResponse?.booking?.timeSlot).toDate());
  const [members, setMembers] = useState<BookingMemberEntityInternal[]>();
  const players = objectValues(detailsResponse?.booking?.players).filter((item) => item.id || item.addedId);
  const { mutateAsync: calculatePrice } = useMutation(bookingCalculatePriceResource.calculatePriceMultiple, {
    onError: onSessionExpired,
  });

  useEffect(() => {
    if (!members) {
      const nextMembers = players?.map((player) => ({
        ...player,
        editable: player.id !== member.id,
        own: player.id === member.id,
      }));

      setMembers(nextMembers);
    }
  }, [members, players, member.id]);

  useFocusEffect(
    useCallback(() => {
      if (visitor) {
        setMembers(members?.map((item) => (item.id === visitor.id ? visitor : item)));
        navigation.setParams({ ...route.params, visitor: undefined });
      }

      if (members && (newPlayers?.length || removedPlayers?.length)) {
        setMembers(newPlayers);
        navigation.setParams({ ...route.params, newPlayers: [], removedPlayers: [] });
      }
    }, [visitor, newPlayers, removedPlayers, members, navigation, route.params]),
  );
  const course = find(coursesResponse?.courses, ['id', detailsResponse?.booking?.golfCourse]);
  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);

  const {
    handleAddItem,
    handleCancelModal,
    handleConfirmDeletePlayer,
    handleDeletePlayer,
    handleEditVisitor,
    handleSubmit,
    isLoadingUpdateBooking,
  } = useHandlers({
    members,
    toggleModal,
    setMembers,
    refetch,
    initialMembers: players.map(({ id }) => id),
    booking: detailsResponse?.booking,
    timeSlot,
  });
  const { data, isLoadingCarsList } = useQueries({ bookingDate: String(detailsResponse?.booking?.timeSlot) });
  const initialExtras = data?.reduce((acc, { label, type }) => {
    // @ts-ignore
    const { quantity } = detailsResponse?.booking?.options[OPTION_TYPE[type]] || {};
    return {
      ...acc,
      [label]: quantity || 0,
    };
  }, {});
  const [date, time] = detailsResponse?.booking?.timeSlot?.split(' ') || [];

  const { playersCost, updatePlayersCost, setCostUpdateInProgress, costUpdateInProgress } = usePlayersCost({
    bookingDate: date,
    time,
    players: (members || []) as any,
    golfCourse: detailsResponse?.booking?.golfCourse || 0,
    calculatePrice,
  });

  useEffect(() => {
    updatePlayersCost();
    setCostUpdateInProgress(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [members]);

  if ((isFetching || isLoadingCarsList) && !isLoadingUpdateBooking) {
    return <ActivityIndicator mode="standalone" />;
  }

  return (
    <FlexView style={styles.screen} flex={1}>
      <Header
        preset="withBorder"
        title="reservationEdit.headerTitle"
        renderHeaderRight={() => (
          <LinkButton t="common.done" onPress={handleSubmitPress} isLoading={isLoadingUpdateBooking} />
        )}
        isSafe
      />
      <Screen style={styles.container} withScroll unsafe>
        <EditTimeSlot
          isVisible={isVisibleEditTimeSlot}
          hide={() => setVisibleEditTimeSlot(false)}
          booking={detailsResponse?.booking}
          players={members?.length}
          setTimeSlot={setTimeSlot}
        />

        <Formik
          innerRef={formRef}
          initialValues={{
            ...initialExtras,
            cancellationInsurance: !!detailsResponse?.booking?.insurance?.cost,
          }}
          onSubmit={handleSubmit}
        >
          {() => (
            <>
              <Touchable onPress={() => setVisibleEditTimeSlot(true)}>
                <FlexView style={styles.header}>
                  <FlexView style={styles.timeSlotContainer}>
                    <FlexView alignItems="center" flexDirection="row">
                      <Text label={course?.title} size={24} />
                    </FlexView>
                    <Gutter isVertical amount={SPACING.XXS} />
                    <FlexView flexDirection="row" justifyContent="space-between">
                      <Text label={formatDate(timeSlot)} size={20} />
                      <Text label={formatTime(timeSlot)} size={20} />
                      <Icon
                        name="pencil"
                        style={styles.editTimeSlotIcon}
                        size={16}
                        color={COLORS.GREY[600]}
                      />
                    </FlexView>
                  </FlexView>
                </FlexView>
              </Touchable>
              <PlayerList
                editable
                items={members}
                onEdit={handleEditVisitor}
                onDelete={handleDeletePlayer}
                onAddItem={handleAddItem}
              />
              <Options
                course={Number(detailsResponse?.booking?.golfCourse)}
                data={data || []}
                playersCost={playersCost}
                isCancellationInsuranceDisabled
              />
            </>
          )}
        </Formik>

        <CustomModal visible={visible} onCancel={toggleModal}>
          <ModalConfirmDelete onConfirm={handleConfirmDeletePlayer} onCancel={handleCancelModal} />
        </CustomModal>
        {costUpdateInProgress && <ActivityIndicator mode="portal" />}
      </Screen>
    </FlexView>
  );
};
