import { ProfileMenuItem } from './components/menu/types';

export const menuItems: ProfileMenuItem[] = [
  {
    title: 'profile.bookingsMenu',
    icon: 'calendar',
    name: 'bookings',
    showArrow: true,
  },
  {
    title: 'profile.editProfileMenu',
    icon: 'edit',
    name: 'edit',
    showArrow: true,
  },
  {
    title: 'profile.languageMenu',
    icon: 'language',
    name: 'language',
    showArrow: true,
  },
  {
    title: 'profile.logout',
    icon: 'logout',
    name: 'logout',
    showArrow: false,
  },
];
