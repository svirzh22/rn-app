import React, { useCallback } from 'react';
import { View } from 'react-native';
import { useQuery } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import { Avatar, Gutter, Screen, Text } from '@components';
import { DATE_FORMAT_SINCE, displayHandicap, formatDate } from '@helpers';
import { logOut } from '@src/actions';
import { userResource } from '@api';
import { memberSelector } from '@selectors';
import { COLORS } from '@constants';
import { useRefetchOnInternetReachable, useSessionExpired } from '@hooks';
import { ActivityIndicator } from '@components/activity-indicator';
import { ProfileMenu, RegistrationDate } from './components';
import { menuItems } from './constants';
import { styles } from './styles';
import { ProfileScreenProps } from './types';

export const ProfileScreen: React.FC<ProfileScreenProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const member = useSelector(memberSelector);
  const { onSessionExpired } = useSessionExpired();
  const { data, isLoading, isSuccess, refetch } = useQuery(
    [userResource.cacheKey, { userToken: member.token }],
    userResource.getUserInfo,
    {
      retry: false,
      onError: onSessionExpired,
    },
  );
  const { user } = data || {};
  useRefetchOnInternetReachable({ refetch });

  const handleMenuItemPress = useCallback(
    (value) => {
      switch (value) {
        case 'bookings':
          return navigation.navigate('MyBookings');
        case 'language':
          return navigation.navigate('ChooseLanguage');
        case 'logout':
          return dispatch(logOut(true));
        case 'edit':
        default:
          return navigation.navigate('ProfileEdit');
      }
    },
    [navigation, dispatch],
  );

  if (isLoading) {
    return <ActivityIndicator mode="standalone" />;
  }

  return (
    <Screen style={styles.content} containerStyle={styles.container} withScroll onRetry={refetch}>
      {isSuccess && (
        <>
          <Gutter amount={16} isVertical />
          <Avatar size={112} image={user?.picture} />
          <Gutter amount={16} isVertical />
          <Text style={styles.name} label={`${user?.firstname} ${user?.lastname}`} align="center" />
          <Text label={`HCP ${displayHandicap(user?.handicap)}`} align="center" color={COLORS.GREY[700]} />
          <Gutter amount={16} isVertical />
          <RegistrationDate value={formatDate(moment(user?.createdAt?.date), DATE_FORMAT_SINCE)} />
          <Gutter amount={32} isVertical />
          <View style={styles.delimiter} />
          <ProfileMenu items={menuItems} onItemPress={handleMenuItemPress} />
        </>
      )}
    </Screen>
  );
};
