import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ProfileScreenRouteProp = RouteProp<AppNavigatorParams, 'Profile'>;
type ProfileNavigationProp = StackNavigationProp<
AppNavigatorParams,
'Profile'
>;

export interface ProfileScreenProps {
  navigation: ProfileNavigationProp;
  route: ProfileScreenRouteProp;
}
