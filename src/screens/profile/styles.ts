import { StyleSheet } from 'react-native';
import { COLORS, SCREEN_WIDTH, SPACING } from '@constants';

export const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 0,
    backgroundColor: COLORS.WHITE,
    alignItems: 'center',
    marginTop: SPACING.XS,
  },
  container: {
    backgroundColor: 'white',
  },
  delimiter: {
    width: SCREEN_WIDTH,
    height: 8,
    backgroundColor: COLORS.GREY[200],
  },
  name: {
    fontSize: 24,
    paddingHorizontal: SPACING.S,
  },
});
