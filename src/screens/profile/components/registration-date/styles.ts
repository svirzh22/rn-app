import { COLORS } from '@constants';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {

  },
  text: {
    color: COLORS.GREY[700],
    fontSize: 14,
    alignSelf: 'center',
    borderRadius: 16,
    borderColor: COLORS.GREY[500],
    borderWidth: 1,
    paddingHorizontal: 17,
    paddingVertical: 8,
  },
});
