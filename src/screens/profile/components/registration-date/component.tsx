import React from 'react';

import { Text } from '@components';
import { styles } from './styles';

interface RegistrationDateProps {
  value: string;
}

export const RegistrationDate: React.FC<RegistrationDateProps> = ({
  value,
}) => (
  <Text
    style={styles.text}
    t="profile.registrationDate"
    translateOptions={{ value }}
  />
);
