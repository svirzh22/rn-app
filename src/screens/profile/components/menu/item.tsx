import React from 'react';
import { View } from 'react-native';

import { FlexView, Icon, Text, Touchable } from '@components';
import { COLORS } from '@constants';
import { ProfileMenuItemProps } from './types';
import { styles } from './styles';

export const ProfileMenuItem: React.FC<ProfileMenuItemProps> = ({ item, value, onPress }) => (
  <>
    <Touchable onPress={onPress}>
      <FlexView
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        style={styles.container}
      >
        <FlexView
          flexDirection="row"
          // justifyContent="space-between"
          alignItems="center"
          flex={1}
          style={styles.content}
        >
          <Icon name={item.icon} style={styles.icon} />
          <Text style={[styles.text, styles.title]} t={item.title} />
          <Text style={[styles.text, styles.value]} label={value} />
        </FlexView>
        {item.showArrow && <Icon name="arrow-right" color={COLORS.GREY[600]} />}
      </FlexView>
    </Touchable>
    <View style={styles.delimiter} />
  </>
);
