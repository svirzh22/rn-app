import { IconNameType } from '@components';

export interface ProfileMenuProps {
  items: ProfileMenuItem[];
  onItemPress: (name: string) => void;
}

export interface ProfileMenuItemProps {
  item: ProfileMenuItem;
  value?: string;
  onPress: () => void;
}

export interface ProfileMenuItem {
  title: string;
  icon: IconNameType;
  name: string;
  showArrow: boolean;
}
