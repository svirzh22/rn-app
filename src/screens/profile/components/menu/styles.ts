import { COLORS, SCREEN_WIDTH } from '@constants';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingLeft: 21,
    paddingRight: 32,
    width: SCREEN_WIDTH,
  },
  content: {
    height: 64,
  },
  icon: {
    marginRight: 20,
  },
  delimiter: {
    height: 1,
    backgroundColor: COLORS.GREY[400],
  },
  text: {
    fontSize: 18,
  },
  title: {
    flex: 1,
  },
  value: {
    color: COLORS.GREY[700],
    paddingRight: 15,
  },
  hideBookings: {
    height: 64,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
    paddingLeft: 21,
    paddingRight: 32,
    justifyContent: 'center',
  },
});
