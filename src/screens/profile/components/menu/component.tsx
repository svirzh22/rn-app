import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { useMutation, useQuery } from 'react-query';

import { userResource } from '@api';
import { FlexView, Switch } from '@components';
import { localeSelector, memberSelector } from '@selectors';
import { useSessionExpired, useTranslate } from '@hooks';
import { ProfileMenuItem } from './item';
import { ProfileMenuProps } from './types';
import { styles } from './styles';

export const ProfileMenu: React.FC<ProfileMenuProps> = ({ items, onItemPress }) => {
  const translate = useTranslate();
  const { onSessionExpired } = useSessionExpired();
  const member = useSelector(memberSelector);
  const { data } = useQuery([userResource.cacheKey, { userToken: member.token }], userResource.getUserInfo, {
    retry: 0,
    onError: onSessionExpired,
  });
  const locale = useSelector(localeSelector);
  const { mutateAsync: updateUser, isLoading: isLoadingUpdateUser } = useMutation(userResource.updateUser);
  const values: { [key: string]: string } = { language: translate(`common.${locale}`) };
  const [hideBookings, setHideBookings] = useState<boolean>(!!data?.user?.private);
  const handleChange = useCallback(
    (value) => {
      updateUser({
        userToken: member.token,
        private: value,
      });
      setHideBookings(value);
    },
    [updateUser, member.token],
  );

  const handleItemPress = useCallback(
    (name) => () => {
      onItemPress(name);
    },
    [onItemPress],
  );

  return (
    <FlexView>
      {items.map((item) => (
        <ProfileMenuItem
          item={item}
          key={`profile-menu-${item.name}`}
          value={values[item.name]}
          onPress={handleItemPress(item.name)}
        />
      ))}
      <FlexView style={styles.hideBookings}>
        <Switch
          value={hideBookings}
          t="privacy.enablePrivacy"
          onChange={handleChange}
          disabled={isLoadingUpdateUser}
        />
      </FlexView>
    </FlexView>
  );
};
