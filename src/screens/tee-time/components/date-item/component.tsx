import React, { FC } from 'react';

import { FlexView, Text } from '@components';
import { Date } from '@screens/tee-time/components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface DateItemProps {
  isSelected: boolean;
  item: Date;
}

export const DateItem: FC<DateItemProps> = ({ isSelected, item }) => (
  <FlexView style={[styles.item, isSelected && styles.selected]}>
    <Text label={`${item.day}`} size={22} weight="medium" />
    <Text label={item.dayName} color={isSelected ? COLORS.BLACK : COLORS.GREY[700]} />
  </FlexView>
);
