import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

const ITEM_WIDTH = 64;

export const styles = StyleSheet.create({
  item: {
    height: 72,
    width: ITEM_WIDTH,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: COLORS.GREY[200],
    justifyContent: 'center',
    alignItems: 'center',
  },
  selected: {
    borderWidth: 0,
    borderColor: COLORS.TRANSPARENT,
    backgroundColor: COLORS.PRIMARY[500],
  },
});

export const itemSize = () => ITEM_WIDTH + SPACING.S;
