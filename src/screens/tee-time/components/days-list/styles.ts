import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  contentContainerStyle: {
    paddingHorizontal: SPACING.M,
    paddingBottom: SPACING.M,
    paddingTop: SPACING.XXS,
  },
});
