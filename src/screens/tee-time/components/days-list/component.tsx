import React, { FC, useEffect, useRef } from 'react';
import { FlatList, ListRenderItem } from 'react-native';
import moment from 'moment';
import times from 'lodash/times';

import { FlexView, Gutter, Touchable } from '@components';
import { SPACING } from '@constants';
import { getDayName } from '@helpers';
import { DateItem, styles as itemStyles, itemSize } from '../date-item';
import { styles } from './styles';

interface DaysListProps {
  onDateChange: (date: number) => void;
  selectedDate: number;
}

export interface Date {
  day: number;
  dayName: string;
}

const getData = (selectedDate: number): Date[] => {
  const daysInMonth = moment(selectedDate).daysInMonth();
  // eslint-disable-next-line operator-linebreak
  const isSameMonth =
    moment().year() === moment(selectedDate).year() && moment().month() === moment(selectedDate).month();
  const date = isSameMonth ? moment().date() : 1;
  const d = daysInMonth - date + 1;

  return times<Date>(d, (index) => ({
    day: index + date,
    dayName: getDayName(+moment(selectedDate).date(index + date)),
  }));
};

export const DaysList: FC<DaysListProps> = ({ onDateChange, selectedDate }) => {
  const flatListRef = useRef<any>();
  const handleChangeDate = (item: Date) => onDateChange(+moment(selectedDate).date(item.day));
  const selectedDay = moment(selectedDate).date();
  const renderItem: ListRenderItem<Date> = ({ item }) => {
    const isSelected = item.day === selectedDay;
    return (
      <Touchable
        onPress={() => handleChangeDate(item)}
        disabled={isSelected}
        style={itemStyles.item}
        borderRadius={12}
      >
        <DateItem isSelected={isSelected} item={item} />
      </Touchable>
    );
  };
  const data = getData(selectedDate);
  const keyExtractor = (item: Date) => `${item.day}`;
  const renderSeparator = () => <Gutter amount={SPACING.S} />;
  const getItemLayout = (_data: any, index: number) =>
    // let itemSize = itemStyles.itemSize();
    ({
      index,
      length: itemSize(),
      offset: index * itemSize(),
    });

  const itemIndex = data.findIndex((item) => item.day === selectedDay);

  useEffect(() => {
    setTimeout(() => flatListRef?.current?.scrollToIndex({ index: itemIndex }), 250);
  }, [itemIndex, selectedDate]);

  return (
    <FlexView>
      <FlatList
        ref={flatListRef}
        horizontal
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ItemSeparatorComponent={renderSeparator}
        contentContainerStyle={styles.contentContainerStyle}
        showsHorizontalScrollIndicator={false}
        getItemLayout={getItemLayout}
        initialScrollIndex={itemIndex}
      />
    </FlexView>
  );
};
