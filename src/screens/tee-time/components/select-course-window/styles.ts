import { StyleSheet, ViewStyle } from 'react-native';

import { COLORS, SPACING } from '@constants';

const courseItem: ViewStyle = {
  height: 57,
  width: '100%',
  justifyContent: 'center',
  alignItems: 'center',
};

export const styles = StyleSheet.create({
  header: {
    marginHorizontal: SPACING.M,
    marginBottom: SPACING.S,
  },
  courseItem,
  courseItemSelected: {
    ...courseItem,
    backgroundColor: COLORS.GREY[200],
  },
  bottomWindow: {
    height: 220,
    paddingVertical: SPACING.S,
  },
  doneButton: {
    right: 0,
    position: 'absolute',
  },
  arrowDown: {
    position: 'absolute',
    left: 0,
  },
});
