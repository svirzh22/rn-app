import React, { FC } from 'react';
import map from 'lodash/map';

import { BottomModalWindow, FlexView, IconButton, Text, Touchable } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface SelectCourseWindowProps {
  isVisible: boolean;
  onPressHide: () => void;
  selectedCourse: number;
  setCourse: (course: number) => void;
  courses?: CourseEntity[];
}

export const SelectCourseWindow: FC<SelectCourseWindowProps> = ({
  isVisible,
  onPressHide,
  selectedCourse,
  setCourse,
  courses,
}) => (
  <BottomModalWindow isVisible={isVisible} hide={onPressHide} windowStyle={styles.bottomWindow}>
    <FlexView flexDirection="row" justifyContent="center" alignItems="center" style={styles.header}>
      <IconButton
        onPress={onPressHide}
        name="arrow-down"
        size={11}
        color={COLORS.GREY[600]}
        containerStyle={styles.arrowDown}
      />
      <Text t="teeTime.selectCourse" size={22} weight="medium" />
    </FlexView>
    {map(courses, (course) => (
      <Touchable
        disabled={course.id === selectedCourse}
        style={course.id === selectedCourse ? styles.courseItemSelected : styles.courseItem}
        onPress={() => {
          setCourse(course.id);
          onPressHide();
        }}
        key={String(course.id)}
      >
        <Text label={course.title} />
      </Touchable>
    ))}
  </BottomModalWindow>
);
