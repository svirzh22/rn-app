import React, { FC, useState, useCallback, useMemo, useEffect } from 'react';
import { find, propEq } from 'ramda';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { useFocusEffect } from '@react-navigation/native';

import {
  FlexView,
  Screen,
  Text,
  Divider,
  Touchable,
  MonthYearPicker,
  ActivityIndicator,
  BookingSlotsList,
} from '@components';
import { COLORS } from '@constants';
import {
  formatDateTimeBE,
  formatMonthYear,
  getSeasonStartDate,
  getTimeslotDate,
} from '@helpers';
import { setLockInfo } from '@src/actions';
import { useAlert, useRefetchOnInternetReachable, useSessionExpired } from '@hooks';
import { TimeSlotEntity } from '@api/resources/time-slots/types';
import { styles } from './styles';
import { DaysList, SelectCourseWindow } from './components';
import { TeeTimeScreenProps } from './types';
import { useQueries } from './hooks';

export const TeeTimeScreen: FC<TeeTimeScreenProps> = ({ navigation, route }) => {
  const { showAlert } = useAlert();
  const dispatch = useDispatch();

  const { bookingDate, golfCourse, players, onChangeDate } = route?.params || {};
  const { onSessionExpired } = useSessionExpired();

  const [selectedCourse, setCourse] = useState<number>(golfCourse || 0);
  const [selectedDate, setDate] = useState<number>(bookingDate || getSeasonStartDate().valueOf());
  const [selectedTimeslot, setSelectedTimeslot] = useState<string>();
  const [isVisibleCourseModal, setVisibleCourseModal] = useState<boolean>(false);
  const [isVisibleMonthYearPicker, setVisibleMonthYearPicker] = useState<boolean>(false);
  const {
    coursesData,
    isLoadingCourses,
    refetchCourses,
    timeslotsData,
    isFetchingTimeslots,
    isLoadingTimeslots,
    refetchTimeslots,
    lockBooking,
    isLoadingLockBooking,
    filterTimeslots,
  } = useQueries({
    selectedCourse,
    selectedDate,
    players,
    setCourse,
    onSessionExpired,
  });

  useEffect(() => {
    if (onChangeDate) {
      onChangeDate(selectedDate);
    }
    setSelectedTimeslot(undefined);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDate]);

  useFocusEffect(
    useCallback(() => {
      if (bookingDate) {
        setDate(bookingDate);
        navigation.setParams({ bookingDate: undefined });
      }

      if (typeof golfCourse === 'number') {
        setCourse(golfCourse);
      }
    }, [bookingDate, golfCourse, navigation]),
  );

  useRefetchOnInternetReachable({
    refetch: refetchTimeslots,
  });
  useRefetchOnInternetReachable({
    extraCondition: !isLoadingCourses && !coursesData,
    refetch: refetchCourses,
  });

  const course = useMemo(() => find(propEq('id', selectedCourse), coursesData?.courses || []), [
    selectedCourse,
    coursesData,
  ]);

  const handlePressCourse = useCallback(() => setVisibleCourseModal(true), []);
  const handleChangeDate = useCallback(setDate, []);
  const handlePressMonth = useCallback(() => {
    setVisibleMonthYearPicker(true);
  }, []);
  const hideCourseModal = useCallback(() => setVisibleCourseModal(false), []);
  const hideMothYearPicker = useCallback(() => setVisibleMonthYearPicker(false), []);

  const handleItemPress = useCallback(
    async (timeslot: TimeSlotEntity) => {
      const timeSlotDate = getTimeslotDate(selectedDate, timeslot.start);

      const timeSlot = formatDateTimeBE(timeSlotDate);
      try {
        const result = await lockBooking({ golfCourse: selectedCourse, timeSlot, nbPlayers: 1 });
        // extract
        if (result.checkId) {
          navigation.navigate('Reservation', {
            golfCourse: selectedCourse,
            golfCourseTitle: coursesData?.courses[selectedCourse].title,
            bookingDate: selectedDate,
            time: timeslot.start,
            timeslotPlayers: Object.values(timeslot.players).filter((item) => item.id || item.addedId),
            availability: timeslot.availability,
          });
          dispatch(setLockInfo({ checkId: result.checkId, timeSlot }));
          setSelectedTimeslot(timeslot.start);
        } else if (result.bookable === false && result.error === false && !result.validityEnd) {
          showAlert({ text: result.reason });
        } else {
          // @ts-ignore
          const waitTill = (moment(result.validityEnd * 1000) - moment()) / 60000;

          showAlert({ t: 'errors.alreadyLocked', options: { value: Math.round(waitTill) + 1 } });
        }
      } catch (e) {
        showAlert({ text: e?.data?.errorMessage });
      }
    },
    [coursesData, dispatch, lockBooking, navigation, selectedCourse, selectedDate, showAlert],
  );

  return (
    <Screen unsafe>
      <Touchable onPress={handlePressCourse}>
        <FlexView style={styles.block}>
          <Text t="teeTime.course" color={COLORS.GREY[700]} />
          {course ? <Text label={course?.title} /> : <ActivityIndicator />}
        </FlexView>
      </Touchable>
      <Divider />
      <Touchable onPress={handlePressMonth}>
        <FlexView style={styles.block}>
          <Text t="common.date" color={COLORS.GREY[700]} />
          <Text label={formatMonthYear(selectedDate)} />
        </FlexView>
      </Touchable>

      <DaysList onDateChange={handleChangeDate} selectedDate={selectedDate} />
      <FlexView style={styles.separator} />
      <BookingSlotsList
        data={timeslotsData}
        isLoading={isLoadingTimeslots}
        isFetching={isFetchingTimeslots}
        refetch={refetchTimeslots}
        selectedTimeslot={selectedTimeslot}
        filterTimeslots={filterTimeslots}
        onPressItem={handleItemPress}
      />

      <SelectCourseWindow
        isVisible={isVisibleCourseModal}
        onPressHide={hideCourseModal}
        selectedCourse={selectedCourse}
        setCourse={setCourse}
        courses={coursesData?.courses}
      />

      <MonthYearPicker
        isVisible={isVisibleMonthYearPicker}
        onHide={hideMothYearPicker}
        selectedDate={new Date(selectedDate)}
        onDateChange={handleChangeDate}
      />
      {isLoadingLockBooking && <ActivityIndicator mode="portal" />}
    </Screen>
  );
};
