import { useMutation, useQuery } from 'react-query';

import { courseResource, lockBookingResource } from '@api';
import { useFetchTimeslots } from '@hooks';

export const useQueries = ({
  selectedCourse,
  selectedDate,
  setCourse,
  players,
  onSessionExpired,
}: {
  selectedCourse: number;
  selectedDate: number;
  setCourse: (course: number) => void;
  players: number;
  onSessionExpired: (data: any) => void;
}) => {
  const {
    data: coursesData,
    isLoading: isLoadingCourses,
    refetch: refetchCourses,
  } = useQuery(
    [courseResource.cacheKey],
    courseResource.getCourses,
    {
      retry: 0,
      onSuccess: (result) => {
        if (!selectedCourse) {
          setCourse(result?.courses && result?.courses[0].id);
        }
      },
    },
  );

  const { mutateAsync: lockBooking, isLoading: isLoadingLockBooking } = useMutation(
    lockBookingResource.lockBooking,
    {
      onError: onSessionExpired,
    },
  );

  const {
    timeslotsData,
    isLoadingTimeslots,
    isFetchingTimeslots,
    refetchTimeslots,
    filterTimeslots,
  } = useFetchTimeslots({
    selectedCourse,
    selectedDate,
    players,
  });

  return {
    coursesData,
    isLoadingCourses,
    refetchCourses,
    lockBooking,
    isLoadingLockBooking,

    timeslotsData,
    isLoadingTimeslots,
    isFetchingTimeslots,
    refetchTimeslots,
    filterTimeslots,
  };
};
