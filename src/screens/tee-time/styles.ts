import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  block: {
    height: 56,
    paddingHorizontal: SPACING.M,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  separator: {
    height: 8,
    backgroundColor: COLORS.GREY[200],
  },
});
