import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type TeeTimeScreenRouteProp = RouteProp<AppNavigatorParams, 'TeeTime'>;
type TeeTimeNavigationProp = StackNavigationProp<
AppNavigatorParams,
'TeeTime'
>;

export interface TeeTimeScreenProps {
  navigation: TeeTimeNavigationProp;
  route: TeeTimeScreenRouteProp;
}
