import React, { FC, useRef } from 'react';
import WebView from 'react-native-webview';

import { ACADEMY_LINK } from '@constants';
import { ActivityIndicator, FlexView, WebViewInternal } from '@components';
import { styles } from './styles';

interface AcademyProps {}

export const AcademyScreen: FC<AcademyProps> = () => {
  const ref = useRef<WebView>(null);
  return (
    <>
      <WebViewInternal
        ref={ref}
        source={{ uri: ACADEMY_LINK }}
        renderLoading={() => (
          <FlexView style={styles.loaderContainer}>
            <ActivityIndicator size="large" />
          </FlexView>
        )}
        startInLoadingState
      />
    </>
  );
};
