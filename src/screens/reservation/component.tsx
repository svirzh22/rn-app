import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { Platform, ScrollView } from 'react-native';
import { Formik } from 'formik';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { CommonActions, useFocusEffect } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { useMutation } from 'react-query';
import NetInfo from '@react-native-community/netinfo';

import { bookingConfirmResource } from '@api';
import { Screen, FlexView, Text, Gutter, Button } from '@components';
import {
  formatDate,
  REVERSED_DATE_FORMAT_DASH,
  sessionExpiredAlert,
} from '@helpers';
import { useAlert, useApiErrorAlert } from '@hooks';
import { SPACING } from '@constants';
import { memberSelector, lockInfoSelector } from '@selectors';
import { ActivityIndicator } from '@components/activity-indicator';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { HttpAlreadyLockedError } from '@services';
import { styles } from './styles';
import { Players, Options } from './components';
import { ReservationScreenProps } from './types';
import { useQueries } from './hooks/use-queries';
import { usePlayersCost } from './hooks/use-players-cost';

export const ReservationScreen: FC<ReservationScreenProps> = ({ navigation, route }) => {
  const { showApiErrorAlert } = useApiErrorAlert();
  const { showAlert } = useAlert();
  const { isInternetReachable } = NetInfo.useNetInfo();
  const formRef = useRef<any>();
  const member = useSelector(memberSelector);
  const lockInfo = useSelector(lockInfoSelector);
  const { mutateAsync: confirmBooking, isLoading: confirmBookingLoading } = useMutation(
    bookingConfirmResource.confirmBooking,
  );

  const { time, bookingDate, golfCourse, golfCourseTitle, timeslotPlayers, newPlayers } = route.params;

  const dateForBooking = `${formatDate(bookingDate, REVERSED_DATE_FORMAT_DASH)} ${time}`;
  const { data, isLoading, calculatePrice, createBooking, user } = useQueries({
    bookingDate,
    member,
  });

  const [players, setPlayers] = useState<PlayerEntityInternal[]>([
    ...timeslotPlayers,
    { ...(user || {}), ...member, editable: false, own: true } as any,
  ]);

  const { playersCost, updatePlayersCost, setCostUpdateInProgress, costUpdateInProgress } = usePlayersCost({
    bookingDate,
    time,
    players,
    golfCourse,
    calculatePrice,
  });

  useEffect(() => {
    // Fix missing user picture
    if (!isLoading) {
      setPlayers([...timeslotPlayers, { ...(user || {}), ...member, editable: false, own: true } as any]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading]);

  const insets = useSafeAreaInsets();
  const [creationInProgress, setCreationInProgress] = useState(false);

  useEffect(() => {
    updatePlayersCost();
    setCostUpdateInProgress(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [players]);

  useFocusEffect(
    useCallback(() => {
      if (newPlayers && newPlayers.length) {
        setPlayers(newPlayers);
        navigation.setParams({ newPlayers: [] });
      }
    }, [navigation, newPlayers]),
  );

  const handleContinueBookingPress = useCallback(async () => {
    if (isInternetReachable === false) {
      return showAlert({ t: 'errors.noConnection' });
    }

    setCreationInProgress(true);
    const extras = data
      ?.map((item) => ({
        id: item.id,
        quantity: Number(formRef.current.values[item.label]),
      }))
      .filter(({ quantity }) => quantity);
    const booking = {
      extras,
      golfCourse,
      timeSlot: dateForBooking,
      players: [
        ...players
          .filter(({ editable, own }) => editable || own)
          .map(({ id, addedId }) => ({
            id: id || addedId,
            type: addedId ? 'ADDED' : 'REGISTERED',
          })),
      ],
      version: Platform.OS === 'ios' ? 'iOS' : 'android',
      checkId: lockInfo.checkId as number,
      insurance: formRef.current.values.cancellationInsurance,
    };

    try {
      const res = await createBooking({ ...booking, userToken: member.token });

      if (res.bookable === false) {
        throw new HttpAlreadyLockedError(res.reason);
      }

      // await freeBooking({ checkId: booking.checkId });

      if (res?.booking?.id) {
        if (res.booking?.totalCost !== '0.00') {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Tabs' },
                { name: 'ReservationDetails', params: { reservationId: res.booking?.id } },
              ],
            }),
          );
          navigation.navigate('ChoosePaymentMethod', {
            bookingId: res?.booking?.id,
          });
        } else {
          await confirmBooking({
            userToken: member.token,
            firstname: user?.firstname as string,
            lastname: user?.lastname as string,
            booking: res?.booking?.id as number,
            email: user?.email || '',
            phone: user?.mobilePhone || '',
          });
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: 'Tabs' }, { name: 'BookingCompleted', params: { booking: res?.booking?.id } }],
            }),
          );
        }
      }
    } catch (exc) {
      if (exc.data.errorType === 'NO_MATCH') {
        return sessionExpiredAlert();
      }

      if (exc.data.errorMessage) {
        showApiErrorAlert(exc);
      } else if (exc.message) {
        showAlert({ text: exc.message });
      } else {
        showAlert({ t: 'common.sessionExpired' });
      }
    } finally {
      setCreationInProgress(false);
    }

    return null;
  }, [
    isInternetReachable,
    data,
    golfCourse,
    dateForBooking,
    players,
    lockInfo.checkId,
    createBooking,
    member.token,
    navigation,
    confirmBooking,
    user,
    showAlert,
    showApiErrorAlert,
  ]);

  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);

  const handleAddPlayerPress = useCallback(
    () =>
      navigation.navigate('AddPlayer', {
        currentPlayers: players,
        available: 4 - players.length,
        routeName: 'Reservation',
      }),
    [navigation, players],
  );

  const handleRemovePlayerPress = useCallback(
    (id: number | null | string) => () => {
      setPlayers(
        players.filter((item) => (item.id && item.id !== id) || (item.addedId && item.addedId !== id)),
      );
    },
    [players],
  );

  if (isLoading) {
    return <ActivityIndicator mode="standalone" />;
  }

  const initialValues = data?.reduce((acc, { label }) => ({ ...acc, [label]: 0 }), {});

  return (
    <Screen unsafe>
      <ScrollView>
        <FlexView style={styles.header}>
          <FlexView>
            <Text label={golfCourseTitle} size={24} />
          </FlexView>
          <Gutter isVertical amount={SPACING.XXS} />
          <FlexView flexDirection="row" justifyContent="space-between">
            <Text label={formatDate(bookingDate)} size={20} />
            <Text label={time} size={20} />
          </FlexView>
        </FlexView>
        <Players
          items={players}
          onAddPlayerPress={handleAddPlayerPress}
          onRemovePlayerPress={handleRemovePlayerPress}
          price={playersCost}
        />
        <Formik
          innerRef={formRef}
          initialValues={{ ...initialValues, cancellationInsurance: false }}
          onSubmit={handleContinueBookingPress}
        >
          {() => <Options course={golfCourse} data={data || []} playersCost={playersCost} />}
        </Formik>
      </ScrollView>

      <FlexView style={styles.buttonContainer}>
        <Button
          onPress={handleSubmitPress}
          t="reservation.continueBooking"
          style={{ marginBottom: insets.bottom }}
        />
      </FlexView>
      {(creationInProgress || costUpdateInProgress || confirmBookingLoading) && (
        <ActivityIndicator mode="portal" />
      )}
    </Screen>
  );
};
