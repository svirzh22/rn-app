import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  header: {
    height: 100,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
    paddingHorizontal: SPACING.M,
    justifyContent: 'center',
  },
  buttonContainer: {
    marginHorizontal: SPACING.M,
    marginVertical: SPACING.XS,
  },
  footer: {
    paddingTop: SPACING.S - 4,
    paddingRight: SPACING.M,
    alignItems: 'flex-end',
  },
});
