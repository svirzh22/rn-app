import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type ReservationScreenRouteProp = RouteProp<AppNavigatorParams, 'Reservation'>;
type ReservationNavigationProp = StackNavigationProp<
AppNavigatorParams,
'Reservation'
>;

export interface ReservationScreenProps {
  navigation: ReservationNavigationProp;
  route: ReservationScreenRouteProp;
}
