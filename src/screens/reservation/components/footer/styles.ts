import { StyleSheet } from 'react-native';

import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  footer: {
    paddingTop: SPACING.S - 4,
    alignItems: 'flex-end',
  },
});
