import React from 'react';

import { FlexView, Text } from '@components';
import { APP_CURRENCY, COLORS } from '@constants';
import { styles } from './styles';

interface ReservationFooterProps {
  price: number;
}

export const ReservationFooter: React.FC<ReservationFooterProps> = ({ price }) => (
  <FlexView style={styles.footer}>
    <Text label={`Total ${price} ${APP_CURRENCY}`} color={COLORS.SECONDARY[500]} weight="medium" />
  </FlexView>
);
