import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    padding: SPACING.M,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
  },
  optionItem: {
    marginTop: SPACING.S,
  },
  cancellationInsuranceContainer: {
    height: 44,
  },
});
