import React, { FC, useRef, useState } from 'react';
// @ts-ignore
import { useFormikContext } from 'formik';

import { View } from 'react-native';
import { useOptionsCost } from '@screens/reservation/hooks/use-options-cost';
import { FlexView, FormOptionItem, FormOptionItemCheckbox, WebViewModal } from '@components';
import { getConditionsUrl } from '@helpers';
import { CarOption } from '@api/resources/types/car-option';
import { INSURANCE_COST } from '@constants';
import { styles } from './styles';
import { ReservationFooter } from '../footer/component';

interface OptionsProps {
  course: number;
  data: CarOption[];
  playersCost: number;
  isCancellationInsuranceDisabled?: boolean;
}

export const Options: FC<OptionsProps> = ({
  course,
  data,
  playersCost,
  isCancellationInsuranceDisabled,
}) => {
  const [isConditionsVisible, setConditionsVisible] = useState<boolean>(false);
  const optionsContainerRef = useRef(null);
  const optionRef = useRef(null);

  const handlePressInfo = () => setConditionsVisible(true);

  const { values } = useFormikContext<{ [key: string]: number }>();
  const optionsCost = useOptionsCost({ values, data, course });

  const total = Number(playersCost) + optionsCost;

  return (
    <>
      <WebViewModal
        uri={getConditionsUrl()}
        isVisible={isConditionsVisible}
        onClose={() => setConditionsVisible(false)}
      />
      <View ref={optionsContainerRef}>
        <FlexView style={styles.container}>
          {data.map(({ label, maxByOrder, availability, unityPrice, unityPrice2 }, index) => (
            <FormOptionItem
              key={label}
              label={label}
              style={index !== 0 && styles.optionItem}
              maxValue={maxByOrder < availability ? maxByOrder : availability}
              cost={course === 0 ? unityPrice : unityPrice2}
              name={label}
            />
          ))}
          <FlexView style={styles.cancellationInsuranceContainer}>
            <FormOptionItemCheckbox
              style={styles.optionItem}
              t="reservation.cancellationInsurance"
              name="cancellationInsurance"
              forwardRef={optionRef}
              cost={INSURANCE_COST}
              onPressInfo={handlePressInfo}
              isDisabled={isCancellationInsuranceDisabled}
            />
          </FlexView>
          <ReservationFooter price={total} />
        </FlexView>
      </View>
    </>
  );
};
