import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';
import { shadowDepth } from '@helpers';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    paddingTop: SPACING.S,
    paddingBottom: SPACING.S + 4,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY[400],
  },
  header: {
    paddingBottom: SPACING.S,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  circleIcon: {
    height: 56,
    width: 56,
    borderStyle: 'dashed',
    borderRadius: 56 / 2,
    borderWidth: 1,
    borderColor: COLORS.GREY[600],
    justifyContent: 'center',
    alignItems: 'center',
  },
  removeButton: {
    height: 18,
    width: 18,
    borderRadius: 9,
    backgroundColor: COLORS.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    zIndex: 1,
    ...shadowDepth(),
  },
  playerSlot: {
    flex: 1 / 4,
  },
});
