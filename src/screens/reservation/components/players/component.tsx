import React, { FC } from 'react';
import times from 'lodash/times';
import { TouchableOpacity } from 'react-native';

import { FlexView, Text, Icon, Gutter } from '@components';
import { APP_CURRENCY, COLORS, SPACING } from '@constants';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { styles } from './styles';
import { Player } from './player';

interface PlayersProps {
  price?: number;
  items: PlayerEntityInternal[];
  onAddPlayerPress: () => void;
  onRemovePlayerPress: (id: number | null | string) => () => void;
}

export const Players: FC<PlayersProps> = ({
  price = 0,
  items = [],
  onAddPlayerPress,
  onRemovePlayerPress,
}) => (
  <FlexView style={styles.container}>
    <FlexView style={styles.header}>
      <Text t="reservation.players" color={COLORS.GREY[700]} />
      {Boolean(price) && <Text label={`${price} ${APP_CURRENCY}`} color={COLORS.SECONDARY[500]} />}
    </FlexView>
    <FlexView flexDirection="row">
      {times(4, (index) => {
        if (index < items.length) {
          const item = items[index];

          return (
            <Player
              item={item}
              key={`player_${item.id || item.addedId}`}
              isRemovable={Boolean(item.editable)}
              onPressRemove={onRemovePlayerPress(item.id || item.addedId)}
            />
          );
        }

        return (
          <TouchableOpacity onPress={onAddPlayerPress} key={`${index}`} style={styles.playerSlot}>
            <FlexView justifyContent="center" alignItems="center">
              <FlexView style={styles.circleIcon}>
                <Icon name="plus" size={19} />
              </FlexView>
              <Gutter isVertical amount={SPACING.XS - 2} />
              <Text t="common.add" size={14} />
            </FlexView>
          </TouchableOpacity>
        );
      })}
    </FlexView>
  </FlexView>
);
