import React, { FC } from 'react';

import { Avatar, FlexView, Gutter, Text, IconButton } from '@components';
import { COLORS, SPACING } from '@constants';
import { displayHandicap, noop } from '@helpers';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { styles } from './styles';

interface PlayerProps {
  item: PlayerEntityInternal;
  isRemovable: boolean;
  onPressRemove?: () => void;
}

export const Player: FC<PlayerProps> = ({
  item = {},
  isRemovable,
  onPressRemove = noop,
}) => (
  <FlexView alignItems="center" style={styles.playerSlot}>
    {isRemovable && (
      <IconButton
        onPress={onPressRemove}
        name="close"
        containerStyle={styles.removeButton}
        color={COLORS.GREY[700]}
        size={8}
        hitSlop={4}
      />
    )}
    <Avatar image={item.picture} size={56} />
    <Gutter isVertical amount={SPACING.XS - 2} />
    {(!item.private || item.own) && <Text label={item.firstname} size={14} numberOfLines={1} />}
    <Text label={`HCP ${displayHandicap(item.handicap as string)}`} size={12} color={COLORS.GREY[700]} />
  </FlexView>
);
