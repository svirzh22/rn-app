import { useState } from 'react';
import moment from 'moment';

import { DATE_FORMAT_DASH, formatDate } from '@helpers';
import { PlayerEntityInternal } from '@api/resources/types/player';
import { bookingCalculatePriceResource } from '@api';

interface Params {
  bookingDate: moment.MomentInput;
  time: string;
  players: PlayerEntityInternal[];
  golfCourse: number;
  calculatePrice: typeof bookingCalculatePriceResource.calculatePriceMultiple;
}

export const usePlayersCost = ({ bookingDate, time, players, golfCourse, calculatePrice }: Params) => {
  const dateForCost = `${formatDate(bookingDate, DATE_FORMAT_DASH)} ${time}`;

  const [costUpdateInProgress, setCostUpdateInProgress] = useState(false);
  const [playersCost, setPlayersCost] = useState(0);

  const updatePlayersCost = async () => {
    const availablePlayers = players?.filter((player) => player.own || player.editable);
    const memberTypes = availablePlayers?.reduce((acc: number[], player) => {
      if ((player.own || player.editable) && !acc.includes(player.memberType)) {
        acc.push(player.memberType || player.member);
      }

      return acc;
    }, []);

    const pricesResult = await Promise.all(
      memberTypes.map(async (memberType) => {
        const result = await calculatePrice({
          memberType,
          date: dateForCost,
          parcours: golfCourse,
        });
        return { memberType, price: result?.tarif || 0 };
      }),
    );
    const price = pricesResult.reduce((acc: { [key: number]: number }, item) => {
      acc[item.memberType] = item.price;
      return acc;
    }, {});

    const total = availablePlayers.reduce(
      (acc: number, player) => acc + price[player.memberType || player.member],
      0,
    );

    setPlayersCost(total);
    setCostUpdateInProgress(false);
  };

  return { playersCost, updatePlayersCost, setCostUpdateInProgress, costUpdateInProgress };
};
