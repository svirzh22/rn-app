import keys from 'lodash/keys';
import { INSURANCE_COST } from '@constants';
import { CarOption } from '@api/resources/types/car-option';

interface Params {
  values: { [key: string]: number };
  data: CarOption[];
  course: number;
}

export const useOptionsCost = ({ values, data, course }: Params) =>
  keys(values).reduce((acc, key) => {
    if (key === 'cancellationInsurance') {
      return acc + (values.cancellationInsurance ? INSURANCE_COST : 0);
    }

    const optionInfo = data.find(({ label }) => key === label);

    if (optionInfo) {
      const optionCost = course === 0 ? optionInfo.unityPrice : optionInfo.unityPrice2;

      return acc + values[key] * optionCost;
    }

    return acc;
  }, 0);
