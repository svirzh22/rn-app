import { useMutation, useQuery } from 'react-query';

import { DATE_FORMAT_DASH, formatDate } from '@helpers';
import {
  bookingCalculatePriceResource,
  bookingResource,
  freeBookingResource,
  getListCarsResource,
  lockBookingResource,
  userResource,
} from '@api';
import { useSessionExpired } from '@hooks';

export const useQueries = ({
  bookingDate,
  member,
}: {
  bookingDate: number;
  member: OwnUserEntity;
}) => {
  const { onSessionExpired } = useSessionExpired();
  const { isLoading: isLoadingCarsList, data } = useQuery(
    [getListCarsResource.cacheKey, { date: formatDate(bookingDate, DATE_FORMAT_DASH) }],
    getListCarsResource.getCars,
    { onError: onSessionExpired },
  );

  const { data: userData, isLoading: isLoadingUserData } = useQuery(
    [userResource.cacheKey, { userToken: member.token }],
    userResource.getUserInfo,
  );

  const { mutateAsync: calculatePrice } = useMutation(
    bookingCalculatePriceResource.calculatePriceMultiple,
    {
      onError: onSessionExpired,
    },
  );

  const { mutateAsync: createBooking } = useMutation(bookingResource.createBooking);
  const { mutateAsync: freeBooking } = useMutation(freeBookingResource.freeBooking);
  const { mutateAsync: lockBooking } = useMutation(lockBookingResource.lockBooking);

  return {
    isLoading: isLoadingCarsList || isLoadingUserData,
    data,
    calculatePrice,
    createBooking,
    freeBooking,
    lockBooking,
    user: userData?.user,
  };
};
