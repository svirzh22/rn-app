import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useQuery } from 'react-query';
import { useFocusEffect } from '@react-navigation/native';

import { BookingEntity } from '@api/resources/types/booking';
import { memberSelector } from '@selectors';
import { Button, ScreenWrapper, BookingsList, ActivityIndicator } from '@components';
import { userBookingsResource } from '@api';
import { styles } from './styles';
import { EmptyMyBookings } from './components';
import { MyBookingsScreenProps } from './types';

export const MyBookingsScreen: React.FC<MyBookingsScreenProps> = ({ navigation }) => {
  const handleTeeTimePress = useCallback(() => {}, []);
  const handlePress = useCallback(
    (item: BookingEntity) => {
      navigation.navigate('ReservationDetails', { reservationId: item.id });
    },
    [navigation],
  );
  const member = useSelector(memberSelector);
  const { data, isLoading, refetch } = useQuery(
    [userBookingsResource.cacheKey, { userToken: member.token }],
    userBookingsResource.fetchUserBookings,
    { retry: 0 },
  );
  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  const renderContent = () => {
    if (isLoading) {
      return <ActivityIndicator size="large" />;
    }

    if (!data?.bookings?.length) {
      return <EmptyMyBookings />;
    }

    return <BookingsList bookings={data.bookings} onPress={handlePress} />;
  };

  return (
    <ScreenWrapper
      buttonStyle={styles.footer}
      style={styles.container}
      footer={!data?.bookings?.length && <Button t="myBookings.emptyButton" onPress={handleTeeTimePress} />}
      withoutScrollView
      onRetry={!data ? refetch : undefined}
    >
      {renderContent()}
    </ScreenWrapper>
  );
};
