import React from 'react';

import { Gutter, Icon, Text } from '@components';

export const EmptyMyBookings = () => (
  <>
    <Icon name="my-bookings" size={56} />
    <Gutter isVertical amount={16} />
    <Text t="myBookings.empty" />
  </>
);
