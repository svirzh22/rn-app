import { StackNavigationProp } from '@react-navigation/stack';
import { AppNavigatorParams } from '@navigation';

type MyBookingsScreenNavigationProp = StackNavigationProp<
AppNavigatorParams,
'MyBookings'
>;

export interface MyBookingsScreenProps {
  navigation: MyBookingsScreenNavigationProp;
}
