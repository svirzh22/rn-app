import { StyleSheet } from 'react-native';

import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: 0,
  },
  footer: {
    marginHorizontal: SPACING.M,
    marginBottom: SPACING.M,
  },
});
