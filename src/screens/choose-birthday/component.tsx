import React, { FC, useCallback, useRef } from 'react';
import { useMutation } from 'react-query';
import { Formik } from 'formik';
import { useNavigation, useRoute, RouteProp } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

import { FormDatePicker, Button, Text, Gutter, FlexView, BackButton, Image, Screen } from '@components';
import { images } from '@assets/images';
import { useAlert } from '@hooks';
import { LoginNavigatorParams } from '@navigation';
import { formatDate, getMaxBirthdayDate, REVERSED_DATE_FORMAT_DASH } from '@helpers';
import { authResource } from '@api';
import { SPACING } from '@constants';
import { setSentOtpTime } from '@src/actions';
import { styles } from './styles';
import { birthdayValidationSchema } from './schema';

interface ChooseBirthdayScreenProps {}

export const ChooseBirthdayScreen: FC<ChooseBirthdayScreenProps> = () => {
  const dispatch = useDispatch();
  const { showAlert } = useAlert();

  const { isLoading, mutateAsync } = useMutation(authResource.askOTP);
  const route = useRoute<RouteProp<LoginNavigatorParams, 'ChooseBirthday'>>();
  const { mobilePhone, countryCode } = route?.params;
  const navigation = useNavigation();
  const formRef = useRef<any>();
  const handleSubmitPress = useCallback(() => {
    formRef.current?.submitForm();
  }, []);

  const handleSubmit = useCallback(
    async (values: { birthDate: Date }) => {
      const birthDate = formatDate(values.birthDate, REVERSED_DATE_FORMAT_DASH);
      const params = {
        mobilePhone,
        countryCode,
        birthDate,
      };
      try {
        const res = await mutateAsync(params);

        if (res) {
          const { error, devOtp } = res;
          if (!error) {
            dispatch(setSentOtpTime(Date.now()));
            return navigation.navigate('VerificationCode', { ...params, devOtp, birthDate });
          }

          return showAlert({ t: 'errors.somethingWrong' });
        }
      } catch (e) {
        return showAlert({ text: e?.data?.errorMessage });
      }

      return null;
    },
    [mobilePhone, countryCode, mutateAsync, navigation, dispatch, showAlert],
  );

  return (
    <Screen style={styles.container}>
      <FlexView style={styles.header}>
        <BackButton />

        <FlexView flex={1} alignItems="center">
          <Image source={images.logo} style={styles.logo} />
        </FlexView>
      </FlexView>

      <FlexView flex={0.9} justifyContent="center">
        <Formik
          initialValues={{
            birthDate: new Date(+getMaxBirthdayDate()),
            // birthDate: new Date('1990-01-01'),
          }}
          onSubmit={handleSubmit}
          validationSchema={birthdayValidationSchema()}
          innerRef={formRef}
        >
          {() => (
            <>
              <Text t="chooseBirthday.title" size={24} align="center" />
              <Gutter isVertical />
              <FormDatePicker name="birthDate" maximumDate={new Date(+getMaxBirthdayDate())} />
              <Gutter isVertical amount={SPACING.XXS} />

              <Button onPress={handleSubmitPress} t="common.continue" loading={isLoading} />
            </>
          )}
        </Formik>
      </FlexView>
    </Screen>
  );
};
