import * as Yup from 'yup';

import { translate } from '@helpers';

export const birthdayValidationSchema = () => Yup.object().shape({
  birthDate: Yup.string().required(translate('validations.required')),
});
