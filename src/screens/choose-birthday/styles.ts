import { StyleSheet } from 'react-native';

import { SCREEN_HEIGHT, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    height: SCREEN_HEIGHT,
  },
  header: {
    marginTop: SPACING.S,
    height: 66,
  },
  logo: {
    height: 65,
    width: 52,
    marginTop: -SPACING.M,
  },
});
