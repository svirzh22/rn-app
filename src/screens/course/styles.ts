import { StyleSheet } from 'react-native';
import { COLORS, PRIMARY_FONT, SCREEN_WIDTH, SPACING } from '@constants';
import { opacity, responsiveFontSize } from '@helpers';

export const IMAGE_HEIGHT = 336;

export const styles = StyleSheet.create({
  navigationHeader: {
    backgroundColor: 'transparent',
  },
  swiperImage: {
    height: IMAGE_HEIGHT,
    width: '100%',
  },
  swiperContainer: {
    height: IMAGE_HEIGHT,
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: -1,
  },
  swiperDot: {
    backgroundColor: opacity(COLORS.WHITE, 0.4),
    width: 6,
    height: 4,
    borderRadius: 2,
  },
  swiperActiveDot: {
    backgroundColor: COLORS.WHITE,
    width: 16,
    height: 4,
    borderRadius: 2,
  },
  listContainer: {
    width: SCREEN_WIDTH,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: COLORS.WHITE,
    position: 'absolute',
    bottom: 0,
  },
  paginationStyle: {
    paddingBottom: 10,
  },
  header: {
    height: 100,
    paddingHorizontal: SPACING.M,
  },
  accordionItem: {
    height: 64,
    justifyContent: 'center',
    paddingLeft: SPACING.M,
    paddingRight: SPACING.M - 8, // 8 - uncontrolled indent from ListAccordion
    padding: 0,
  },
  accordionLeftIcon: {
    marginRight: SPACING.XS,
  },
  accordionTitle: {
    fontSize: responsiveFontSize(18),
    ...PRIMARY_FONT.regular,
    color: COLORS.BLACK,
  },
  holesMapImage: {
    width: 327,
    height: 271,
    borderRadius: 16,
  },
  accordionContent: {
    paddingLeft: 0,
    alignItems: 'center',
    paddingTop: SPACING.XS,
    paddingBottom: SPACING.S,
  },
  loaderContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
});
