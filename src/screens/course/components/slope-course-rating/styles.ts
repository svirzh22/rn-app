import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  handicapPicker: {
    width: '100%',
    height: 48,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.GREY[500],
    paddingHorizontal: SPACING.S,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    // paddingTop: SPACING.S,
    paddingHorizontal: SPACING.M,
  },
  handicapWindow: {
    height: 380,
  },
  handicapWindowHeader: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: SPACING.M,
  },
  handicapItem: {
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  handicapItemSelected: {
    backgroundColor: COLORS.GREY[200],
  },
  handicapsListContentContainer: {
    alignItems: 'center',
  },
  arrowDown: {
    position: 'absolute',
    left: SPACING.M,
  },
  stats: {
    marginVertical: SPACING.S,
  },
});
