import { StyleSheet } from 'react-native';
import { COLORS } from '@constants';

export const styles = StyleSheet.create({
  item: {
    height: 40,
    flexDirection: 'row',
  },
  itemHighlighted: {
    backgroundColor: COLORS.GREY[200],
  },
});
