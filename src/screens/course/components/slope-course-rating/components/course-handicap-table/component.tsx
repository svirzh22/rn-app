import React, { FC } from 'react';
import { FlatList, ListRenderItem } from 'react-native';
import { not } from 'ramda';

import { FlexView, Text, Gutter } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface CourseHandicapTableProps {}

interface CourseHandicap {
  id: string;
  handicapIndex: string;
  courseHandicap: string;
}

// @TODO MOCK
const DATA: CourseHandicap[] = [
  { id: '1', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
  { id: '2', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
  { id: '3', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
  { id: '4', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
  { id: '5', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
  { id: '6', handicapIndex: '+4.0 — +3.3', courseHandicap: '+2' },
];

export const CourseHandicapTable: FC<CourseHandicapTableProps> = () => {
  const renderItem: ListRenderItem<CourseHandicap> = ({ item, index }) => (
    <FlexView style={[styles.item, not(index % 2) && styles.itemHighlighted]}>
      <FlexView flex={0.5} justifyContent="center" alignItems="center">
        <Text label={item.handicapIndex} />
      </FlexView>
      <FlexView flex={0.5} justifyContent="center" alignItems="center">
        <Text label={item.courseHandicap} />
      </FlexView>
    </FlexView>
  );
  const keyExtractor = (item: CourseHandicap) => item.id;
  const renderHeader = () => (
    <FlexView style={styles.item}>
      <FlexView flex={0.5} justifyContent="center">
        <Text t="courseHandicap.handicapIndex" color={COLORS.GREY[700]} />
      </FlexView>
      <FlexView flex={0.5} alignItems="center" justifyContent="center">
        <Text t="courseHandicap.courseHandicap" color={COLORS.GREY[700]} />
      </FlexView>
    </FlexView>
  );
  return (
    <FlexView>
      <FlatList
        renderItem={renderItem}
        data={DATA}
        keyExtractor={keyExtractor}
        ListHeaderComponent={renderHeader}
        ListFooterComponent={<Gutter isVertical />}
      />
    </FlexView>
  );
};
