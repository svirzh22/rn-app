import { GenderEnum, TeesTypeEnum } from '@api';
import { COLORS } from '@constants';

export interface CourseHandicapValue {
  gender: GenderEnum;
  teesType: TeesTypeEnum;
}

export interface CourseHandicap {
  value: CourseHandicapValue;
  title: string;
  color: string;
}

export const COURSE_HANDICAPS_OPTIONS: CourseHandicap[] = [
  {
    value: {
      gender: GenderEnum.man,
      teesType: TeesTypeEnum.champions,
    },
    title: 'Men Champions Tees (black)',
    color: COLORS.BLACK,
  },
  {
    value: {
      gender: GenderEnum.man,
      teesType: TeesTypeEnum.back,
    },
    title: 'Men Back Tees (white)',
    color: COLORS.GREY[600],
  },
  {
    value: {
      gender: GenderEnum.man,
      teesType: TeesTypeEnum.forward,
    },
    title: 'Men Forward Tees (yellow)',
    color: COLORS.PRIMARY[500],
  },
  {
    value: {
      gender: GenderEnum.woman,
      teesType: TeesTypeEnum.back,
    },
    title: 'Ladies Back Tees (blue)',
    color: COLORS.BLUE,
  },
  {
    value: {
      gender: GenderEnum.woman,
      teesType: TeesTypeEnum.forward,
    },
    title: 'Ladies Forward Tees (red)',
    color: COLORS.RED,
  },
];
