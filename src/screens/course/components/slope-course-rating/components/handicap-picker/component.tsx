import React, { FC, useState } from 'react';
import { FlatList, ListRenderItem } from 'react-native';

import { COLORS, SCREEN_WIDTH } from '@constants';
import { BottomModalWindow, FlexView, Icon, IconButton, Text, Touchable } from '@components';
import { CourseHandicap, CourseHandicapValue, COURSE_HANDICAPS_OPTIONS } from './constants';
import { styles } from '../../styles';

export * from './constants';

interface HandicapPickerProps {
  value: CourseHandicapValue;
  onSelect: (value: CourseHandicapValue) => void;
}
const isSelectedOption = (
  option: CourseHandicapValue,
  selectedValue: CourseHandicapValue,
) => option.gender === selectedValue.gender && option.teesType === selectedValue.teesType;

export const HandicapPicker: FC<HandicapPickerProps> = ({ value, onSelect }) => {
  const [isVisibleHandicapPicker, setVisibleHandicapPicker] = useState<boolean>(false);
  const handleHandicapPickerPress = () => {
    setVisibleHandicapPicker(true);
  };
  const selectedValue: CourseHandicap = COURSE_HANDICAPS_OPTIONS.find((option) =>
    isSelectedOption(option.value, value),
  ) || COURSE_HANDICAPS_OPTIONS[0];
  const hideHandicapPicker = () => {
    setVisibleHandicapPicker(false);
  };

  const renderHandicapItem: ListRenderItem<CourseHandicap> = ({ item }) => (
    <Touchable
      onPress={() => {
        onSelect(item.value);
        hideHandicapPicker();
      }}
      style={{ width: SCREEN_WIDTH }}
    >
      <FlexView
        style={[
          styles.handicapItem,
          isSelectedOption(item.value, selectedValue.value) && styles.handicapItemSelected,
        ]}
      >
        <Text label="•" color={item.color} />
        <Text label={item.title} />
      </FlexView>
    </Touchable>
  );
  const keyExtractor = (item: CourseHandicap) => `handicap_item_${item.value.gender}_${item.value.teesType}`;

  return (
    <FlexView>
      <Touchable style={styles.handicapPicker} onPress={handleHandicapPickerPress}>
        <FlexView flexDirection="row">
          <Text label="•" color={selectedValue.color} />
          <Text label={selectedValue.title} />
        </FlexView>
        <Icon name={isVisibleHandicapPicker ? 'arrow-up' : 'arrow-down'} color={COLORS.GREY[700]} />
      </Touchable>

      <BottomModalWindow
        isVisible={isVisibleHandicapPicker}
        hide={hideHandicapPicker}
        windowStyle={styles.handicapWindow}
      >
        <>
          <FlexView style={styles.handicapWindowHeader}>
            <IconButton
              name="arrow-down"
              color={COLORS.GREY[700]}
              containerStyle={styles.arrowDown}
              onPress={hideHandicapPicker}
            />
            <Text t="courseHandicap.windowHeader" weight="semiBold" size={22} />
          </FlexView>
          <FlatList
            data={COURSE_HANDICAPS_OPTIONS}
            renderItem={renderHandicapItem}
            keyExtractor={keyExtractor}
            contentContainerStyle={styles.handicapsListContentContainer}
          />
        </>
      </BottomModalWindow>
    </FlexView>
  );
};
