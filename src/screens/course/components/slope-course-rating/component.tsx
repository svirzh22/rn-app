import React, { FC, useState } from 'react';
import { RouteProp, useRoute } from '@react-navigation/native';
import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';
import { useDebounce } from 'use-debounce';

import { FlexView, Text, Gutter, ActivityIndicator, HandicapInput } from '@components';
import { COLORS } from '@constants';
import { courseHandicapResource } from '@api';
import { AppNavigatorParams } from '@navigation';
import { memberSelector } from '@selectors';
import { styles } from './styles';
import { HandicapPicker, COURSE_HANDICAPS_OPTIONS, CourseHandicapValue } from './components/handicap-picker';

interface SlopeCourseRatingProps {}

const StatText = ({ t, label, value }: { t?: string; label?: string; value?: string | number }) => (
  <FlexView flexDirection="row">
    <Text t={t} label={label} color={COLORS.GREY[700]} />
    <Gutter amount={2} />
    <Text label={value} />
  </FlexView>
);

export const SlopeCourseRating: FC<SlopeCourseRatingProps> = () => {
  const route = useRoute<RouteProp<AppNavigatorParams, 'Course'>>();
  const member = useSelector(memberSelector);
  const [options, setOptions] = useState<CourseHandicapValue>(COURSE_HANDICAPS_OPTIONS[0]?.value);
  const [handicap, setHandicap] = useState(member.handicap);
  const [debouncedHandicap] = useDebounce(handicap, 300);
  const { data: courseHandicap, isLoading } = useQuery(
    [courseHandicapResource.cacheKey, {
      golfCourse: route.params.golfCourse.id,
      handicap: debouncedHandicap,
      ...options,
    }],
    courseHandicapResource.getCourseHandicap,
    { retry: 0 },
  );

  return (
    <FlexView style={styles.container}>
      <HandicapInput withoutLabel value={handicap} onChange={setHandicap} />
      <HandicapPicker value={options} onSelect={setOptions} />

      <FlexView flexDirection="row" justifyContent="center" style={styles.stats}>
        {isLoading ? <ActivityIndicator /> : (
          <StatText label="Course handicap" value={courseHandicap?.courseHandicap} />
        )}
      </FlexView>
      {/* <CourseHandicapTable /> */}
    </FlexView>
  );
};
