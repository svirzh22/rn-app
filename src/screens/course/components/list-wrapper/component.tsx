import React, { FC } from 'react';
import {
  Directions,
  FlingGestureHandler,
  FlingGestureHandlerStateChangeEvent,
  State,
} from 'react-native-gesture-handler';
import { Animated } from 'react-native';

import { useDownScale, useWindowHeight } from '../../hooks';
import { animateListTranslate } from '../../animations';
import { styles } from '../../styles';

interface ListWrapperProps {
  children: React.ReactNode;
  setScrollEnabled: (value: boolean) => void;
  setPosition: (value: Directions) => void;
  translateY: Animated.Value;
  position: Directions;
}

export const ListWrapper: FC<ListWrapperProps> = ({
  children,
  setScrollEnabled,
  position,
  setPosition,
  translateY,
}) => {
  const DOWN_TRANSLATE = useDownScale();
  const windowHeight = useWindowHeight();
  const onSwipe = (direction: Directions) => (event: FlingGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.END) {
      if (direction === Directions.UP && direction === position) {
        return setScrollEnabled(true);
      }

      if (direction === Directions.UP) {
        setScrollEnabled(true);
      }

      setPosition(direction);
      animateListTranslate({
        translateY,
        value: direction === Directions.UP ? 0 : DOWN_TRANSLATE,
      });
    }

    return false;
  };

  return (
    <FlingGestureHandler direction={Directions.DOWN} onHandlerStateChange={onSwipe(Directions.DOWN)}>
      <FlingGestureHandler direction={Directions.UP} onHandlerStateChange={onSwipe(Directions.UP)}>
        <Animated.View
          style={[
            styles.listContainer,
            {
              height: windowHeight,
              transform: [{ translateY }],
            },
          ]}
        >
          {children}
        </Animated.View>
      </FlingGestureHandler>
    </FlingGestureHandler>
  );
};
