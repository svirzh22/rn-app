import { StyleSheet } from 'react-native';
import { COLORS, PRIMARY_FONT, SPACING } from '@constants';
import { responsiveFontSize } from '@helpers';

export const styles = StyleSheet.create({
  header: {
    height: 50,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderTopColor: COLORS.GREY[500],
    borderBottomColor: COLORS.GREY[500],
    alignItems: 'center',
  },
  headerItem: {
    height: 30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  headerText: {
    fontSize: responsiveFontSize(13),
    ...PRIMARY_FONT.medium,
  },
  headerSubtext: {
    fontSize: responsiveFontSize(9),
    ...PRIMARY_FONT.regular,
    letterSpacing: -0.3,
  },
  headerItemHdc: {
    height: '100%',
    backgroundColor: COLORS.GREY[200],
    justifyContent: 'center',
    flex: 1,
  },
  container: {
    paddingHorizontal: SPACING.M - 4,
  },
  scoreboardItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
  },
  scoreboardHdcItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    backgroundColor: COLORS.GREY[200],
  },
  scoreboardItemText: {
    ...PRIMARY_FONT.regular,
    fontSize: responsiveFontSize(12),
    color: COLORS.GREY[700],
  },
  scoreboardItemHoleText: {
    ...PRIMARY_FONT.medium,
    fontSize: responsiveFontSize(12),
  },
  contentContainerStyle: {},
});
