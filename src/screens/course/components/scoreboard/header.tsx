import React, { FC } from 'react';

import { COLORS } from '@constants';
import { FlexView, Text } from '@components';
import { styles } from './styles';

interface HeaderProps {}

export const Header: FC<HeaderProps> = () => (
  <FlexView style={styles.header} flexDirection="row">
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.hole" style={styles.headerText} />
    </FlexView>
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.white" style={styles.headerText} color={COLORS.GREY[600]} />
      <Text style={styles.headerSubtext} label="75.8/142" />
    </FlexView>
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.yellow" style={styles.headerText} color={COLORS.PRIMARY[500]} />
      <Text style={styles.headerSubtext} label="75.8/142" />
    </FlexView>
    <FlexView style={styles.headerItemHdc}>
      <FlexView style={styles.headerItem}>
        <Text t="course.hdcm" style={styles.headerText} />
      </FlexView>
    </FlexView>
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.par" style={styles.headerText} />
    </FlexView>
    <FlexView style={styles.headerItemHdc}>
      <FlexView style={styles.headerItem}>
        <Text t="course.hdcl" style={styles.headerText} />
      </FlexView>
    </FlexView>
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.blue" style={styles.headerText} color={COLORS.BLUE} />
      <Text style={styles.headerSubtext} label="75.8/142" />
    </FlexView>
    <FlexView style={styles.headerItem} flex={1}>
      <Text t="course.red" style={styles.headerText} color={COLORS.RED} />
      <Text style={styles.headerSubtext} label="75.8/142" />
    </FlexView>
  </FlexView>
);
