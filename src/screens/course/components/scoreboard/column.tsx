import React, { FC } from 'react';
import { FlexView, Text } from '@components';
import { ScoreboardItem } from './component';
import { styles } from './styles';

interface ColumnProps {
  item: ScoreboardItem;
}

export const Column: FC<ColumnProps> = ({ item }) => (
  <FlexView flexDirection="row">
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.hole} style={styles.scoreboardItemHoleText} />
    </FlexView>
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.white} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.yellow} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardHdcItem}>
      <Text label={item.hdcm} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.par} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardHdcItem}>
      <Text label={item.hdcl} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.blue} style={styles.scoreboardItemText} />
    </FlexView>
    <FlexView style={styles.scoreboardItem}>
      <Text label={item.red} style={styles.scoreboardItemText} />
    </FlexView>
  </FlexView>
);
