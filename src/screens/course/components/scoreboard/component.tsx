import React, { FC } from 'react';
import { FlatList, ListRenderItem } from 'react-native';
import times from 'lodash/times';

import { Column } from '@screens/course/components/scoreboard/column';
import { FlexView } from '@components';
import { Header } from './header';
import { styles } from './styles';

interface ScoreboardProps {}

export interface ScoreboardItem {
  hole: number;
  white: number;
  yellow: number;
  hdcm: number;
  par: number;
  hdcl: number;
  blue: number;
  red: number;
}

// @TODO MOCK DATA
const DATA = times<ScoreboardItem>(20, (index) => ({
  hole: index,
  white: 201,
  yellow: 201,
  hdcm: 201,
  par: 201,
  hdcl: 201,
  blue: 201,
  red: 201,
}));

export const Scoreboard: FC<ScoreboardProps> = () => {
  const renderItem: ListRenderItem<ScoreboardItem> = ({ item }) => <Column item={item} />;
  const keyExtractor = (item: ScoreboardItem) => `${item.hole}`;

  return (
    <FlexView style={styles.container}>
      <FlatList
        ListHeaderComponent={<Header />}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </FlexView>
  );
};
