import React, { FC, useCallback } from 'react';
import { FlatList, ListRenderItem, TouchableHighlight } from 'react-native';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { useQuery } from 'react-query';
import { courseHolesResource } from '@api';
import { AppNavigatorParams } from '@navigation';

import { HoleEntity } from '@api/resources/course-holes/types';
import { ActivityIndicator, FlexView } from '@components';
import { opacity } from '@helpers';
import { COLORS } from '@constants';
import { HolesTableProps } from './types';
import { styles } from './styles';
import { Header } from './header';
import { RowItem } from './row-item';

export const HolesTable: FC<HolesTableProps> = () => {
  const route = useRoute<RouteProp<AppNavigatorParams, 'Course'>>();

  const { data, isLoading } = useQuery(
    [courseHolesResource.cacheKey, { golfCourse: route.params.golfCourse.id }],
    courseHolesResource.getHoles,
    { retry: 0 },
  );
  const navigation = useNavigation();
  const { holes } = data || {};
  const handleItemPress = useCallback(
    (holeNumber: number) =>
      navigation.navigate('CourseHole', {
        golfCourse: route.params.golfCourse,
        holeNumber,
      }),
    [navigation, route.params.golfCourse],
  );

  const renderItem: ListRenderItem<HoleEntity> = ({ item, index }) => (
    <TouchableHighlight
      onPress={() => handleItemPress(item.number)}
      underlayColor={opacity(COLORS.BLACK[900], 0.7)}
    >
      <RowItem item={item} style={index % 2 ? styles.rowGrey : styles.row} />
    </TouchableHighlight>
  );
  const keyExtractor = (item: HoleEntity) => `${item.number}`;

  return (
    <FlexView style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={holes}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          ListHeaderComponent={<Header />}
          ListFooterComponent={<></>}
        />
      )}
    </FlexView>
  );
};
