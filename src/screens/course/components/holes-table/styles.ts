import { StyleSheet, ViewStyle } from 'react-native';
import { COLORS, PRIMARY_FONT, SPACING } from '@constants';
import { responsiveFontSize } from '@helpers';

const row: ViewStyle = {
  height: 40,
  backgroundColor: COLORS.WHITE,
  paddingHorizontal: SPACING.S,
};

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingHorizontal: SPACING.M,
  },
  header: {
    height: 40,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: SPACING.S,
    marginTop: SPACING.XS,
  },
  row,
  rowGrey: {
    ...row,
    backgroundColor: COLORS.GREY[200],
  },
  headerText: {
    fontSize: responsiveFontSize(16),
    color: COLORS.GREY[700],
  },
  holeSummaryText: {
    color: COLORS.BLACK,
    ...PRIMARY_FONT.medium,
  },
});
