import React from 'react';

import { FlexView, Gutter, Text } from '@components';
import { styles } from './styles';

export const Header = () => (
  <FlexView style={styles.header} flexDirection="row" flex={1}>
    <FlexView flex={0.3} justifyContent="center" style={{ marginLeft: -10 }}>
      <Text t="course.hole" style={styles.headerText} />
    </FlexView>
    <Gutter amount={10} />

    <FlexView flex={0.2} justifyContent="center" alignItems="flex-end">
      <Text t="course.meters" style={styles.headerText} />
    </FlexView>

    <FlexView flex={0.3} alignItems="flex-end" justifyContent="center">
      <Text t="course.par" style={styles.headerText} />
    </FlexView>
  </FlexView>
);
