import React, { FC } from 'react';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import i18n from 'i18n-js';

import { FlexView, Icon, Text } from '@components';
import { COLORS } from '@constants';
import { HoleEntity } from '@api/resources/course-holes/types';

interface RowItemProps {
  item: HoleEntity;
  style?: StyleProp<ViewStyle>;
  holeTextStyle?: StyleProp<TextStyle>;
  holeT?: string;
}

export const RowItem: FC<RowItemProps> = ({ item, style, holeTextStyle, holeT }) => {
  const meters = i18n.toNumber(item.distance, { delimiter: '’', precision: 0 });
  return (
    <FlexView style={style} flexDirection="row" flex={1}>
      <FlexView flex={0.3} justifyContent="center">
        <Text color={COLORS.SECONDARY[500]} label={item.number} size={16} style={holeTextStyle} t={holeT} />
      </FlexView>

      <FlexView flex={0.2} justifyContent="center" alignItems="flex-end">
        <Text label={meters} size={16} />
      </FlexView>

      <FlexView flex={0.3} alignItems="flex-end" justifyContent="center">
        <Text label={item.par} size={16} />
      </FlexView>

      <FlexView flex={0.2} alignItems="flex-end" justifyContent="center">
        <Icon name="arrow-right" color={COLORS.GREY[600]} />
      </FlexView>
    </FlexView>
  );
};
