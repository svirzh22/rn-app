import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { AppNavigatorParams } from '@navigation';

type CourseScreenRouteProp = RouteProp<AppNavigatorParams, 'Course'>;
type CourseNavigationProp = StackNavigationProp<
AppNavigatorParams,
'Course'
>;

export interface CourseScreenProps {
  navigation: CourseNavigationProp;
  route: CourseScreenRouteProp;
}
