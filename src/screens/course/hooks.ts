import { SCREEN_HEIGHT } from '@constants';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { IMAGE_HEIGHT } from '@screens/course/styles';

const HEADER_HEIGHT = 40;
const BOTTOM_TABS_HEIGHT = 50;

export const useWindowHeight = () => {
  const insets = useSafeAreaInsets();

  return SCREEN_HEIGHT - (insets.top + insets.bottom + BOTTOM_TABS_HEIGHT + HEADER_HEIGHT);
};

export const useDownScale = () => {
  const insets = useSafeAreaInsets();
  const buffer = 100;

  return IMAGE_HEIGHT - (insets.top + HEADER_HEIGHT + buffer);
};
