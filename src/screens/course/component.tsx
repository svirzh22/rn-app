import React, { useState, useRef, useCallback } from 'react';
import { Animated, FlatList, NativeSyntheticEvent, NativeScrollEvent, ListRenderItem } from 'react-native';
import Swiper from 'react-native-swiper';
import { List } from 'react-native-paper';
import { Directions } from 'react-native-gesture-handler';
import { useQuery } from 'react-query';

import { COLORS } from '@constants';
import { useTranslate } from '@hooks';
import {
  FlexView,
  Header,
  IconNameType,
  Image,
  Screen,
  Text,
  Icon,
  Divider,
  HTMLContent,
  ActivityIndicator,
} from '@components';
import { courseDetailsResource } from '@api';
import { styles } from './styles';
import { useDownScale } from './hooks';
import { animateListTranslate } from './animations';
import {
  ListWrapper,
  HolesTable,
  // Scoreboard,
  // SlopeCourseRating
} from './components';
import { CourseScreenProps } from './types';

interface ListItem {
  icon: IconNameType;
  title: string;
  renderContent: () => React.ReactNode;
}

type Item = ListItem;

export const CourseScreen: React.FC<CourseScreenProps> = ({
  route,
}) => {
  const [position, setPosition] = useState<Directions>(Directions.DOWN);
  const [scrollEnabled, setScrollEnabled] = useState<boolean>(false);

  const { data: courseDetails, isLoading } = useQuery(
    [courseDetailsResource.cacheKey, { golfCourse: route.params.golfCourse.id }],
    courseDetailsResource.getCourseDetails,
    { retry: 0 },
  );
  const DOWN_TRANSLATE = useDownScale();
  const translate = useTranslate();
  const translateY = useRef(new Animated.Value(DOWN_TRANSLATE)).current;
  let beginDragOffset = 0;

  const onScrollBeginDrag = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    beginDragOffset = event.nativeEvent.contentOffset.y;
  };
  const onScrollEndDrag = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { contentOffset } = event.nativeEvent;

    if (beginDragOffset >= contentOffset.y) {
      if (!contentOffset.y) {
        setScrollEnabled(false);
        setPosition(Directions.DOWN);
        animateListTranslate({ translateY, value: DOWN_TRANSLATE });
      }
    }
  };

  const handleAccordionPress = useCallback(() => {
    if (position === Directions.DOWN) {
      setScrollEnabled(true);
      setPosition(Directions.UP);
      animateListTranslate({ translateY, value: 0 });
    }
  }, [position, translateY]);

  const renderHeader = () => (
    <FlexView justifyContent="center" style={styles.header}>
      <Text label={courseDetails?.course.title} size={24} />
      <Text
        // eslint-disable-next-line max-len
        label={`${courseDetails?.course.nbHoles} Hole - Par ${courseDetails?.course.par} - ${courseDetails?.course.distance} m.`}
        size={16}
        color={COLORS.GREY[700]}
      />
    </FlexView>
  );

  const renderItem: ListRenderItem<Item> = ({ item }) => (
    <List.Accordion
      title={translate(item.title)}
      titleStyle={styles.accordionTitle}
      left={() => <Icon name={item.icon} size={18} style={styles.accordionLeftIcon} />}
      style={styles.accordionItem}
      onPress={handleAccordionPress}
    >
      {item.renderContent()}
    </List.Accordion>
  );

  const renderHoles = () => (
    <FlexView style={styles.accordionContent}>
      <Image source={{ uri: courseDetails?.course.holesMap }} style={styles.holesMapImage} />
      <HolesTable />
    </FlexView>
  );
  // const renderScoreboard = () => <Scoreboard />;
  // const renderSlopeCourseRating = () => <SlopeCourseRating />;
  const renderDescription = () => <HTMLContent content={courseDetails?.course.description} />;
  const renderLocalRules = () => (
    <HTMLContent
      content={courseDetails?.course.rules}
      customTagStyle={{ ul: { paddingTop: 0 } }}
    />
  );

  const data: Item[] = [
    { icon: 'info', title: 'course.description', renderContent: renderDescription },
    { icon: 'holes', title: 'course.holes', renderContent: renderHoles },
    // { icon: 'scorecard', title: 'course.scoreboard', renderContent: renderScoreboard },
    { icon: 'rules', title: 'course.localRules', renderContent: renderLocalRules },
    // { icon: 'rate', title: 'course.slopeAndCourseRating', renderContent: renderSlopeCourseRating },
  ];

  const keyExtractor = (item: Item) => item.title;
  const renderContent = () => {
    if (isLoading) return <ActivityIndicator mode="standalone" />;

    return (
      <>
        <FlexView style={styles.swiperContainer}>
          <Swiper
            activeDotStyle={styles.swiperActiveDot}
            dotStyle={styles.swiperDot}
            paginationStyle={styles.paginationStyle}
          >
            <Image source={{ uri: courseDetails?.course.backgroundPicture }} style={styles.swiperImage} />
          </Swiper>
        </FlexView>
        <ListWrapper
          position={position}
          setScrollEnabled={setScrollEnabled}
          setPosition={setPosition}
          translateY={translateY}
        >
          <FlatList
            data={data}
            renderItem={renderItem}
            scrollEventThrottle={16}
            onScrollBeginDrag={onScrollBeginDrag}
            onScrollEndDrag={onScrollEndDrag}
            scrollEnabled={scrollEnabled}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={() => <Divider />}
            keyExtractor={keyExtractor}
            ListHeaderComponent={renderHeader}
            bounces={false}
          />
        </ListWrapper>
      </>
    );
  };

  return (
    <Screen>
      <Header
        style={styles.navigationHeader}
        title="course.title"
      />
      {renderContent()}
    </Screen>
  );
};
