import { Animated } from 'react-native';

interface AnimateListTranslateParams {
  translateY: Animated.Value;
  value: number;
}

export const animateListTranslate = ({ translateY, value }: AnimateListTranslateParams) =>
  Animated.spring(translateY, {
    toValue: value,
    useNativeDriver: true,
    tension: 40,
    friction: 10,
  }).start();
