import * as Yup from 'yup';

import { translate } from '@helpers';

export const verificationCodeValidationSchema = () => Yup.object().shape({
  code: Yup
    .string()
    .required(translate('validations.required'))
    .min(4, translate('validations.minChars', { count: 4 })),
});
