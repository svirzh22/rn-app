import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { sentOtpTimeSelector } from '@selectors';
import { AVAILABILITY_RESEND_OTP } from '@constants';

const calculateSeconds = (sentOtpTime: number | null) =>
  (sentOtpTime ? Math.ceil((sentOtpTime + AVAILABILITY_RESEND_OTP - Date.now()) / 1000) : 0);

export const useTimer = () => {
  const sentOtpTime = useSelector(sentOtpTimeSelector);
  const [seconds, setSeconds] = useState<number>(calculateSeconds(sentOtpTime));
  let interval: NodeJS.Timeout;

  useEffect(() => {
    const nextSeconds = calculateSeconds(sentOtpTime);
    if (nextSeconds <= 0) {
      setSeconds(0);
      return clearInterval(interval);
    }

    setSeconds(nextSeconds);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    interval = setInterval(() => {
      setSeconds(calculateSeconds(sentOtpTime));
    }, 1000);

    return () => clearInterval(interval);
  }, [seconds, sentOtpTime]);

  return {
    seconds,
    setSeconds,
  };
};
