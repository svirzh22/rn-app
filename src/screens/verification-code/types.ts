import { LoginNavigatorParams } from '@navigation';
import { RouteProp } from '@react-navigation/native';

type ProfileScreenRouteProp = RouteProp<LoginNavigatorParams, 'VerificationCode'>;

export interface VerificationCodeProps {
  route: ProfileScreenRouteProp;
}

export interface FormValues {
  code: string;
}
