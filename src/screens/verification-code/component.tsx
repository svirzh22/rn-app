import React, { FC } from 'react';
import { Formik } from 'formik';
import isEmpty from 'lodash/isEmpty';
import { useDispatch, useSelector } from 'react-redux';
import { useMutation } from 'react-query';

import {
  Screen,
  FlexView,
  Image,
  Text,
  Button,
  CodeInput,
  LinkButton,
  BackButton,
  Gutter,
} from '@components';
import { formatAPIErrorMessage } from '@helpers';
import { COLORS, SCREEN_HEIGHT, SPACING } from '@constants';
import { images } from '@assets/images';
import { authResource } from '@api';
import { setAppLoading, signIn, setSentOtpTime } from '@src/actions';
import { useAlert, useSessionExpired } from '@hooks';
import { appLoadingSelector } from '@selectors';
import { styles } from './styles';
import { VerificationCodeProps, FormValues } from './types';
import { useTimer } from './hooks';
import { verificationCodeValidationSchema } from './schema';

export const VerificationCode: FC<VerificationCodeProps> = ({ route }) => {
  const { showAlert } = useAlert();
  const { seconds } = useTimer();
  const { mobilePhone, countryCode, devOtp, birthDate, errorMessage } = route.params;
  const { onSessionExpired } = useSessionExpired();
  const appLoading = useSelector(appLoadingSelector);
  const { isLoading: isLoadingVerifyOTP, mutateAsync: verifyOtp } = useMutation(authResource.verifyOTP, {
    onError: onSessionExpired,
  });
  const { isLoading: isLoadingAskOtp, mutateAsync: askOtp } = useMutation(authResource.askOTP);
  const dispatch = useDispatch();
  const handleFormSubmit = async ({ code }: FormValues) => {
    if (code) {
      try {
        dispatch(setAppLoading(true));
        const res = await verifyOtp({ mobilePhone, countryCode, otpCode: code, birthDate });

        dispatch(signIn(res));
      } catch (e) {
        if (['NETWORK_ERROR', 'TIMEOUT_ERROR'].includes(e.name)) {
          dispatch(setAppLoading(false));
          return showAlert({ t: 'errors.noConnection' });
        }

        showAlert({ text: formatAPIErrorMessage(e?.data?.msg) });
        dispatch(setAppLoading(false));
      }
    }
    return null;
  };

  const handlePressResend = async () => {
    try {
      const res = await askOtp({ mobilePhone, countryCode, birthDate });

      if (res && !res.error) {
        dispatch(setSentOtpTime(Date.now()));
      }

      return null;
    } catch (e) {
      if (['NETWORK_ERROR', 'TIMEOUT_ERROR'].includes(e.name)) {
        return showAlert({ t: 'errors.noConnection' });
      }
      return null;
    }
  };

  return (
    <Screen style={styles.container} withScroll extraHeight={SCREEN_HEIGHT * 0.17}>
      <FlexView style={styles.header}>
        <BackButton />
        <FlexView flex={1} alignItems="center">
          <Image source={images.logo} style={styles.logo} />
        </FlexView>
      </FlexView>

      <FlexView flex={0.9} justifyContent="center" alignItems="center">
        <Text t="verificationCode.title" size={24} />

        <Formik
          initialValues={{
            code: devOtp || '',
          }}
          onSubmit={handleFormSubmit}
          validationSchema={verificationCodeValidationSchema()}
        >
          {({ handleChange, handleSubmit, errors, values }) => (
            <>
              <Gutter amount={SPACING.L} isVertical />
              <CodeInput
                code={values.code}
                onChangeCode={handleChange('code')}
                cellCount={6}
                // onFulfill={handleSubmit}
              />
              {errorMessage && (
                <>
                  <Gutter amount={SPACING.S} isVertical />
                  <Text label={errorMessage} size="S" color={COLORS.ERROR} align="center" />
                </>
              )}
              <Gutter amount={SPACING.L} isVertical />
              <Button
                t="common.continue"
                loading={isLoadingVerifyOTP || appLoading}
                onPress={handleSubmit}
                disabled={!isEmpty(errors)}
              />
            </>
          )}
        </Formik>
        <FlexView flexDirection="row" justifyContent="space-between" style={styles.resendCodeContainer}>
          {seconds ? (
            <FlexView flex={0.9}>
              <Text
                t="verificationCode.secondsToBeAvailable"
                size={18}
                color={COLORS.GREY[700]}
                translateOptions={{
                  seconds,
                }}
              />
            </FlexView>
          ) : (
            <FlexView />
          )}
          <LinkButton t="common.resend" onPress={handlePressResend} disabled={!!seconds || isLoadingAskOtp} />
        </FlexView>
      </FlexView>
    </Screen>
  );
};
