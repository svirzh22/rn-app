import React, { FC } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import isEmpty from 'lodash/isEmpty';
import { useDispatch, useSelector } from 'react-redux';
import { useMutation } from 'react-query';
import upperFirst from 'lodash/upperFirst';

import { Screen, FlexView, Image, Text, Button, FormPhoneInput, Gutter, Touchable } from '@components';
import { COLORS, SCREEN_HEIGHT } from '@constants';
import { images } from '@assets/images';
import { AskOtpErrorTypes } from '@api/resources/auth-resource/types';
import { authResource } from '@api';
import { setSentOtpTime } from '@src/actions';
import { useAlert, useTranslate } from '@hooks';
import { localeSelector } from '@selectors';
import { parsePhoneNumber } from '@helpers';
import { styles } from './styles';
import { LoginScreenProps } from './types';

interface FormValues {
  phoneNumber: string;
}

export const Login: FC<LoginScreenProps> = ({ navigation }) => {
  const { showAlert } = useAlert();
  const dispatch = useDispatch();
  const { isLoading, mutateAsync } = useMutation(authResource.askOTP);
  const navToChooseLanguage = () => navigation.navigate('ChooseLanguage');
  const locale = useSelector(localeSelector);
  const translate = useTranslate();
  const selectedLanguage = upperFirst(locale);
  const handlePressContinue = async ({ phoneNumber }: FormValues) => {
    if (phoneNumber) {
      const { nationalNumber, countryCallingCode } = parsePhoneNumber(phoneNumber);
      const params = { mobilePhone: nationalNumber, countryCode: countryCallingCode };
      try {
        const res = await mutateAsync(params);

        if (res) {
          const { error, devOtp } = res;
          if (!error) {
            dispatch(setSentOtpTime(Date.now()));
            return navigation.navigate('VerificationCode', { ...params, devOtp });
          }

          return showAlert({ t: 'errors.somethingWrong' });
        }
      } catch (e) {
        const { errorType, errorMessage, code } = e?.data || {};

        // @TODO determine if it is multiple match
        if (code) {
          dispatch(setSentOtpTime(Date.now()));
          return navigation.navigate('VerificationCode', {
            ...params,
            devOtp: '',
            errorMessage: translate('errors.otpRegeneration'),
          });
        }

        if (['NETWORK_ERROR', 'TIMEOUT_ERROR'].includes(e.name)) {
          return showAlert({ t: 'errors.noConnection' });
        }

        if (errorType === AskOtpErrorTypes.MULTIPLE_MATCH) {
          return navigation.navigate('ChooseBirthday', params);
        }

        if (errorType === AskOtpErrorTypes.NO_MATCH) {
          return navigation.navigate('SignUp', { phoneNumber });
        }

        return showAlert(errorMessage ? { text: errorMessage } : { t: 'errors.somethingWrong' });
      }
    }

    return null;
  };

  return (
    <Screen style={styles.container} withScroll extraHeight={SCREEN_HEIGHT * 0.15}>
      <FlexView style={styles.header}>
        <Image source={images.logo} style={styles.logo} />
        <Touchable onPress={navToChooseLanguage} style={styles.languagePickerButton} borderRadius={8}>
          <Text label={selectedLanguage} />
        </Touchable>
      </FlexView>

      <FlexView flex={0.8} justifyContent="center" alignItems="center">
        <Text t="login.title" size={24} align="center" />
        <Text t="login.description" size={14} color={COLORS.GREY[700]} />
        <Gutter isVertical />
        <Formik
          initialValues={{
            phoneNumber: '',
          }}
          onSubmit={handlePressContinue}
          validationSchema={Yup.object().shape({
            // @ts-expect-error
            phoneNumber: Yup.string().isValidNumber(),
          })}
        >
          {({ handleSubmit, errors }) => (
            <>
              <FormPhoneInput name="phoneNumber" options={{}} />
              <Button
                loading={isLoading}
                t="common.continue"
                disabled={!isEmpty(errors)}
                onPress={handleSubmit}
              />
            </>
          )}
        </Formik>
      </FlexView>
    </Screen>
  );
};
