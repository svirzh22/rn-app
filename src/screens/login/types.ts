import { StackNavigationProp } from '@react-navigation/stack';
import { LoginNavigatorParams } from '@navigation';

type LoginScreenNavigationProp = StackNavigationProp<
LoginNavigatorParams,
'Login'
>;

export interface LoginScreenProps {
  navigation: LoginScreenNavigationProp;
}
