import { StyleSheet } from 'react-native';

import { COLORS, SCREEN_HEIGHT, SPACING } from '@constants';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: SPACING.M,
    height: SCREEN_HEIGHT,
  },
  header: {
    marginTop: SPACING.M + 6,
    height: 40,
    alignItems: 'center',
  },
  logo: {
    height: 65,
    width: 52,
    marginTop: -SPACING.M,
  },
  phoneInputContainer: {
    marginVertical: SPACING.L,
  },
  languagePickerButton: {
    position: 'absolute',
    right: 0,
    width: 38,
    height: 38,
    borderWidth: 1,
    borderColor: COLORS.GREY[600],
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -16,
  },
});
