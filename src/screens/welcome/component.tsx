import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';
import upperFirst from 'lodash/upperFirst';
import { useSelector } from 'react-redux';

import { Screen, Image, FlexView, Text, Button, Gutter, Touchable } from '@components';
import { images } from '@assets/images';
import { SPACING } from '@constants';
import { localeSelector } from '@selectors';
import { styles } from './styles';

interface WelcomeProps {
  locale: string;
}

export const Welcome: FC<WelcomeProps> = () => {
  const navigation = useNavigation();
  const navToLogin = () => navigation.navigate('Login');
  const navToChooseLanguage = () => navigation.navigate('ChooseLanguage');
  const locale = useSelector(localeSelector);

  const selectedLanguage = upperFirst(locale);

  return (
    <Screen style={styles.container}>
      <FlexView flex={0.1} alignItems="flex-end">
        <Touchable onPress={navToChooseLanguage} style={styles.languagePickerButton} borderRadius={8}>
          <Text label={selectedLanguage} />
        </Touchable>
      </FlexView>

      <FlexView flex={0.4} alignItems="center">
        <Image source={images.logo} style={styles.logo} />
        <Gutter isVertical amount={SPACING.L} />
        <Text t="welcome.title" align="center" size={36} weight="semiBold" />
      </FlexView>

      <FlexView flex={0.5} alignItems="center">
        <Button onPress={navToLogin} t="welcome.imMember" />
      </FlexView>
    </Screen>
  );
};
