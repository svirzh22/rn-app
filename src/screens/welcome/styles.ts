import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '@constants';

export const styles = StyleSheet.create({
  logo: {
    height: 130,
    width: 104,
  },
  container: {
    paddingHorizontal: SPACING.M,
  },
  languagePickerButton: {
    width: 38,
    height: 38,
    borderWidth: 1,
    borderColor: COLORS.GREY[600],
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: SPACING.XS,
  },
});
