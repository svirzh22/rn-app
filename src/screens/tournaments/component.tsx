import React, { FC, useRef } from 'react';
import { WebView } from 'react-native-webview';

import { TOURNAMENTS_LINK } from '@constants';
import { ActivityIndicator, FlexView, WebViewInternal } from '@components';
import { styles } from './styles';

interface TournamentsProps {}

export const TournamentsScreen: FC<TournamentsProps> = () => {
  const ref = useRef<WebView>(null);
  return (
    <WebViewInternal
      ref={ref}
      source={{ uri: TOURNAMENTS_LINK }}
      renderLoading={() => (
        <FlexView style={styles.loaderContainer}>
          <ActivityIndicator size="large" />
        </FlexView>
      )}
      startInLoadingState
      scalesPageToFit
    />
  );
};
