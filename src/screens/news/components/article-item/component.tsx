import React, { FC } from 'react';
import moment from 'moment';
import upperFirst from 'lodash/upperFirst';

import { FlexView, Image, Text } from '@components';
import { COLORS } from '@constants';
import { styles } from './styles';

interface ArticleItemProps {
  item: Article;
}

export interface Article {
  id: string;
  timestamp: number;
  image: string;
  description: string;
  title: string;
}

export const ArticleItem: FC<ArticleItemProps> = ({ item }) => (
  <FlexView>
    <Image source={{ uri: item.image }} style={styles.image} />
    <Text
      label={upperFirst(moment(item.timestamp).fromNow())}
      size={14}
      weight="medium"
      color={COLORS.GREY[700]}
    />
    <Text label={item.title} size={18} weight="medium" style={styles.title} />
    <Text label={item.description} size={16} />
  </FlexView>
);
