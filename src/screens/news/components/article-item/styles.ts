import { StyleSheet } from 'react-native';

import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  image: {
    height: 168,
    width: '100%',
    borderRadius: 16,
    marginBottom: SPACING.S + 2,
  },
  container: {},
  title: {
    marginTop: SPACING.XS,
    marginBottom: SPACING.S - 2,
  },
});
