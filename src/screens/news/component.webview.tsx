import React, { useRef, useState } from 'react';
import WebView from 'react-native-webview';

import { ActivityIndicator, FlexView, Header, Screen, WebViewInternal } from '@components';
import { styles } from './styles';
import { NewsScreenProps } from './types';

const newsUrl = 'https://www.golfcrans.ch/fr/news/';

export const NewsScreen: React.FC<NewsScreenProps> = ({
  navigation,
}) => {
  const ref = useRef<WebView>(null);
  const [canBack, setCanBack] = useState(false);

  return (
    <Screen>
      <Header
        title="news.headerTitle"
        preset="withBorder"
        leftButtonProps={{
          onPress: () => {
            if (canBack) ref.current?.goBack();
            else navigation.goBack();
          },
        }}
      />
      <WebViewInternal
        ref={ref}
        source={{ uri: newsUrl }}
        renderLoading={() => (
          <FlexView style={styles.loaderContainer}>
            <ActivityIndicator size="large" />
          </FlexView>
        )}
        onNavigationStateChange={({ canGoBack }) => setCanBack(canGoBack)}
        startInLoadingState
      />
    </Screen>
  );
};
