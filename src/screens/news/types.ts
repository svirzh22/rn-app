import { StackNavigationProp } from '@react-navigation/stack';
import { AppNavigatorParams } from '@navigation';

type NewsScreenNavigationProp = StackNavigationProp<
AppNavigatorParams,
'News'
>;

export interface NewsScreenProps {
  navigation: NewsScreenNavigationProp;
}
