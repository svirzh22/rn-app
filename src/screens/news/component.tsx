import React, { FC, useCallback } from 'react';
import { FlatList, ListRenderItem, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Screen, Header, Gutter } from '@components';
import { SPACING } from '@constants';
import { ArticleItem, Article } from './components';
import { styles } from './styles';

interface ListWrapperProps {
  children: React.ReactNode;
}

// @TODO MOCK DATA
const MOCK_DATA: Article[] = [
  {
    id: '1',
    title: 'Infos importantes sur la situation actuelle, les compétitions à venir, etc.',
    description:
      // eslint-disable-next-line max-len
      'Annulation des compétitions de ce week-end Au vu de la situation actuelle, nous ne souhaitons pas prendre de risques inutiles et sommes donc au...',
    image: 'https://www.villagelinksgolf.com/images/slideshows/banner_1.jpg',
    timestamp: Date.now(),
  },
  {
    id: '2',
    title: 'Message du Greenkeeper',
    description:
      // eslint-disable-next-line max-len
      'Travaux de fin de saison sur le Seve Ballesteros Durant ces 2 prochaines semaines nous allons préparer le terrain pour les mois d’hiver. Sur...',
    image: 'https://www.villagelinksgolf.com/images/slideshows/banner_1.jpg',
    timestamp: Date.now(),
  },
];

export const NewsScreen: FC<ListWrapperProps> = () => {
  const navigation = useNavigation();
  const handleArticlePress = useCallback(() => {
    navigation.navigate('NewsArticle');
  }, [navigation]);
  const renderItem: ListRenderItem<Article> = ({ item }) => (
    <TouchableOpacity onPress={handleArticlePress}>
      <ArticleItem item={item} />
    </TouchableOpacity>
  );
  const keyExtractor = (article: Article) => article.id;
  const renderSeparator = () => <Gutter isVertical amount={SPACING.M} />;

  return (
    <Screen>
      <Header title="news.headerTitle" preset="withBorder" />
      <FlatList
        data={MOCK_DATA}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </Screen>
  );
};
