import { StyleSheet } from 'react-native';
import { SPACING } from '@constants';

export const styles = StyleSheet.create({
  contentContainerStyle: {
    padding: SPACING.M,
  },
  loaderContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
});
