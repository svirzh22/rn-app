import React, { FC } from 'react';
import moment from 'moment';
import upperFirst from 'lodash/upperFirst';

import { Screen, SwipeableScrollView, Text, Image, Header } from '@components';
import { COLORS } from '@constants';
import { MOCK_DESCRIPTION } from './mock';
import { styles } from './styles';

interface NewsArticleProps {}

// @TODO MOCK
const MOCK_DATA = {
  timestamp: moment().subtract(3, 'days'),
  title: 'Message du Greenkeeper',
  description: MOCK_DESCRIPTION,
  image: 'https://www.villagelinksgolf.com/images/slideshows/banner_1.jpg',
};

export const NewsArticleScreen: FC<NewsArticleProps> = () => {
  const timeFromNow = upperFirst(moment(MOCK_DATA.timestamp).fromNow());
  return (
    <Screen statusBarStyle="light-content">
      <Header leftButtonColor={COLORS.WHITE} />
      <Image
        source={{
          uri: MOCK_DATA.image,
        }}
        style={styles.articleImage}
      />

      <SwipeableScrollView contentContainerStyle={styles.container}>
        <Text label={timeFromNow} color={COLORS.GREY[700]} size={14} weight="medium" />
        <Text label={MOCK_DATA.title} size={24} weight="medium" style={styles.title} />
        <Text label={MOCK_DATA.description} />
      </SwipeableScrollView>
    </Screen>
  );
};
