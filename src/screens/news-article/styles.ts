import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH, SPACING } from '@constants';

export const styles = StyleSheet.create({
  articleImage: {
    height: 336,
    width: SCREEN_WIDTH,
    zIndex: -5,
    position: 'absolute',
  },
  container: {
    paddingHorizontal: SPACING.M,
    paddingVertical: SPACING.L,
  },
  title: {
    marginVertical: SPACING.XS,
  },
});
