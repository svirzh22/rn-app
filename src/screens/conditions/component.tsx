import React, { FC } from 'react';
import { ScrollView } from 'react-native';

import { FlexView, Gutter, Text } from '@components';
import { FONT_SIZES } from '@constants';

interface ConditionsProps {
  locale: string;
}

const Title = () => (
  <Text
    t="conditions.title"
    style={{ textAlign: 'center', fontWeight: '700', fontSize: FONT_SIZES.XXL }}
  />
);

const SubTitle: React.FC<{ t: string }> = ({ t }) => (
  <Text
    t={t}
    style={{ fontWeight: '600', fontSize: FONT_SIZES.L }}
  />
);

export const ConditionsScreen: FC<ConditionsProps> = () => (
  <ScrollView>
    <FlexView style={{ margin: 20 }}>
      <Title />
      <Gutter isVertical />
      <SubTitle t="conditions.subTitle1" />
      <Gutter isVertical />
      <Text t="conditions.text1" />
      <Gutter isVertical />
      <SubTitle t="conditions.subTitle2" />
      <Gutter isVertical />
      <Text t="conditions.text2" />
      <Gutter isVertical />
      <SubTitle t="conditions.subTitle3" />
      <Gutter isVertical />
      <Text t="conditions.text3" />
      <Gutter isVertical />
      <SubTitle t="conditions.subTitle4" />
      <Gutter isVertical />
      <Text t="conditions.text4" />
    </FlexView>
  </ScrollView>
);
