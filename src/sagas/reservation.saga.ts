import { freeBookingResource } from '@api/resources/free-booking';
import { lockInfoSelector } from '@selectors';
import { all, call, put, select, takeEvery } from 'redux-saga/effects';

import * as types from '../actions';
import { resetLockInfo } from '../actions';

function* freeBooking() {
  try {
    const lockInfo = yield select(lockInfoSelector);
    yield call(freeBookingResource.freeBooking, { checkId: lockInfo.checkId } as any);
    yield put(resetLockInfo());
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e, 'e');
  }
}

export function* reservationSagas() {
  yield all([takeEvery(types.FREE_BOOKING, freeBooking)]);
}
