import { all, takeEvery } from 'redux-saga/effects';
import moment from 'moment';
import 'moment/locale/de';
import 'moment/locale/fr';
import { Action } from 'redux-actions';
import i18n from 'i18n-js';

import { httpService } from '@services';
import { getAPIUrl } from '@helpers';
import * as types from '../actions/settings';

function* setLocale({ payload }: Action<string>) {
  try {
    yield (i18n.locale = payload);
    httpService.setAPIUrl(getAPIUrl(payload));
    moment.locale(payload);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e, 'e');
  }
}

export function* settingsSagas() {
  yield all([takeEvery(types.SET_LOCALE, setLocale)]);
}
