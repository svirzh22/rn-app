import { logOutResource } from '@api/resources/log-out';
import { CommonActions } from '@react-navigation/native';
import { memberSelector } from '@selectors';
import { AuthEntities, authService, httpService } from '@services';
import { setAppLoading } from '@src/actions';
import { all, call, put, select, takeEvery } from 'redux-saga/effects';

import { AuthResourceVerifyOTPResult } from '@api/resources/auth-resource/types';
import { setSentryUser } from '@api';
import * as types from '../actions/auth';
import { logOut, resetStore, setMember, signIn } from '../actions/auth';

function* updateTokensSaga({ authorization }: AuthResourceVerifyOTPResult) {
  yield call([authService, authService.saveToStorage], AuthEntities.bearer, authorization);
}

function* signInSaga({ payload }: ReturnType<typeof signIn>) {
  yield call(updateTokensSaga, payload);
  const { authorization, datas } = payload;

  yield call([httpService, httpService.setAuthHeader], authorization);
  setSentryUser({
    id: datas.id,
    mobilePhone: datas.mobilePhone,
  });
  yield put(setMember(datas));
  yield put(setAppLoading(false));
  yield put(
    CommonActions.reset({
      index: 1,
      routes: [{ name: 'Home' }],
    }),
  );
}

function* logOutSaga({ payload }: ReturnType<typeof logOut>) {
  try {
    if (payload) {
      const { token } = yield select(memberSelector);

      yield call(logOutResource.logOut, { queryKey: ['', { userToken: token }] } as any);
    }
    setSentryUser(null);
    yield call([httpService, httpService.removeAuthHeader]);
    yield put(resetStore());
  } catch (exc) {
    // eslint-disable-next-line no-console
    console.log(exc);
  }
}

export function* authSagas() {
  yield all([takeEvery(types.SIGN_IN, signInSaga)]);
  yield all([takeEvery(types.SESSION_EXPIRED, logOutSaga)]);
  yield all([takeEvery(types.LOG_OUT, logOutSaga)]);
}
