import { fork } from 'redux-saga/effects';
import { authSagas } from '@sagas/auth.saga';
import { settingsSagas } from '@sagas/./settings.saga';
import { reservationSagas } from '@sagas/reservation.saga';

export const rootSaga = function* root() {
  yield fork(authSagas);
  yield fork(settingsSagas);
  yield fork(reservationSagas);
};
