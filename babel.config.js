module.exports = {
  presets: ['module:metro-react-native-babel-preset', '@babel/preset-typescript'],
  plugins: [
    [
      '@babel/plugin-proposal-decorators',
      {
        legacy: true,
      },
    ],
    [
      'module-resolver',
      {
        extensions: ['.ts', '.tsx'],
        alias: {
          '@src': './src',
          '@services': './src/services',
          '@screens': './src/screens',
          '@components': './src/components',
          '@api': './src/api',
          '@assets': './src/assets',
          '@constants': './src/constants',
          '@helpers': './src/helpers',
          '@hooks': './src/hooks',
          '@selectors': './src/selectors',
          '@store': './src/store',
          '@navigation': './src/navigation',
          '@sagas': './src/sagas',
          '@typings': './src/typings',
        },
      },
    ],
  ],
};
