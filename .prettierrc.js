module.exports = {
  jsxBracketSameLine: false,
  singleQuote: true,
  tabWidth: 2,
  bracketSpacing: true,
  printWidth: 110,
  trailingComma: 'all',
};
